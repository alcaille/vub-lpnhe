#include <iostream>
#include <cstdlib>
#include <deque>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH2F.h"

#include "TLorentzVector.h"
#include "TVector3.h"

#include "EventMixer.h"
#include "EventMixerBanner.h"

#include "TRandom.h"

#include "TMath.h"
#include "omp.h"
inline double CalculateMCorr( const TVector3& PV, const TVector3& SV, const TLorentzVector& B)
{
  TVector3 BFlight = SV-PV;

  double pPerp = B.Perp(BFlight);

  return sqrt(pPerp*pPerp + B.M2()) + pPerp;

}

int main(int ac, char* av[])
{

  DoBanner(kTRUE);
  
  if (ac < 2)
  {
   DoHelp();
   return 1;
  }

  if ( std::string(av[ac-1]) != "Closest" and std::string(av[ac-1]) != "Sampling")
  {
    DoHelp();
    return 1;
  }
  
  std::string InputFile = av[1];
  std::string OutputFile;

  bool UseClosestPoint = false;

  if ( ac ==3 )
  {
    //    OutputFile = std::regex_replace(InputFile, std::regex("DATATUPLES_RAW_[a-zA-Z0-9]*|MCTUPLES_RAW_[a-zA-Z0-9]*"), "Combinatoric_RAW_Apr18");
    OutputFile = std::regex_replace(InputFile, std::regex("DTT_[a-zA-Z0-9]*"), "Combinatoric_");
    UseClosestPoint = std::string(av[2]) == "Closest";
  }
  else if ( ac ==4 )
  {
    OutputFile = av[2];
    UseClosestPoint = std::string(av[3]) == "Closest";
  }
  else
    return 1;


  std::cout << "Will read data from:        " << InputFile <<std::endl;
  std::cout << "Will write combinatorics to:" << OutputFile << "\n";

  // return 0;

  // SV Location will be randomly sampled
  TFile* f_B_Flight = TFile::Open("B_FD.root", "READ");
  TH2F*  h_B_Flight = (TH2F*)f_B_Flight->Get("B_FD")->Clone();
  h_B_Flight->SetDirectory(0);
  f_B_Flight->Close();
  delete f_B_Flight;

  TFile* F_In = TFile::Open(InputFile.c_str());
  TTree* T_In = (TTree*)F_In->Get("B2XuMuNuBs2K_Tuple");
  int NeV = T_In ->GetEntries();
  //  NeV=(NeV < 1000000)?NeV:1000000;
  //  NeV = 1000;
 
  //  T_In->Show(1);
  
  TFile* F_Out = TFile::Open(OutputFile.c_str(), "RECREATE");
  TTree* T_Out = new TTree("B2XuMuNuBs2K_Tuple", "B2XuMuNuBs2K_Tuple");
  //TTree* T_Out = new TTree("Bs2KmuNuTuple", "Bs2KmuNuTuple");
  
  //IncomingEvent Input  (T_In, "(B_Hlt2SingleMuonDecision_TOS || B_Hlt2TopoMu2BodyBBDTDecision_TOS)"); // RUN 1
  IncomingEvent Input  (T_In, "(B_Hlt2SingleMuonDecision_TOS || B_Hlt2TopoMu2BodyDecision_TOS)"); // RUN 2
  OutgoingEvent Output (Input, T_Out);

  // Input.Print();
  
  ///////////////////////////////////////////////////////////////
  //
  //  Now Reconstruct the B and calculate new variables
  //
  ///////////////////////////////////////////////////////////////


  TLorentzVector K, Mu, Bs;
  Output.AddVariableOverride("B_PX", [&K, &Mu, &Bs](auto F)
  {
    double  KX = F("K_PX" );
    double  KY = F("K_PY" );
    double  KZ = F("K_PZ" );

    double MuX = F("mu_PX" );
    double MuY = F("mu_PY" );
    double MuZ = F("mu_PZ" );

    K. SetXYZM(KX,  KY,  KZ,  493.677);
    Mu.SetXYZM(MuX, MuY, MuZ, 105.658);
    Bs = K + Mu;


   return Bs.Px();

   });

  Output.AddVariableOverride("B_PY", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Py();
   });

  Output.AddVariableOverride("B_PZ", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Pz();
   });

  Output.AddVariableOverride("B_PE", [&K, &Mu, &Bs](auto F)
   {
    return Bs.E();
   });

  Output.AddVariableOverride("B_P", [&K, &Mu, &Bs](auto F)
   {
    return Bs.P();
   });

  Output.AddVariableOverride("B_PT", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Pt();
   });

  Output.AddVariableOverride("B_PHI", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Phi();
   });

  /* Doesn't exist in tuple
  Output.AddVariableOverride("B_ETA", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Eta();
    });*/

  Output.AddVariableOverride("B_M", [&K, &Mu, &Bs](auto F)
   {
    return Bs.M();
   });
  /* Not in tuple
  Output.AddVariableOverride("B_MM", [&K, &Mu, &Bs](auto F)
   {
    return Bs.M();
   });
  */

  // now reconstruct the SV
  TVector3 SV, PV;
  TRandom RND;

  if ( UseClosestPoint )
  {
      Output.AddVariableOverride("B_ENDVERTEX_X", [&K, &Mu, &SV, &PV](auto F)
       {
        double B_OX = F("B_OWNPV_X");
        double B_OY = F("B_OWNPV_Y");
        double B_OZ = F("B_OWNPV_Z");
        PV.SetXYZ(B_OX, B_OY, B_OZ);

        // https://en.wikipedia.org/wiki/Skew_lines#Nearest_Points

        double K_OX = F("K_ORIVX_X");
        double K_OY = F("K_ORIVX_Y");
        double K_OZ = F("K_ORIVX_Z");

        double Mu_OX = F("mu_ORIVX_X");
        double Mu_OY = F("mu_ORIVX_Y");
        double Mu_OZ = F("mu_ORIVX_Z");

        TVector3 P1(K_OX,  K_OY,  K_OZ  );
        TVector3 P2(Mu_OX, Mu_OY, Mu_OZ );

        TVector3 D1 = K .Vect().Unit();
        TVector3 D2 = Mu.Vect().Unit();

        TVector3 n1 = D1.Cross(D2.Cross(D1));
        TVector3 n2 = D2.Cross(D1.Cross(D2));

        TVector3 C1 = P1 + (P2-P1).Dot(n2)/(D1.Dot(n2)) * D1;
        TVector3 C2 = P2 + (P1-P2).Dot(n1)/(D2.Dot(n1)) * D2;

        SV = (C1 + C2) * 0.5;

        return SV.X();

       });

  }
  else
  {
    Output.AddVariableOverride("B_ENDVERTEX_X", [&PV, &SV, &h_B_Flight, &RND](auto F)
     {

      double B_OX = F("B_OWNPV_X");
      double B_OY = F("B_OWNPV_Y");
      double B_OZ = F("B_OWNPV_Z");
      PV.SetXYZ(B_OX, B_OY, B_OZ);

      double R, Z;
      h_B_Flight->GetRandom2(R, Z);

      double angle = RND.Uniform(0, 2*TMath::Pi());

      double X = R * cos(angle);
      double Y = R * sin(angle);


      SV = PV + TVector3(X, Y, Z);

      return SV.X();

     });
  }
  Output.AddVariableOverride("B_ENDVERTEX_Y", [&SV](auto F)
  {
    return SV.Y();
  });

  Output.AddVariableOverride("B_ENDVERTEX_Z", [&SV](auto F)
  {
    return SV.Z();
  });


  // now reconstruct the Corrected Mass
  Output.AddVariableOverride("B_MCORR", [&Bs, &SV, &PV](auto F)
   {
    return CalculateMCorr(PV, SV, Bs);
  });


  Output.AddVariableOverride("B_MCORRERR", [&Bs, &SV, &PV, &RND](auto F)
   {

    int nToy = 100;
    std::vector<double> MCORR(nToy);

    for(int n=0; n<nToy; ++n)
    {

      TVector3 NewSV;
      NewSV.SetX(RND.Gaus(SV.X(), 0.0263 ));
      NewSV.SetY(RND.Gaus(SV.Y(), 0.0263 ));
      NewSV.SetZ(RND.Gaus(SV.Z(), 0.2669  ));

      MCORR[n] = CalculateMCorr(PV, NewSV, Bs);
      //std::cout << MCORR[n] << std::endl;
    }

    double MCORRMu =0;
    for(auto mc: MCORR)
      MCORRMu += mc;
    MCORRMu /= nToy;

    double Chisq = 0;
    for(auto mc: MCORR)
      //Chisq += mc*mc - MCORRMu*MCORRMu;
      Chisq += (mc - MCORRMu)*(mc - MCORRMu) ; // bug found by blaise
    //std::cout << sqrt(Chisq/nToy) << std::endl;
    return sqrt(Chisq/nToy);

   });

  Output.AddVariableOverride("B_DIRA_OWNPV", [&Bs, &SV, &PV](auto F)
  {
    TVector3 BFlight    = (SV-PV)  .Unit();
    TVector3 BsMomentum = Bs.Vect().Unit();
    
    return BFlight.Dot(BsMomentum);
  });

  Output.AddVariableOverride("B_FD_OWNPV", [&SV, &PV](auto F)
  {
    return (SV-PV).Mag();
  });

  ///////////////////////////////////////////////////////////////
  //
  //  Now Implement the Selection
  //
  ///////////////////////////////////////////////////////////////


  Output.SetSelection([&Bs, &SV](auto F)
  {
    // R is the B Decay Radius. Require the B decays inside the VELO
    double R = sqrt( SV.X()*SV.X() + SV.Y()*SV.Y());

    //   return true;
    
    //    std::cout<<Bs.M()<<" "<<F("B_MCORR")<<" R "<<R<<std::endl;
    
    if ( Bs.M()  > 7000)             return false;
    if ( Bs.M()  < 1000)             return false;
    if ( R > 6 )                     return false;
    //if ( F("B_DIRA_OWNPV") < 0.999) return false;
    if ( F("B_MCORR") > 7000 )      return false;
    //if ( F("B_MCORR") < 2200 )      return false;
    return true;
  });

  ///////////////////////////////////////////////////////////////
  //
  //  Now Do The Mixing
  //
  ///////////////////////////////////////////////////////////////



  const int BufferSize = 16;
  const int NCombi = 5;
  std::deque<PartialEventContainer> EventBuffer;
  PartialEventContainer P_Event( Input,
                            {
                                std::regex("B_.*"),
                                std::regex("kaon_m_.*"),
                                std::regex("muon_p_.*"),
                            } );

  for( int buff = 0; buff < BufferSize; buff++)
  {
    Input.ReadNextEvent();
    EventBuffer.push_back(P_Event);
  }



  std::cout << "\n\n\n";

  int nSaved = 0;
  std::cout<<"kiki "<<std::endl;
  //  for (int ev = 0; Input.ReadNextEvent(); ev++ )
  for (int ev = 0; ev < NeV; ev++ )
  {
    if (!Input.ReadNextEvent()) break;
    
    for(int nC =0; nC < NCombi; nC++)
    {
      std::vector<PartialEvent*> PartialEvents;
      PartialEvents.push_back(&EventBuffer[     0].m_PartialEvents[0]);
      PartialEvents.push_back(&EventBuffer[  nC+1].m_PartialEvents[1]);
      PartialEvents.push_back(&EventBuffer[2*nC+2].m_PartialEvents[2]);
      PartialEvents.push_back(&EventBuffer[3*nC+3].m_PartialEvents[3]);


      Output.MergePartialEvents(PartialEvents);
      Output.OverrideValues();
      nSaved += Output.WriteEvent();
    }

    if(ev%100 == 0)
    {
      std::cout << "\r\033[1A|" << std::string(69 * ev/NeV, '=') << std::string(69 - 69* ev/NeV, '-') << "| " << 100 * ev/NeV+1 << "%" << std::endl;
      std::cout << "|Processing Event: "<< ev << " Of: " << NeV << " Generated: " << NCombi*ev << " Saved: " <<  nSaved << "           " << std::flush;
    }
    P_Event.Update();
    EventBuffer.pop_front();
    EventBuffer.push_back(P_Event);

  }
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;

  std::cout<<"kiki "<<std::endl;
  //  delete h_B_Flight;

  std::cout<<"kiki write"<<std::endl;
  
  F_Out->cd();

  //  T_Out->Write("",kFlushBasket);
  T_Out->Write();
  std::cout<<"kiki flush "<<std::endl;

  //  F_Out->Flush();

  std::cout<<"kiki close "<<std::endl;

  F_Out->Close("R");
  // F_In->Close("R");

  std::cout<<"kiki delete tree "<<std::endl;

  // delete T_Out;
  T_In->Delete();
  
  std::cout<<"kiki done "<<std::endl;

  return 0;
}
