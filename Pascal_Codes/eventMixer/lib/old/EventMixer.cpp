#include "EventMixer.h"

#include <iostream>
#include <algorithm>

void RawEvent::Print()
{
  for(auto& B: m_Branches_F) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_D) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_I) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_i) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_O) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_L) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_l) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_S) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_V) std::cout << B.first << "  " << B.second << std::endl;
  for(auto& B: m_Branches_s) std::cout << B.first << "  " << B.second << std::endl;

}

RawEvent::~RawEvent()
{

  m_Branches_F.clear();
  m_Branches_D.clear();
  m_Branches_I.clear();
  m_Branches_i.clear();
  m_Branches_O.clear();
  m_Branches_L.clear();
  m_Branches_l.clear();
  m_Branches_S.clear();
  m_Branches_V.clear();
  m_Branches_v.clear();


}

/***************************************************************
 *
 * Member Functions for the Incoming Event
 *
 *
 ***************************************************************/


IncomingEvent::IncomingEvent(TTree* T_in, std::string Selection)
  :m_T_In( T_in )
  ,m_evNum(0)
  ,IncomingCut(Selection.c_str(), Selection.c_str(), T_in)
{

  TObjArray& BranchList = *T_in->GetListOfBranches();
  const int nBranch = BranchList.GetEntries();

  TObjArray *leaves = T_in->GetListOfLeaves();
  TLeaf *leaf;
  
  // First Loop To Fill The Vectors
  for( int b = 0; b < nBranch; b++)
  {
    TBranch*    Branch = (TBranch*) BranchList[b];
    std::string Branch_Title = Branch->GetTitle();
    std::string Branch_Name  = Branch->GetName();
    char        Branch_Type  = Branch_Title.back();

    TVector3 *VNull = NULL;
    std::vector<float> *vNULL= NULL;
    
    leaf = (TLeaf *)leaves->At(b);
    
    Branches.push_back(Branch);

    switch(Branch_Type)
    {
      case 'F' : m_Branches_F.push_back({Branch_Name, 0 }); break;
      case 'D' : m_Branches_D.push_back({Branch_Name, 0 }); break;
      case 'I' : m_Branches_I.push_back({Branch_Name, 0 }); break;
      case 'i' : m_Branches_i.push_back({Branch_Name, 0 }); break;
      case 'O' : m_Branches_O.push_back({Branch_Name, 0 }); break;
      case 'L' : m_Branches_L.push_back({Branch_Name, 0 }); break;
      case 'l' : m_Branches_l.push_back({Branch_Name, 0 }); break;
      case 'S' : m_Branches_S.push_back({Branch_Name, 0 }); break;
      default  :
        std::cout << "OOPS! Forgot to code Branch type: " << Branch_Type;
        std::cout <<"\tName : "<<Branch_Name.c_str();
        std::cout <<"\tTitle : "<<Branch_Title.c_str();
        std::cout <<"\tType : "<<leaf->GetTypeName()<<std::endl;
        if (leaf->GetTypeName() == "TVector3") {
          m_Branches_V.push_back({Branch_Name, NULL });
        }
        else if (leaf->GetTypeName() == "vector<float>") {
          m_Branches_v.push_back({Branch_Name, NULL });
        };
        std::cout<<"Recover branch type"<<leaf->GetTypeName()<<std::endl;
        // std::exit(404);
        break;
    }
  }

  // Second Loop To SetBranchAddress

  for(auto& B: m_Branches_F)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_D)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_I)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_i)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_O)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_L)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_l)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_S)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_V)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_v)
    T_in->SetBranchAddress(B.first.c_str(), &B.second);


}


bool IncomingEvent::ReadNextEvent()
{


  while ( m_evNum < m_T_In->GetEntries() )
  {
    m_T_In->GetEntry(m_evNum );
    m_evNum++;

    if ( IncomingCut.EvalInstance() )
      return true;
  }
  return false;

}



/***************************************************************
 *
 * Member Functions for the Partial Event
 *
 *
 ***************************************************************/



bool PartialEvent::match(std::string BranchName)
{
  return std::regex_match(BranchName, m_BranchFilter);
}



void PartialEvent::Update()
{
  for(int i = m_Branches_F.size()-1; i --> 0 ; )
    m_Branches_F[i].second = *m_BranchLinker_F[i].second;

  for(int i = m_Branches_D.size()-1; i --> 0 ; )
    m_Branches_D[i].second = *m_BranchLinker_D[i].second;

  for(int i = m_Branches_I.size()-1; i --> 0 ; )
    m_Branches_I[i].second = *m_BranchLinker_I[i].second;

  for(int i = m_Branches_i.size()-1; i --> 0 ; )
    m_Branches_i[i].second = *m_BranchLinker_i[i].second;

  for(int i = m_Branches_O.size()-1; i --> 0 ; )
    m_Branches_O[i].second = *m_BranchLinker_O[i].second;

  for(int i = m_Branches_L.size()-1; i --> 0 ; )
    m_Branches_L[i].second = *m_BranchLinker_L[i].second;

  for(int i = m_Branches_l.size()-1; i --> 0 ; )
    m_Branches_l[i].second = *m_BranchLinker_l[i].second;

  for(int i = m_Branches_S.size()-1; i --> 0 ; )
    m_Branches_S[i].second = *m_BranchLinker_S[i].second;

  for(int i = m_Branches_V.size()-1; i --> 0 ; )
    m_Branches_V[i].second = &m_BranchLinker_V[i].second;

  for(int i = m_Branches_v.size()-1; i --> 0 ; )
    m_Branches_v[i].second = &m_BranchLinker_v[i].second;


}


/***************************************************************
 *
 * Member Functions for the Partial Event Container
 *
 *
 ***************************************************************/


template<typename T>
void Attacher(T& Branches, std::vector<PartialEvent>& m_PartialEvents)
{

  for( auto& B: Branches )
  {
    for(auto& partialEv: m_PartialEvents)
      if ( partialEv.AttachBranch(B) ) break;
  }

}

PartialEventContainer::PartialEventContainer(IncomingEvent& Ev_In, std::vector<std::regex> PartialEvents)
  :m_EventMatches(PartialEvents)
{
  for (auto& branchRegex: m_EventMatches)
    m_PartialEvents.emplace_back(branchRegex);
  m_PartialEvents.emplace_back(std::regex(".*"));


  Attacher(Ev_In.m_Branches_F, m_PartialEvents);
  Attacher(Ev_In.m_Branches_D, m_PartialEvents);
  Attacher(Ev_In.m_Branches_I, m_PartialEvents);
  Attacher(Ev_In.m_Branches_i, m_PartialEvents);
  Attacher(Ev_In.m_Branches_O, m_PartialEvents);
  Attacher(Ev_In.m_Branches_L, m_PartialEvents);
  Attacher(Ev_In.m_Branches_l, m_PartialEvents);
  Attacher(Ev_In.m_Branches_S, m_PartialEvents);
  Attacher(Ev_In.m_Branches_V, m_PartialEvents);
  Attacher(Ev_In.m_Branches_v, m_PartialEvents);

}


void PartialEventContainer::Update()
{
  for(auto& Pev: m_PartialEvents)
    Pev.Update();

}

void PartialEventContainer::Print()
{
  for(auto& Pev: m_PartialEvents)
    Pev.Print();


}


/***************************************************************
 *
 * Member Functions for the Outgoing Event
 *
 *
 ***************************************************************/




OutgoingEvent::OutgoingEvent(IncomingEvent& Ev_In, TTree* T_Out)
  :m_T_Out(T_Out)

{

  m_Branches_F = Ev_In.m_Branches_F;
  m_Branches_D = Ev_In.m_Branches_D;
  m_Branches_I = Ev_In.m_Branches_I;
  m_Branches_i = Ev_In.m_Branches_i;
  m_Branches_O = Ev_In.m_Branches_O;
  m_Branches_L = Ev_In.m_Branches_L;
  m_Branches_l = Ev_In.m_Branches_l;
  m_Branches_S = Ev_In.m_Branches_S;
  m_Branches_V = Ev_In.m_Branches_V;
  m_Branches_v = Ev_In.m_Branches_v;


  for(auto& B: m_Branches_F)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_D)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_I)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_i)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_O)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_l)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_L)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_S)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_V)
    T_Out->Branch(B.first.c_str(), &B.second);
  for(auto& B: m_Branches_v)
    T_Out->Branch(B.first.c_str(), &B.second);


}


template<typename T1>
void Updater(T1& PartialEvent, T1& MergedEvent)
{
  auto it_p   = PartialEvent.begin();
  auto it_Out = MergedEvent .begin();

  for(  ; it_p != PartialEvent.end()  ;  )
  {
    if ( it_p->first == it_Out->first )
    {
      it_Out->second = it_p->second;
      ++it_p;
    }
    it_Out++;
  }


}


void OutgoingEvent::MergePartialEvents(std::vector<PartialEvent*> PartialEvents)
{
  for( auto PartialEv: PartialEvents)
  {

    Updater(PartialEv->m_Branches_F, m_Branches_F);
    Updater(PartialEv->m_Branches_D, m_Branches_D);
    Updater(PartialEv->m_Branches_I, m_Branches_I);
    Updater(PartialEv->m_Branches_i, m_Branches_i);
    Updater(PartialEv->m_Branches_O, m_Branches_O);
    Updater(PartialEv->m_Branches_L, m_Branches_L);
    Updater(PartialEv->m_Branches_l, m_Branches_l);
    Updater(PartialEv->m_Branches_S, m_Branches_S);
    Updater(PartialEv->m_Branches_V, m_Branches_V);
    Updater(PartialEv->m_Branches_v, m_Branches_v);
  }
}


bool OutgoingEvent::WriteEvent()
{
  int bytesWritten = 0;

  if ( Selection([this](std::string v){return GetVar(v);}) )
    bytesWritten = m_T_Out->Fill();

  return bytesWritten > 0;

}



template<typename T, typename T2>
double BranchSearcher(T& BranchList, T2& BranchCache, std::string varName)
{

  if ( BranchCache.count(varName) )
  {
    auto v = BranchCache[varName];
    if ( v)
      return *v;
    else
      return 0;

  }

  for(auto& B: BranchList)
  {
    if ( B.first == varName )
    {
        BranchCache[varName] = &B.second;
        return B.second;
    }
  }

  BranchCache[varName] = nullptr;
}
/*
template<typename T, typename T2>
TVector3 VBranchSearcher(T& BranchList, T2& BranchCache, std::string varName)
{

  if ( BranchCache.count(varName) )
  {
    auto v = BranchCache[varName];
    if ( v)
      return *v;
    else
      return 0;

  }

  for(auto& B: BranchList)
  {
    if ( B.first == varName )
    {
        BranchCache[varName] = &B.second;
        return B.second;
    }
  }

  BranchCache[varName] = nullptr;
}

template<typename T, typename T2>
std::vector<float> vBranchSearcher(T& BranchList, T2& BranchCache, std::string varName)
{

  if ( BranchCache.count(varName) )
  {
    auto v = BranchCache[varName];
    if ( v)
      return *v;
    else
      return nullptr;

  }

  for(auto& B: BranchList)
  {
    if ( B.first == varName )
    {
        BranchCache[varName] = &B.second;
        return B.second;
    }
  }

  BranchCache[varName] = nullptr;
}
*/

double OutgoingEvent::GetVar(std::string VarName)
{

  double Val = 0;
  Val +=  BranchSearcher(m_Branches_F, m_Cache_F, VarName);
  Val +=  BranchSearcher(m_Branches_D, m_Cache_D, VarName);
  Val +=  BranchSearcher(m_Branches_I, m_Cache_I, VarName);
  Val +=  BranchSearcher(m_Branches_i, m_Cache_i, VarName);
  Val +=  BranchSearcher(m_Branches_O, m_Cache_O, VarName);
  Val +=  BranchSearcher(m_Branches_L, m_Cache_L, VarName);
  Val +=  BranchSearcher(m_Branches_l, m_Cache_l, VarName);
  Val +=  BranchSearcher(m_Branches_S, m_Cache_S, VarName);
  //  Val += VBranchSearcher(m_Branches_V, m_Cache_V, VarName);
  //  Val += vBranchSearcher(m_Branches_v, m_Cache_v, VarName);

  return Val;

}


template<typename T, typename T2>
void BranchSetter(T& BranchList, T2& BranchCache, std::string varName, double Value)
{

  if ( BranchCache.count(varName) > 0)
  {
    auto v = BranchCache[varName];
    if ( v )
      *v = Value;
    return;
  }

  for(auto& B: BranchList)
    if ( B.first == varName )
    {
        BranchCache[varName] = &B.second;
        B.second = Value;
        return;
    }

  BranchCache[varName] = nullptr;

}



void OutgoingEvent::SetVar(std::string VarName, double Value)
{

  BranchSetter(m_Branches_F, m_Cache_F, VarName, Value);
  BranchSetter(m_Branches_D, m_Cache_D, VarName, Value);
  BranchSetter(m_Branches_I, m_Cache_I, VarName, Value);
  BranchSetter(m_Branches_i, m_Cache_i, VarName, Value);
  BranchSetter(m_Branches_O, m_Cache_O, VarName, Value);
  BranchSetter(m_Branches_L, m_Cache_L, VarName, Value);
  BranchSetter(m_Branches_l, m_Cache_l, VarName, Value);
  BranchSetter(m_Branches_S, m_Cache_S, VarName, Value);
  //  BranchSetter(m_Branches_V, m_Cache_V, VarName, Value);
  // BranchSetter(m_Branches_v, m_Cache_v, VarName, Value);

}


void OutgoingEvent::OverrideValues()
{

  for(auto& p_override: VariableOverrides)
  {

    std::string vName = p_override.first;
    double val = p_override.second([this](std::string v){return GetVar(v);});

    SetVar(vName, val);

  }


}







