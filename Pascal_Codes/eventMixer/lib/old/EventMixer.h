#pragma once

#include <cstdlib>
#include <string>
#include <iostream>

#include <vector>
#include <functional>
#include <regex>

#include <unordered_map>

#include "Rtypes.h"
#include "TVector3.h"
#include "TTree.h"
#include "TTreeFormula.h"

/***************************************************************
 *
 * Class Definition for the raw event container
 *
 *
 ***************************************************************/




class RawEvent
{
public:

  virtual ~RawEvent();
  virtual void Print();

  std::vector< std::pair< std::string, Char_t        >> m_Branches_B;
  std::vector< std::pair< std::string, UChar_t       >> m_Branches_b;
  std::vector< std::pair< std::string, Short_t       >> m_Branches_S;
  std::vector< std::pair< std::string, UShort_t      >> m_Branches_s;
  std::vector< std::pair< std::string, Int_t         >> m_Branches_I;
  std::vector< std::pair< std::string, UInt_t        >> m_Branches_i;
  std::vector< std::pair< std::string, Float_t       >> m_Branches_F;
  std::vector< std::pair< std::string, Double_t      >> m_Branches_D;
  std::vector< std::pair< std::string, Long64_t      >> m_Branches_L;
  std::vector< std::pair< std::string, ULong64_t     >> m_Branches_l;
  std::vector< std::pair< std::string, Bool_t        >> m_Branches_O;
  std::vector< std::pair< std::string, const TVector3 *    >> m_Branches_V;
  std::vector< std::pair< std::string, const std::vector<float>* >> m_Branches_v;


  /*
  B : an 8 bit signed integer (Char_t)
  b : an 8 bit unsigned integer (UChar_t)
  S : a 16 bit signed integer (Short_t)
  s : a 16 bit unsigned integer (UShort_t)
  I : a 32 bit signed integer (Int_t)
  i : a 32 bit unsigned integer (UInt_t)
  F : a 32 bit floating point (Float_t)
  D : a 64 bit floating point (Double_t)
  L : a 64 bit signed integer (Long64_t)
  l : a 64 bit unsigned integer (ULong64_t)
  O : [the letter o, not a zero] a boolean (Bool_t)

  */
};



/***************************************************************
 *
 * Class Definition for the Incoming event container
 *
 *
 ***************************************************************/

class IncomingEvent
  :public RawEvent
{

public:

  IncomingEvent(TTree* T_in, std::string Selection);
  bool ReadNextEvent ();


private:


  int           m_evNum;
  TTree*        m_T_In;
  TTreeFormula IncomingCut;

  std::vector<TBranch*> Branches;

};





/***************************************************************
 *
 * Class Definition for the Partial event container
 *
 *
 ***************************************************************/


template<typename T>
bool AttachBranchHelper
(
  std::pair  <std::string, T>&              InputBranch,
  std::vector<std::pair<std::string, T  >>& BranchContainer,
  std::vector<std::pair<std::string, const T* >>& BranchLinker
)
{
  BranchContainer.push_back(InputBranch);
  BranchLinker   .push_back({InputBranch.first, &InputBranch.second});

  return true;
}


template<typename T1, typename T2>
bool AttachBranchHelper
(
  std::pair  <std::string, T1>&              InputBranch,
  std::vector<std::pair<std::string, T2  >>& BranchContainer,
  std::vector<std::pair<std::string, const T2* >>& BranchLinker
)
{
  return false;
}



class PartialEvent
  :public RawEvent
{

public:

  PartialEvent(std::regex BranchFilter) : m_BranchFilter(BranchFilter) {};



  bool match(std::string BranchName);
  void Update();

  template<typename T>
  bool AttachBranch(std::pair<std::string, T>& Branch)
  {
    if ( not match(Branch.first) )
      return false;

    bool flag_attached = false;

    flag_attached |= AttachBranchHelper(Branch, m_Branches_F, m_BranchLinker_F);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_D, m_BranchLinker_D);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_I, m_BranchLinker_I);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_i, m_BranchLinker_i);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_O, m_BranchLinker_O);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_L, m_BranchLinker_L);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_l, m_BranchLinker_l);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_S, m_BranchLinker_S);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_V, &m_BranchLinker_V);
    flag_attached |= AttachBranchHelper(Branch, m_Branches_v, &m_BranchLinker_v);

    return flag_attached;

  }


private:

  std::vector< std::pair< std::string, const Float_t       * >> m_BranchLinker_F;
  std::vector< std::pair< std::string, const Double_t      * >> m_BranchLinker_D;
  std::vector< std::pair< std::string, const Int_t         * >> m_BranchLinker_I;
  std::vector< std::pair< std::string, const UInt_t        * >> m_BranchLinker_i;
  std::vector< std::pair< std::string, const Bool_t        * >> m_BranchLinker_O;
  std::vector< std::pair< std::string, const Long64_t      * >> m_BranchLinker_L;
  std::vector< std::pair< std::string, const ULong64_t     * >> m_BranchLinker_l;
  std::vector< std::pair< std::string, const Short_t       * >> m_BranchLinker_S;
  std::vector< std::pair< std::string, const TVector3       >> m_BranchLinker_V;
  std::vector< std::pair< std::string, const std::vector<float>  >> m_BranchLinker_v;

  std::regex m_BranchFilter;

};


/***************************************************************
 *
 * Class Definition for the PartialEventContainer container
 *
 *
 ***************************************************************/



class PartialEventContainer
{
public:
  PartialEventContainer(IncomingEvent& Ev_In, std::vector<std::regex> PartialEvents);

  void Update();
  void Print();

  std::vector<std::regex>   m_EventMatches;
  std::vector<PartialEvent> m_PartialEvents;

};




/***************************************************************
 *
 * Class Definition for the Outgoing Event container
 *
 *
 ***************************************************************/



using LookupFunction    = std::function< double(std::function< double(std::string) >) >;
using SelectionFunction = std::function< bool  (std::function< double(std::string) >) >;


class OutgoingEvent
  :public RawEvent
{

public:

  OutgoingEvent(IncomingEvent& Ev_In, TTree* T_out);

  void MergePartialEvents(std::vector<PartialEvent*> PartialEvents);

  bool WriteEvent ();

  double GetVar(std::string VarName);
  void   SetVar(std::string Varname, double Value);


  template<typename T>
  void AddVariableOverride(std::string Name, T Func)
  {
    VariableOverrides.push_back({Name, Func});
  }

  template<typename T>
  void SetSelection(T Func)
  {
    Selection = Func;
  }


  void OverrideValues();





private:

  TTree* m_T_Out;

  std::vector<std::pair<std::string, LookupFunction>> VariableOverrides;
  SelectionFunction Selection;

  std::map<std::string, Float_t   * > m_Cache_F;
  std::map<std::string, Double_t  * > m_Cache_D;
  std::map<std::string, Int_t     * > m_Cache_I;
  std::map<std::string, UInt_t    * > m_Cache_i;
  std::map<std::string, Bool_t    * > m_Cache_O;
  std::map<std::string, Long64_t  * > m_Cache_L;
  std::map<std::string, ULong64_t * > m_Cache_l;
  std::map<std::string, Short_t   * > m_Cache_S;
  std::map<std::string, TVector3   * > m_Cache_V;
  std::map<std::string, std::vector<float> * > m_Cache_v;

};
