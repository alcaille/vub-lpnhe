#include <chrono>
#include <thread>

#include <cstdlib>
#include <iostream>


void DoBanner(Bool_t doit = kFALSE)
{
  if (doit)
  {
    std::cout << "\e[38;05;196m\e[1m" << R"(.o##RADIUS##############################################################o.)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;197m\e[1m" << R"(########################XX///X############################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;198m\e[1m" << R"(#######################XX///////X#########################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;199m\e[1m" << R"(#####################"#P"""""""Y#"########################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;200m\e[1m" << R"(####################( #"^##P"##.# )#######################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;201m\e[1m" << R"(#####################.#b.  .b .d#.########################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;202m\e[1m" << R"(#######################"b.---.d##.:%0#####################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;203m\e[1m" << R"(#####################"  `#m.m#'....::%0###################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;204m\e[1m" << R"(##################P"  .. |"""|   ....::%0#################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;205m\e[1m" << R"(##############P"" . .... `. ,'      ..:.::%0##############################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;206m\e[1m" << R"(#############.  . ....::  |.|         ....:::%0###########################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;207m\e[1m" << R"(#############b .. .:.:::  |.|  . .:.     ...::::%0########################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;208m\e[1m" << R"(##########"  `#:.::::::   .|  . :.:%%%..   ....::::%0#####################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;209m\e[1m" << R"(########"" .md##::::%#:: :.| ....:.::%%#o.  ....:.:::%0###################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;210m\e[1m" << R"(########mm#######::%##%. ..| ..:.:.::%%###o.  .....::::###################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;211m\e[1m" << R"(#####P""    ""^Y#######m.. |  . :.:.%:%#####o.  ..mm^^0###################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;212m\e[1m" << R"(###(     "^"     )##########mm.. :.:.:%#######o..#P     `^################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;213m\e[1m" << R"(#####mmm.....mmm################################## m  m . )###############)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;214m\e[1m" << R"(##################################################m##.m.b.################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;215m\e[1m" << R"(#mm...""^^##################################0P""""          """^Y#########)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;216m\e[1m" << R"(########mmmm...""^^########################(         "^"         )########)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;217m\e[1m" << R"(#################mmmm....""^^###############0mmm...       ...mmm0#########)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;218m\e[1m" << R"(###########################mmmm....""""^^#################################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;219m\e[1m" << R"(######################################mmmmm.....""""######################)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;220m\e[1m" << R"('^######################################################################^')" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout                                                                                                            <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout                                                                                                            <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[38;05;196m"                                                                                         <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(  _    _      _                            _____       _____ _            )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"( | |  | |    | |                          |_   _|     |_   _| |           )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"( | |  | | ___| | ___ ___  _ __ ___   ___    | | ___     | | | |__   ___   )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"( | |/\| |/ _ \ |/ __/ _ \| '_ ` _ \ / _ \   | |/ _ \    | | | '_ \ / _ \  )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"( \  /\  /  __/ | (_| (_) | | | | | |  __/   | | (_) |   | | | | | |  __/  )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(  \/  \/ \___|_|\___\___/|_| |_| |_|\___|   \_/\___/    \_/ |_| |_|\___|  )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                                                                          )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                                                                          )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                        _____                _    ___  ____               )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                       |  ___|              | |   |  \/  (_)              )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"( Created By:           | |____   _____ _ __ | |_  | .  . |___  _____ _ __ )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(      Iwan Smith       |  __\ \ / / _ \ '_ \| __| | |\/| | \ \/ / _ \ '__|)" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(   iwan.smith@cern.ch  | |___\ V /  __/ | | | |_  | |  | | |>  <  __/ |   )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                       \____/ \_/ \___|_| |_|\__| \_|  |_/_/_/\_\___|_|   )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout <<                         R"(                                                                          )" <<  std::endl; std::this_thread::sleep_for(std::chrono::milliseconds(20));
    std::cout << "\e[0m";
  };
  
  std::cout << "Created By:     Iwan Smith ( iwan.smith@cern.ch) " <<std::endl;
}


void DoHelp()
{

  std::cout << "Usage: \n";
  std::cout << "    ./Mixer InputFile Sampling|Closest\n";
  std::cout << "    ./Mixer InputFile OutputFile Sampling|Closest\n";
  std::cout << std::endl;
  
}

