
#include <iostream>
#include <cstdlib>
#include <deque>

#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"

#include "TLorentzVector.h"
#include "TVector3.h"

#include "EventMixer.h"
#include "EventMixerBanner.h"

#include "TRandom.h"

#include "TMath.h"
#include "omp.h"
inline double CalculateMCorr( const TVector3& PV, const TVector3& SV, const TLorentzVector& B)
{
  TVector3 BFlight = SV-PV;

  double pPerp = B.Perp(BFlight);

  return sqrt(pPerp*pPerp + B.M2()) + pPerp;

}




int main(int ac, char* av[])
{

  DoBanner();

  if (ac < 2)
  {
   DoHelp();
   return 1;
  }

  if ( std::string(av[ac-1]) != "Closest" and std::string(av[ac-1]) != "Sampling")
  {
    DoHelp();
    return 1;
  }

  std::string InputFile = av[1];
  std::string OutputFile;

  bool UseClosestPoint = false;

  if ( ac ==3 )
  {
    OutputFile = std::regex_replace(InputFile, std::regex("DATATUPLES_RAW_[a-zA-Z0-9]*|MCTUPLES_RAW_[a-zA-Z0-9]*"), "Combinatoric_RAW_Apr18");
    UseClosestPoint = std::string(av[2]) == "Closest";
  }
  else if ( ac ==4 )
  {
    OutputFile = av[2];
    UseClosestPoint = std::string(av[3]) == "Closest";
  }
  else
    return 1;


  std::cout << "Will read data from:\n    " << InputFile << \
   "\n\nWill write combinatorics to:\n    " << OutputFile << "\n";

  // SV Location will be randomly sampled
  TFile* f_B_Flight = TFile::Open("B_FD.root", "READ");
  TH2F*  h_B_Flight = (TH2F*)f_B_Flight->Get("B_FD")->Clone();
  h_B_Flight->SetDirectory(0);
  f_B_Flight->Close();
  delete f_B_Flight;



  TFile* F_In = TFile::Open(InputFile.c_str());
  TTree* T_In = (TTree*)F_In->Get("Bs2DsMuNuTuple/DecayTree");
  const int NeV = T_In ->GetEntries();

  TFile* F_Out = TFile::Open(OutputFile.c_str(), "RECREATE");
  F_Out->mkdir("Bs2DsMuNuTuple");
  F_Out->cd("Bs2DsMuNuTuple");
  TTree* T_Out = new TTree("DecayTree", "DecayTree");



  IncomingEvent Input  (T_In, "(Bs_L0MuonDecision_TOS && (Bs_Hlt2SingleMuonDecision_TOS || Bs_Hlt2TopoMu2BodyBBDTDecision_TOS))");
  OutgoingEvent Output (Input, T_Out);


  ///////////////////////////////////////////////////////////////
  //
  //  Now Reconstruct the B and calculate new variables
  //
  ///////////////////////////////////////////////////////////////


  TLorentzVector K, Mu, Bs;
  Output.AddVariableOverride("Bs_PX", [&K, &Mu, &Bs](auto F)
  {
    double  KX = F("Ds_PX" );
    double  KY = F("Ds_PY" );
    double  KZ = F("Ds_PZ" );

    double MuX = F("muon_p_PX" );
    double MuY = F("muon_p_PY" );
    double MuZ = F("muon_p_PZ" );

    K. SetXYZM(KX,  KY,  KZ,  493.677);
    Mu.SetXYZM(MuX, MuY, MuZ, 105.658);
    Bs = K + Mu;


   return Bs.Px();

   });

  Output.AddVariableOverride("Bs_PY", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Py();
   });

  Output.AddVariableOverride("Bs_PZ", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Pz();
   });

  Output.AddVariableOverride("Bs_PE", [&K, &Mu, &Bs](auto F)
   {
    return Bs.E();
   });

  Output.AddVariableOverride("Bs_P", [&K, &Mu, &Bs](auto F)
   {
    return Bs.P();
   });

  Output.AddVariableOverride("Bs_PT", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Pt();
   });

  Output.AddVariableOverride("Bs_PHI", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Phi();
   });

  Output.AddVariableOverride("Bs_ETA", [&K, &Mu, &Bs](auto F)
   {
    return Bs.Eta();
   });

  Output.AddVariableOverride("Bs_M", [&K, &Mu, &Bs](auto F)
   {
    return Bs.M();
   });

  Output.AddVariableOverride("Bs_MM", [&K, &Mu, &Bs](auto F)
   {
    return Bs.M();
   });


  // now reconstruct the SV
  TVector3 SV, PV;
  TRandom RND;

  if ( UseClosestPoint )
  {
      Output.AddVariableOverride("Bs_ENDVERTEX_X", [&K, &Mu, &SV, &PV](auto F)
       {
        double B_OX = F("Bs_OWNPV_X");
        double B_OY = F("Bs_OWNPV_Y");
        double B_OZ = F("Bs_OWNPV_Z");
        PV.SetXYZ(B_OX, B_OY, B_OZ);

        // https://en.wikipedia.org/wiki/Skew_lines#Nearest_Points

        double K_OX = F("Ds_ORIVX_X");
        double K_OY = F("Ds_ORIVX_Y");
        double K_OZ = F("Ds_ORIVX_Z");

        double Mu_OX = F("muon_p_ORIVX_X");
        double Mu_OY = F("muon_p_ORIVX_Y");
        double Mu_OZ = F("muon_p_ORIVX_Z");

        TVector3 P1(K_OX,  K_OY,  K_OZ  );
        TVector3 P2(Mu_OX, Mu_OY, Mu_OZ );

        TVector3 D1 = K .Vect().Unit();
        TVector3 D2 = Mu.Vect().Unit();

        TVector3 n1 = D1.Cross(D2.Cross(D1));
        TVector3 n2 = D2.Cross(D1.Cross(D2));

        TVector3 C1 = P1 + (P2-P1).Dot(n2)/(D1.Dot(n2)) * D1;
        TVector3 C2 = P2 + (P1-P2).Dot(n1)/(D2.Dot(n1)) * D2;

        SV = (C1 + C2) * 0.5;

        return SV.X();

       });

  }
  else
  {
    Output.AddVariableOverride("Bs_ENDVERTEX_X", [&PV, &SV, &h_B_Flight, &RND](auto F)
     {

      double B_OX = F("Bs_OWNPV_X");
      double B_OY = F("Bs_OWNPV_Y");
      double B_OZ = F("Bs_OWNPV_Z");
      PV.SetXYZ(B_OX, B_OY, B_OZ);

      double R, Z;
      h_B_Flight->GetRandom2(R, Z);

      double angle = RND.Uniform(0, 2*TMath::Pi());

      double X = R * cos(angle);
      double Y = R * sin(angle);


      SV = PV + TVector3(X, Y, Z);

      return SV.X();

     });
  }
  Output.AddVariableOverride("Bs_ENDVERTEX_Y", [&SV](auto F)
  {
    return SV.Y();
  });

  Output.AddVariableOverride("Bs_ENDVERTEX_Z", [&SV](auto F)
  {
    return SV.Z();
  });


  // now reconstruct the Corrected Mass
  Output.AddVariableOverride("Bs_MCORR", [&Bs, &SV, &PV](auto F)
   {
    return CalculateMCorr(PV, SV, Bs);
  });


  Output.AddVariableOverride("Bs_MCORRERR", [&Bs, &SV, &PV, &RND](auto F)
   {

    int nToy = 100;
    std::vector<double> MCORR(nToy);

    for(int n=0; n<nToy; ++n)
    {

      TVector3 NewSV;
      NewSV.SetX(RND.Gaus(SV.X(), 0.0263 ));
      NewSV.SetY(RND.Gaus(SV.Y(), 0.0263 ));
      NewSV.SetZ(RND.Gaus(SV.Z(), 0.2669  ));

      MCORR[n] = CalculateMCorr(PV, NewSV, Bs);
      //std::cout << MCORR[n] << std::endl;
    }

    double MCORRMu =0;
    for(auto mc: MCORR)
      MCORRMu += mc;
    MCORRMu /= nToy;

    double Chisq = 0;
    for(auto mc: MCORR)
      Chisq += mc*mc - MCORRMu*MCORRMu;

    //std::cout << sqrt(Chisq/nToy) << std::endl;
    return sqrt(Chisq/nToy);

   });

  Output.AddVariableOverride("Bs_DIRA_OWNPV", [&Bs, &SV, &PV](auto F)
  {
    TVector3 BFlight    = (SV-PV)  .Unit();
    TVector3 BsMomentum = Bs.Vect().Unit();

    return BFlight.Dot(BsMomentum);
  });

  Output.AddVariableOverride("Bs_FD_OWNPV", [&SV, &PV](auto F)
  {
    return (SV-PV).Mag();
  });

  ///////////////////////////////////////////////////////////////
  //
  //  Now Implement the Selection
  //
  ///////////////////////////////////////////////////////////////


  Output.SetSelection([&Bs, &SV](auto F)
  {
    // R is the B Decay Radius. Require the B decays inside the VELO
    double R = sqrt( SV.X()*SV.X() + SV.Y()*SV.Y());

    if ( Bs.M()  > 8000)             return false;
    if ( Bs.M()  < 2200)             return false;
    if ( R > 6 )                     return false;
    if ( F("Bs_DIRA_OWNPV") < 0.999) return false;
    if ( F("Bs_MCORR") > 8000 )      return false;
    if ( F("Bs_MCORR") < 2200 )      return false;
    return true;
  });

  ///////////////////////////////////////////////////////////////
  //
  //  Now Do The Mixing
  //
  ///////////////////////////////////////////////////////////////



  const int BufferSize = 16;
  const int NCombi = 5;
  std::deque<PartialEventContainer> EventBuffer;
  PartialEventContainer P_Event( Input,
                            {
                                std::regex("Bs_.*"),
                                std::regex("Ds_.*|kaon_m.*|kaon_p.*|pi_p.*"),
                                std::regex("muon_p_.*"),
                            } );

  for( int buff = 0; buff < BufferSize; buff++)
  {
    Input.ReadNextEvent();
    EventBuffer.push_back(P_Event);
  }



  std::cout << "\n\n\n";

  int nSaved = 0;
  for (int ev = 0; Input.ReadNextEvent(); ev++ )
  {
    for(int nC =0; nC < NCombi; nC++)
    {
      std::vector<PartialEvent*> PartialEvents;
      PartialEvents.push_back(&EventBuffer[     0].m_PartialEvents[0]);
      PartialEvents.push_back(&EventBuffer[  nC+1].m_PartialEvents[1]);
      PartialEvents.push_back(&EventBuffer[2*nC+2].m_PartialEvents[2]);
      PartialEvents.push_back(&EventBuffer[3*nC+3].m_PartialEvents[3]);


      Output.MergePartialEvents(PartialEvents);
      Output.OverrideValues();
      nSaved += Output.WriteEvent();
    }

    if(ev%100 == 0)
    {
      std::cout << "\r\033[1A|" << std::string(69 * ev/NeV, '#') << std::string(69 - 69* ev/NeV, '-') << "| " << 100 * ev/NeV+1 << "%" << std::endl;
      std::cout << "Processing Event: "<< ev << " Of: " << NeV << " Generated: " << NCombi*ev << " Saved: " <<  nSaved << "           " << std::flush;
    }
    P_Event.Update();
    EventBuffer.pop_front();
    EventBuffer.push_back(P_Event);

  }
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;

  delete h_B_Flight;

  T_Out->Write();
  F_Out->Close();




  return EXIT_SUCCESS;
}
