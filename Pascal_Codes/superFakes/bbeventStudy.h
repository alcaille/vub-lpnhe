#ifndef SUPERFAKES_BBEVENTSTUDY_H 
#define SUPERFAKES_BBEVENTSTUDY_H 1
/** @class bbeventStudy bbeventStudy.h superFakes/bbeventStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-09
 */
// Include files

// Local
#include "superFake.h"
#include "superStudy.h"
#include "mixedEvent.h"

class bbeventStudy  : public superFake
{
public: 
  /// Standard constructor
  bbeventStudy( ){}; 
  bbeventStudy( TChain * ); 

  virtual ~bbeventStudy( ); ///< Destructor

  void createHistograms( ) override;
  void fillHistograms  ( BKGCAT ) override;
  void drawHistograms  ( Int_t ) override;
  void eventSummary    ( ) override;
  void save            ( ) override;
  
  void appendPaveText  ( TPaveText * );
  void drawHistogram   ( const char *, Mycategories & );
  void drawGraphs      ( Int_t );
  void drawGraph       ( const char *, Mycategories & );

  void SetVerbose( Int_t v){verbose = v;};
protected:

private:
  Mycategories CFake, CComb;

  Double_t cutQ2Val   = __Q2_CUT_VALUE__;

  mixedEvent ME;
};
#endif // SUPERFAKES_BBEVENTSTUDY_H
