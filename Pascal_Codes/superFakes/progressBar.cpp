// Include files 



// local
#include "progressBar.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : progressBar
//
// 2024-04-02 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
progressBar::progressBar(  ) {
  nTuplEntries = 0;
}

progressBar::progressBar( Int_t n ) {
  nTuplEntries = n;
}
//=============================================================================
// Destructor
//=============================================================================
progressBar::~progressBar() {} 

//=============================================================================

void progressBar::Bar()
{
  static Int_t ii = 0;
  static Bool_t first = kTRUE;
  static Int_t iTuplEntry = 0;

  if (first) 
  {
    timer = new TTimer((TObject *)this, 100);
    timer -> Start(100, kFALSE);
    first = kFALSE;
  };
  
  mutex m;
  m.lock();

  ii++;

  if (!iTuplEntry) Banner(iTuplEntry);

  if (!(ii%50))  Banner(iTuplEntry);
  else if (iTuplEntry) std::cout <<"\r\033[1A\033[1A";

  std::cout << Form("\e[38;05;%dm\e[1m|",ii)
            << std::string(69 * iTuplEntry/nTuplEntries , '=')
            << "\e[0m"
            << std::string(69 - 69 * iTuplEntry/nTuplEntries -1, '-')<< "] "
            << 100 * iTuplEntry/nTuplEntries+1 << "%" << std::endl;
  std::cout << "|Processing Event: "<< iTuplEntry << " Of: " << nTuplEntries << std::endl;
  std::cout << std::flush;

  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  m.unlock();

  iTuplEntry++;
  
  return;
};

void progressBar::Bar( Int_t iEntry, Int_t nEntries)
{
  static Int_t ii = 0;
  
  ii++;

  if (!iEntry) Banner(iEntry);

  if (!(ii%50))  Banner(iEntry);
  else if (iEntry) std::cout <<"\r\033[1A\033[1A";

  std::cout << Form("\e[38;05;%dm\e[1m|",ii)
            << std::string(69 * iEntry/nEntries , '=')
            << "\e[0m"
            << std::string(69 - 69 * iEntry/nEntries -1, '-')<< "] "
            << 100 * iEntry/nEntries+1 << "%" << std::endl;
  std::cout << "|Processing Event: "<< iEntry << " Of: " << nEntries << std::endl;
  std::cout << std::flush;
};

#include <chrono>
#include <thread>

void progressBar::Banner( Int_t iEntry )
{
  static Int_t col[6] = {41, 41, 41, 41, 41, 41};
  static Int_t newC  =  0;
  static Int_t iline = -1;
  
  if (!iEntry) std::cout <<"\n\n\n\n\n\n\n\n";

  std::cout <<"\r\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A";
  cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|                                           _	                   )"<<endl; 
  cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|                                          |_|                   )"<<endl; 
  cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|    _ __ _   _ _ __     ___  _ __    ____  ___ __ ___  _   _    )"<<endl; 
  cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(|   | '__| | | | '_ \   / _ \| '_ \  (   _)| | '_ ` _  \ | | |   )"<<endl; 
  cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(|   | |  | |_| | | | | | (_) | | | |  \  \ | | | | | | | |_| |   )"<<endl; 
  cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(|   |_|   \___/|_| |_|  \___/|_| |_| |____)|_|_| |_| |_|\___/    )"<<endl;
  cout<< "\e[0m";
  std::cout << std::flush;

  iline++; 
  
  if (iline == 6) {iline = 0; newC++;}
  
  col[iline] = newC;
  
  return;
};

