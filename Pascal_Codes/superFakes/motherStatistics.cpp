
#include "motherStudy.h"

// use this order for safety on library loading
using namespace std;

void motherStudy::eventSummary(  )
{
    CGood.stat = 100*Float_t(CGood.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    std::cout<<"|" << std::string(68 , '=')<<"]"<<std::endl;
    std::cout<<"|Total statistics  : "<<CGood.nTot+CFake.nTot+CComb.nTot<<std::endl;

    /*******************************************************
     *
     * G O O D   E V E N T S
     *
     *********/
    CGood.stat = 100*Float_t(CGood.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    std::cout<<"|--- Good : " << CGood.nTot<<Form(" (%.2f%%) ", CGood.stat)<<std::endl;
    for (const auto& [key, value] : CGood.PM) 
    {
      std::cout << "| " << setw(16) << left << key << ": "
                << setw(9)  << left << value
                << Form("(%.2f%%)",100.*Float_t(value)/CGood.nTot)<<"\n";
      CGood.sM[key] = 100.*Float_t(value)/(CGood.nTot+CFake.nTot+CComb.nTot);
    };
    CGood.sM["Fake"] = 100.*Float_t(CFake.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    CGood.sM["Comb"] = 100.*Float_t(CComb.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    
    /*******************************************************
     *
     * F A K E   E V E N T S
     *
     *********/
    CFake.stat = 100*Float_t(CFake.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    std::cout<<"|--- Fake : " << CFake.nTot<<Form(" (%.2f%%) ",CFake.stat)<<std::endl;
    for (const auto& [key, value] : CFake.PM) 
    {                                                
      std::cout << "| " << setw(16) << left << key << ": "
                << setw(9)  << left << value
                << Form("(%.2f%%)",100.*Float_t(value)/CFake.nTot)<<"\n";
      CFake.sM[key] = 100.*Float_t(value)/CFake.nTot;
    };
                                                  
    /*******************************************************
     *
     * C O M B I N A T O R I A L
     *
     *********/
    CComb.stat = 100*Float_t(CComb.nTot)/(CGood.nTot+CFake.nTot+CComb.nTot);
    std::cout<<"|--- SupF : " << CComb.nTot<<Form(" (%.2f%%) ",CComb.stat)<<std::endl;
    for (const auto& [key, value] : CComb.PM) 
    {
      std::cout << "| " << setw(16) << left << key << ": "
                << setw(9)  << left << value
                << Form("(%.2f%%)",100.*Float_t(value)/CComb.nTot)<<"\n";
      CComb.sM[key] = 100.*Float_t(value)/CComb.nTot;
    };
    std::cout<< "|" << std::string(68 , '=')<<"]"<<"\n\n";
    return;
};


