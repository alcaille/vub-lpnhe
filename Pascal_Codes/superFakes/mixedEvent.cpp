// Include files 



// local
#include "mixedEvent.h"


//-----------------------------------------------------------------------------
// Implementation file for class : mixedEvent
//
// 2024-03-26 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
mixedEvent::mixedEvent( ) : superBanner ( __BANNER_DATA__ )
{
  iColor = 0;
  timer  = NULL;
  firstBar = 0;
  firstBan = 0;
  
  tree  = new TChain("mixedEventTree");
};

//=============================================================================
// Destructor
//=============================================================================
mixedEvent::~mixedEvent()
{
  BM.clear();
  FM.clear();
  DM.clear();
} 

//=============================================================================

void mixedEvent::linkTree(  )
{
  cout<<"|2016 MagUp   : "<<dataSamples.Up<<endl; 
  tree->Add(dataSamples.Up);
  cout<<"|2016 MagDown : "<<dataSamples.Down<<endl;
  tree->Add(dataSamples.Down);
  cout<<"|"<<endl;
  
  std::vector<std::string> BN = 
    {"B_L0MuonDecision_TOS", "B_Hlt2TopoMu2BodyDecision_TOS", "B_Hlt2SingleMuonDecision_TOS", "K_isMuon"};

  std::vector<std::string> FN = 
    {"B_ENDVERTEX_CHI2", "B_FD_S", "K_PAIR_M", "mu_PAIR_M", "TMVA_Charge_BDT_Run1", "TMVA_SS_Afc_BDT_Run1"};
  
  std::vector<std::string> DN = 
    {"B_MCORR", "B_MCORRERR", "B_M","B_Regression_Q2_BEST_LINEAR_Run2", "Mass_Kmu_as_mumu",
     "K_IsoMinBDT", "K_PX", "K_PY", "K_PZ", "mu_IsoMinBDT", "mu_PX", "mu_PY", "mu_PZ"};
  
  for (vector<string>::iterator i = BN.begin(); i != BN.end(); ++i) BM[i->c_str()] = 0;
  for (vector<string>::iterator i = FN.begin(); i != FN.end(); ++i) FM[i->c_str()] = 0;
  for (vector<string>::iterator i = DN.begin(); i != DN.end(); ++i) DM[i->c_str()] = 0;

  tree->SetBranchStatus ( "*" , 0 );

  attachBranch( BM );
  attachBranch( FM );
  attachBranch( DM );
}

template<typename T>
void mixedEvent::attachBranch( T &b ) 
{
  for (const auto& [key, value] : b)
  {
    tree->SetBranchStatus (key.c_str() , 1 );
    tree->SetBranchAddress(key.c_str(), &b[key.c_str()]);
    if (!&b[key.c_str()]) std::cout<<"\n\nW A R N : N I L L   P O I N T E R   4  - "<<key.c_str()<<"\n\n";
  };
};

Bool_t mixedEvent::eventSelection(  )
{
  if (cutTrimmer)
  {
    if ( DM["B_MCORR"] < massRange[0] ) return(kFALSE);
    if ( DM["B_MCORR"] > massRange[1] ) return(kFALSE);
    if (!BM["B_L0MuonDecision_TOS"] )   return(kFALSE);
    if (!BM["B_Hlt2TopoMu2BodyDecision_TOS"] && !BM["B_Hlt2SingleMuonDecision_TOS"]) return(kFALSE);
    if ( FM["B_ENDVERTEX_CHI2"] >= 4 )  return(kFALSE);
    if (TMath::Min(DM["K_IsoMinBDT"],DM["mu_IsoMinBDT"]) < -0.9) return(kFALSE);
  };

  if (cutOffline)
  {
    if ((DM["Mass_Kmu_as_mumu"] > 3072) && (DM["Mass_Kmu_as_mumu"] < 3130)) return(kFALSE);
    if ( BM["K_isMuon"] ) return(kFALSE);
    if ((FM["K_PAIR_M"]-892)  <= 60 ) return(kFALSE);
    if ((FM["K_PAIR_M"]-1430) <= 90 ) return(kFALSE);
    if ((3072 < FM["mu_PAIR_M"] ) && (FM["mu_PAIR_M"] < 3130)) return(kFALSE);
  };

  if (cutFit)
  {
    if (((DM["K_PX"] * DM["mu_PX"]) < 0. ) &&
        ((DM["K_PY"] * DM["mu_PY"]) < 0. )) return(kFALSE);
    if ( FM["B_FD_S"]  <= 40 ) return(kFALSE);
    //    if ( DM["Bs_M"] >= 5175. ) return(kFALSE);
    //  if ( DM["Bs_MCORRERR"] >= 100. ) return(kFALSE);
    if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] <= 0 ) return(kFALSE);
    if ( FM["TMVA_Charge_BDT_Run1"] <= 0.05 ) return(kFALSE);
    // if ( FM["TMVA_SS_Afc_BDT_Run1"] <= 0 ) return(kFALSE);
  };

  return(kTRUE);
};

void mixedEvent::eventLoop( ) 
{
  if (!nTuplEntries) nTuplEntries = tree -> GetEntries();

  TThread *t1;
  TThread *t2;
  
  if (banner) {t1= new TThread("barAndBanner", barAndBanner, this); t1->Run();};
  
  t2 = new TThread("threadTree",   threadTree,   this); t2->Run();

  t2 ->Join();
  
  if (banner) endBar(  );

  return;
};


void mixedEvent::readTree( ) 
{
  createHistograms( );

  /*********************************************************************************************
   *
   * L O O P   O N   E V E N T S
   *
   *****/

  for(iTuplEntry = 0; iTuplEntry < nTuplEntries; ++iTuplEntry)
  {
    tree->GetEntry(iTuplEntry);
    
    if (!eventSelection(  )) continue;

    nSelEntries++;
    
    fillHistograms( );
  
  };

  if (timer)
  {
    timer -> TurnOff();
  } else if (banner) cout<<"WARNING : NILL TTimer\n";

  return;
};

void mixedEvent::end( )
{
  if(banner && fireworks) {thread = new TThread("fireThread", fireThread, this); thread->Run();};
  
  eventSummary( );

  drawHistograms( 0x7 );

  if ( banner && fireworks && thread ) thread ->Join();
  
  if ( saveWork ) save( );
};

/*********************************************************************************************
 *
 * H I S T O G R A M I N G
 *
 *****/

void mixedEvent::createHistograms( )
{
  TString hname[3] = { "mixedFullQ2Region", "mixedLowerQ2Region", "mixedUpperQ2Region" };
  Int_t icolor[3] = { kBlack, kGreen+2, kBlue+2 };
  
  TH1F * dummy;  
  for ( auto i = 0; i < 3 ; i++ )
  {
    dummy = (TH1F *) gROOT -> FindObject(hname[i]);
    if (dummy) delete dummy;
  
    histo[i] = new TH1F(hname[i],hname[i], __NBINS__, massRange[0], massRange[1]);
    histo[i]->SetMarkerStyle(20);
    histo[i]->SetMarkerColor(icolor[i]);
    histo[i]->SetFillStyle(1001);
    histo[i]->SetLineStyle(0);
    histo[i]->SetLineColor(icolor[i]);
    histo[i]->SetLineWidth(2);
  };
};

void mixedEvent::fillHistograms(  )
{
  histo[0] -> Fill( DM["B_MCORR"], 1. );

  if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] <= cutQ2Val )
    histo[1] -> Fill( DM["B_MCORR"], 1. );

  if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] >  cutQ2Val ) 
    histo[2] -> Fill( DM["B_MCORR"], 1. );

  return;
};

void mixedEvent::drawHistograms( Int_t )
{
  gStyle->SetPalette(kOcean);

  canvas = new TCanvas("mixedEventDataCanvas", "mixedEventDataCanvas", 10, 10, 1100, 800);
  canvas ->Draw();
  canvas ->cd();
  TPad *p1 = new TPad("p1","p1",0.5,0.5,1.0,0.95); p1->Draw(); p1->cd(); 
  histo[1]->SetMaximum(histo[1]->GetMaximum()*1.35);
  histo[1]->SetTitle(Form("Low q^{2} region;%s (MeV/c^{2});Entries",varName.Data())); 
  histo[1]->Draw("EP");

  canvas ->cd();
  TPad *p2 = new TPad("p2","p2",0.5,0.05,1.0,0.5); p2->Draw(); p2->cd();
  histo[2]->SetMaximum(histo[2]->GetMaximum()*1.35);
  histo[2]->SetTitle(Form("High q^{2} region;%s (MeV/c^{2});Entries",varName.Data())); 
  histo[2]->Draw("EP");

  canvas ->cd();
  TPad *p = new TPad("p","p",0.01,0.0,0.5,1.0); p->Draw(); p->cd();
  THStack *stack = new THStack("mixed_HStack","mixed_HStack");
  stack->Add( histo[1],"HIST E" );
  stack->Add( histo[2],"HIST E" );

  stack->SetTitle(Form("#color[2]{Mixed events};%s (MeV/c^{2});Entries",varName.Data()));
  stack->SetMaximum(stack->GetMaximum()*1.25);
  stack->Draw("pfc");

  TLegend *slegend = new TLegend(0.15, 0.5, 0.45, 0.6);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.03);
  slegend->AddEntry(histo[0], "All selected events", "pe");
  slegend->AddEntry(histo[1], "Low q^{2} region", "F");
  slegend->AddEntry(histo[2], "High q^{2} region", "F");

  slegend->Draw();

  TPaveText* sampleText = new TPaveText(0.15, 0.70, 0.40, 0.85, "BRNDC");
  sampleText->SetFillColor(0);
  sampleText->SetTextAlign(12);
  sampleText->SetBorderSize(0);
  sampleText->SetLineWidth(0);
  sampleText->SetTextSize(0.025);
  sampleText->SetFillStyle(0);
  sampleText->AddText("2016 Data");
  sampleText->AddText("Magnet up and down");
  sampleText->AddText(Form("Selection: TRIMMER=%d, OffLine=%d, Fit cuts=%d", cutTrimmer, cutOffline, cutFit));
  if (q2Flag == 1) sampleText->AddText(Form("Q2 #leq %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else if (q2Flag == 2) sampleText->AddText(Form("Q2 > %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else sampleText->AddText("No cut on Q2");
  sampleText->Draw();
  
  drawLHCbLogo();
  
  canvas ->Update();
  return;
};

TH1F *mixedEvent::GetHistogram()
{
  TFile *fws;

  try {
    fws = new TFile(Form("ws/mixedEvent_E_%d_%d_Q0_S%d%d%d_%s%s.root",
                         Int_t(massRange[0]), Int_t(massRange[1]), cutTrimmer, cutOffline, cutFit,
                         varName.Data(), (isTrimmer)?"TRIMMER":""));
  }
  catch (const std::exception& err) {
    std::cerr << "Oups !" << std::endl;
    std::cerr << err.what() << std::endl;
    return(NULL);
  }
  
  RooWorkspace* wspace = (RooWorkspace*) fws->Get("myWS") ;

  TH1F *histo;
  switch (q2Flag)
  {
  case 0:
    histo = (TH1F *) wspace->genobj("mixedFullQ2Region");
    break;
  case 1:
    histo = (TH1F *) wspace->genobj("mixedLowerQ2Region");
    break;
  case 2:
    histo = (TH1F *) wspace->genobj("mixedUpperQ2Region");
    break;
  default:
    break;
  };
  
  return histo;
};

TH1F *mixedEvent::drawH( Double_t norm )
{

  static Int_t i = 0;
  
  TH1F *histo = (TH1F *)GetHistogram();

  TH1F *clone = (TH1F *)histo->Clone(Form("%s_c%d", histo->GetName(),++i));

  clone->GetXaxis()->SetRangeUser(massRange[0], massRange[1]);
  clone->SetMarkerColor(kBlack);
  clone->SetLineColor(kBlack);

  clone->Scale(1./clone->Integral());
    
  clone->Sumw2();

  clone->Scale(norm);

  clone->Draw( "SAME PE" );

  return(clone);
};

void mixedEvent::save( )
{
  canvas->SaveAs(Form("figures/data/%s_E_%d_%d_Q%d_S%s_%s%s.png",
                      canvas->GetName(), Int_t(massRange[0]), Int_t(massRange[1]),
                      q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/mixedEvent_E_%d_%d_Q%d_S%s_%s%s.root",
                            Int_t(massRange[0]), Int_t(massRange[1]),
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (auto i = 0; i < 3; i++)
    wspace->import(*histo[i], kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
  
  if (fws) fws->Close();
  
  return;
};
