// Include files 
#include "superFake.h"


BKGCAT superFake::selectb2cKmunuX()
{

  BKGCAT category = BKGUNKNOWN;

  if ((TMath::Abs(IM["muon_p_TRUEID"]) == 13  && isHadron(TMath::Abs(IM["muon_p_MC_MOTHER_ID"]),5)) &&
      (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (isHadron(TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]),4) ||
                                                   isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                                                   isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4))) &&
      isHadron(TMath::Abs(IM["Bs_TRUEID"]),5)
      )
  { // Hb -> Hc (->KX) mu nu X'
    category = BKGGOOD;
  } else {
    if (
        (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && isHadron(TMath::Abs(IM["muon_p_MC_MOTHER_ID"]),5)) ||
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (isHadron(TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]),4) ||
                                              isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                                              isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4)))
        )
    {
      category = BKGFAKE;
    } else if (
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && isHadron(TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]),5))  ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 &&
                (isHadron(TMath::Abs(IM["muon_p_MC_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]),4)))
               )
    { // miss ID
      category = BKGFAKE;
    } else if ((TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4))
                ) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4)))
               )
    {  // K -> mu 
      category = BKGFAKE;
    } else if ((TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4))
                ) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (isHadron(TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]),4) ||
                 isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4)))
               )
    {  // K -> pi 
      category = BKGFAKE;
    } else if ((TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 && isHadron(TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]),4)
                ) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 && isHadron(TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]),4))
               )
    { // K -> pion -> mu
      category = BKGFAKE;
    } else 
    { // Super fake
      category = BKGSUPFAKE;
    };
  };

  return category;
};
