// Include files 
#include "superFake.h"


BKGCAT superFake::selectB2JpsiK()
{

  BKGCAT category = BKGUNKNOWN;

  if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_MOTHER_KEY"]) &&
      (IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
      (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
       TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
      (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
      (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0))
  { // B -> J/psi K -> mu+ mu- K
    category = BKGGOOD;
  } else {
    if ((TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
         TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) ||
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521 &&
         (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ))
    {
      category = BKGFAKE;
    } else if ((TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 443 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 521 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ))
    { // miss ID
      category = BKGFAKE;
    } else if (
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  &&
                TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
                TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211  &&
                TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 211  &&
                TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  &&
                TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
                TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 531)
               )
    { // K decay in mu or pi and can be missid & pi -> mu
      category = BKGFAKE;
    } else if (TMath::Abs(IM["muon_p_TRUEID"]) && TMath::Abs(IM["kaon_m_TRUEID"]))
    {
      category = BKGSUPFAKE;
    };
  };

  return category;
};
