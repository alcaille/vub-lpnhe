#ifndef SUPERFAKES_SAMESIGNDATA_H 
#define SUPERFAKES_SAMESIGNDATA_H 1

// Include files
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <deque>
#include <unordered_map>
#include <vector>
#include <mutex>
#include <thread>
#include <chrono>

#include <TROOT.h>
#include <TRint.h>
#include <TObject.h>
#include <TTimer.h>
#include <TThread.h>
#include <TDatime.h>
#include <TMutex.h>
#include <TTree.h>
#include <TExec.h>
#include <TChain.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TDatabasePDG.h>


// Local
#include "superStudy.h"
#include "superBanner.h"

using namespace std;

/** @class sameSignData sameSignData.h test/sameSignData.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-05
 */
class sameSignData :
  public superBanner,
  public fireWorks
{
public: 
  /// Standard constructor
  sameSignData( ); 

  virtual ~sameSignData( ); ///< Destructor

  void linkTree( ); 
  static void *threadTree( void * ptr ){
    sameSignData *s = (sameSignData *)ptr;
    s->readTree( );
    return NULL;
  };
  void readTree( );
  template<typename T>
  void attachBranch( T & );

  void eventLoop( );
  Bool_t eventSelection( );
  void fillHistograms( );
  TH1F *drawH( Double_t );

  void SetSampleName( const char *s ){};
  void SetSampleTag ( const char *s ){};
  void SetSampleID  ( Int_t i ){};
  void SetWhat2Plot ( Int_t i ){};
  
  void SetVarName   ( const char *s ){varName.Append(s); return;};
  void SetIsTrimmer ( Bool_t i ){isTrimmer = i; return;};
  void SetVerbosity ( Int_t v ){Verbose = v;};
  void SetMassRange ( Float_t *m ){massRange[0] = m[0]; massRange[1] = m[1];};
  void SetQ2ValCut  ( Int_t q ){q2Flag = q;};
  void SetCutTrimmer( Bool_t c ){cutTrimmer = c;};
  void SetBanner    ( Bool_t i ){banner = i;}; 
  void SetCutOffline( Bool_t c ){cutOffline = c;};
  void SetCutFit    ( Bool_t c ){cutFit     = c;};
  void SetCutQ2Val  ( Double_t c ){cutQ2Val = c;};
  void SetLogScale  (Bool_t l){logscale = l;};
  void SetSave      (Bool_t s){saveWork = s;};
  void SetSaveMacros(Bool_t s){saveMacros = s;};

  Float_t  GetMiniMass  ( ){return massRange[0];};
  Float_t  GetMaxiMass  ( ){return massRange[1];};
  void     GetMassRange ( Float_t *m ){m[0] = massRange[0]; m[1] = massRange[1];};
  Int_t    GetQ2ValCut  ( ){return q2Flag;};

  TH1F *   GetHistogram(){return histo[q2Flag];};
  virtual void fillHistograms( BKGCAT b ){b = BKGUNUSED; return;};
  virtual void createHistograms( );
  virtual void drawHistograms( Int_t );
  virtual void eventSummary( ){};
  virtual void save();
  void end();

protected:

  BoolMap   BM; 
  FloatMap  FM; 
  DoubleMap DM;

  Bool_t  logscale = kFALSE;

private:
  Int_t Verbose;
  
  TChain  *tree;
  TH1F    *histo[3];
  TCanvas *canvas;
  
  Bool_t saveWork = kFALSE;
  Bool_t saveMacros = kFALSE;

  TThread *thread;
  /*
  auto ithread[3] = {0, 0, 0};

  dataSampleDef dataSamples[3] = {
    {"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2016_Up_TRIMMER.root/B2XuMuNuBs2KSS_Tuple",
     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2016_Down_TRIMMER.root/B2XuMuNuBs2KSS_Tuple"},
    {"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2017_Up_TRIMMER.root/B2XuMuNuBs2KSS_Tuple",
     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2017_Down_TRIMMER.root/B2XuMuNuBs2KSS_Tuple"},
    {"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2018_Up_TRIMMER.root/B2XuMuNuBs2KSS_Tuple",
     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2018_Down_TRIMMER.root/B2XuMuNuBs2KSS_Tuple"},
  };
  */
  
  dataSampleDef dataSamples = {
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2016_Up_TRIMMER.root/B2XuMuNuBs2KSS_Tuple",
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/trimmer/DTT_90000000_2016_Down_TRIMMER.root/B2XuMuNuBs2KSS_Tuple"};
  
  TString varName;
  Float_t massRange[2];
  Bool_t  isTrimmer;
  Int_t   q2Flag;
  Bool_t  banner;

  Double_t cutQ2Val = __Q2_CUT_VALUE__;
  Bool_t cutTrimmer;
  Bool_t cutOffline;
  Bool_t cutFit;   
};
#endif // SUPERFAKES_SAMESIGNDATA_H
