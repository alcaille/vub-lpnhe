// Include files 
#include "superFake.h"

BKGCAT superFake::selectB2ccbarKX()
{

  BKGCAT category = BKGUNKNOWN;

  if (
      (IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_MOTHER_KEY"]) &&
      (IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
      (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
      (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
      (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&
      TMath::Abs(IM["Bs_TRUEID"]) == 521
      )
  { // B -> J/psi K+ (pi+ pi- / eta / omega)-> mu+ mu- K+ (pi+ pi- / eta / omega)
    category = BKGGOOD; 
  }
      
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> J/psi K*+ -> mu+ mu- K+ pi0
    category = BKGGOOD; 
  }
      
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> J/psi K1(1270)+ -> mu+ mu- rho0 K+
    category = BKGGOOD;
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10311 || TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 315) &&
            TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
            TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521))
  { // B -> J/psi K1(1270)+ -> mu+ mu- K*0(1430) pi -> mu+ mu- (K+ pi-) pi
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10321 || TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 325) &&
            TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
            TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0))
  { // B -> J/psi K1(1270)+ -> mu+ mu- K*0(1430)+ pi0 -> mu+ mu- (K+ pi0) pi0
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 313 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
            TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521))
  { // B -> J/psi K1(1270)+ -> mu+ mu- K*0 pi -> mu+ mu- (K+ pi-) pi
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
            TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0) )
  { // B -> J/psi K1(1270)+ -> mu+ mu- K*+ pi0 -> mu+ mu- (K+ pi0) pi
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 100443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0))
  { // B -> psi(2S) K+ (pi+ pi-) -> mu+ mu- K+ (pi+ pi-)
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100443 &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0))
  { // B -> psi(2S) K+ (pi+ pi-) -> J/psi K+ -> mu+ mu- (pi+ pi-/pi0 pi0/eta) K+
    category = BKGGOOD; 
  }
  else if ((IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
            (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10441 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 ||
             TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 445   || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100445) &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 100443) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0))
  { // B -> psi(2S) K+ (pi+ pi-) -> Chi_c0/1/2 gamma K+ -> J/psi gamma K+-> mu+ mu- gamma gamma K+
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 100443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> psi(2S) K*+ -> mu+ mu- K+ pi0
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
            TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
            TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100443 &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> psi(2S) K*+ -> J/psi (pi+ pi-/pi0 pi0/eta) K+ pi0 -> mu+ mu- (pi+ pi-/pi0 pi0/eta) K+ pi0
    category = BKGGOOD; 
  }
  else if ((IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
            TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
            (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100441 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 ||
             TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 445    || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100445) &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 100443) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> psi(2S) K*+ -> Chi_c0/1/2 k+ pi0 -> J/psi gamma K+ pi0 -> mu+ mu- gamma K+ pi0
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
            TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
            (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10441 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 ||
             TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 445    || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100445) &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> Chi_c0/1/2 K+ -> J/psi gamma K+ -> mu+ mu- gamma K+
    category = BKGGOOD; 
  }
  else if ((IM["muon_p_MC_GD_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
           (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
           (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 &&
            TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) &&
           (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) &&
           (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0))
  { // B -> Chi_c1 K*+ -> J/psi gamma K+ pi0 -> mu+ mu- gamma K+ pi0
    category = BKGGOOD; 
  } else {
    if ((TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) ||
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521   && IM["kaon_m_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
        // B -> J/psi K+ (pi+ pi- / eta / omega)-> mu+ mu- K+
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
        // B -> J/psi K*+ -> mu+ mu- K+ pi0
        // B -> psi(2S) K*+ -> mu+ mu- K+ pi0
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10323 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
        // B -> J/psi K1(1270) -> mu+ mu- rho0 K
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10311 || TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 315) &&
         TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
         TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) ||
        // B -> J/psi K1(1270) -> mu+ mu- K*0(1430) pi -> mu+ mu- (K+ pi-) pi
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && (TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 10321 || TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 325) &&
         TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
         TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 &&
         (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
        // B -> J/psi K1(1270) -> mu+ mu- K*0(1430)+ pi0 -> mu+ mu- (K+ pi0) pi0
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 313   && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
         TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) ||
        // B -> J/psi K1(1270) -> mu+ mu- K*0 pi -> mu+ mu- (K+ pi-) pi
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 &&
         TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 &&
         (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
        // B -> J/psi K1(1270) -> mu+ mu- K*+ pi0 -> mu+ mu- (K+ pi0) pi
        (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 100443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521) ||
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 521 && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0)) ||
        // B -> psi(2S) K+ (pi+ pi-) -> mu+ mu- K+
        (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100443 &&
         TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) ||
        // B -> psi(2S) K+ (pi+ pi-) -> J/psi K+ -> mu+ mu- (pi+ pi-/pi0 pi0/eta) K+
        // B -> psi(2S) K*+ -> J/psi (pi+ pi-/pi0 pi0/eta) K+ pi0 -> mu+ mu- (pi+ pi-/pi0 pi0/eta) K+ pi0
        (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
         (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10441 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 ||
          TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 445   || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100445) &&
         TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 100443) ||
        // B -> psi(2S) K+ (pi+ pi-) -> Chi_c0/1/2 gamma K+ -> J/psi gamma K+-> mu+ mu- gamma gamma K+
        (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 443 &&
         (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10441 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 20443 ||
          TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 445    || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 100445) &&
         TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521)
        // B -> Chi_c0/1/2 K+ -> J/psi gamma K+ -> mu+ mu- gamma K+
        // B -> Chi_c1 K*+ -> J/psi gamma K+ pi0 -> mu+ mu- gamma K+ pi0
        )
    {
      category = BKGFAKE; 
    } else if ((TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 443   && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 100443 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 443 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 100443 &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 443 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10441 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 20443 ||
                 TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 445   || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 100445) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 100443) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 443 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 100441 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 20443 ||
                 TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 445    || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 100445) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521) ||
              
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 521   && IM["muon_p_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 &&
                IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 10323 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 &&
                IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && (TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 10311 || TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 315) &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && (TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 10321 || TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 325) &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 313   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 521 && (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0)))
    { // miss ID
      category = BKGFAKE;          
    } else if (
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 315) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 325) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 313   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0)) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 315) &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 325) &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 313   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 && (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0)) ||

               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 &&
                IM["kaon_m_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10323 && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 315) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 325) &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 313   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 521 && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0)) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10323 && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 315) &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 325) &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 313   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 323   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 211 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 521 && (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0)) ||
               // K -> pi -> mu
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 &&
                IM["kaon_m_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 323 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10323 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 315)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 && TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 325) &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 313) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 323 &&
                (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_MOTHER_ID"] > 0) &&  (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 521 && (IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0)) ||
                   
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 &&
                IM["muon_p_TRUEID"]*IM["Bs_TRUEID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 323 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10323 && IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10311 || TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 315)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 && TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 &&
                (TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 10321 || TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 325) &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) && (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 313) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 && 
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321   && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 323 &&
                (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_MOTHER_ID"] > 0) &&  (IM["muon_p_TRUEID"]*IM["muon_p_MC_GD_GD_MOTHER_ID"] > 0)) ||
               (TMath::Abs(IM["muon_p_TRUEID"]) == 13 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 211 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 321 && TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 521 && (IM["muon_p_TRUEID"]*IM["muon_p_MC_MOTHER_ID"] > 0))                    
               )
    { // K decay into muon or pion and can be missid
      category = BKGFAKE;          
    } else if (TMath::Abs(IM["muon_p_TRUEID"]) && TMath::Abs(IM["kaon_m_TRUEID"]))
    {
      category = BKGSUPFAKE;
    };
  };
  return category;
};
