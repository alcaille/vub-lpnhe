// Include files 



// local
#include "motherStudy.h"

//-----------------------------------------------------------------------------
// Implementation file for class : motherStudy
//
// 2024-03-24 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
motherStudy::motherStudy( TChain *t ) : superFake ( t )
{
  for (auto &c : CGood.canvas ) c = NULL;
  for (auto &c : CFake.canvas ) c = NULL;
  for (auto &c : CComb.canvas ) c = NULL;

  CGood.SM = NULL;
  CFake.SM = NULL;
  CComb.SM = NULL;
}
//=============================================================================
// Destructor
//=============================================================================
motherStudy::~motherStudy() {} 

//=============================================================================
