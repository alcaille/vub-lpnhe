// Include files 
#include "superFake.h"

BKGCAT superFake::selectBs2Kstrmunu( Int_t whichKstr )
{

  BKGCAT category = BKGUNKNOWN;

  if ((IM["muon_p_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_MOTHER_KEY"]) &&
      (IM["muon_p_MC_GD_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["muon_p_MC_GD_MOTHER_KEY"]) &&
      (IM["Bs_MC_MOTHER_KEY"] == IM["kaon_m_MC_GD_GD_MOTHER_KEY"]) &&
      (TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 531) &&
      (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == whichKstr &&
       TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 531) &&
      (IM["kaon_m_TRUEID"]*IM["muon_p_TRUEID"] > 0 && IM["kaon_m_TRUEID"]*IM["kaon_m_MC_MOTHER_ID"] > 0) )
  { // b~ -> B~s -> K*- mu+ nu -> K- pi0 mu+ nu
    category = BKGGOOD;
  } else {
    if ((TMath::Abs(IM["muon_p_TRUEID"]) == 13  && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 531) ||
        (TMath::Abs(IM["kaon_m_TRUEID"]) == 321 && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == whichKstr &&
         TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == 531) )
    {
      category = BKGFAKE;
    } else if ((TMath::Abs(IM["muon_p_TRUEID"]) == 321 && TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == whichKstr &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == 531) ||
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  && TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 531 ) )
    { // Miss ID
      category = BKGFAKE;
    } else if (
               (TMath::Abs(IM["kaon_m_TRUEID"]) == 13  &&
                TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == whichKstr &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 13  &&
                TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == whichKstr &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["kaon_m_TRUEID"]) == 211  &&
                TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["kaon_m_MC_GD_MOTHER_ID"]) == whichKstr &&
                TMath::Abs(IM["kaon_m_MC_GD_GD_MOTHER_ID"]) == 531) ||

               (TMath::Abs(IM["muon_p_TRUEID"]) == 211  &&
                TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) == 321 &&
                TMath::Abs(IM["muon_p_MC_GD_MOTHER_ID"]) == whichKstr &&
                TMath::Abs(IM["muon_p_MC_GD_GD_MOTHER_ID"]) == 531)                    
               )
    { // K decay into mu or pion
      category = BKGFAKE;
    } else if (TMath::Abs(IM["muon_p_TRUEID"]) && TMath::Abs(IM["kaon_m_TRUEID"]))
    {
      category = BKGSUPFAKE;
    };
  };

  return category;
};
