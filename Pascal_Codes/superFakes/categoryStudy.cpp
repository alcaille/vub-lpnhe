//-----------------------------------------------------------------------------
// Implementation file for class : categoryStudy
//
// 2024-03-21 : Pascal Vincent
//-----------------------------------------------------------------------------
// Include files 

// local
#include "categoryStudy.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================

categoryStudy::categoryStudy( TChain *t ) : superFake ( t )
{
  *canvas = NULL;
};

//=============================================================================
// Destructor
//=============================================================================
categoryStudy::~categoryStudy() {} 

//=============================================================================

void categoryStudy::createHistograms( )
{

  /*********************************************************************************************
   *
   * D E C L A R E   A N D   P A I N T   H I S T O G R A M S
   *
   *****/

  std::vector<std::string> HN = {
    "Good", "Fake", "AFak",
    "SFak", "SFNO",
    "SFBa", "SFMe", "SFUN"}; 
  
  std::vector<paintHistogram> PN = {
    {2, kRed+2,   1, 0, 0, sampleName.Data()},
    {2, kBlack,   9, 0, 0, sampleName.Data()},  
    {2, kBlue+2,  1, 0, 0, sampleName.Data()},
    {2, kGreen+2, 1, 0, 0, sampleName.Data()},
    {2, kGreen+2, 1, 0, 0, sampleName.Data()},

    {2, kBlue+2,  1, 0, 0, sampleName.Data()},
    {2, kRed+2,   1, 0, 0, sampleName.Data()},
    {2, kBlack,   1, 0, 0, sampleName.Data()}
  };

  auto ii = 0;
  for (vector<string>::iterator i = HN.begin(); i != HN.end(); i++)
  {

    HM[i->c_str()] = new TH1F(Form("CH_%s_%s", i->c_str(), sampleTag.Data()), Form("%s", sampleTag.Data()),
                              __NBINS__, massRange[0], massRange[1]);
    HM[i->c_str()]->SetName(Form("H_categoryStudy_%s_%s_%s%s",
                                 sampleTag.Data(), i->c_str(), varName.Data(), (isTrimmer)?"TRIMMER":""));
    HP[i->c_str()] = PN.at(ii);
    ii++;
  };
};

void categoryStudy::setupHistogram(string s, paintHistogram p)
{

  switch (what2plot)
  {
  case 0: HM[s.c_str()]->SetTitle(Form("%s;M_{corr} (MeV/c^{2});Entries", p.title)); break;
  case 1: HM[s.c_str()]->SetTitle(Form("%s;Bs_MM (MeV/c^{2});Entries"   , p.title)); break;
  case 2: HM[s.c_str()]->SetTitle(Form("%s;Bs_M (MeV/c^{2});Entries"    , p.title)); break;
  default: break;
  };

  HM[s.c_str()]->SetLineWidth(p.linew);
  HM[s.c_str()]->SetLineColor(p.linec);
  HM[s.c_str()]->SetLineStyle(p.lines);
  
  HM[s.c_str()]->SetFillColor(p.fillc);
  HM[s.c_str()]->SetFillStyle(p.fills);

  return;
};

void categoryStudy::fillHistograms( BKGCAT category )
{
  Double_t massVal = DM[varName.Data()];

  switch (category)
  {
  case BKGGOOD:
    HM["Good"]->Fill(massVal,1.);
    if (verbose & 0x1) eventDump("VG : ");
    break;
  case BKGFAKE:
    HM["AFak"]->Fill(massVal,1.);
    HM["Fake"]->Fill(massVal,1.);
    // if (verbose & 0x2) eventDump("FA : ");
    if ((verbose & 0x2) && !GetIM("Bs_TRUEID") && massVal>5000.) eventDump("FA :");
    break;
  case BKGSUPFAKE:
    HM["AFak"]->Fill(massVal,1.);
    HM["SFak"]->Fill(massVal,1.);
    HM["SFNO"]->Fill(massVal,1.);
    if      (isMeson (TMath::Abs(IM["Bs_TRUEID"]),5)) HM["SFMe"]->Fill(massVal,1.); 
    else if (isBaryon(TMath::Abs(IM["Bs_TRUEID"]),5)) HM["SFBa"]->Fill(massVal,1.); 
    else                                              HM["SFUN"]->Fill(massVal,1.);
    //      if (verbose & 0x4)eventDump("SF : ");
    if ((verbose & 0x4) && !IM["Bs_TRUEID"] && massVal>5000.) eventDump("SF :");
    break;
  default:
    break;
  };
};

void categoryStudy::drawHistograms( Int_t dummy )
{
  for (const auto& [key, value] : HP)
    setupHistogram(key, value);
  
  canvas[0] = new TCanvas(Form("EventType_%s", sampleTag.Data()),
                          Form("EventType %s", sampleName.Data()), 10, 10, 1100, 800);
  canvas[0]->Divide(2,1);
  canvas[0]->Update();

  /****************************************************************************************
   *
   * D R A W   G O O D   A N D   F A K E
   *
   ************************/
  
  canvas[0]->cd(1);
  gPad->SetLogy();
  
  HM["Good"]->SetMaximum(HM["Good"]->GetMaximum()*1.1);
  HM["Good"]->Draw("HIST E");
  HM["AFak"]->Draw("SAME HIST E");

  drawLegend(1);

  drawLHCbLogo();

  canvas[0]->Update();

  /****************************************************************************************
   *
   * D R A W   F A K E   A N D   S U P E R   F A K E
   *
   ************************/
  
  canvas[0]->cd(2);
  gPad->SetLogy();

  TF1 *fun = new TF1("fun","exp([0]+[1]*x)",3000.,massRange[1]);
  fun->SetParameters(6., -0.0005);
  TFitResultPtr r = HM["SFak"]->Fit("fun","SR");

  HM["Fake"]->SetMaximum(HM["Fake"]->GetMaximum()*10.);
  HM["Fake"]->SetMinimum(0.1);

  HM["Fake"]->Draw("HIST E");
  HM["SFak"]->Draw("SAME HIST");
  HM["AFak"]->Draw("SAME HIST");
  HM["SFak"]->Draw("SAME E");

  drawLegend(2);
  
  drawFitParam(r);
  
  drawLHCbLogo();
  
  canvas[0]->Update();

  /****************************************************
   *
   * B A R Y O N   A N D   M E S O N
   *
   ****/
  canvas[1] = new TCanvas(Form("EventType2_%s", sampleTag.Data()),
                          Form("EventType %s" , sampleName.Data()), 10, 10, 1100, 800);
  canvas[1]->cd();

  gPad -> SetLeftMargin(0.1) ;
  gPad -> SetRightMargin(0.1) ;

  HM["SFMe"]->Draw("HIST E");
  HM["SFBa"]->Draw("SAME HIST E");
  HM["SFUN"]->Draw("SAME HIST E");
  
  drawLegend(3);
  
  drawLHCbLogo();  

  canvas[1]->Update();
  
  HM["SFNO"]->Sumw2();
  HM["SFNO"]->Scale(1./HM["SFNO"]->Integral());

  return;
};

void categoryStudy::save( )
{
  Int_t i = 0;
  for (auto ii : canvas)
    ii->SaveAs(Form("figures/categoryStudy/categoryStudy%d_%s_Q%d_S%s_%s_%s.png",++i,
                    sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                    varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/categoryStudy_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (auto i = HM.begin(); i != HM.end(); ++i)
    wspace->import(*i->second,kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
  
  if (fws) fws->Close();
  
  return;
};


void categoryStudy::drawFitParam( TFitResultPtr r )
{
  TMatrixDSym cov = r->GetCovarianceMatrix();   //  to access the covariance matrix
  Double_t chi2   = r->Chi2();                  // to retrieve the fit chi2
  Double_t par0   = r->Parameter(0);            // retrieve the value for the parameter 0
  Double_t err0   = r->ParError(0);             // retrieve the error for the parameter 0
  Double_t par1   = r->Parameter(1);            // retrieve the value for the parameter 0
  Double_t err1   = r->ParError(1);             // retrieve the error for the parameter 0
  Int_t ndf = r->Ndf();
  
  TString *str1 = new TString();
  TString *str2 = new TString();
  TString *str3 = new TString();
  str3->Append(Form("#chi^{2}/ndf = %.1f/%d = %.2f", chi2, ndf, chi2/ndf));
  str2->Append(Form("Const = %.2f #pm %.2f", par0, err0));
  str1->Append(Form("Slope = %.2e #pm %.2e", par1, err1));

  TPaveText* fitText = new TPaveText(0.15, 0.75, 0.5, 0.85, "BRNDC");
  fitText->SetFillColor(0);
  fitText->SetTextAlign(12);
  fitText->SetBorderSize(0);
  fitText->SetLineWidth(0);
  fitText->SetTextSize(0.03);
  fitText->SetFillStyle(0);
  fitText->AddText(str1->Data());
  fitText->AddText(str2->Data());
  fitText->AddText(str3->Data());
  fitText->Draw();
};

void categoryStudy::drawLegend(Int_t ican)
{
  TLegend *legend;
  TPaveText *dataText;
  switch(ican)
  {
  case 1:
    dataText = new TPaveText(0.55, 0.65, 0.85, 0.75, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("2016 simulations");
    dataText->AddText("Magnet Up");
    if (isTrimmer) dataText->AddText("TRIMMER");

    legend = new TLegend(0.15, 0.15, 0.5, 0.25);
    legend->SetLineWidth(0);
    legend->AddEntry(HM["AFak"], "All fake", "L");
    if (!sampleID) legend->AddEntry(HM["Good"], "True signal", "L");
    else legend->AddEntry(HM["Good"], "True backgrounds", "L");
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    break;
  case 2:
    legend = new TLegend(0.15, 0.15, 0.3, 0.25);
    legend->SetLineWidth(0);
    legend->AddEntry(HM["AFak"], "All fake", "L");
    legend->AddEntry(HM["Fake"], "Fake", "L");
    legend->AddEntry(HM["SFak"], "Super fake", "L");
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    legend->SetFillStyle(0);
    break;
  case 3:
    legend = new TLegend(0.65, 0.45, 0.85, 0.65);
    legend->SetLineWidth(0);
    legend->AddEntry(HM["SFMe"], "Meson",   "L");
    legend->AddEntry(HM["SFBa"], "Baryon",  "L");
    legend->AddEntry(HM["SFUN"], "Unknown", "L");
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    legend->Draw();
    break;
  default:
    break;
  };
  legend->Draw();

  return;
};

