
//-----------------------------------------------------------------------------
// Implementation file for class : cinematicStudy
//
// 2024-04-10 : Pascal Vincent
//-----------------------------------------------------------------------------

// Include files 



// local
#include "cinematicStudy.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
cinematicStudy::cinematicStudy(  ) {
}

cinematicStudy::cinematicStudy( TChain *t ) : superFake ( t )
{
}
//=============================================================================
// Destructor
//=============================================================================
cinematicStudy::~cinematicStudy()
{
  HM.clear();
  HS.clear();

  FN.clear();
  DN.clear();

  xMin.clear();
  xMax.clear();
  yLog.clear();

  canvas.clear();
} 

//=============================================================================

void cinematicStudy::createHistograms( )
{
  // First ask for new branches to be linked
  for ( auto i : DN  ) SetDM (i.c_str());
  for ( auto i : FN  ) SetFM (i.c_str());
  //  for ( auto i : FVN ) SetF3M(i.c_str());
  for ( auto i : IN  ) SetIM (i.c_str());

  //  dumpMap(F3M);

  // Add existing variables to the list of histograms
  std::vector<std::string> FF = 
    {"Bs_ENDVERTEX_CHI2", "Bs_FD_S", "kaon_m_PAIR_M", "muon_p_PAIR_M"};

  std::vector<std::string> DD = 
    {"Mass_Kmu_asJpsi",
     "kaon_m_IsoMinBDT", "kaon_m_PX", "kaon_m_PY", "kaon_m_PZ",
     "muon_p_IsoMinBDT", "muon_p_PX", "muon_p_PY", "muon_p_PZ" };

  DN.insert(DN.end(), DD.cbegin(), DD.cend());
  FN.insert(FN.end(), FF.cbegin(), FF.cend());

  // define log scale mode
  std::vector<Bool_t> dLog = {
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // Bs
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, // kaons
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, // muons
    0, 0, // others
    0, 0, 0, 0, 0, 0, 0, 0, 0 // global variables
  };
  
  std::vector<Bool_t> fLog = {
    1, // Bs
    1, 0, 1, 1 // global variables
  };

  // define edges
  std::vector<Double_t> dMIN = {
    0., 1.5, -1.*TMath::Pi(), 0., 0., 0. , 0., 0., 0., 1E-5, 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,  // Bs
    0., -12., -12., -120., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.,  // Kaons
    20., -15., 2., -200., 0., 0., 0., 0., 0., 0., 0., 0.2, 0., 1., // muons
    500., 0., // other new variables
    1., 1., 1., 1., 1., 1., 1., 1., 1. // global variables
  };

  std::vector<Double_t> fMIN = {
    1., // Bs
    0., 1., 0., 0. // global variables
  };

  std::vector<Double_t> dMAX = {
    120., 5., TMath::Pi(), 100., 2., 1000. , 3., 0.01, 1000., 1E-4, 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., // Bs
    160., 10., 8., 70., 1., 1., 0.2, 1., 1., 1., 1000., 3., 0.2, 1.,       // kaons
    -200, 2., 15., 40., 1., 1., 0.2, 1., 1., 1., 4000., 2., 0.05, 1.,     //muons
    1E+4, 1E+4, // other new variables
    1., 1., 1., 1., 1., 1., 1., 1., 1. // existing variables
  };

  std::vector<Double_t> fMAX = {
    1., // Bs
    4., 1., 1.E+4, 1.E+4 // existing variables
  };

  // Map edges for histograms
  for (Int_t i = 0; i < DN.size(); i++) {
    xMin.insert_or_assign(DN.at(i), dMIN.at(i));
    xMax.insert_or_assign(DN.at(i), dMAX.at(i));
    yLog.insert_or_assign(DN.at(i), dLog.at(i));
  };
  for (Int_t i = 0; i < FN.size(); i++) {
    xMin.insert_or_assign(FN.at(i), fMIN.at(i));
    xMax.insert_or_assign(FN.at(i), fMAX.at(i));
    yLog.insert_or_assign(FN.at(i), fLog.at(i));
  };

  // Create histograms
  for ( auto i : DN )  {HM[i.c_str()] = NULL; HS[i.c_str()] = NULL;};
  for ( auto i : FN )  {HM[i.c_str()] = NULL; HS[i.c_str()] = NULL;};
  
  if (sampleID) initSignalHistograms( );
  
  // Setup histograms
  Double_t xmin = 1.;
  Double_t xmax = 1.;
  for (const auto& [key, value] : HM)
  {
    xmin = xMin[key]; xmax = xMax[key];
    if (HS[key]) {
      xmin = HS[key]->GetXaxis()->GetXmin();
      xmax = HS[key]->GetXaxis()->GetXmax();
    };
    
    HM[key] = new TH1F(Form("H_%d_%s", sampleID, key.c_str()),
                       Form("%s;; entries",key.c_str()), 50, xmin, xmax);
  };

  /***********************************************************************************
   *
   * D e f i n e   u s e r ' s   v a r i a b l e s   f o r   h i s t o g r a m m i n g
   *
   */
  std::vector<std::string> UD = {
    "Bs_OWNPV_CHI2DOF",
    "Kaon_PT_vs_Bs",   "Muon_PT_vs_Bs",
    "Kaon_PL_vs_Bs",   "Muon_PL_vs_Bs",
    "Kaon_PT_vs_Bdir", "Muon_PT_vs_Bdir"};
  std::vector<Bool_t>    uLog = { 0, 0, 0, 0, 0, 0, 0 };
  std::vector<Double_t>  uMIN = { 1., 500., 500., 1., 1., 1., 1. };
  std::vector<Double_t>  uMAX = { 1., 2700., 2700., 1., 1., 1., 1. };
  for ( auto i : UD ) {HM[i.c_str()] = NULL; HS[i.c_str()] = NULL;};

  if (sampleID) initSignalHistograms( UD );

  for (Int_t i = 0; i < UD.size(); i++)
  { 
    xmin = uMIN.at(i); xmax = uMAX.at(i);
    if (HS[UD.at(i).c_str()]) {
      xmin = HS[UD.at(i).c_str()]->GetXaxis()->GetXmin();
      xmax = HS[UD.at(i).c_str()]->GetXaxis()->GetXmax();
    };
    
    HM[UD.at(i).c_str()] = new TH1F(Form("H_%d_%s", sampleID, UD.at(i).c_str()),
                                    Form("%s;; entries",UD.at(i).c_str()), 50, xmin, xmax);
  };
  
  for (Int_t i = 0; i < UD.size(); i++) 
    yLog.insert_or_assign(UD.at(i).c_str(), uLog.at(i));
    
  return;
};

void cinematicStudy::initSignalHistograms( std::vector<std::string> U )
{
  fws = new TFile(Form("ws/cinematicStudy_Q%d_S%d%d%d_%s%s.root",
                       q2Flag, cutTrimmer, cutOffline, cutFit,
                       varName.Data(), (isTrimmer)?"TRIMMER":""));
  if (!fws) {std::cout<<"Oups! WS file doesn't exist\n"; return;};
    
  wspace = (RooWorkspace*) fws->Get("myWS") ;
  if (!wspace) {std::cout<<"Oups! wspace doesn't exist\n"; return;};

  for ( auto u : U ) 
    HS[u] = (TH1F *) wspace->genobj(Form("H_0_%s",u.c_str()));
  
  if (fws){fws->Close(); delete fws; fws = NULL;};
  
  return;
};

void cinematicStudy::initSignalHistograms( )
{
  fws = new TFile(Form("ws/cinematicStudy_Q%d_S%d%d%d_%s%s.root",
                       q2Flag, cutTrimmer, cutOffline, cutFit,
                       varName.Data(), (isTrimmer)?"TRIMMER":""));
  if (!fws) {std::cout<<"Oups! WS file doesn't exist\n"; return;};
    
  wspace = (RooWorkspace*) fws->Get("myWS") ;
  if (!wspace) {std::cout<<"Oups! wspace doesn't exist\n"; return;};

  for (const auto& [key, value] : HM) 
    HS[key] = (TH1F *) wspace->genobj(Form("H_0_%s",key.c_str()));
  
  if (fws){fws->Close(); delete fws; fws = NULL;};
  
  return;
};

Double_t cinematicStudy::KptVsBs(  )
{
  TVector3 K(DM["kaon_m_PX"], DM["kaon_m_PY"], DM["kaon_m_PZ"]);
  TVector3 B(DM["Bs_PX"],     DM["Bs_PY"],     DM["Bs_PZ"]);

  return (K.Mag()*TMath::Sin(K.Angle(B)));
};

Double_t cinematicStudy::KplVsBs(  )
{
  TVector3 K(DM["kaon_m_PX"], DM["kaon_m_PY"], DM["kaon_m_PZ"]);
  TVector3 B(DM["Bs_PX"],     DM["Bs_PY"],     DM["Bs_PZ"]);

  return (K.Mag()*TMath::Cos(K.Angle(B)));
};

Double_t cinematicStudy::KptVsBsDir(  )
{
  TVector3 SV(DM["Bs_ENDVERTEX_X"], DM["Bs_ENDVERTEX_Y"], DM["Bs_ENDVERTEX_Z"]);
  TVector3 PV(DM["Bs_OWNPV_X"],     DM["Bs_OWNPV_Y"],     DM["Bs_OWNPV_Z"]);
  TVector3 dV = SV-PV;
  
  TVector3 K(DM["kaon_m_PX"], DM["kaon_m_PY"], DM["kaon_m_PZ"]);

  return (K.Mag()*TMath::Sin(K.Angle(dV)));
};

Double_t cinematicStudy::MuptVsBs(  )
{
  TVector3 M(DM["muon_p_PX"], DM["muon_p_PY"], DM["muon_p_PZ"]);
  TVector3 B(DM["Bs_PX"],     DM["Bs_PY"],     DM["Bs_PZ"]);

  return (M.Mag()*TMath::Sin(M.Angle(B)));
};

Double_t cinematicStudy::MuplVsBs(  )
{
  TVector3 M(DM["muon_p_PX"], DM["muon_p_PY"], DM["muon_p_PZ"]);
  TVector3 B(DM["Bs_PX"],     DM["Bs_PY"],     DM["Bs_PZ"]);

  return (M.Mag()*TMath::Cos(M.Angle(B)));
};

Double_t cinematicStudy::MuptVsBsDir(  )
{
  TVector3 SV(DM["Bs_ENDVERTEX_X"], DM["Bs_ENDVERTEX_Y"], DM["Bs_ENDVERTEX_Z"]);
  TVector3 PV(DM["Bs_OWNPV_X"],     DM["Bs_OWNPV_Y"],     DM["Bs_OWNPV_Z"]);
  TVector3 dV = SV-PV;
  
  TVector3 M(DM["muon_p_PX"], DM["muon_p_PY"], DM["muon_p_PZ"]);

  return (M.Mag()*TMath::Sin(M.Angle(dV)));
};

void cinematicStudy::fillHistograms  ( BKGCAT b )
{
  if (!sampleID && (b != BKGGOOD)) return;
  
  if ( sampleID && (IM["Bs_BKGCAT"] != 110)) return;

  for ( auto i : FN ) HM[i.c_str()] -> Fill(FM[i.c_str()], 1.);  
  for ( auto i : DN ) HM[i.c_str()] -> Fill(DM[i.c_str()], 1.);  

  // User's variables
  HM["Kaon_PT_vs_Bs"] -> Fill(KptVsBs (  ), 1.);  
  HM["Muon_PT_vs_Bs"] -> Fill(MuptVsBs(  ), 1.);
  HM["Kaon_PL_vs_Bs"] -> Fill(KplVsBs (  ), 1.);  
  HM["Muon_PL_vs_Bs"] -> Fill(MuplVsBs(  ), 1.);
  HM["Bs_OWNPV_CHI2DOF"] -> Fill(DM["Bs_OWNPV_CHI2"]/IM["Bs_OWNPV_NDOF"], 1.);

  HM["Kaon_PT_vs_Bdir"] -> Fill(KptVsBsDir (  ), 1.);
  HM["Muon_PT_vs_Bdir"] -> Fill(MuptVsBsDir(  ), 1.);
  //  cout<<" X "<<*F3M["Bs_PVX"]<< " Y "<<*F3M["Bs_PVY"]<< " Z "<<*F3M["Bs_PVZ"]<<endl;
  return;
};

TH1F *cinematicStudy::GetSignalHist(string var)
{
  if (!fws)
    fws = new TFile(Form("ws/cinematicStudy_Q%d_S%d%d%d_%s%s.root",
                         q2Flag, cutTrimmer, cutOffline, cutFit,
                         varName.Data(), (isTrimmer)?"TRIMMER":""));
  if (!fws) {std::cout<<"Oups! WS file doesn't exist\n"; return NULL;};
    
  wspace = (RooWorkspace*) fws->Get("myWS") ;
  if (!wspace) {std::cout<<"Oups! wspace doesn't exist\n"; return NULL;};

  TH1F *histo = (TH1F *) wspace->genobj(Form("H_0_%s",var.c_str()));

  if (fws){fws->Close(); delete fws; fws = NULL;};
  
  return histo;
};

void cinematicStudy::drawSignal( string s )
{
  if (HS[s])
  {
    HS[s] -> SetMarkerStyle(20);
    HS[s] -> SetMarkerColor(kBlack);
    HS[s] -> SetLineColor(kBlack);
    HS[s] -> SetLineWidth(2);
    HS[s] -> Draw("SAME PE");
  };
  return;
};

void cinematicStudy::drawHistograms( Int_t i )
{
  // drawFullHistograms( i );
  drawUserHistograms( i );
  return;
};

void cinematicStudy::drawUserHistograms( Int_t i )
{
  Int_t dColor[9] = {
    kBlack, kRed+2, kBlue+2, kBlack, kGreen+2, kGreen+2, kOrange+1, kOrange+1, kOrange+1
  };  
      
  const Int_t raw = 3;
  const Int_t column = 3;
  const Int_t occupancy = raw*column;
  Int_t ihist = 0;
  Int_t ncanvas = 0;
  Int_t ipad = 1;
  TCanvas *cv = NULL;

  TLegend *slegend = new TLegend(0.45, 0.65, 0.85, 0.85);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.045);
  
  for ( auto key : DD )  
  {
    if ( !(ihist%occupancy) )
    {
      if (ncanvas) canvas[ncanvas-1] -> Update();
      ipad = 1; 
      cv = new TCanvas(Form("canvas%d_%s",ncanvas+1,sampleTag.Data()), sampleTag.Data(), 10, 10, 1100, 800);
      cv -> Divide(column,raw);
      canvas.push_back(cv);  
      ncanvas++;
    }
    canvas[ncanvas-1] -> cd(ipad++);
    if (!key.size()) {ihist++; continue;};
    
    if (yLog[key.c_str()]) gPad->SetLogy();
    HM[key.c_str()] -> SetLineColor(dColor[sampleID]);
    HM[key.c_str()] -> SetLineWidth(2);
    if (sampleID)
    {
      HM[key.c_str()] -> Sumw2();
      HM[key.c_str()] -> Scale(Double_t(HS[key.c_str()]->Integral())/HM[key.c_str()]->Integral());
      if (HM[key.c_str()] ->GetMaximum()<HS[key.c_str()]->GetMaximum())
        if (yLog[key.c_str()]) HM[key.c_str()] ->SetMaximum(HS[key.c_str()]->GetMaximum()*5.);
        else HM[key.c_str()] ->SetMaximum(HS[key.c_str()]->GetMaximum()*1.25);
    }
    HM[key.c_str()] -> Draw("HIST E");
    if (!ihist) slegend->AddEntry(HM[key.c_str()], sampleName.Data(), "LE");
    if (sampleID) {
      drawSignal(key.c_str());
      if (!ihist) slegend->AddEntry(HS[key.c_str()], samples[0].legend.Data(), "PE");
    };
    if (!(ihist%column)) slegend->Draw();
    ihist++;
    canvas[ncanvas-1] -> Update();
  };

  return;
};

void cinematicStudy::drawFullHistograms( Int_t i )
{
  Int_t dColor[9] = {
    kBlack, kRed+2, kBlue+2, kBlack, kGreen+2, kGreen+2, kOrange+1, kOrange+1, kOrange+1
  };  
      
  const Int_t raw = 3;
  const Int_t column = 3;
  const Int_t occupancy = raw*column;
  Int_t ihist = 0;
  Int_t ncanvas = 0;
  Int_t ipad = 1;
  TCanvas *cv = NULL;

  TLegend *slegend = new TLegend(0.5, 0.65, 0.85, 0.85);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.045);
  
  for (const auto& [key, value] : HM)
  {
    if ( !(ihist%occupancy) )
    {
      if (ncanvas) canvas[ncanvas-1] -> Update();
      ipad = 1; 
      cv = new TCanvas(Form("canvas%d_%s",ncanvas+1,sampleTag.Data()), sampleTag.Data(), 10, 10, 1100, 800);
      cv -> Divide(column,raw);
      canvas.push_back(cv);  
      ncanvas++;
    }
    canvas[ncanvas-1] -> cd(ipad++);
    if (yLog[key]) gPad->SetLogy();
    value -> SetLineColor(dColor[sampleID]);
    value -> SetLineWidth(2);
    if (sampleID)
    {
      value -> Sumw2();
      value -> Scale(Double_t(HS[key]->Integral())/value->Integral());
      if (value ->GetMaximum()<HS[key]->GetMaximum())
        if (yLog[key]) value ->SetMaximum(HS[key]->GetMaximum()*5.);
        else value ->SetMaximum(HS[key]->GetMaximum()*1.25);
    }
    value -> Draw("HIST E");
    if (!ihist) slegend->AddEntry(value, sampleName.Data(), "LE");
    if (sampleID) {
      drawSignal(key);
      if (!ihist) slegend->AddEntry(HS[key], samples[0].legend.Data(), "PE");
    };
    if (!(ihist%3)) slegend->Draw();
    ihist++;
    canvas[ncanvas-1] -> Update();
  };

  return;
};

void cinematicStudy::save( )
{
  auto i(0);
  for (auto ii : canvas)
    ii->SaveAs(Form("figures/cinematicStudy/cinematicStudy%d_%s_Q%d_S%s_%s%s.png", ++i,
                    sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                    varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/cinematicStudy_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (const auto& [key, value] : HM)
    wspace->import(*HM[key],kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
    
  if (fws) fws->Close();

};
