//-----------------------------------------------------------------------------
// Implementation file for class : sameSignData
//
// 2024-04-05 : Pascal Vincent
//-----------------------------------------------------------------------------

// Include files 



// local
#include "sameSignData.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
sameSignData::sameSignData(  ) : superBanner ( __BANNER_DATA__ )
{
  timer  = NULL;

  iColor = 0;
  firstBar = 0;
  firstBan = 0;
  
  tree  = new TChain("sameSignDataTree");

  createHistograms( );
}
//=============================================================================
// Destructor
//=============================================================================
sameSignData::~sameSignData()
{
  BM.clear();
  FM.clear();
  DM.clear();
} 

//=============================================================================

void sameSignData::linkTree(  )
{
  cout<<"|2016 MagUp   : \n| "<<dataSamples.Up.Data()<<endl; 
  tree->Add(dataSamples.Up.Data());
  cout<<"|2016 MagDown : \n| "<<dataSamples.Down.Data()<<endl;
  tree->Add(dataSamples.Down.Data());
  cout<<"|"<<endl;
  
  std::vector<std::string> BN = 
    {"B_L0MuonDecision_TOS", "B_Hlt2TopoMu2BodyDecision_TOS", "B_Hlt2SingleMuonDecision_TOS", "K_isMuon"};

  std::vector<std::string> FN = 
    {"B_ENDVERTEX_CHI2", "B_FD_S", "K_PAIR_M", "mu_PAIR_M", "TMVA_Charge_BDT_Run1", "TMVA_SS_Afc_BDT_Run1"};
  
  std::vector<std::string> DN = 
    {"B_MCORR", "B_MCORRERR", "B_M","B_Regression_Q2_BEST_LINEAR_Run2", "Mass_Kmu_as_mumu",
     "K_IsoMinBDT", "K_PX", "K_PY", "K_PZ", "mu_IsoMinBDT", "mu_PX", "mu_PY", "mu_PZ"};
  
  for (vector<string>::iterator i = BN.begin(); i != BN.end(); ++i) BM[i->c_str()] = 0;
  for (vector<string>::iterator i = FN.begin(); i != FN.end(); ++i) FM[i->c_str()] = 0;
  for (vector<string>::iterator i = DN.begin(); i != DN.end(); ++i) DM[i->c_str()] = 0;

  tree->SetBranchStatus ( "*" , 0 );

  attachBranch( BM );
  attachBranch( FM );
  attachBranch( DM );
}

template<typename T>
void sameSignData::attachBranch( T &b ) 
{
  for (const auto& [key, value] : b)
  {
    tree->SetBranchStatus (key.c_str() , 1 );
    tree->SetBranchAddress(key.c_str(), &b[key.c_str()]);
    if (!&b[key.c_str()]) std::cout<<"\n\nW A R N : N I L L   P O I N T E R   4  - "<<key.c_str()<<"\n\n";
  };
};

Bool_t sameSignData::eventSelection(  )
{
  if (cutTrimmer)
  {
    if ( DM["B_MCORR"] < massRange[0] ) return(kFALSE);
    if ( DM["B_MCORR"] > massRange[1] ) return(kFALSE);
    if (!BM["B_L0MuonDecision_TOS"] )   return(kFALSE);
    if (!BM["B_Hlt2TopoMu2BodyDecision_TOS"] && !BM["B_Hlt2SingleMuonDecision_TOS"]) return(kFALSE);
    if ( FM["B_ENDVERTEX_CHI2"] >= 4 )  return(kFALSE);
    if (TMath::Min(DM["K_IsoMinBDT"],DM["mu_IsoMinBDT"]) <= -0.9) return(kFALSE);
  };

  if (cutOffline)
  {
    if ((DM["Mass_Kmu_as_mumu"] > 3072) && (DM["Mass_Kmu_as_mumu"] < 3130)) return(kFALSE);
    if ( BM["K_isMuon"] ) return(kFALSE);
    if ((FM["K_PAIR_M"]-892)  <= 60 ) return(kFALSE);
    if ((FM["K_PAIR_M"]-1430) <= 90 ) return(kFALSE);
    if ((3072 < FM["mu_PAIR_M"] ) && (FM["mu_PAIR_M"] < 3130)) return(kFALSE);
  };

  if (cutFit)
  {
    if (((DM["K_PX"] * DM["mu_PX"]) < 0. ) &&
        ((DM["K_PY"] * DM["mu_PY"]) < 0. )) return(kFALSE);
    if ( FM["B_FD_S"]  <= 40 ) return(kFALSE);
    //    if ( DM["Bs_M"] >= 5175. ) return(kFALSE);
    // if ( DM["Bs_MCORRERR"] >= 100. ) return(kFALSE);
    if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] <= 0 ) return(kFALSE);
    if ( FM["TMVA_Charge_BDT_Run1"] <= 0.05 ) return(kFALSE);
    // if ( FM["TMVA_SS_Afc_BDT_Run1"] <= 0 ) return(kFALSE);
  };

  return(kTRUE);
};

void sameSignData::eventLoop( ) 
{
  if (!nTuplEntries) nTuplEntries = tree -> GetEntries();

  TThread *t1;
  TThread *t2;
  
  if (banner) {t1= new TThread("barAndBanner", barAndBanner, this); t1->Run();};
  
  t2 = new TThread("threadTree",   threadTree,   this); t2->Run();

  t2 ->Join();
  
  if (banner) endBar(  );

  return;
};


void sameSignData::readTree( ) 
{
  static Int_t cthread = 0;
  static Bool_t first = kTRUE;
  /*

  mutex m;
  if (first)
  {
    m.lock();
    Int_t i = 0;
    for (i = 0; i < 3; i++) if ( !iThred[0] ) break;
    cthread = i;
    ithrad[i] = i;
    first = kFALSE;
    m.unlock();
  };
  */
  
  /*********************************************************************************************
   *
   * L O O P   O N   E V E N T S
   *
   *****/

  for(iTuplEntry = 0; iTuplEntry < nTuplEntries; ++iTuplEntry)
  {
    tree->GetEntry(iTuplEntry);

    if (!eventSelection(  )) continue;

    nSelEntries++;
      
    fillHistograms( );
  
  };

  if (timer)
  {
    timer -> TurnOff();
  } else if (banner) cout<<"WARNING : NILL TTimer\n";

  return;
};

#include <chrono>
#include <thread>

void sameSignData::end( )
{
  if(banner && fireworks) {thread = new TThread("fireThread", fireThread, this); thread->Run();};
  
  eventSummary( );

  drawHistograms( 0x7 );

  if ( banner && fireworks && thread ) thread ->Join();

  if ( saveWork ) save( );
};

/*********************************************************************************************
 *
 * H I S T O G R A M I N G
 *
 *****/

void sameSignData::createHistograms( )
{
  TString hname[3] = { "ssignFullQ2Region", "ssignLowerQ2Region", "ssignUpperQ2Region" };
  Int_t icolor[3] = { kBlack, kGreen+2, kBlue+2 };

  TH1F * dummy;
  for ( auto i = 0; i < 3 ; i++ )
  {
    dummy = (TH1F *) gROOT -> FindObject(hname[i]);
    if (dummy) delete dummy;
  
    histo[i] =new TH1F(hname[i],hname[i], 30, 2500., 7000.);
    histo[i]->SetMarkerStyle(20);
    histo[i]->SetMarkerColor(icolor[i]);
    histo[i]->SetFillStyle(1001);
    histo[i]->SetLineStyle(0);
    histo[i]->SetLineColor(icolor[i]);
    histo[i]->SetLineWidth(2);
  };
  
  return;
};

void sameSignData::fillHistograms(  )
{
  histo[0] -> Fill( DM["B_MCORR"], 1. );
 
  if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] <  cutQ2Val ) histo[1] -> Fill( DM["B_MCORR"], 1. );

  if ( DM["B_Regression_Q2_BEST_LINEAR_Run2"] >= cutQ2Val ) histo[2] -> Fill( DM["B_MCORR"], 1. );

  return;
};

void sameSignData::drawHistograms( Int_t )
{
  canvas = new TCanvas("sameSignDataCanvas", "sameSignDataCanvas", 10, 10, 1100, 800);
  /*
  histo ->SetMarkerStyle(20);
  histo ->SetMarkerColor(kBlack);
  histo ->SetLineColor(kBlack);
  histo ->SetTitle(Form("Same sign events;%s (MeV/c^{2});Entries",varName.Data())); 
  histo ->Draw("EP");
  */
  
  canvas ->Draw();
  canvas ->cd();
  TPad *p1 = new TPad("p1","p1",0.5,0.5,1.0,0.95); p1->Draw(); p1->cd(); 
  histo[1]->SetMaximum(histo[1]->GetMaximum()*1.35);
  histo[1]->SetTitle(Form("Low q^{2} region;%s (MeV/c^{2});Entries",varName.Data())); 
  histo[1]->Draw("EP");

  canvas ->cd();
  TPad *p2 = new TPad("p2","p2",0.5,0.05,1.0,0.5); p2->Draw(); p2->cd();
  histo[2]->SetMaximum(histo[2]->GetMaximum()*1.35);
  histo[2]->SetTitle(Form("High q^{2} region;%s (MeV/c^{2});Entries",varName.Data())); 
  histo[2]->Draw("EP");

  canvas ->cd();
  TPad *p = new TPad("p","p",0.01,0.0,0.5,1.0); p->Draw(); p->cd();
  THStack *stack = new THStack("mixed_HStack","mixed_HStack");
  stack->Add( histo[1],"HIST E" );
  stack->Add( histo[2],"HIST E" );

  stack->SetTitle(Form("#color[2]{Same sign events};%s (MeV/c^{2});Entries",varName.Data()));
  stack->SetMaximum(stack->GetMaximum()*1.25);
  stack->Draw("pfc");

  TLegend *slegend = new TLegend(0.55, 0.5, 0.85, 0.6);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.03);
  slegend->AddEntry(histo[0], "All selected events", "pe");
  slegend->AddEntry(histo[1], "Low q^{2} region", "F");
  slegend->AddEntry(histo[2], "High q^{2} region", "F");

  slegend->Draw();

  TPaveText* sampleText = new TPaveText(0.15, 0.75, 0.40, 0.85, "BRNDC");
  sampleText->SetFillColor(0);
  sampleText->SetTextAlign(12);
  sampleText->SetBorderSize(0);
  sampleText->SetLineWidth(0);
  sampleText->SetTextSize(0.025);
  sampleText->SetFillStyle(0);
  sampleText->AddText("2016 data");
  sampleText->AddText("Magnet up and down");
  sampleText->AddText(Form("Selection: TRIMMER=%d, OffLine=%d, Fit cuts=%d", cutTrimmer, cutOffline, cutFit));
  sampleText->AddText("No cut on Q2");
  sampleText->Draw();
  
  drawLHCbLogo();
  
  canvas ->Update();
  return;
};

TH1F *sameSignData::drawH( Double_t norm )
{
  static Int_t i = 0;
  
  TH1F *clone = (TH1F *)histo[q2Flag]->Clone(Form("%s_c%d", histo[q2Flag]->GetName(),++i));
  
  clone->Sumw2();

  clone->Scale(norm/histo[q2Flag]->Integral());

  clone->Draw( "SAME PE" );

  return(clone);
};

void sameSignData::save( )
{
  canvas->SaveAs(Form("figures/data/%s_Q%d_S%s_%s%s.png",
                      canvas->GetName(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/sameSignEvent_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (auto i = 0; i < 3; i++)
    wspace->import(*histo[i], kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
  
  if (fws) fws->Close();
  
  return;
};
