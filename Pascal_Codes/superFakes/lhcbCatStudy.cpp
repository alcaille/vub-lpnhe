// Include files 



// local
#include "lhcbCatStudy.h"

//-----------------------------------------------------------------------------
// Implementation file for class : lhcbCatStudy
//
// 2024-04-03 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
lhcbCatStudy::lhcbCatStudy( ) : superFake ( )
{
};

lhcbCatStudy::lhcbCatStudy( TChain *t ) : superFake ( t )
{
  for (auto &c : CGood.canvas ) c = NULL;
  for (auto &c : CFake.canvas ) c = NULL;
  for (auto &c : CComb.canvas ) c = NULL;

  CGood.SM = NULL;
  CFake.SM = NULL;
  CComb.SM = NULL;

  for (auto cat : LHCbBkgCat)
  {
    CDESC[cat.sID.c_str()] = cat.description.c_str();
    CCOND[cat.sID.c_str()] = cat.condition.c_str();
  };

}
//=============================================================================
// Destructor
//=============================================================================
lhcbCatStudy::~lhcbCatStudy() {} 

//=============================================================================

void lhcbCatStudy::createHistograms ( )
{
  ME.SetMassRange(massRange);
  ME.SetQ2ValCut(q2Flag);
  ME.SetVarName(varName.Data());
  ME.SetIsTrimmer(isTrimmer);

  ME.SetCutTrimmer( cutTrimmer );
  ME.SetCutOffline( cutOffline );
  ME.SetCutFit    ( cutFit     );
  ME.SetCutQ2Val  ( cutQ2Val   );

  ME.SetBanner    ( banner );
};
  
void lhcbCatStudy::fillHistograms( BKGCAT fillCatMap )
{
  switch(fillCatMap)
  {
  case BKGGOOD:
    CGood.nTot++;
    CGood.PM[Form("%d",IM["Bs_BKGCAT"])]++;
    if (auto search = CGood.HM.find(Form("%d",IM["Bs_BKGCAT"])); search == CGood.HM.end())
      CGood.HM[Form("%d",IM["Bs_BKGCAT"])] =
        new TH1F(Form("H_lhcbCatStudy_Good_%s_%s_%s%s",Form("%d",IM["Bs_BKGCAT"]),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    CGood.HM[Form("%d",IM["Bs_BKGCAT"])]->Fill(DM[varName.Data()],1);
    CGood.cID = fillCatMap;
    break;
  case BKGFAKE:
    CFake.nTot++;
    CFake.PM[Form("%d",IM["Bs_BKGCAT"])]++;
    if (auto search = CFake.HM.find(Form("%d",IM["Bs_BKGCAT"])); search == CFake.HM.end())
      CFake.HM[Form("%d",IM["Bs_BKGCAT"])] =
        new TH1F(Form("H_lhcbCatStudy_Fake_%s_%s_%s%s",Form("%d",IM["Bs_BKGCAT"]),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    CFake.HM[Form("%d",IM["Bs_BKGCAT"])]->Fill(DM[varName.Data()],1);
    CFake.cID = fillCatMap;
    break;
  case BKGSUPFAKE:
    CComb.nTot++;
    CComb.PM[Form("%d",IM["Bs_BKGCAT"])]++;
    if (auto search = CComb.HM.find(Form("%d",IM["Bs_BKGCAT"])); search == CComb.HM.end())
      CComb.HM[Form("%d",IM["Bs_BKGCAT"])] =
        new TH1F(Form("H_lhcbCatStudy_Comb_%s_%s_%s%s",Form("%d",IM["Bs_BKGCAT"]),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    CComb.HM[Form("%d",IM["Bs_BKGCAT"])]->Fill(DM[varName.Data()],1);
    CComb.cID = fillCatMap;
    break;
  default:
    break;
  };
  
  return;
}


void lhcbCatStudy::drawHistograms( Int_t whichOne )
{
  if (whichOne & 0x1 ) drawHistogram( "Good", CGood );
  if (whichOne & 0x2 ) drawHistogram( "Fake", CFake );
  if (whichOne & 0x4 ) drawHistogram( "Comb", CComb );

  //  drawGraphs( whichOne );
}

void lhcbCatStudy::drawHistogram( const char * s, Mycategories &cat )
{
  const Double_t statCut = 0.01;
  
  const Int_t nCol = 16;
  Int_t palette[nCol] = { 2, 3, 4, 6, 7, 9, 20, 25, 28, 30, 33, 38, 40, 41, 46, 49};
  
  gStyle->SetPalette(kOcean);

  cat.canvas[0] = new TCanvas(Form("%s_Hist_%s",s,sampleTag.Data()),
                              Form("HH_%s_%s",s,sampleTag.Data()), 10, 10, 1100, 800);
  cat.canvas[0]->Divide(2,1);
  cat.canvas[0]->cd(1);

  auto maxHist (0);
  auto nLines  (0);
  
  for (const auto& [key, value] : cat.PM) 
  {
    maxHist = (maxHist<cat.HM[key]->GetMaximum())?cat.HM[key]->GetMaximum():maxHist;
    if (Double_t(value)/cat.nTot < statCut) continue;
    nLines++;
  };

  TPaveText* sampleText = new TPaveText(0.15, 0.75, 0.5, 0.85, "BRNDC");
  sampleText->SetFillColor(0);
  sampleText->SetTextAlign(12);
  sampleText->SetBorderSize(0);
  sampleText->SetLineWidth(0);
  sampleText->SetTextSize(0.04);
  sampleText->SetFillStyle(0);
  sampleText->AddText(catNames[cat.cID-1].Data());
  if (q2Flag == 1) sampleText->AddText(Form("Q2 #leq %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else if (q2Flag == 2) sampleText->AddText(Form("Q2 > %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else sampleText->AddText("No cut on Q2");

  /********************************************************************
   *
   * D R A W   S T A C K
   *
   **********/

  TLegend *slegend = new TLegend(0.15, 0.7-0.04*nLines, 0.45, 0.7);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.03);

  cat.SM = new THStack(Form("HStack_%s",sampleTag.Data()),Form("%s",sampleName.Data()));

  for (const auto& [key, value] : cat.PM) 
  {
    if (Double_t(value)/cat.nTot < statCut) continue; 
    cat.HM[key]->SetLineStyle(1);
    cat.HM[key]->SetLineColor(1);
    cat.HM[key]->SetFillStyle(1001);
    cat.SM->Add(cat.HM[key],"HIST E");
    slegend->AddEntry(cat.HM[key], CDESC[key].c_str(), "F");
  };

  cat.SM->SetMaximum(cat.SM->GetMaximum()*1.35);
  cat.SM->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varName.Data())); 
  cat.SM->Draw("pfc");

  slegend->Draw();

  sampleText->Draw();

  drawLHCbLogo();
  
  cat.canvas[0]->Update();

  /********************************************************************
   *
   * D R A W   H I S T O G R A M S
   *
   **********/

  cat.canvas[0]->cd(2);
  if (logscale) gPad->SetLogy();
  
  auto iLine(1);
  auto iCol (0);
  auto done (0);

  TString drawOpt = "HIST E";

  TLegend *hlegend = new TLegend(0.15, 0.7-0.035*(nLines+2), 0.45, 0.7);
  hlegend->SetLineWidth(0);
  hlegend->SetFillStyle(0);
  hlegend->SetTextFont(62);
  hlegend->SetTextSize(0.03);

  for (const auto& [key, value] : cat.PM)
  {
    if (Double_t(value)/cat.nTot < statCut) continue;
    cat.HM[key]->SetMaximum(maxHist*3.);

    cat.HM[key]->SetLineWidth(2);
    cat.HM[key]->SetLineColor(palette[iCol]);
    cat.HM[key]->SetLineStyle(iLine);
    cat.HM[key]->SetFillStyle(0);
    cat.HM[key]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varName.Data())); 

    cat.HM[key]->Draw(drawOpt.Data());
    hlegend->AddEntry(cat.HM[key], CDESC[key].c_str(), "L");
    if (!done) drawOpt.Append(" SAME "); done ++;

    if ( ++iCol>=nCol ){
      iCol = 0;
      iLine ++;
      if ( iLine>8 ) iLine = 1;
    };
  };

  if (auto search = cat.HM.find("110"); search != cat.HM.end()) {
    hlegend->AddEntry(ME.drawH(cat.HM["110"]->Integral()), "Data : mixed events", "LEP");
    hlegend->AddEntry((TObject*)0, Form("scaled to %s",CDESC["110"].c_str()), "");
  };
  
  hlegend->Draw();

  sampleText->Draw();

  drawLHCbLogo();
  
  cat.canvas[0]->Update();
}

void lhcbCatStudy::save( )
{
  Int_t i = 0;

  for (auto &ii : CGood.canvas)
    if (ii)
      ii->SaveAs(Form("figures/lhcbCatStudy/lhcbCatStudy%d_%s_Q%d_S%s_%s_%s.png", ++i,
                      ii->GetName(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));
  i = 0;

  for (auto &ii : CFake.canvas)
    if (ii)
      ii->SaveAs(Form("figures/lhcbCatStudy/lhcbCatStudy%d_%s_Q%d_S%s_%s%s.png", ++i,
                      ii->GetName(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));
  i = 0;
  for (auto &ii : CComb.canvas)
    if (ii)
      ii->SaveAs(Form("figures/lhcbCatStudy/lhcbCatStudy%d_%s_Q%d_S%s_%s_%s.png", ++i,
                      ii->GetName(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/lhcbCatStudy_Q%d_S%s_%s_%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (const auto& [key, value] : CGood.HM)
    wspace->import(*CGood.HM[key],kTRUE);

  for (const auto& [key, value] : CFake.HM)
    wspace->import(*CFake.HM[key],kTRUE);

  for (const auto& [key, value] : CComb.HM)
    wspace->import(*CComb.HM[key],kTRUE);

  TH1F *dummy = ME.GetHistogram();
  dummy->SetName(Form("H_lhcbCatStudy_mixedEventsData"));
  wspace->import(*dummy,kTRUE);
        
  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
    
  if (fws) fws->Close();
  
  return;
};
