/** @class superFake superFake.h superFakes/superFake.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-05
 */

#ifndef SUPERFAKES_SUPERFAKE_H 
#define SUPERFAKES_SUPERFAKE_H 1

// Include files

#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <deque>
#include <unordered_map>
#include <vector>
#include <mutex>
#include <thread>
#include <chrono>

#include <TROOT.h>
#include <TRint.h>
#include <TObject.h>
#include <TTimer.h>
#include <TThread.h>
#include <TDatime.h>
#include <TMutex.h>
#include <TTree.h>
#include <TExec.h>
#include <TChain.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TDatabasePDG.h>
// Local
#include "superStudy.h"
#include "superBanner.h"

#define DUMP_INFO 0x0
#define TRIMMER_CUT kTRUE
#define OFFLINE_CUT kTRUE
#define FIT_CUT kFALSE

// use this order for safety on library loading
using namespace std;

class superFake :
  public superBanner,
  public fireWorks
{
public: 
  /// Standard constructor
  superFake( ){}; 
  superFake( TChain * ); 

  virtual ~superFake( ); ///< Destructor

  void linkTree( ); 
  static void *threadTree( void * ptr ){
    superFake *s = (superFake *)ptr;
    s->readTree( );
    return NULL;
  };
  void readTree( ); 
  template<typename T>
  void attachBranch( T & );
  
  BKGCAT selectBs2Kstrmunu( Int_t );
  BKGCAT selectBs2Jpsiphi ( );
  BKGCAT selectB2ccbarKX  ( );
  BKGCAT selectb2cKmunuX  ( );
  BKGCAT selectBs2Kmunu   ( );
  BKGCAT selectB2JpsiK    ( );

  Bool_t isHadron( Int_t , Int_t );
  Bool_t isBaryon( Int_t , Int_t );
  Bool_t isMeson ( Int_t , Int_t );

  void SetBM ( std::string );  
  void SetIM ( std::string );  
  void SetFM ( std::string );  
  void SetF3M( std::string );  
  void SetDM ( std::string );
  void SetVarName   ( const char *s ){varName.Append(s);};
  void SetSampleName( const char *s ){sampleName.Append(s);};
  void SetSampleTag ( const char *s ){sampleTag.Append(s);};
  void SetSampleID  ( Int_t i ){sampleID = i;};
  void SetWhat2Plot ( Int_t i ){what2plot = i;};
  void SetIsTrimmer ( Bool_t i ){isTrimmer = i;}; 
  void SetBanner    ( Bool_t i ){banner = i;}; 
  void SetMassRange ( Float_t *m ){massRange[0] = m[0]; massRange[1] = m[1];};
  void SetQ2ValCut  ( Int_t q ){q2Flag = q;};
  void SetVerbosity ( Int_t v ){Verbose = v;};
  void SetCutTrimmer(Bool_t c){cutTrimmer = c;};
  void SetCutOffline(Bool_t c){cutOffline = c;};
  void SetCutFit    (Bool_t c){cutFit = c;};
  void SetLogScale  (Bool_t l){logscale = l;};
  void SetSave      (Bool_t s){saveWork = s;};
  void SetSaveMacros(Bool_t s){saveMacros = s;};

  Int_t    GetIM(const char * s) {return IM[s];};
  Double_t GetDM(const char * s) {return DM[s];};
  TString  GetVarName       ( ){return varName;};
  TString  GetSampleName    ( ){return sampleName;};
  TString  GetSampleTag     ( ){return sampleTag;};
  Int_t    GetSampleID      ( ){return sampleID;};
  Bool_t   GetTrimmerStatus ( ){return isTrimmer;};
  Float_t  GetMiniMass      ( ){return massRange[0];};
  Float_t  GetMaxiMass      ( ){return massRange[1];};
  void     GetMassRange     ( Float_t *m ){m[0] = massRange[0]; m[1] = massRange[1];};
  Int_t    GetWhat2Plot     ( ){return what2plot;};
  Int_t    GetQ2ValCut      ( ){return q2Flag;};

  Bool_t eventSelection( );
  Bool_t eventSelectionNotTrim( );
  Bool_t eventSelectionTrimmer( );

  virtual void createHistograms( ){return;};
  virtual void drawHistograms( Int_t i )  {return;};
  virtual void fillHistograms( BKGCAT b ){b = BKGUNUSED; std::cout<<" WARNING : not OverRided"<<std::endl;return;};
  virtual void eventSummary( ) {return;};
  virtual void save()          {return;};
  void end();
  
  void eventLoop( );

  template<typename T>
  void  dumpMap   ( T );
  void  dumpMaps  (   );
  void  dumpMap   ( TH1Map );
  void  dumpMap   ( F3Map );
  void  eventDump ( TString );
  
protected:
  Int_t   verbose = 0;  

  TString varName;
  TString sampleName;
  TString sampleTag;
  Int_t   sampleID;
  Bool_t  logscale = kFALSE;
  Bool_t  isTrimmer;
  Bool_t  banner;
  Int_t   what2plot;
  Int_t   q2Flag;
  Float_t massRange[2];
  Bool_t  saveWork   = kFALSE;
  Bool_t  saveMacros = kFALSE;
  
  BoolMap   BM;
  DoubleMap DM; 
  FloatMap  FM; 
  IntMap    IM;
  F3Map     F3M;

  Bool_t cutTrimmer;
  Bool_t cutOffline;
  Bool_t cutFit;

  Int_t Verbose = 0;

private:
  TChain *tree;
  TThread *thread;
};
#endif // SUPERFAKES_SUPERFAKE_H
