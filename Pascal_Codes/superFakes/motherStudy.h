#ifndef SUPERFAKES_MOTHERSTUDY_H 
#define SUPERFAKES_MOTHERSTUDY_H 1
/** @class motherStudy motherStudy.h superFakes/motherStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-24
 */
// Include files

// Local
#include "superFake.h"

class motherStudy : public superFake
{
public: 
  /// Standard constructor
  motherStudy( ){};
  motherStudy( TChain * );

  virtual ~motherStudy( ); ///< Destructor

  void fillHistograms  ( BKGCAT ) override;
  void drawHistograms  ( Int_t ) override;
  void eventSummary    ( ) override;
  void save            ( ) override;

  void drawHistogram   ( const char *, Mycategories & );
  void drawGraphs      ( Int_t );
  void drawGraph       ( const char *, Mycategories & );

  void SetVerbose( Int_t v){verbose = v;};
protected:

private:
  Mycategories CGood, CFake, CComb;

  Double_t cutQ2Val   = __Q2_CUT_VALUE__;
};
#endif // SUPERFAKES_MOTHERSTUDY_H
