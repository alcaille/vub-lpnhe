// Include files 



// local
#include "superBanner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : superBanner
//
// 2024-04-22 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
superBanner::superBanner(  ) {

}

superBanner::superBanner( Int_t t ) {
  bannerType = t;
}

//=============================================================================
// Destructor
//=============================================================================
superBanner::~superBanner() {} 

//=============================================================================
void spark(Int_t w)
{
  switch(w) 
  {
  case 0:
    std::cout << "\e[38;05;1m\e[1m"<<"\033[D"<<"\033[D"<<"\033[D"<<"\033[A"<<"\\"<<"\033[C"<<"\033[C"<<"\033[B";
    break;
  case 1:
    std::cout << "\e[38;05;1m\e[1m"<<"\033[D"<<"\033[A"<<"/" <<"\033[C"<<"\033[B";
    break;
  case 2:
    std::cout << "\e[38;05;1m\e[1m"<<"\033[D"<<"\033[D"<<"\033[D"<<"-"<<"\033[C"<<"\033[C";
    break;
  default:
    break;
  };
  
  return;
};

void superBanner::drawBar(  )
{
  static Int_t iCol = 41; 

  if (!firstBar) {
    timer = new TTimer((TObject *)this, 10);
    timer -> Start(10, kFALSE);
    firstBar++;
  };

  if (iTuplEntry >= nTuplEntries) return;

  iCol = drawBanner( iCol );
  
  mutex m;
  m.lock();

  std::cout << Form("\e[38;05;%dm\e[1m|",iCol)<< std::string(83 * iTuplEntry/nTuplEntries , ' ');
  std::cout << "\e[38;05;1m\e[1m"<<"*=";
  //
  //  Int_t where = ( Int_t ) uniform(0., 3.); spark(where);
  std::cout << "\e[0m"
            << std::string(83 - 83 * iTuplEntry/nTuplEntries -1, '=')<< "] "
            << 100 * iTuplEntry/nTuplEntries << "%" << std::endl;
  std::cout << "|Processing Event: "<< iTuplEntry << " Of: " << nTuplEntries
            << " - Selected events "<< nSelEntries <<Form(" (%.1f%%)",(1000 * nSelEntries/nTuplEntries)/10.)
            << std::string(20 , ' ')<< std::endl;
  std::cout << std::flush;
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  m.unlock();

  iColor = iCol;
  
  return;
};

void superBanner::endBar(  )
{
  selectBanner( NULL );
  
  std::cout << Form("\e[38;05;%dm\e[1m|",iColor)
            << std::string(84, '-')<< "\e[0m"<< "] "<< 100 << "%" << std::endl;
  std::cout << "|Processing Event: "<< iTuplEntry << " Of: " << nTuplEntries
            << " - Selected events "<< nSelEntries <<Form(" (%.1f%%)",(1000 * nSelEntries/nTuplEntries)/10.)
            << std::endl;
  std::cout << std::flush;

  return;
};

void superBanner::selectBanner( Int_t *col ) 
{
  static Float_t speed = 1.;
  Double_t counter = Double_t(iTuplEntry)/Double_t(nTuplEntries);

  if (!col) 
  {
    std::cout <<"\r\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A";

    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(|                 __         __                                                     )"<<endl;   
    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(|                (__)       (__)                                                    )"<<endl;  
    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(|  ______ __   __ __ ____    __ ____   ______ __   __ ____    ____ ______  ____     )"<<endl;
    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(| (_    _)  |_|  |  (   _)  |  (   _) (_    _)  |_|  |   _)  |   _)  '_  \|  _ \    )"<<endl;
    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(|   |  | |   _   |  |\  \   |  |\  \    |  | |   _   |  __)  |  __)  | |  | |_) |   )"<<endl;
    cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(|   |__| |__| |__|__(____)  |__(____)   |__| |__| |__|____)  |____)__| |__|____/    )"<<endl;
    return;
  };
  
  if ((counter <= 0.01) ||
      ((counter >= 0.20) && (counter <= 0.25)) ||
      ((counter >= 0.40) && (counter <= 0.45)) ||
      ((counter >= 0.60) && (counter <= 0.65)) ||
      ((counter >= 0.80) && (counter <= 0.85)) )
  {
    speed = 1.;
    if (bannerType == __BANNER_SIMU__)
    {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|                                                 __                                    )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|                                                (__)                                   )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|  __ __ __   __ __ ____    _____  __ ____   ____ _____ ___ ____  __   __               )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(| |  '__)  | |  |  '_   \  /  _  \|  '_   \ (   _)  |  '_  ` _   \  | |  |              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(| |  |  |  |_|  |  | |  | |  (_)  |  | |  |  \  \|  |  | |  | |  |  |_|  |              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(| |__|   \_____/|__| |__|  \_____/|__| |__| (____)__|__| |__| |__|\_____/               )"<<endl;
    } else {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|                                                                                    )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|                                                                                    )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|  __ __ __   __ __ ____    _____  __ ____   ____  ______  ______ ______             )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(| |  '__)  | |  |  '_   \  /  _  \|  '_   \ |  _ \/   _   (_    _)   _   \           )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(| |  |  |  |_|  |  | |  | |  (_)  |  | |  | | |_) |  (_)  | |  | |  (_)  |           )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(| |__|   \_____/|__| |__|  \_____/|__| |__| |____/|__| |__| |__| |__| |__|           )"<<endl;
    };
  } else {
    if (speed <= 2.) {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|       _                             _                             _                )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|      |"|                           |"|                           |"|               )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|     /   \            _            /   \             _           /   \              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(|     |   |      \____|o|____/      |   |       \____|o|____/     |   |              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(|     '| |'          __ __          '| |'           __ __         '| |'              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(|     _| |_         _\   /_         _| |_          _\   /_        _| |_              )"<<endl;
    } else if ((speed <= 3.) || (speed > 6.)) {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|       _                             _                             _                )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|      |"|             _             |"|              _            |"|               )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|     /   \          _|o|_          /   \           _|o|_         /   \              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(|    /     \        /     \        /     \         /     \       /     \             )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(|   '  | |  '     _/ /   \ \_     '  | |  '      _/ /   \ \_    '  | |  '            )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(|     _| |_         _\   /_         _| |_          _\   /_        _| |_              )"<<endl;
    } else if ((speed <= 4.) || (speed > 5.)) {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|                      _                              _                              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|       _             |o|             _              |o|            _                )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|     _|"|_          /   \          _|"|_           /   \         _|"|_              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(|    /     \        /     \        /     \         /     \       /     \             )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(|  _/ /   \ \_     '  | |  '     _/ /   \ \_      '  | |  '    _/ /   \ \_           )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(|    _\   /_         _| |_         _\   /_          _| |_        _\   /_             )"<<endl;
    } else if (speed <= 5.) {
      cout<< Form("\e[38;05;%dm\e[1m",col[0])<<R"(|                      _                              _                              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[1])<<R"(|                     |o|                            |o|                             )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[2])<<R"(|       _            /   \            _             /   \           _                )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[3])<<R"(| \____|"|____/      |   |      \____|"|____/       |   |     \____|"|____/          )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[4])<<R"(|     __ __          '| |'          __ __           '| |'         __ __              )"<<endl; 
      cout<< Form("\e[38;05;%dm\e[1m",col[5])<<R"(|    _\   /_         _| |_         _\   /_          _| |_        _\   /_             )"<<endl;
    };
    if (speed >= 6.9) speed = 0.9;
    speed+=0.2;
  };
};
  

Int_t superBanner::drawBanner( Int_t iCol )
{
  static Int_t col[6] = {41, 41, 41, 41, 41, 41};
  static Int_t newC  =  0;
  static Int_t iline = -1;
  
  mutex m;
  m.lock();

  if (!firstBan) {std::cout <<"\n\n\n\n\n\n\n\n"; firstBan++;};

  std::cout <<"\r\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A\033[1A";

  selectBanner( col );
  
  cout<< "\e[0m";
  m.unlock();
    
  iline++; 
  
  if (iline == 6) {iline = 0; iCol = newC; newC++; }
  
  col[iline] = newC;
  
  return (iCol);
};

