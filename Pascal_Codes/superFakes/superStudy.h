#ifndef SUPERFAKES_SCANHISTORY_H 
#define SUPERFAKES_SCANHISTORY_H 1
// Include files

#include <stdlib.h>
#include <cstdlib>
#include <string>
#include <iostream>

#include <TROOT.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TPaveText.h>
#include <TFitResult.h>
#include <THStack.h>

#include "RooWorkspace.h"

/**
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-08
 */
#define __Q2_CUT_VALUE__ 7E+06
#define __NBINS__ 30

extern void drawLHCbLogo(Float_t x1 = 0.75, Float_t y1 = 0.82, Float_t x2 = 0.89, Float_t y2 = 0.89);

typedef std::unordered_map<std::string, Int_t >   IntMap;  
typedef std::unordered_map<std::string, Double_t> DoubleMap;
typedef std::unordered_map<std::string, Float_t>  FloatMap;
typedef std::unordered_map<std::string, Bool_t>   BoolMap;
typedef std::unordered_map<std::string, Int_t>    PDGMap;
typedef std::unordered_map<std::string, TH1F *>   TH1Map;
typedef std::unordered_map<std::string, TH2F *>   TH2Map;
typedef std::unordered_map<std::string, std::string> StringMap;
typedef std::unordered_map<std::string, Float_t [3]> F3Map;

enum BKGCAT { BKGUNKNOWN = -1, BKGUNUSED, BKGGOOD, BKGFAKE, BKGSUPFAKE };
enum SAMPLES {
  BS2KMUNU______ = 0,
  B2CKMUNUX_____, B2CCBARKX_____,
  BS2DSMUNUX____, 
  B2JPSIK_______, BS2JPSIPHI____,
  BS2KSTRMUNU___, BS2K1430MUNU__, BS2K2MUNU_____,
  LAST_SAMPLES__
};

enum SAMPLES_TRIMMER {
  BS2KMUNU_____T = LAST_SAMPLES__+1,
  B2CMUX_______T, B2CCBARKX____T, 
  B2JPSIK______T, BD2JPSIKSTR__T, BU2JPSIKSTR__T,
  BS2KSTRMUNU__T, BS2K1430MUNU_T, BS2K2MUNU____T, B2KPI________T
};

struct sLHCbBkgCat
{
  Int_t ID;
  std::string sID;
  std::string description;
  std::string condition;
};


const sLHCbBkgCat LHCbBkgCat[] = {
  {   0,   "0", "Signal",                   "!G && !K && !L && A && B && C && D && E"},
  {  10,  "10", "Quasi-signal",             "!G && !K && !L && A && B && C && D && !E"},
  {  20,  "20", "Phys. back. (full rec.)",  "!G && !K && !L && A && B && C && !D"},
  {  30,  "30", "Reflection (mis-ID)",      "!G && !K && !L && A && B && !C"},
  {  40,  "40", "Phys. back. (part. rec.)", "!G && !K && !L && A && !B && !(C && F)"},
  {  50,  "50", "Low-mass background",      "!G && !K && !L && A && !B && C && F"},
  {  60,  "60", "Ghost",                    "G"},
  {  63,  "63", "Clone",                    "!G && K"},
  {  66,  "66", "Hierarchy",                "!G && K && L"},
  {  70,  "70", "FromPV",                   "!G && !K && !L && M"},
  {  80,  "80", "AllFromSamePV",            "!G && !K && !L && N"},
  { 100, "100", "Pileup/FromDifferentPV",   "!G && !K && !L && !A && H"},
  { 110, "110", "bb event",                 "!G && !K && !L && !A && !H && I"},
  { 120, "120", "cc event",                 "!G && !K && !L && !A && !H && !I && J"},
  { 130, "130", "light-flavour event",      "!G && !K && !L && !A && !H && !I && !J"}
};

const TString varNames[3] = { "Bs_MCORR", "Bs_MM", "Bs_M"};
  
struct paintHistogram
{
  Int_t linew;
  Int_t linec;
  Int_t lines;
  
  Int_t fillc;
  Int_t fills;
  
  const char *title;
};

typedef std::unordered_map<std::string, paintHistogram> PAINT;

struct dataSampleDef
{
  TString Up;
  TString Down;
};

struct sampleDef
{
  TString rootFileUp[3];
  TString rootFileDown[3];
  TString treeName;
  TString legend;    
  TString plotName;
};


struct Mycategories
{
  BKGCAT   cID  = BKGUNUSED;
  Int_t    nTot = 0;
  Double_t stat = 0.;
  TH1Map    HM;
  PDGMap    PM;
  DoubleMap sM;
  THStack  *SM;
  TCanvas  *canvas[2];
};

const TString catNames[3] = { "Good events", "Fake events", "Combinatorial"};

#ifndef SUPERFAKES_SCANHISTORY_C
extern sampleDef samples[];
extern sampleDef samplesTrimmer[];
#else
sampleDef samples[] = {
  {{"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/old/bs2kmunu_MagUp_2016_reduced.root","",""},
   {"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/old/bs2kmunu_MagDown_2016_reduced.root","",""},
   "Bs2KMuNuDetached/DecayTree", "B^{0}_{s} #rightarrow K^{-} #mu^{+} #nu", "Bs2Kmunu"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/b2cMuX_ReDecay/DTT_10010037_2016_MagUp.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/b2cMuX_ReDecay/DTT_10010037_2016_MagDown.root","",""},
   "DecayTree","H_{b} #rightarrow H_{c}(#rightarrow KX) #mu #nu X'","b2cKmunuX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/B2ccbarKX_ReDecay/DTT_12445022_2016_MagUp.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/B2ccbarKX_ReDecay/DTT_12445022_2016_MagDown.root","",""},
    "DecayTree","B^{+} #rightarrow c #bar{c} K X","B2ccbarKX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2DsMuNuX/bsdsmunux_MagUp_2016","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2DsMuNuX/bsdsmunux_MagDown_2016","",""},
    "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow D_{s} #mu^{+} #nu X'","Bs2DsMuNuX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bu2JpsiK/DTT_12143001_2016_MagUp.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bu2JpsiK/DTT_12143001_2016_MagDown.root","",""},
    "Bs2KMuNuDetached/DecayTree","B^{+} #rightarrow J/#psi K^{+}","B2JpsiK"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2JpsiPhi/DTT_13144001_2016_MagUp.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2JpsiPhi/DTT_13144001_2016_MagDown.root","",""},
   "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow J/#psi #phi","Bs2Jpsiphi"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagUp_2016.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagDown_2016.root","",""},
   "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow K^{*}(892)- #mu^{+} #nu","Bs2Kstrmunu"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst1430MuNu/bs2kst1430munu_MagUp_2016.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst1430MuNu/bs2kst1430munu_MagDown_2016.root","",""},
    "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow K^{*}(1430)- #mu^{+} #nu","Bs2K1430munu"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst2MuNu/bs2kst2munu_MagUp_2016.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst2MuNu/bs2kst2munu_MagDown_2016.root","",""},
   "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow K^{*}_{2}(1430)- #mu^{+} #nu","Bs2K2strmunu"}
};


sampleDef samplesTrimmer[] = {
  {{"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/old/bs2kmunu_MagUp_2016_TRIMMER.root","",""},
   {"/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/old/bs2kmunu_MagDown_2016_TRIMMER.root","",""},
   "DecayTree","B^{0}_{s} #rightarrow K^{-} #mu^{+} #nu","Bs2Kmunu"}, 
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/b2cMuX_ReDecay/DTT_10010037_2016_MagUp_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/b2cMuX_ReDecay/DTT_10010037_2016_MagDown_TRIMMER.root","",""},
   "DecayTree","H_{b} #rightarrow H_{c}(#rightarrow KX) #mu #nu X'","b2cKmunuX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/B2ccbarKX_ReDecay/DTT_12445022_2016_MagUp_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/B2ccbarKX_ReDecay/DTT_12445022_2016_MagDown_TRIMMER.root","",""},
   "DecayTree","B^{+} #rightarrow c #bar{c} K X","B2ccbarKX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2DsMuNuX/bsdsmunux_MagUp_2016","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2DsMuNuX/bsdsmunux_MagDown_2016","",""},
   "DecayTree","B^{0}_{s} #rightarrow D_{s} #mu^{+} #nu X'","Bs2DsMuNuX"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bu2JpsiK/DTT_12143001_2016_MagUp_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bu2JpsiK/DTT_12143001_2016_MagDown_TRIMMER.root","",""},
   "DecayTree","B^{+} #rightarrow J/#psi K^{+}","B2JpsiK"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2JpsiPhi/DTT_13144001_2016_MagUp_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2JpsiPhi/DTT_13144001_2016_MagDown_TRIMMER.root","",""},
   "DecayTree","B^{0}_{s} #rightarrow J/#psi #phi","Bs2Jpsiphi"},
  /*  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagUp_2016.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagDown_2016.root","",""},
   "Bs2KMuNuDetached/DecayTree","B^{0}_{s} #rightarrow K*(892)- #mu^{+} #nu","Bs2Kstrmunu"},*/
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagUp_2016_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2KstMuNu/bs2kstmunu_MagDown_2016_TRIMMER.root","",""},
   "DecayTree","B^{0}_{s} #rightarrow K*(892)- #mu^{+} #nu","Bs2Kstrmunu"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst1430MuNu/bs2kst1430munu_MagUp_2016_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst1430MuNu/bs2kst1430munu_MagDown_2016_TRIMMER.root","",""},
   "DecayTree","B^{0}_{s} #rightarrow K*_{0}(1430)- #mu^{+} #nu","Bs2K1430munu"},
  {{"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst2MuNu/bs2kst2munu_MagUp_2016_TRIMMER.root","",""},
   {"/eos/lhcb/user/d/dfazzini/public/Vub/Tuples/MC/Bs2Kst2MuNu/bs2kst2munu_MagDown_2016_TRIMMER.root","",""},
   "DecayTree","B^{0}_{s} #rightarrow K^{*}_{2}(1430)- #mu^{+} #nu","Bs2K2strmunu"}
};

/*
sampleDef samplesTrimmer[] =
{
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KMuNu/DTT_13512010_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{s} #rightarrow K^{-} #mu^{+} #nu",
    "Bs2Kmunu_TRIMMER"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/b2cMuX/DTT_10010037_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "H_{b} #rightarrow H_{c}(#rightarrow KX) #mu #nu X'",
    "b2cmuX"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2018_Up_TRIMMER.root"
    },
    {
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2016_Down_TRIMMER.root",
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2017_Down_TRIMMER.root",
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/B2ccbarKX/DTT_12445022_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{+} #rightarrow c #bar{c} K X",
    "B2ccbarKX"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{+} #rightarrow J/#psi K^{+}",
    "B2JpsiK"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2JpsiKst/DTT_11144001_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{d} #rightarrow J/#psi K^{*}(892)",
    "Bd2JpsiKstr"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Down_TRIMMER.root"
    },
    "B2XuMuNuBs2K_Tuple",
    "B^{0}_{u} #rightarrow  J/#psi K^{*}(892)+",
    "Bu2JpsiKstr"
 },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2KstMuNu/DTT_13512400_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{s} #rightarrow K^{*}(892)- #mu^{+} #nu",
    "Bs2Kstrmunu"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2018_Up_TRIMMER.root"
    },
    {
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2016_Down_TRIMMER.root",
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2017_Down_TRIMMER.root",
    "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{s} #rightarrow K^{*}_{0}(1430)- #mu^{+} #nu",
    "Bs2K1430munu"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{s} #rightarrow K^{*}_{2}(1430)- #mu^{+} #nu",
    "Bs2K2strmunu"
  },
  {
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2016_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2017_Up_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2018_Up_TRIMMER.root"
    },
    {
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2016_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2017_Down_TRIMMER.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/MC/trimmer/Bd2Kpi/DTT_11102003_2018_Down_TRIMMER.root"
    },
    "DecayTree",
    "B^{0}_{d} #rightarrow K+ #pi-",
    "b2cKmunuX"
  }
};
*/

#endif

#endif // SUPERFAKES_SCANHISTORY_H
