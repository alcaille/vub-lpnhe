//-----------------------------------------------------------------------------
// Implementation file for class : fireWorks
//
// 2024-05-17 : Pascal Vincent
//-----------------------------------------------------------------------------
// Include files 



// local
#define __FIREWORKS_H__ 1
#include "fireWorks.h"
#undef __FIREWORKS_H__ 

using namespace std;


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
fireWorks::fireWorks(  ) {

}
//=============================================================================
// Destructor
//=============================================================================
fireWorks::~fireWorks() {} 

//=============================================================================

Bool_t fireWorks::isIn( Int_t x, Int_t y ) {
  if (x >= 0 && x < termSizeXY[0] && y >=0 && y < termSizeXY[1]) return kTRUE;
  return kFALSE;
};

Bool_t fireWorks::outOfTheBox( Int_t x, Int_t y , Float_t t) {
  if ( x < 8 || x > termSizeXY[0]-8 || y < 6 ) return kTRUE;
  if ( t > 1 && (((x < 83) && (y > termSizeXY[1]-14)) || (y > termSizeXY[1]-6))) return kTRUE;
  return kFALSE;
};


void fireWorks::fire(Int_t x, Int_t y) {
  static Int_t col = 41;
  
  if (!isIn(x, y)) return;
  gotoxy( x, y );
  std::cout << Form("\e[38;05;%dm\e[1m",col++) <<"*"<<flush;
};

void fireWorks::ball (Int_t x, Int_t y) {
  static Int_t iColor = 1;
  gotoxy(x-1, y-3);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↑↑↑)";   
  gotoxy(x-4, y-2);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↑↑↑↑↑↑↑↑↑)";  
  gotoxy(x-6, y-1);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↖↖↖↖↖↖↖↗↗↗↗↗↗)";
  gotoxy(x-7, y+0);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(←←←←←←←→→→→→→→→)";
  gotoxy(x-6, y+1);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↙↙↙↙↙↙↙↘↘↘↘↘↘)";
  gotoxy(x-4, y+2);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↓↓↓↓↓↓↓↓↓)";
  gotoxy(x-1, y+3);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↓↓↓)";
  cout.flush();

  iColor++;
  return;
};

void fireWorks::shower (Int_t x, Int_t y) {
  static Int_t iColor = 1;
  
  gotoxy(x-1, y-3);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↓↓↓)";   cout.flush(); usleep(10000);
  gotoxy(x-4, y-2);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↓↓↓↓↓↓↓↓↓)";  cout.flush(); usleep(10000);
  gotoxy(x-6, y-1);  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(↓↓↓↓↓↓↓↓↓↓↓↓↓)";  cout.flush(); usleep(10000);
  for (Int_t i = 0; i < 7; i++) {
    gotoxy(x-7, y+i);  cout<< Form("\e[38;05;%dm\e[1m",iColor++)<<R"(↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓)"; usleep(10000); cout.flush();};

  iColor++;
  return;
};

void fireWorks::star(Int_t cx, Int_t cy) {
  Int_t size = ( Int_t )uniform(1, 3);
  Int_t y;

  for (Int_t x = cx-5*size; x <= cx+5*size; x++) {fire( x, cy ); x++;};
  for (Int_t y = cy-3*size; y <= cy+3*size; y++) {fire( cx, y ); };
  y = cy-2*size; for (Int_t x = cx-2*size; x <= cx+2*size; x++) {fire( x, y );  y++;};
  y = cy-2*size; for (Int_t x = cx+2*size; x >= cx-2*size; x--) {fire( x, y );  y++;};
  
  return;
}; 

void fireWorks::motion(float time, Int_t x0, Int_t y0, float v0x, float v0y, Int_t &x, Int_t &y){
  x =  x0 + Int_t (v0x * time);
  y =  y0 - Int_t (-1./2.*__GRAVITY__* time*time + v0y * time) ;
  return;
};
    

void fireWorks::pull()
{
  float time = 0.;
  //  Int_t x0   = termSizeXY[0]/2+21;
  Int_t x0   = 84;
  Int_t y0   = termSizeXY[1]-3;
  float v0x  = uniform(-50, +20); 
  float v0y  = uniform(+20, +40);
  Int_t x    = x0;
  Int_t y    = y0;

  gotoxy(x0, y0);
  while (isIn(x, y)) 
  {
    fire( x, y );
    
    usleep(50000);

    time += 0.1;
    motion(time, x0, y0, v0x, v0y, x, y); 
    if (outOfTheBox(x, y, time)) {star(x, y); break;}
  };
};

void fireWorks::straight(Int_t x0, Int_t y0, Int_t x, Int_t y){
  Int_t xdir = 1;
  Int_t count= termSizeXY[0];

  if      ( x == x0 ) xdir =  0; 
  else if ( x < x0  ) xdir = -1;
  
  Double_t slope = Double_t(y-y0)/Double_t(x-x0);
  Int_t c = x0;
  Int_t l = 0;
  while ( 1 ) {
    l = y0 - slope * (x0 - c);
    if (l <= y ) break;
    
    fire( c, l );

    c += xdir;
    if (count-- < 0) break; // be safe
  };
  
  return;
}


void fireWorks::thefinal()
{
  static Int_t count = 0;
  static Int_t speed = 1000000;
  
  float time = 0.;
  //  Int_t x0   = termSizeXY[0]/2+21;
  Int_t x0   = 84;
  Int_t y0   = termSizeXY[1]-3;
  Int_t x    = Int_t (uniform(1./4.*termSizeXY[0], 3./4.*termSizeXY[0]));
  Int_t y    = Int_t (uniform(1./6.*termSizeXY[1], 1./2.*termSizeXY[1]));

  straight(x0, y0, x, y);
  if (uniform(0, 3) >=1) ball(x, y); else shower(x, y);

  if (!(count%5))
    for (int ii = 0; ii < 10; ii++) {
      x = Int_t (uniform(1./4.*termSizeXY[0], 3./4.*termSizeXY[0]));
      y = Int_t (uniform(1./6.*termSizeXY[1], 1./2.*termSizeXY[1]));
      straight(x0, y0, x, y);
      ball(x, y);
      usleep(100000);
    }

  usleep(speed);
  speed -= 100000;
  
  count++;
  return;
};

void fireWorks::flag( )
{
  Int_t iColor = 41;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                                                              )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(  ____________                          ____________          )"<<endl;   
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"( (VUBVUBVUBVUB)                        (VUBVUBVUBVUB)         )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"( (VUBVUBVUBVUB)                        (VUBVUBVUBVUB)         )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"( (VUBVUBVUBVUB)                        (VUBVUBVUBVUB)         )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(  \  VUBVUB\                           /  VUBVUB/             )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(   \  VUBVUB\                         /  VUBVUB/              )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(    \  VUBVUB\                       /  VUBVUB/               )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(     \  VUBVUB\                     /  VUBVUB/                )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(      \  VUBVUB\                   /  VUBVUB/                 )"<<endl;   
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(       \  VUBVUB\                 /  VUBVUB/                  )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(        \  VUBVUB\               /  VUBVUB/                   )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(         \  VUBVUB\             /  VUBVUB/                    )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(          \  VUBVUB\           /  VUBVUB/                     )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(           \  VUBVUB\         /  VUBVUB/                      )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(            \  VUBVUB\       /  VUBVUB/                       )"<<endl;   
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(             \  VUBVUB\_____/  VUBVUB/        ________        )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(              \  VUBVUBVUBVUBVUBVUBV/        (VUBVUBVU)       )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(               \  VUBVUBVUBVUBVUVUB/_     ___|_ VUBVUB|       )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                \  VUBVUBVUBVUBVUB/ B)   (VUBVU) VUBVU\____   )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                 \_______________/VUB|   |BVUBV|  VUBVUBVUB\  )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                               | VUBV|___|UBVUB|  VUBVUUBVUB) )"<<endl;   
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                               | VUBVUBVUBVUBVU|  V/   \UBVU| )"<<endl;  
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                               | BVUBVUBVUBVUBV|  B\___/UBVU| )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                               | UBVUBVUBVUBVUB|  VUBVUUBVUB) )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                               (_______________)___________/  )"<<endl;
  cout<< Form("\e[38;05;%dm\e[1m",iColor)<<R"(                                                              )"<<endl;
  return;
};

void fireWorks::playTheGame(  )
{
  // Check window's size
  windowSize( termSizeXY );

  // This to correct a bug in root
  termSizeXY[1] -= termSizeXY[0]*100;
  
  // clear screen
  gotoxy( 0, 0 );
  for ( Int_t i = 0; i < termSizeXY[1]-9; i++ )
    std::cout << std::string( termSizeXY[0]-1, ' ' );

  // Rockets
  for (int ii = 0; ii < 20; ii++) pull();

  // Final
  for (int ii = 0; ii < 10; ii++) thefinal();

  // Flag
  gotoxy( 0, 0 );
  flag();
  
  // End of game 
  gotoxy(0, termSizeXY[1]-1);
  std::cout<<"\e[0m";
  std::cout.flush();
  
  return;  
}

