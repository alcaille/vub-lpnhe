// Include files 



// local
#include "rootinoStudy.h"

//-----------------------------------------------------------------------------
// Implementation file for class : rootinoStudy
//
// 2024-03-22 : Pascal Vincent
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
rootinoStudy::rootinoStudy( TChain *t ) : superFake ( t )
{
  for (auto cat : LHCbBkgCat)
  {
    CDESC[cat.sID.c_str()] = cat.description.c_str();
    CCOND[cat.sID.c_str()] = cat.condition.c_str();
  };
  
  
  *canvas = NULL;
}
//=============================================================================
// Destructor
//=============================================================================
rootinoStudy::~rootinoStudy() {} 

//=============================================================================

void rootinoStudy::createHistograms(  )
{
  /*********************************************************************************************
   *
   * D E C L A R E   A N D   P A I N T   H I S T O G R A M S
   *
   *****/

  std::vector<std::string> HN = {
    "muon_Ghost", "muon_MissID", "muon_Prompt", 
    "kaon_Ghost", "kaon_MissID", "kaon_Prompt",
    "Fake", "Comb"
  }; 
  
  std::vector<paintHistogram> PN = {
    {2, kRed+2,   1, 0, 1001, sampleName.Data()},
    {2, kGreen+2, 1, 0, 1001, sampleName.Data()},  
    {2, kBlue+2,  1, 0, 1001, sampleName.Data()},  
    {2, kRed+2,   1, 0, 1001, sampleName.Data()},
    {2, kGreen+2, 1, 0, 1001, sampleName.Data()},
    {2, kBlue+2,  1, 0, 1001, sampleName.Data()},

    {2, kBlue+2,  1, 0, 0, sampleName.Data()},
    {2, kGreen+2, 1, 0, 0, sampleName.Data()}
  };

  auto ii = 0;
  for (vector<string>::iterator i = HN.begin(); i != HN.end(); i++)
  {
    HM[i->c_str()] = new TH1F(Form("RS_%s_%s", i->c_str(), sampleTag.Data()),
                              Form("%s",sampleName.Data()),
                              __NBINS__, massRange[0], massRange[1]);
    HM[i->c_str()] ->SetName(Form("H_rootinoStudy_%s_%s_%s%s",
                                  sampleTag.Data(), i->c_str(), varName.Data(), (isTrimmer)?"TRIMMER":""));
    HP[i->c_str()] = PN.at(ii);
    ii++;
  };

  ME.SetMassRange(massRange);
  ME.SetQ2ValCut(q2Flag);
  ME.SetVarName(varName.Data());
  ME.SetIsTrimmer(isTrimmer);
  
  ME.SetCutTrimmer( cutTrimmer );
  ME.SetCutOffline( cutOffline );
  ME.SetCutFit    ( cutFit     );
  ME.SetCutQ2Val  ( cutQ2Val   );
  
  ME.SetBanner    ( banner );
};

void rootinoStudy::fillBackgroundCat( BKGCAT category )
{
  switch (category)
  {
  case BKGFAKE:
    if (auto search = BF.find(Form("%d",IM["Bs_BKGCAT"])); search == BF.end())
      BF[Form("%d",IM["Bs_BKGCAT"])] =
        new TH1F(Form("H_rootinoStudy_Fake_%s_%s_%s%s",Form("%d",IM["Bs_BKGCAT"]),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);        
    BF[Form("%d",IM["Bs_BKGCAT"])]->Fill(DM[varName.Data()],1);
    break;
  case BKGSUPFAKE:
    if (auto search = BC.find(Form("%d",IM["Bs_BKGCAT"])); search == BC.end())
      BC[Form("%d",IM["Bs_BKGCAT"])] =
        new TH1F(Form("H_rootinoStudy_Comb_%s_%s_%s%s",Form("%d",IM["Bs_BKGCAT"]),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    BC[Form("%d",IM["Bs_BKGCAT"])]->Fill(DM[varName.Data()],1);
    break;
  default:
    break;
  };
  
  return;
};

void rootinoStudy::fillHistograms( BKGCAT category )
{
  auto used = 0;
  
  if (IM["Bs_TRUEID"]) return;

  fillBackgroundCat( category );
    
  if (verbose & 0x2) eventDump("");

  Double_t massVal = DM[varName.Data()];

  if (IM["muon_p_TRUEID"] && (IM["muon_p_ID"] != IM["muon_p_TRUEID"])) {
    HM["muon_MissID"]->Fill(massVal,1.); used = 1;}; 

  if (!IM["muon_p_TRUEID"]){
    HM["muon_Ghost"]->Fill(massVal,1.); used = 1;};

  if (TMath::Abs(IM["muon_p_TRUEID"] == 13) && (TMath::Abs(IM["muon_p_MC_MOTHER_ID"]) <= 6)){
    HM["muon_Prompt"]->Fill(massVal,1.); used = 1;};

  if (IM["kaon_m_TRUEID"] && (IM["kaon_m_ID"] != IM["kaon_m_TRUEID"])){
    HM["kaon_MissID"]->Fill(massVal,1.); used = 1;};

  if (!IM["kaon_m_TRUEID"]){
    HM["kaon_Ghost"]->Fill(massVal,1.); used = 1;};

  if (TMath::Abs(IM["kaon_m_TRUEID"] == 321) && (TMath::Abs(IM["kaon_m_MC_MOTHER_ID"]) <= 6)){
    HM["kaon_Prompt"]->Fill(massVal,1.); used = 1;};

  if (used) return;
  
  switch (category)
  {
  case BKGFAKE:
    HM["Fake"]->Fill(massVal,1.);
    break;
  case BKGSUPFAKE:
    HM["Comb"]->Fill(massVal,1.);
    if ((verbose & 0x4) && !IM["Bs_TRUEID"] && massVal>5000.) eventDump("SF :");
    break;
  default:
    break;
  };
};


void rootinoStudy::setupHistogram(string s, paintHistogram p)
{

  switch (what2plot)
  {
  case 0: HM[s.c_str()]->SetTitle(Form("%s;M_{corr} (MeV/c^{2});Entries", p.title)); break;
  case 1: HM[s.c_str()]->SetTitle(Form("%s;Bs_MM (MeV/c^{2});Entries"   , p.title)); break;
  case 2: HM[s.c_str()]->SetTitle(Form("%s;Bs_M (MeV/c^{2});Entries"    , p.title)); break;
  default: break;
  };

  HM[s.c_str()]->SetLineWidth(p.linew);
  HM[s.c_str()]->SetLineColor(p.linec);
  HM[s.c_str()]->SetLineStyle(p.lines);
  
  HM[s.c_str()]->SetFillColor(p.fillc);
  HM[s.c_str()]->SetFillStyle(p.fills);

  return;
};


void rootinoStudy::drawHistograms( Int_t i = 0 )
{
  i = 1; // just to prevent warning during compilation

  const Int_t nCol = 18;
  Int_t palette[nCol] = { 2, 3, 4, 6, 7, 8, 9, 20, 25, 28, 30, 33, 38, 40, 41, 46, 49};
  
  gStyle->SetPalette(kOcean);

  for (const auto& [key, value] : HP)
    setupHistogram(key, value);
  
  canvas[0] = new TCanvas(Form("MissID_Ghost_%s", sampleTag.Data()),
                          Form("EventType %s",    sampleTag.Data()), 10, 10, 1100, 800);
  canvas[0]->Divide(2,1);
  
  /****************************************************************************************
   *
   * D R A W   G H O S T   A N D   B A D   I D
   *
   ************************/
  THStack *stackMu = new THStack(Form("HStack_Muon_%s",sampleTag.Data()),Form("%s",sampleName.Data()));
  stackMu->Add(HM["muon_MissID"],"HIST E");
  stackMu->Add(HM["muon_Prompt" ],"HIST E");
  stackMu->Add(HM["muon_Ghost" ],"HIST E");
  stackMu->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varName.Data())); 

  canvas[0]->cd(1);
  if (logscale) gPad->SetLogy();
  if (logscale) stackMu->SetMaximum(stackMu->GetMaximum()*3.);
  else stackMu->SetMaximum(stackMu->GetMaximum()*1.5);
  stackMu->Draw("pfc");
  ME.drawH(HM["muon_MissID"]->Integral());

  drawLHCbLogo();
  drawLegend(1, ME.GetHistogram());

  THStack *stackK = new THStack(Form("HStack_Kaon_%s",sampleTag.Data()),Form("%s",sampleName.Data()));
  stackK->Add(HM["kaon_MissID"],"HIST E");
  stackK->Add(HM["kaon_Prompt"],"HIST E");
  stackK->Add(HM["kaon_Ghost" ],"HIST E");
  stackK->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varName.Data())); 

  canvas[0]->cd(2);
  if (logscale) gPad->SetLogy();
  if (logscale) stackK->SetMaximum(stackK->GetMaximum()*3.);
  else stackK->SetMaximum(stackK->GetMaximum()*1.5);
  stackK->Draw("pfc");
  ME.drawH(HM["kaon_MissID"]->Integral());

  drawLHCbLogo();
  drawLegend(2, ME.GetHistogram());

  canvas[0]->Update();

  /****************************************************************************************
   *
   * D R A W   F A K E   A N D   S U P E R   F A K E
   *
   ************************/
  
  canvas[1] = new TCanvas(Form("FakeComb_%s",   sampleTag.Data()),
                          Form("EventType %s" , sampleTag.Data()), 10, 10, 1100, 800);
  canvas[1]->Divide(2,1);

  canvas[1]->cd(1);
  HM["Fake"]->SetMaximum(HM["Fake"]->GetMaximum()*1.5);
  HM["Fake"]->Draw("HIST E");
  ME.drawH(HM["Fake"]->Integral());
  
  drawLHCbLogo();
  drawLegend(3, ME.GetHistogram());

  canvas[1]->cd(2);
  HM["Comb"]->SetMaximum(HM["Comb"]->GetMaximum()*1.5);
  HM["Comb"]->Draw("HIST E");

  drawLHCbLogo();
  drawLegend(4, NULL);
  
  canvas[1]->Update();

  /****************************************************************************************
   *
   * D R A W   B A C K G R O U N D   C A T E G O R I E S
   *
   ************************/

  /*****
   *
   * F A K E   S T A C K
   *
   *****/

  auto maxHist (0);

  THStack *stack[2];
  
  canvas[2] = new TCanvas(Form("ROOTINO_Fake_CAT_%s",   sampleTag.Data()),
                          Form("EventType %s" , sampleTag.Data()), 10, 10, 1100, 800);
  canvas[2]->Divide(2,1);
  canvas[2]->cd(1);

  stack[0] = new THStack(Form("BKGFAKE_CAT_Stack_%s",sampleTag.Data()),Form("%s",sampleName.Data()));

  for (const auto& [key, value] : BF) 
  {
    maxHist = (maxHist<BF[key]->GetMaximum())?BF[key]->GetMaximum():maxHist;
    BF[key]->SetLineStyle(1);
    BF[key]->SetLineColor(1);
    BF[key]->SetFillStyle(1001);
    stack[0]->Add(BF[key],"HIST E");
  };

  stack[0]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 
  stack[0]->SetMaximum(stack[0]->GetMaximum()*1.5);
  stack[0]->Draw("pfc");

  drawLHCbLogo();

  drawLegend(5, NULL);

  canvas[2]->Update();

  canvas[2]->cd(2);

  if (logscale) gPad->SetLogy();
  
  auto iLine(1);
  auto iCol (0);
  auto done (0);

  TString drawOpt = "HIST E";
  
  for (const auto& [key, value] : BF)
  {
    if (logscale) BF[key]->SetMaximum(maxHist*3.);
    else BF[key]->SetMaximum(maxHist*1.25);

    BF[key]->SetLineWidth(2);
    BF[key]->SetLineColor(palette[iCol]);
    BF[key]->SetLineStyle(iLine);
    BF[key]->SetFillStyle(0);
    BF[key]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 

    BF[key]->Draw(drawOpt.Data());
    if (!done) drawOpt.Append(" SAME "); done ++;

    if ( ++iCol>=nCol ){
      iCol = 0;
      iLine ++;
      if ( iLine>8 ) iLine = 1;
    };
  };

  if (auto search = BF.find("110"); search != BF.end()) {
    drawLegend(7, ME.drawH(BF["110"]->Integral()));
  } else {
    drawLegend(7, NULL);
  };
  
  drawLHCbLogo();

  canvas[2]->Update();
  
  /*****
   *
   * C O M B I N A T O R I A L 
   *
   *****/
  
  canvas[3] = new TCanvas(Form("ROOTINO_Comb_CAT_%s",   sampleTag.Data()),
                          Form("EventType %s" , sampleTag.Data()), 10, 10, 1100, 800);
  canvas[3]->Divide(2,1);
  canvas[3]->cd(1);

  stack[1] = new THStack(Form("ROOTINO_Comb_CAT_Stack_%s",sampleTag.Data()),Form("%s",sampleName.Data()));

  for (const auto& [key, value] : BC) 
  {
    //    if (Double_t(value)/cat.nTot < statCut) continue;
    BC[key]->SetLineStyle(1);
    BC[key]->SetLineColor(1);
    BC[key]->SetFillStyle(1001);
    stack[1]->Add(BC[key],"HIST E");
  };

  stack[1]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 
  stack[1]->SetMaximum(stack[1]->GetMaximum()*1.5);
  stack[1]->Draw("pfc");

  drawLHCbLogo();
  drawLegend(6, NULL);

  canvas[3]->Update();

  canvas[3]->cd(2);

  iLine= 1;
  iCol = 0;
  done = 0;

  drawOpt.Clear();
  drawOpt.Append("HIST E");

  maxHist = 0.;
  for (const auto& [key, value] : BC)
    maxHist = (maxHist<BC[key]->GetMaximum())?BC[key]->GetMaximum():maxHist;
  
  for (const auto& [key, value] : BC)
  {
    BC[key]->SetMaximum(maxHist*1.35);

    BC[key]->SetLineWidth(2);
    BC[key]->SetLineColor(palette[iCol]);
    BC[key]->SetLineStyle(iLine);
    BC[key]->SetFillStyle(0);
    BC[key]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 

    BC[key]->Draw(drawOpt.Data());
    if (!done) drawOpt.Append(" SAME "); done ++;

    if ( ++iCol>=nCol ){
      iCol = 0;
      iLine ++;
      if ( iLine>8 ) iLine = 1;
    };
  };

  if (auto search = BC.find("110"); search != BC.end()) {
    drawLegend(8, ME.drawH(BC["110"]->Integral()));
  } else {
    drawLegend(8, NULL);
  };
  
  drawLHCbLogo();

  canvas[3]->Update();
  
  return;
};


void rootinoStudy::save( )
{
  Int_t i = 0;

  if (!std::filesystem::exists("figures/rootinoStudy")) 
    std::filesystem::create_directories("figures/rootinoStudy");

  for (auto ii : canvas)
    ii->SaveAs(Form("figures/rootinoStudy/rootinoStudy%d_%s_Q%d_S%s_%s%s.png", ++i,
                    sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                    varName.Data(), (isTrimmer)?"TRIMMER":""));

  if (!std::filesystem::exists("ws")) 
    std::filesystem::create_directories("ws");

  TString workSpaceName;
  workSpaceName.Append(Form("ws/rootinoStudy_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (const auto& [key, value] : HM)
    wspace->import(*HM[key],kTRUE);

  for (const auto& [key, value] : BF)
    wspace->import(*BF[key],kTRUE);

  for (const auto& [key, value] : BC)
    wspace->import(*BC[key],kTRUE);

  TH1F *dummy = ME.GetHistogram();
  dummy->SetName(Form("H_lhcbCatStudy_mixedEventsData"));
  wspace->import(*dummy,kTRUE);
        
  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
    
  if (fws) fws->Close();
  
  return;
};

void rootinoStudy::appendPaveText( TPaveText *t )
{
  TString selections = "Selections : ";
  selections.Append((cutTrimmer)?"TRIMMER":"");
  selections.Append((cutOffline)?"+Offline":"");
  selections.Append((cutFit)?"+Fit":"");
  TString q2Cut;
  if (!q2Flag) q2Cut.Append("0 GeV^{2}/c^{4} < q^{2}");
  else if (q2Flag == 1) q2Cut.Append("q^{2} #leq 7 GeV^{2}/c^{4}");
  else q2Cut.Append("7 GeV^{2}/c^{4} < q^{2}");
  t->AddText(selections.Data());
  t->AddText(q2Cut.Data());
};

void rootinoStudy::drawLegend(Int_t ican, TH1F *dataH)
{
  TLegend *legend;
  TPaveText *dataText;
  switch(ican)
  {
  case 1:
    legend = new TLegend(0.5, 0.5, 0.85, 0.75);
    legend->SetLineWidth(0);
    legend->SetHeader("#scale[1.2]{#font[62]{#color[2]{Rootino : Muons}}}");
    legend->AddEntry(HM["muon_Ghost" ], "Ghost",   "FL");
    legend->AddEntry(HM["muon_MissID"], "Miss ID", "FL");
    legend->AddEntry(HM["muon_Prompt"], "Prompt",  "FL");
    if (dataH) {
      legend->AddEntry(dataH, "Data : mixed events", "LEP");
      legend->AddEntry((TObject*)0, "scaled to miss ID", "");
    };
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.035);
    legend->Draw();

    dataText = new TPaveText(0.15, 0.775, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 2:
    legend = new TLegend(0.5, 0.5, 0.85, 0.75);
    legend->SetLineWidth(0);
    legend->SetHeader("#scale[1.2]{#font[62]{#color[4]{Rootino : Kaons}}}");
    legend->AddEntry(HM["kaon_Ghost" ], "Ghost",   "FL");
    legend->AddEntry(HM["kaon_MissID"], "Miss ID", "FL");
    legend->AddEntry(HM["kaon_Prompt"], "Prompt",  "FL");
    if (dataH) {
      legend->AddEntry(dataH, "Data : mixed events", "LEP");
      legend->AddEntry((TObject*)0, "scaled to miss ID", "");
    };
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.035);
    legend->Draw();

    dataText = new TPaveText(0.15, 0.775, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 3:
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[4]{Fake category}");
    dataText->AddText("Not miss ID, not Ghost, not prompt");
    appendPaveText( dataText );
    dataText->Draw();
    if (!dataH) break;

    legend = new TLegend(0.15, 0.6, 0.45, 0.7);
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    legend->AddEntry(HM["Fake"], "Fake simulated events", "LE");
    legend->AddEntry(dataH, "Data : mixed events (scaled)", "LEP");
    legend->Draw();
    break;
  case 4:
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[3]{Combinatorial category}");
    dataText->AddText("Not miss ID, not Ghost, not prompt");
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 5:
    legend = new TLegend(0.15, 0.7-0.04*(BF.size()+2), 0.45, 0.7);
    legend->SetHeader("LHCb categories");
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    for (const auto& [key, value] : BF) 
      legend->AddEntry(BF[key], CDESC[key].c_str(), "F");
    if (dataH) {
      legend->AddEntry(dataH, "Data : mixed events", "LEP");
      legend->AddEntry((TObject*)0, Form("scaled to %s",CDESC["110"].c_str()), "");
    };
    legend->Draw();
    
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[4]{Fake category}");
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 6:
    legend = new TLegend(0.15, 0.7-0.04*(BC.size()+1), 0.45, 0.7);
    legend->SetHeader("LHCb categories");
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    for (const auto& [key, value] : BC) 
      legend->AddEntry(BC[key], CDESC[key].c_str(), "F");
    legend->Draw();
    
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[3]{combinatorial}");
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 7:
    legend = new TLegend(0.15, 0.7-0.04*(BF.size()+2), 0.45, 0.7);
    legend->SetHeader("LHCb categories");
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    for (const auto& [key, value] : BF) 
      legend->AddEntry(BF[key], CDESC[key].c_str(), "LE");
    if (dataH) {
      legend->AddEntry(dataH, "Data : mixed events", "LEP");
      legend->AddEntry((TObject*)0, Form("scaled to %s",CDESC["110"].c_str()), "");
    };
    legend->Draw();
    
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[4]{Fake category}");
    appendPaveText( dataText );
    dataText->Draw();
    break;
  case 8:
    legend = new TLegend(0.15, 0.7-0.04*(BF.size()+2), 0.45, 0.7);
    legend->SetHeader("LHCb categories");
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextFont(62);
    legend->SetTextSize(0.03);
    for (const auto& [key, value] : BC) 
      legend->AddEntry(BC[key], CDESC[key].c_str(), "LE");
    if (dataH) {
      legend->AddEntry(dataH, "Data : mixed events", "LEP");
      legend->AddEntry((TObject*)0, Form("scaled to %s",CDESC["110"].c_str()), "");
    };
    legend->Draw();
    
    dataText = new TPaveText(0.15, 0.75, 0.45, 0.85, "BRNDC");
    dataText->SetFillColor(0);
    dataText->SetTextAlign(12);
    dataText->SetBorderSize(0);
    dataText->SetLineWidth(0);
    dataText->SetTextSize(0.03);
    dataText->SetFillStyle(0);
    dataText->AddText("#font[62]{#color[2]{Rootino}} from #color[3]{combinatorial}");
    appendPaveText( dataText );
    dataText->Draw();
    break;
  default:
    break;
  };

  return;
};

