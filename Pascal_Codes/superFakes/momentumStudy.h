/** @class momentumStudy momentumStudy.h superFakes/momentumStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-05-29
 */
#ifndef SUPERFAKES_MOMENTUMSTUDY_H 
#define SUPERFAKES_MOMENTUMSTUDY_H 1

// Include files
#include "superFake.h"
#include "superStudy.h"

using namespace std;

class momentumStudy : public superFake
{
public: 
  /// Standard constructor
  momentumStudy( );
  momentumStudy( TChain * );
                                               
  virtual ~momentumStudy( ); ///< Destructor

  void createHistograms( ) override;
  void fillHistograms( BKGCAT ) override;
  void drawHistograms( Int_t ) override;
  void eventSummary( ) override;
  void save() override;

  void openWS( Bool_t );
  void nuMomentumConstructor( );
  void drawHistogram ( Int_t *, Int_t );
  void drawHistogramCorr (  );
    
  void SetVerbose( Int_t v){verbose = v;};

protected:

private:
  Int_t nSelected = 0;
  Int_t nBadDelta = 0;
  
  TFile* fws = NULL;
  RooWorkspace* wspace = NULL;
  TString workSpaceName;

  TH1Map    H1M;
  TH2Map    H2M;
  DoubleMap MM;

  Double_t xMax [17] = {
    5.E+5, 5.E+5, 5.E+5, 2.5E+3,  5.E+5, 5.E+5, 5.E+5, 2.5E+3,
    1.E+5, 1.E+5, 1.E+5, 3.E+3, 2.E+5, 2.E+5, 2.E+5, 3.E+3, 8.};
  /*
  Double_t xMax [16] = {
    5.E+5, 5.E+5, 5.E+5, 2.5E+3,  5.E+5, 5.E+5, 5.E+5, 2.5E+3,
    1.E+5, 1.E+5, 1.E+5, 1.E+4, 2.E+5, 2.E+5, 2.E+5, 1.E+4};
  */
  Double_t x2Min [5] = {0., 0., 0., 0., 0.2};
  Double_t x2Max [5] = {2.5E+3, 2.5E+5, 2.5E+3, 2.5E+5, 20.};

  Double_t yMax [17] = {
    0.15, 0.15, 0.15, 0.05,  0.15, 0.15, 0.15, 0.05,
    0.1, 0.1, 0.1, 0.07,  0.1, 0.1, 0.1, 0.07, 0.08};
  Double_t y2Min [5] = {0., 0., 0., 0., 3.};
  Double_t y2Max [5] = {0., 0., 25., 25., 25.};

  Int_t n2Bins [5] = {50, 50, 50, 50, 50};

  Int_t hColor[9]   = {kBlack, kRed+2, kBlue+2, kBlack, kGreen+2, kOrange+1, kRed+2, kBlue+2, kGreen+2};  

  std::vector <TCanvas *> canvas;
  Int_t ncanvas = 0;
  
  // User variable names
  std::vector<std::string> hTitle =
    {
      "Missing particles", "Missing particles", "Missing particles", "Missing particles", 
      "K#mu system", "K#mu system", "K#mu system", "K#mu system", 
      "Kaon", "Kaon", "Kaon", "Kaon", 
      "Muon", "Muon", "Muon", "Muon", sampleName.Data()
    };

  std::vector<std::string> xTitle =
    {
      "E (MeV)", "P (MeV/c)", "P_{l} (MeV/c)", "P_{t} (MeV/c)", 
      "E (MeV)", "P (MeV/c)", "P_{l} (MeV/c)", "P_{t} (MeV/c)", 
      "E (MeV)", "P (MeV/c)", "P_{l} (MeV/c)", "P_{t} (MeV/c)", 
      "E (MeV)", "P (MeV/c)", "P_{l} (MeV/c)", "P_{t} (MeV/c)",
      "m^{2}_{K#pi} (GeV^{2}/c^{4})"
    };

  std::vector<std::string> h2Title =
    {
      "B^{0}_{s} M_{Corr} vs P^{#perp}_{#nu}; P^{#perp}_{#nu} (MeV/c); M_{Corr}(B^{0}_{s}) (MeV/c^{2})",
      "B^{0}_{s} M_{Corr} vs P^{#parallel}_{#nu}; P^{#parallel}_{#nu} (MeV/c); M_{Corr}(B^{0}_{s}) (MeV/c^{2})",
      "q^{2} vs P^{#perp}_{#nu}; P^{#perp}_{#nu} (MeV/c); q^{2} (GeV^{2}/c^{2})",
      "q^{2} vs P^{#parallel}_{#nu}; P^{#parallel}_{#nu} (MeV/c); q^{2} (GeV^{2}/c^{2})",
      "m^{2}_{K#mu} vs m^{2}_{K#pi}; m^{2}_{K#pi} (GeV^{2}/c^{4}); m^{2}_{K#mu} (GeV^{2}/c^{4})"
    };

  std::vector<std::string> Text =
    {
      "Energy", "Momentum",  "Longitudinal momentum", "Transverse momentum",
      "Energy", "Momentum",  "Longitudinal momentum", "Transverse momentum",
      "Energy", "Momentum",  "Longitudinal momentum", "Transverse momentum",
      "Energy", "Momentum",  "Longitudinal momentum", "Transverse momentum"
    };

    // User variable names
  std::vector<std::string> U1N =
    {
      "Nu_PE", "Nu_P", "Nu_PL", "Nu_PT",
      "X_PE",  "X_P",  "X_PL",  "X_PT",
      "K_PE",  "K_P",  "K_PL",  "K_PT",
      "M_PE",  "M_P",  "M_PL",  "M_PT",
      "M2_KP"
    };

  std::vector<std::string> U2N =
    {
      "E_CorrVSNu_PT", "E_CorrVSNu_PL",
      "Bs_Regression_Q2_BEST_PT", "Bs_Regression_Q2_BEST_PL",
      "Dalit_Kpi_vs_Kmu"
    };

  // access additional variables from tuple
  std::vector<std::string> DN =
    {
      "Bs_TRUEENDVERTEX_X", "Bs_TRUEENDVERTEX_Y", "Bs_TRUEENDVERTEX_Z",
      "Bs_TRUEORIGINVERTEX_X", "Bs_TRUEORIGINVERTEX_Y", "Bs_TRUEORIGINVERTEX_Z",
      "Bs_TRUEP_X", "Bs_TRUEP_Y", "Bs_TRUEP_Z", "Bs_TRUEP_E",
      "kaon_m_TRUEP_E", "kaon_m_TRUEP_X", "kaon_m_TRUEP_Y", "kaon_m_TRUEP_Z",
      "muon_p_TRUEP_E", "muon_p_TRUEP_X", "muon_p_TRUEP_Y", "muon_p_TRUEP_Z",

      "Bs_ENDVERTEX_X", "Bs_ENDVERTEX_Y", "Bs_ENDVERTEX_Z",
      "Bs_OWNPV_X", "Bs_OWNPV_Y", "Bs_OWNPV_Z",
      "kaon_m_Pi0_E", "kaon_m_Pi0_PX", "kaon_m_Pi0_PY", "kaon_m_Pi0_PZ",
      "kaon_m_NIsoTr_PE", "kaon_m_NIsoTr_PX", "kaon_m_NIsoTr_PY", "kaon_m_NIsoTr_PZ",
      "kaon_m_PE", 
      "muon_p_PE", "muon_p_PT"
    };
  std::vector<std::string> FN =
    {
      "kaon_m_PT"
    };
};
#endif // SUPERFAKES_MOMENTUMSTUDY_H
