#ifndef SUPERFAKES_FIREWORKS_H 
#define SUPERFAKES_FIREWORKS_H 1
/** @class fireWorks fireWorks.h superFakes/fireWorks.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-05-17
 */
// Include files
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

#include <TROOT.h>

#define __GRAVITY__ 9.8

class fireWorks {
public: 
  /// Standard constructor
  fireWorks( ); 

  virtual ~fireWorks( ); ///< Destructor

  Double_t uniform( Double_t a, Double_t b) {return (Double_t(rand())/RAND_MAX*(b-a)+a);};
  void gotoxy  ( Int_t x, Int_t y ) {std::cout << "\033[" << (y+1) << ";" << (x+1) << "H";};
  void shower  ( Int_t, Int_t );
  void ball    ( Int_t, Int_t );
  void fire    ( Int_t, Int_t );
  void star    ( Int_t, Int_t );
  void flag    ( );
  void pull    ( );
  void motion  ( Float_t, Int_t, Int_t, Float_t, Float_t, Int_t &, Int_t & );
  void straight( Int_t, Int_t, Int_t, Int_t );
  void thefinal( );
  //  void playTheGame( Int_t, Int_t );
  void playTheGame( );

  void SetWindowSize( Int_t *s){ termSizeXY[0] = s[0]; termSizeXY[1] = s[1];};

  Bool_t isIn       ( Int_t, Int_t );
  Bool_t outOfTheBox( Int_t, Int_t, Float_t );
  
  static void *fireThread( void * ptr ){
    fireWorks *p = (fireWorks *) ptr;
    // mixedEvent *p = (mixedEvent *) ptr;
    //    std::cout<<"Thread said "<<termSizeXY[0]<<" "<<termSizeXY[1]<<std::endl;
    //    p->playTheGame( termSizeXY[0], termSizeXY[1] ); return NULL;
    p->playTheGame(  ); return NULL;
  };

protected:
  Int_t termSizeXY[2];

private:

};

#ifdef __FIREWORKS_H__
int windowSize( Int_t *xy ) {
  FILE *fp;
  char path[1024];
  std::vector <std::string> command = {"tput cols", "tput lines"};

  for (int i = 0; i < 2 ; i++) {
    fp = popen(command.at(i).c_str(), "r");
    if (fp == NULL) {printf("Failed to run command\n" );exit(1);}
    while (fgets(&path[0], sizeof(path), fp) != NULL) xy[i] = atoi(path);
  };
  pclose(fp);
  return 0;
};
#else
extern int windowSize( Int_t * );
#endif

#endif // SUPERFAKES_FIREWORKS_H
