#ifndef SUPERFAKES_LHCBCATSTUDY_H 
#define SUPERFAKES_LHCBCATSTUDY_H 1
/** @class lhcbCatStudy lhcbCatStudy.h superFakes/lhcbCatStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-03
 */
// Include files

// Local
#include "superFake.h"
#include "mixedEvent.h"

class lhcbCatStudy : public superFake
{
public: 
  /// Standard constructor
  lhcbCatStudy( ); 
  lhcbCatStudy( TChain * ); 

  virtual ~lhcbCatStudy( ); ///< Destructor

  void createHistograms ( ) override;
  void fillHistograms   ( BKGCAT ) override;
  void drawHistogram    ( const char *, Mycategories & );
  void drawHistograms   ( Int_t ) override;
  void save( ) override;

protected:

private:
  Mycategories CGood, CFake, CComb;

  mixedEvent ME;

  StringMap CDESC;
  StringMap CCOND;

  Double_t cutQ2Val   = __Q2_CUT_VALUE__;
};
#endif // SUPERFAKES_LHCBCATSTUDY_H
