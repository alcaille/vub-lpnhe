#ifndef SUPERFAKES_CINEMATICSTUDY_H 
#define SUPERFAKES_CINEMATICSTUDY_H 1
/** @class cinematicStudy cinematicStudy.h superFakes/cinematicStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-10
 */

// Include files
#include <TLorentzVector.h>
#include <TVector.h>
#include <TVector3.h>

// Local
#include "superFake.h"
#include "superStudy.h"
#include "mixedEvent.h"

class cinematicStudy : public superFake
{
public: 
  /// Standard constructor
  cinematicStudy( ); 
  cinematicStudy( TChain * ); 

  virtual ~cinematicStudy( ); ///< Destructor

  void createHistograms( ) override;
  void fillHistograms  ( BKGCAT ) override;
  void initSignalHistograms( );
  void initSignalHistograms( std::vector<std::string> );
  TH1F *GetSignalHist  ( string );
  void drawHistograms  ( Int_t ) override;
  void drawFullHistograms( Int_t );
  void drawUserHistograms( Int_t );
  void drawSignal      ( string );
  void save            ( ) override;
  
  void SetVerbose( Int_t v){verbose = v;};

  Double_t KptVsBs (  );
  Double_t MuptVsBs(  );
  Double_t KplVsBs (  );
  Double_t MuplVsBs(  );
  Double_t KptVsBsDir (  );
  Double_t MuptVsBsDir(  );
  
protected:

private:
  Double_t cutQ2Val   = __Q2_CUT_VALUE__;

  mixedEvent ME;

  TFile *fws = NULL;
  RooWorkspace* wspace = NULL;
  TH1Map HM;
  TH1Map HS;
  std::vector <TCanvas *> canvas;

  DoubleMap xMin;
  DoubleMap xMax;
  BoolMap   yLog;
  
  std::vector<std::string> DD =
    {"Bs_ENDVERTEX_CHI2", "Bs_ETA", "Bs_FD_S",
     "Bs_TAU", "Bs_TAUCHI2", "Bs_TAUERR", "Bs_DTF_CTAU", "Bs_FlightLength", "",
     
     "Bs_FD_OWNPV", "Bs_IP_OWNPV", "Bs_IPCHI2_OWNPV" , "Bs_OWNPV_CHI2",  "Bs_OWNPV_CHI2DOF", "",
     "Mass_mumu_pair", "Mass_KK_pair", "Mass_Kmu_asJpsi",
     
     "kaon_m_PAIR_M", "kaon_m_IsoMinBDT", "kaon_m_PAIR_VtxChi2", 
     "kaon_m_ProbNNe", "kaon_m_ProbNNghost", "kaon_m_ProbNNk", "kaon_m_ProbNNp", "kaon_m_ProbNNpi", "Kaon_PT_vs_Bs",
     "Kaon_PL_vs_Bs", "Kaon_PT_vs_Bdir",

     "muon_p_PAIR_M", "muon_p_IsoMinBDT", "muon_p_PAIR_VtxChi2", 
     "muon_p_ProbNNghost", "muon_p_ProbNNk", "muon_p_ProbNNp", "muon_p_ProbNNpi", "muon_p_ProbNNmu", "Muon_PT_vs_Bs",
     "Muon_PL_vs_Bs", "Muon_PT_vs_Bdir"
    };

  // New branch to be attached for this study
  std::vector<std::string> IN = {"Bs_OWNPV_NDOF", "nPV"};
  
  std::vector<std::string> FVN =
    { "Bs_PVX",  "Bs_PVY",  "Bs_PVZ" }; // Does not exist
  
  std::vector<std::string> DN =
    {"Bs_OWNPV_CHI2", "Bs_ETA", "Bs_PHI", "Bs_FD_OWNPV", "Bs_IP_OWNPV", "Bs_IPCHI2_OWNPV" , "Bs_DTF_CTAU",
     "Bs_TAU", "Bs_TAUCHI2", "Bs_TAUERR", "Bs_FDCHI2_OWNPV", "Bs_PE", "Bs_PX", "Bs_PY", "Bs_PZ", 
     "Bs_ENDVERTEX_X", "Bs_ENDVERTEX_Y", "Bs_ENDVERTEX_Z",
     "Bs_OWNPV_X", "Bs_OWNPV_Y", "Bs_OWNPV_Z",
     
     "kaon_m_PIDK", "kaon_m_PIDe", "kaon_m_PIDmu", "kaon_m_PIDp",
     "kaon_m_ProbNNe", "kaon_m_ProbNNmu",  "kaon_m_ProbNNghost", "kaon_m_ProbNNk", "kaon_m_ProbNNp", "kaon_m_ProbNNpi",
     "kaon_m_PAIR_VtxChi2", "kaon_m_TRACK_CHI2NDOF", "kaon_m_TRACK_GhostProb", "kaon_m_PE", 

     "muon_p_PIDK", "muon_p_PIDe", "muon_p_PIDmu", "muon_p_PIDp",
     "muon_p_ProbNNe", "muon_p_ProbNNmu", "muon_p_ProbNNghost", "muon_p_ProbNNk", "muon_p_ProbNNp", "muon_p_ProbNNpi",
     "muon_p_PAIR_VtxChi2", "muon_p_TRACK_CHI2NDOF", "muon_p_TRACK_GhostProb", "muon_p_PE",

     "Mass_KK_pair", "Mass_mumu_pair"
    };

  std::vector<std::string> FN = {
    "Bs_FlightLength"
  };

};
#endif // SUPERFAKES_CINEMATICSTUDY_H
