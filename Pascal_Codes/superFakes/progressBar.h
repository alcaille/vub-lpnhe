#ifndef SUPERFAKES_PROGRESSBAR_H 
#define SUPERFAKES_PROGRESSBAR_H 1
/** @class progressBar progressBar.h superFakes/progressBar.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-02
 */

// Include files
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <mutex>
#include <thread>
#include <iostream>
#include <fstream>

#include <TROOT.h>
#include <TRint.h>
#include <TObject.h>
#include <TTimer.h>
#include <TThread.h>

class progressBar : public TTimer {
public: 
  /// Standard constructor
  progressBar( ); 
  progressBar( Int_t ); 

  virtual ~progressBar( ); ///< Destructor
  static void *barAndBanner( void * ptr ){
    progressBar *p = (progressBar *) ptr;
    p->Bar(  ); return NULL;
  };
  void Bar( );
  void Bar(Int_t, Int_t);
  void Banner(Int_t);
  
  void SetEntries( Int_t n ){nTuplEntries = n;};
  
  TTimer *GetTimer (){return timer;};
  virtual Bool_t HandleTimer(TTimer *t) override
  {
    Bar(  ); return kTRUE;
  };
  
protected:

private:
  Int_t nTuplEntries;
  TTimer *timer;
};
#endif // SUPERFAKES_PROGRESSBAR_H
