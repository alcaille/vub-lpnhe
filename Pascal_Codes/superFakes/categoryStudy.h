#ifndef SUPERFAKES_CATEGORYSTUDY_H 
#define SUPERFAKES_CATEGORYSTUDY_H 1
/** @class categoryStudy categoryStudy.h superFakes/categoryStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-21
 */
// Include files
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <deque>
#include <unordered_map>
#include <vector>

// local
#include "superStudy.h"
#include "superFake.h"

#define __NBINS_CAT__ 30

using namespace std;

class categoryStudy : public superFake
{
public: 
  /// Standard constructor
  categoryStudy( ){}; 
  categoryStudy( TChain * ); 

  virtual ~categoryStudy( ); ///< Destructor

  void createHistograms( ) override;
  void setupHistogram( string, paintHistogram );
  void fillHistograms( BKGCAT ) override;
  void drawHistograms( Int_t ) override;
  void drawFitParam  ( TFitResultPtr );
  void drawLegend    ( Int_t );
  void save          ( ) override;
  
protected:

private:
  TCanvas *canvas[2];

  TH1Map HM;
  PAINT  HP;

  Double_t cutQ2Val   = __Q2_CUT_VALUE__;
};
#endif // SUPERFAKES_CATEGORYSTUDY_H
