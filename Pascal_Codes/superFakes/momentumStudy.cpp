//-----------------------------------------------------------------------------
// Implementation file for class : momentumStudy
//
// 2024-05-29 : Pascal Vincent
//-----------------------------------------------------------------------------

// Include files 
#include <TLorentzVector.h>
#include <TVector.h>
#include <TVector3.h>

// local
#include "momentumStudy.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
momentumStudy::momentumStudy(  ) {
}

momentumStudy::momentumStudy( TChain *t ) : superFake ( t )
{
}
//=============================================================================
// Destructor
//=============================================================================
momentumStudy::~momentumStudy() {} 

//=============================================================================

void momentumStudy::createHistograms( )
{
  // First ask for new branches to be linked
  for ( auto i : DN  ) SetDM (i.c_str());
  for ( auto i : FN  ) SetFM (i.c_str());

  // Create histograms
  auto ii = 0;
  for ( auto i : U1N ) 
  {
    H1M[i.c_str()] = new TH1F(Form("HNU_%d_%s", sampleID, i.c_str()),
                       Form("%s;%s;a.u.",hTitle[ii].c_str(),xTitle[ii].c_str()), 50, 0., xMax[ii]);
    H1M[i.c_str()] -> SetMarkerStyle(20);
    H1M[i.c_str()] -> SetMarkerColor(hColor[sampleID]);
    MM[i.c_str()] = yMax[ii];
    ii++;
  };
  
  ii = 0;
  y2Min[0] = massRange[0];
  y2Min[1] = massRange[0];
  y2Max[0] = massRange[1];
  y2Max[1] = massRange[1];
  for ( auto i : U2N ) 
  {
    H2M[i.c_str()] = new TH2F(Form("H2NU_%d_%s", sampleID, i.c_str()), h2Title[ii].c_str(),
                              n2Bins[ii], x2Min[ii], x2Max[ii],
                              n2Bins[ii], y2Min[ii], y2Max[ii]);
    ii++;
  };
};

void momentumStudy::fillHistograms( BKGCAT category )
{
  //  if (category != BKGGOOD) return;

  if (Verbose == 1) eventDump("");  
  
  nuMomentumConstructor( );

  return;
};

void momentumStudy::drawHistograms( Int_t i )
{
  Int_t ibkg[6] = { 1, 2, 4, 6, 7, 8 };
  
      
  //  drawHistogram( ibkg , 3 );

  //  if (sampleID) return;

  // drawHistogram( ibkg+3 , 3 );

  drawHistogramCorr(  );
};

void momentumStudy::drawHistogram( Int_t *ibkg, Int_t nbkg )
{
  const Int_t raw = 2;
  const Int_t column = 2;
  const Int_t occupancy = raw*column;
  Int_t ipad = 1;
  Int_t ihist = 0;
  TCanvas *cv = NULL;
  TH1F *h;

  if (!sampleID) openWS( kFALSE );

  TPaveText *dataText = new TPaveText(0.45, 0.75, 0.85, 0.85, "BRNDC");
  dataText->SetFillColor(0);
  dataText->SetTextAlign(12);
  dataText->SetBorderSize(0);
  dataText->SetLineWidth(0);
  dataText->SetTextSize(0.045);
  dataText->SetFillStyle(0);

  TLegend *slegend = new TLegend(0.45, 0.4, 0.85, 0.75);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.045);
  
  for ( auto i : U1N )
  {
    if (!(ihist%occupancy))
    {
      ipad = 1;
      cv = new TCanvas(Form("canvas%d_%s",ncanvas++,sampleTag.Data()), sampleTag.Data(), 10, 10, 1100, 800);
      cv -> Divide(column,raw);
      canvas.push_back(cv);
    }
    cv->cd(ipad++);
    H1M[i.c_str()] -> Scale(1./H1M[i.c_str()]->Integral());
    H1M[i.c_str()] -> SetMaximum(MM[i.c_str()]);
    H1M[i.c_str()] -> Draw("EP");
    if (!ihist) slegend->AddEntry(H1M[i.c_str()], sampleName.Data(), "PE");
    if (wspace && !sampleID)
      for (Int_t ii = 0; ii < nbkg; ii++)
      {
        h = (TH1F *) wspace->genobj(Form("HNU_%d_%s",ibkg[ii],i.c_str()));
        if (h) {
          h -> SetMarkerStyle(1);
          h -> SetFillStyle(0);
          h -> SetLineWidth(2);
          h -> SetLineColor(hColor[ibkg[ii]]);
          h -> Draw("HIST SAME E");
          if (!ihist) slegend->AddEntry(h, samples[ibkg[ii]].legend.Data(), "LE");
        };
      };
    cv->Update();
    if (!sampleID) {
      if (!(ihist%raw)) slegend->Draw();
      dataText->Clear( );
      dataText->AddText( Text[ihist].c_str() );
      dataText->DrawClone();
    };
    cv->Update();
    ihist++;
  };
  
  if (fws) fws->Close();

};

void momentumStudy::drawHistogramCorr(  )
{
  TCanvas *cv = NULL;

  for ( auto i : U2N )
  {
    cv = new TCanvas(Form("canvas%d_%s",ncanvas++,sampleTag.Data()), sampleTag.Data(), 10, 10, 1100, 800);
    canvas.push_back(cv);

    cv->cd();
    H2M[i.c_str()] -> Draw("COLZ");
  };
  H2M["Dalit_Kpi_vs_Kmu"] ->SetTitle(sampleName.Data());
};

void momentumStudy::openWS( Bool_t create = kFALSE )
{
  wspace = NULL;

  workSpaceName.Clear(); 
  
  if (!std::filesystem::exists("ws")) 
    std::filesystem::create_directories("ws");

  workSpaceName.Append(Form("ws/momentumStudy_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";

  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else if (create) {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };
  return;
};

void momentumStudy::save( )
{
  Int_t i = 0;

  openWS( kTRUE );

  if (!wspace) return;
  
  for (const auto& [key, value] : H1M) wspace->import(*H1M[key],kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
    
  if (fws) fws->Close();

  if (!std::filesystem::exists("figures/momentumStudy")) 
    std::filesystem::create_directories("figures/momentumStudy");

  for (auto ii : canvas)
    ii->SaveAs(Form("figures/momentumStudy/momentumStudy%d_%s_Q%d_S%s_%s%s.png", ++i,
                    sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                    varName.Data(), (isTrimmer)?"TRIMMER":""));
  return;
};

void momentumStudy::nuMomentumConstructor( )
{

  /*
  TLorentzVector K(DM["kaon_m_TRUEP_X"], DM["kaon_m_TRUEP_Y"], DM["kaon_m_TRUEP_Z"], DM["kaon_m_TRUEP_E"]);
  TLorentzVector M(DM["muon_p_TRUEP_X"], DM["muon_p_TRUEP_Y"], DM["muon_p_TRUEP_Z"], DM["muon_p_TRUEP_E"]);
  TVector3 bvtx( DM["Bs_TRUEENDVERTEX_X"],    DM["Bs_TRUEENDVERTEX_Y"],    DM["Bs_TRUEENDVERTEX_Z"]   );
  TVector3 pvtx( DM["Bs_TRUEORIGINVERTEX_X"], DM["Bs_TRUEORIGINVERTEX_Y"], DM["Bs_TRUEORIGINVERTEX_Z"]);
  */
  //  TLorentzVector P(DM["kaon_m_NIsoTr_PX"], DM["kaon_m_NIsoTr_PY"], DM["kaon_m_NIsoTr_PZ"], DM["kaon_m_NIsoTr_PE"]);
  TLorentzVector P(DM["kaon_m_Pi0_PX"], DM["kaon_m_Pi0_PY"], DM["kaon_m_Pi0_PZ"], DM["kaon_m_Pi0_E"]);
  TLorentzVector K(DM["kaon_m_PX"], DM["kaon_m_PY"], DM["kaon_m_PZ"], DM["kaon_m_PE"]);
  TLorentzVector M(DM["muon_p_PX"], DM["muon_p_PY"], DM["muon_p_PZ"], DM["muon_p_PE"]);
  TVector3 bvtx( DM["Bs_ENDVERTEX_X"], DM["Bs_ENDVERTEX_Y"], DM["Bs_ENDVERTEX_Z"] );
  TVector3 pvtx( DM["Bs_OWNPV_X"],     DM["Bs_OWNPV_Y"],     DM["Bs_OWNPV_Z"]     );

  TLorentzVector X = K+M;

  TLorentzVector KP = K+P;
  TLorentzVector KM = K+M;

  TVector3 bdir = bvtx - pvtx; bdir = bdir.Unit();
  Double_t mbs  = TDatabasePDG::Instance()->GetParticle(531)->Mass() * 1000.;
  
  TVector3 K_p     = K.Vect();
  TVector3 K_pl    = K_p.Dot(bdir)*bdir;
  TVector3 K_pt    = K_p - K_pl;
  Double_t K_ptmag = K_pt.Mag();
  Double_t K_plmag = K_pl.Mag();

  TVector3 M_p     = M.Vect();
  TVector3 M_pl    = M_p.Dot(bdir)*bdir;
  TVector3 M_pt    = M_p - M_pl;
  Double_t M_ptmag = M_pt.Mag();
  Double_t M_plmag = M_pl.Mag();

  TVector3 X_p     = X.Vect();
  TVector3 X_pl    = X_p.Dot(bdir)*bdir;
  TVector3 X_pt    = X_p - X_pl;
  Double_t X_ptmag = X_pt.Mag();
  Double_t X_plmag = X_pl.Mag();
  Double_t X_E     = X.E();
  Double_t X_E2    = X_E*X_E;
  Double_t X_m2    = X.M2();

  Double_t dm2 = mbs*mbs-X_m2;

  Double_t a = 4.*(X_ptmag*X_ptmag + X_m2);
  Double_t b = 4.* X_plmag*(2.*X_ptmag*X_ptmag - dm2);
  Double_t c = 4.* X_ptmag*X_ptmag*(X_plmag*X_plmag+mbs*mbs)-dm2*dm2;
  Double_t delta = b*b-4.*a*c;
  Double_t x = 0.;
  if (delta >= 0.) x = ( -1.*b + TMath::Sqrt(delta) )/(2.*a);
  else x = -1.*b/(2.*a);
  
  TVector3 N_pl = x * bdir;
  TVector3 N_pt = -1. * X_pt;
  TVector3 N_p  = N_pl + N_pt;
  TLorentzVector N(N_p, N_p.Mag());

  TLorentzVector B = K+M+N;

  //  cout<<" X_p = "<<X.Vect().Mag()<<" pt "<<X_ptmag<<" pl "<<X_plmag<<" E "<<X_E<<" m2 "<<X_m2<<endl;
  //  cout<<" N_p = "<<N_p.Mag()<<" pt "<<N_pt.Mag()<<" pl "<<N_pl.Mag()<<" E "<<N.E()<<" m2 "<<N.M2()<<endl;
  //  cout<<" K_p = "<<K_p.Mag()<<" pt "<<K_pt.Mag()<<" pl "<<K_pl.Mag()<<" E "<<K.E()<<" m2 "<<K.M2()<<endl;
  //  cout<<"delta = "<<delta<<" Mcorr = "<<DM["Bs_MCORR"]<<" computed = "<<B.M()<<endl;
  
  //  if (delta < 0.) {nBadDelta++; return;};
  nSelected++;
  
  H1M["Nu_PT"] -> Fill(N_pt.Mag(), 1.);  
  H1M["Nu_PL"] -> Fill(N_pl.Mag(), 1.);  
  H1M["Nu_P"]  -> Fill(N_p.Mag(),  1.);  
  H1M["Nu_PE"] -> Fill(N.E(),      1.);  

  H1M["X_PT"]  -> Fill(X_pt.Mag(), 1.);  
  H1M["X_PL"]  -> Fill(X_pl.Mag(), 1.);  
  H1M["X_P"]   -> Fill(X_p.Mag(),  1.);  
  H1M["X_PE"]  -> Fill(X.E(),      1.);
  
  // H1M["K_PT"]  -> Fill(FM["kaon_m_PT"], 1.);  
  H1M["K_PT"]  -> Fill(K_pt.Mag(), 1.);  
  H1M["K_PL"]  -> Fill(K_pl.Mag(), 1.);  
  H1M["K_P"]   -> Fill(K_p.Mag(),  1.);  
  H1M["K_PE"]  -> Fill(K.E(),      1.);  

  // H1M["M_PT"]  -> Fill(DM["muon_p_PT"], 1.);  
  H1M["M_PT"]  -> Fill(M_pt.Mag(), 1.);  
  H1M["M_PL"]  -> Fill(M_pl.Mag(), 1.);  
  H1M["M_P"]   -> Fill(M_p.Mag(),  1.);  
  H1M["M_PE"]  -> Fill(M.E(),      1.);  

  H1M["M2_KP"] -> Fill(KP.M2()/1.E+06, 1.);  

  H2M["E_CorrVSNu_PT"] -> Fill(N_pt.Mag(), DM["Bs_MCORR"], 1.);
  H2M["E_CorrVSNu_PL"] -> Fill(N_pl.Mag(), DM["Bs_MCORR"], 1.);

  H2M["Bs_Regression_Q2_BEST_PT"] -> Fill(N_pt.Mag(), DM["Bs_Regression_Q2_BEST"]/1.E+06, 1.);
  H2M["Bs_Regression_Q2_BEST_PL"] -> Fill(N_pl.Mag(), DM["Bs_Regression_Q2_BEST"]/1.E+06, 1.);

  //  cout<<" KP_m2 = "<<KP.M2()/1.E+06<<" KM_m2 "<<KM.M2()/1.E+06<<endl;
  H2M["Dalit_Kpi_vs_Kmu"] -> Fill(KP.M2()/1.E+06, KM.M2()/1.E+06, 1.);
  
  return;
};

void momentumStudy::eventSummary(  )
{
  std::cout<<"|" << std::string(68 , '=')<<"]"<<std::endl;
  std::cout<<"|Total statistics  : "<<nTuplEntries
           <<"; good events : "<<nSelected
           <<"; Delta < 0 : "<<nBadDelta
           <<Form("(%.1f %%)",Double_t(nBadDelta)/nSelected*100.)
           <<std::endl;
    
};
