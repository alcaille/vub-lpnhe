
#include "motherStudy.h"

// use this order for safety on library loading
using namespace std;

void motherStudy::fillHistograms( BKGCAT fillCatMap )
{
  TDatabasePDG *databasePDG = TDatabasePDG::Instance();

  if (databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"])) == NULL)
  {
    std::cout<<"\n\n WARN : Nill pointer for pc ID : "<<IM["Bs_TRUEID"]<<"\n\n"<<std::endl;
    return;
  };
  
  switch(fillCatMap)
  {
  case BKGGOOD:
    CGood.nTot++;
    CGood.PM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]++;
    if (auto search = CGood.HM.find(databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName());
        search == CGood.HM.end())
      CGood.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()] =
        new TH1F(Form("H_motherStudy_Good_%s_%s_%s%s",
                      databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName(),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    CGood.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]->Fill(DM[varNames[what2plot].Data()],1);
    CGood.cID = fillCatMap;
    break;
  case BKGFAKE:
    CFake.nTot++;
    CFake.PM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]++;
    // Histogram not found ? do it !
    if (auto search = CFake.HM.find(databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName());
        search == CFake.HM.end())
      CFake.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()] =
        new TH1F(Form("H_motherStudy_Fake_%s_%s_%s%s",
                      databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName(),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    // Fill histogram
    CFake.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]->Fill(DM[varNames[what2plot].Data()],1);
    CFake.cID = fillCatMap;
    break;
  case BKGSUPFAKE:
    CComb.nTot++;
    CComb.PM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]++;
    if (auto search = CComb.HM.find(databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName());
        search == CComb.HM.end())
      CComb.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()] =
        new TH1F(Form("H_motherStudy_Comb_%s_%s_%s%s",
                      databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName(),
                      sampleTag.Data(), varName.Data(), (isTrimmer)?"TRIMMER":""),
                 sampleName, __NBINS__, massRange[0], massRange[1]);
    CComb.HM[databasePDG->GetParticle(TMath::Abs(IM["Bs_TRUEID"]))->GetName()]->Fill(DM[varNames[what2plot].Data()],1);
    CComb.cID = fillCatMap;
    break;
  default:
    break;
  };
  
  return;
}


void motherStudy::drawHistograms( Int_t whichOne )
{
  if (whichOne & 0x1 ) drawHistogram( "Good", CGood );
  if (whichOne & 0x2 ) drawHistogram( "Fake", CFake );
  if (whichOne & 0x4 ) drawHistogram( "Comb", CComb );

  drawGraphs( whichOne );
}

void motherStudy::drawHistogram( const char * s, Mycategories &cat )
{
  const Double_t statCut = 0.01;
  
  const Int_t nCol = 16;
  Int_t palette[nCol] = { 2, 3, 4, 6, 7, 9, 20, 25, 28, 30, 33, 38, 40, 41, 46, 49};
  
  gStyle->SetPalette(kOcean);

  cat.canvas[0] = new TCanvas(Form("%s_Hist_%s",s,sampleTag.Data()),
                              Form("HH_%s_%s",s,sampleTag.Data()), 10, 10, 1100, 800);
  cat.canvas[0]->Divide(2,1);
  cat.canvas[0]->cd(1);

  auto maxHist = 0;
  auto nLines = 0;
  
  for (const auto& [key, value] : cat.PM) 
  {
    maxHist = (maxHist<cat.HM[key]->GetMaximum())?cat.HM[key]->GetMaximum():maxHist;
    if (Double_t(value)/cat.nTot < statCut) continue;
    nLines++;
  };

  TLegend *slegend = new TLegend(0.6, 0.85-0.03*nLines, 0.85, 0.85);
  slegend->SetLineWidth(0);
  slegend->SetFillStyle(0);
  slegend->SetTextFont(62);
  slegend->SetTextSize(0.03);

  cat.SM = new THStack(Form("HStack_%s",sampleTag.Data()),Form("%s",sampleName.Data()));

  for (const auto& [key, value] : cat.PM) 
  {
    if (Double_t(value)/cat.nTot < statCut) continue; 
    cat.HM[key]->SetLineStyle(1);
    cat.HM[key]->SetLineColor(1);
    cat.HM[key]->SetFillStyle(1001);
    cat.SM->Add(cat.HM[key],"HIST E");
    slegend->AddEntry(cat.HM[key], key.c_str(), "F");
  };

  cat.SM->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 
  cat.SM->Draw("pfc");

  slegend->Draw();

  drawLHCbLogo();
  
  cat.canvas[0]->Update();

  cat.canvas[0]->cd(2);
  auto iLine(1);
  auto iCol (0);
  auto done (0);

  TString drawOpt = "HIST E";

  TLegend *hlegend = new TLegend(0.6, 0.85-0.03*nLines, 0.85, 0.85);
  hlegend->SetLineWidth(0);
  hlegend->SetFillStyle(0);
  hlegend->SetTextFont(62);
  hlegend->SetTextSize(0.03);

  for (const auto& [key, value] : cat.PM)
  {
    if (Double_t(value)/cat.nTot < statCut) continue;
    cat.HM[key]->SetMaximum(maxHist*1.1);

    cat.HM[key]->SetLineWidth(2);
    cat.HM[key]->SetLineColor(palette[iCol]);
    cat.HM[key]->SetLineStyle(iLine);
    cat.HM[key]->SetFillStyle(0);
    cat.HM[key]->SetTitle(Form("%s;%s (MeV/c^{2});Entries",sampleName.Data(), varNames[what2plot].Data())); 

    cat.HM[key]->Draw(drawOpt.Data());
    hlegend->AddEntry(cat.HM[key], key.c_str(), "L");
    if (!done) drawOpt.Append(" SAME "); done ++;

    if ( ++iCol>=nCol ){
      iCol = 0;
      iLine ++;
      if ( iLine>8 ) iLine = 1;
    };
  };
  hlegend->Draw();

  TPaveText* fitText = new TPaveText(0.15, 0.75, 0.5, 0.85, "BRNDC");
  fitText->SetFillColor(0);
  fitText->SetTextAlign(12);
  fitText->SetBorderSize(0);
  fitText->SetLineWidth(0);
  fitText->SetTextSize(0.04);
  fitText->SetFillStyle(0);
  fitText->AddText(catNames[cat.cID-1].Data());
  if (q2Flag == 1) fitText->AddText(Form("Q2 #leq %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else if (q2Flag == 2) fitText->AddText(Form("Q2 > %.1f GeV^{2}/c^{4}",__Q2_CUT_VALUE__/1E+06));
  else fitText->AddText("No cut on Q2");
  fitText->Draw();

  drawLHCbLogo();
  
  cat.canvas[0]->Update();
}

void motherStudy::drawGraphs( Int_t whichOne )
{
  if (whichOne & 0x1 ) drawGraph( "Good", CGood );
  if (whichOne & 0x2 ) drawGraph( "Fake", CFake );
  if (whichOne & 0x4 ) drawGraph( "Comb", CComb );
}


void motherStudy::drawGraph( const char * s, Mycategories &cat )
{

  Int_t i = 0;
  Int_t nx = 0;
  const Double_t statCut = 0.05;
  
  Int_t color[3] = { kRed-3, kBlue-4, kGreen-2 };
  Double_t maxVal = 0.;

  Int_t xSize = 1000;
  Int_t ySize = 800;
  if (cat.cID == 3) xSize = 1500;
  
  cat.canvas[1] = new TCanvas(Form("%s_Prop_%s",s,sampleTag.Data()),Form("%s",sampleTag.Data()), xSize, ySize);
  cat.canvas[1]->cd();
  
  /*********************************************
   *
   *  F I L T E R I N G   E V E N T 
   *
   **********/

  TPaveText* fitText = new TPaveText(0.15, 0.75, 0.85, 0.85, "BRNDC");
  fitText->SetFillColor(0);
  fitText->SetTextAlign(12);
  fitText->SetBorderSize(0);
  fitText->SetLineWidth(0);
  fitText->SetTextSize(0.025);
  fitText->SetTextColor(kBlack);
  fitText->SetFillStyle(0);
  if (cat.cID-1) fitText->AddText(Form("%s: %d events",catNames[cat.cID-1].Data(),cat.nTot));
  else fitText->AddText(Form("All statistics: %d events",CGood.nTot+CFake.nTot+CComb.nTot));

  TString addedText = "Not plotted (<0,05%): ";
  auto nadded = 0;
  for (const auto& [key, value] : cat.sM) 
  {
    if (value >= statCut) {nx++;}    
    else {
      nadded++;
      addedText.Append(Form("%s ",key.c_str(),value));
      if (!(nadded%10)) {
        fitText->AddText(addedText.Data());
        addedText.Clear();
      };
    };
  };
  if (nadded && addedText.Length()>0) fitText->AddText(addedText.Data());

  /*********************************************
   *
   *  F I L L   H I S T O G R A M
   *
   **********/
  TString X[nx];
  Double_t Y[nx];
  for (const auto& [key, value] : cat.sM) 
  {
    if (value < statCut) continue;
    
    X[i].Append(key);
    Y[i] = value;
    maxVal = (maxVal<value)?value:maxVal;
    i++;
  }
   
  gStyle->SetHistMinimumZero();
  TH1F *h1b = new TH1F(Form("HProp_%s_%s",s,sampleTag.Data()),Form("%s",sampleName.Data()),nx,0,nx);
  for (i=1; i<=nx; i++) {
    h1b->SetBinContent(i, Y[i-1]);
    h1b->GetXaxis()->SetBinLabel(i, X[i-1].Data());
  }

  h1b->SetTitle(Form("%s;;Proportion (%%)",sampleName.Data())); 
  h1b->SetLineColor(color[cat.cID-1]+3);
  h1b->SetLineWidth(2);
  h1b->SetFillColor(color[cat.cID-1]);
  h1b->SetFillStyle(1001);
  h1b->SetBarWidth(0.4);
  h1b->SetBarOffset(0.3);
  h1b->SetStats(0);
  h1b->SetMinimum(0);
  h1b->SetMaximum(Int_t(maxVal+40));
  h1b->GetXaxis()->SetLabelSize(0.05);
  h1b->GetXaxis()->SetLabelOffset(0.01);
  h1b->Draw("bar3");

  drawLHCbLogo();

  cat.canvas[1]->Update();

  Double_t x;
  Double_t y;
  TLatex l;
   
  l.SetTextSize(0.03);
  l.SetTextFont(42);
  l.SetTextAlign(21);
  l.SetTextColor(color[cat.cID-1]+1);

  for (i=1; i<=nx; i++) {
    x = h1b->GetBinCenter(i);
    y = h1b->GetBinContent(i);
    l.DrawLatex(x,y+2,Form("(%4.2f%%)",y));
  }

  TText *t2;
  
  if (cat.cID-1) {
    t2 = fitText->GetLineWith(catNames[cat.cID-1].Data());
    t2->SetTextColor(color[cat.cID-1]);
  }
  else 
  {
    t2 = fitText->GetLineWith("All statistics");
  }
  t2->SetTextSize(0.04);
   
  fitText->Draw();

  drawLHCbLogo();
  
  cat.canvas[1]->Update();

  return;
}

void motherStudy::save( )
{
  Int_t i = 0;

  for (auto &ii : CGood.canvas)
    if (ii)
      ii->SaveAs(Form("figures/motherStudy/motherStudy%d_%s_Good_Q%d_S%s_%s_%s.png", ++i,
                      sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));
  i = 0;

  for (auto &ii : CFake.canvas)
    if (ii)
      ii->SaveAs(Form("figures/motherStudy/motherStudy%d_%s_Fake_Q%d_S%s_%s_%s.png", ++i,
                      sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));
  i = 0;
  for (auto &ii : CComb.canvas)
    if (ii)
      ii->SaveAs(Form("figures/motherStudy/motherStudy%d_%s_Comb_Q%d_S%s_%s_%s.png", ++i,
                      sampleTag.Data(), q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                      varName.Data(), (isTrimmer)?"TRIMMER":""));

  TString workSpaceName;
  workSpaceName.Append(Form("ws/motherStudy_Q%d_S%s_%s%s.root",
                            q2Flag, Form("%d%d%d", cutTrimmer, cutOffline, cutFit),
                            varName.Data(), (isTrimmer)?"TRIMMER":""));
  
  RooWorkspace* wspace;
  TFile* fws = NULL;
  if (Verbose == 2) cout<<"Check work space file : "<<workSpaceName.Data()<<" exist?";
  
  if (std::filesystem::exists(workSpaceName.Data())) {
    if (Verbose == 2) cout<<" Yes it exist!"<<endl;
    fws = new TFile(workSpaceName.Data(), "UPDATE") ;
    wspace = (RooWorkspace*) fws->Get("myWS") ;
  } else {
    if (Verbose == 2) cout<<" No! create ..."<<endl;
    wspace = new RooWorkspace("myWS");
  };

  for (const auto& [key, value] : CGood.HM)
    wspace->import(*CGood.HM[key],kTRUE);

  for (const auto& [key, value] : CFake.HM)
    wspace->import(*CFake.HM[key],kTRUE);

  for (const auto& [key, value] : CComb.HM)
    wspace->import(*CComb.HM[key],kTRUE);

  wspace->writeToFile(workSpaceName.Data());

  if (Verbose == 2) wspace->Print();
    
  if (fws) fws->Close();
  
  return;
};



