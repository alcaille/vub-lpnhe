#ifndef SUPERFAKES_MIXEDEVENT_H 
#define SUPERFAKES_MIXEDEVENT_H 1
/** @class mixedEvent mixedEvent.h superFakes/mixedEvent.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-26
 */

// Include files
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <deque>
#include <unordered_map>
#include <vector>
#include <mutex>
#include <thread>
#include <chrono>

#include <TROOT.h>
#include <TRint.h>
#include <TObject.h>
#include <TTimer.h>
#include <TThread.h>
#include <TDatime.h>
#include <TMutex.h>
#include <TTree.h>
#include <TExec.h>
#include <TChain.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TDatabasePDG.h>

#include <RooWorkspace.h>

// Local
#include "superStudy.h"
#include "superBanner.h"

// use this order for safety on library loading
using namespace std;
class mixedEvent :
  public superBanner,
  public fireWorks
{
public: 
  /// Standard constructor 
  mixedEvent( ); 

  virtual ~mixedEvent( ); ///< Destructor

  void linkTree( ); 
  static void *threadTree( void * ptr ){
    mixedEvent *s = (mixedEvent *)ptr;
    s->readTree( );
    return NULL;
  };
  void readTree( );
  template<typename T>
  void attachBranch( T & );

  void eventLoop( );
  Bool_t eventSelection( );
  void fillHistograms( );
  virtual void createHistograms( );
  virtual void fillHistograms( BKGCAT b ){b = BKGUNUSED; return;};
  virtual void drawHistograms( Int_t );
  virtual void eventSummary( ){};
  virtual void save( );
  void end( );
  TH1F *drawH( Double_t );

  void SetSampleName( const char *s ){};
  void SetSampleTag ( const char *s ){};
  void SetSampleID  ( Int_t i ){};
  void SetWhat2Plot ( Int_t i ){};

  void SetVarName   ( const char *s ){varName.Append(s); return;};
  void SetIsTrimmer ( Bool_t i ){isTrimmer = i; return;};
  void SetVerbosity ( Int_t v ){Verbose = v;};
  void SetMassRange ( Float_t *m ){massRange[0] = m[0]; massRange[1] = m[1];};
  void SetCutTrimmer( Bool_t c ){cutTrimmer = c;};
  void SetBanner    ( Bool_t i ){banner = i;}; 
  void SetCutOffline( Bool_t c ){cutOffline = c;};
  void SetCutFit    ( Bool_t c ){cutFit     = c;};
  void SetCutQ2Val  ( Double_t c ){cutQ2Val = c;};
  void SetQ2ValCut  ( Int_t q ){q2Flag = q;};
  void SetLogScale  (Bool_t l){logscale = l;};
  void SetSave      (Bool_t s){saveWork = s;};
  void SetSaveMacros(Bool_t s){saveMacros = s;};
  
  Float_t  GetMiniMass  ( ){return massRange[0];};
  Float_t  GetMaxiMass  ( ){return massRange[1];};
  void     GetMassRange ( Float_t *m ){m[0] = massRange[0]; m[1] = massRange[1];};
  Int_t    GetQ2ValCut  ( ){return q2Flag;};

  TH1F *   GetHistogram();
  
protected:
  BoolMap   BM; 
  FloatMap  FM; 
  DoubleMap DM;

  Bool_t  logscale = kFALSE;

private:
  TChain *tree;
  TH1F   *histo[3];
  TCanvas *canvas;
  
  dataSampleDef dataSamples = {
    "/eos/lhcb/user/v/vincentp/vub/data/eventmixed/Clos_90000000_2016_Up_TRIMMER.root/B2XuMuNuBs2K_Tuple",
    "/eos/lhcb/user/v/vincentp/vub/data/eventmixed/Clos_90000000_2016_Down_TRIMMER.root/B2XuMuNuBs2K_Tuple"
  };

  Float_t massRange[2];
  Int_t   q2Flag;
  Bool_t  banner;
  TString varName;
  Bool_t  isTrimmer;

  Double_t cutQ2Val = __Q2_CUT_VALUE__;
  Bool_t   cutTrimmer;
  Bool_t   cutOffline;
  Bool_t   cutFit;   
  Int_t    Verbose;

  Bool_t saveWork   = kFALSE;
  Bool_t saveMacros = kFALSE;

  TThread *thread;
};


#endif // SUPERFAKES_MIXEDEVENT_H
