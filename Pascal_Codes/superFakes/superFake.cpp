
#include "superFake.h"

auto print_key_value = [](const auto& key, const auto& value)
 { 
   std::cout << " Key:[" << key << "] Value:[" << value << "]\n";
 };

superFake::superFake( TChain *t ) : superBanner ( __BANNER_SIMU__ )
{
  verbose = DUMP_INFO;

  tree = t;
  thread = NULL;

  iColor = 0;

  firstBar = 0;
  firstBan = 0;
  timer = NULL;
};

superFake::~superFake( )
{
  BM.clear();
  IM.clear();
  FM.clear();
  DM.clear();

  F3M.clear();
};

void superFake::linkTree(  )
{
  std::vector<std::string> BN[2] = {
    {"Bs_L0MuonDecision_TOS", "Bs_Hlt2TopoMu2BodyDecision_TOS", "Bs_Hlt2SingleMuonDecision_TOS", "kaon_m_isMuon"},
    {"Bs_L0MuonDecision_TOS", "Bs_Hlt2TopoMu2BodyDecision_TOS", "Bs_Hlt2SingleMuonDecision_TOS", "kaon_m_isMuon"}};
  
  std::vector<std::string> IN[2] = {
    {"Bs_BKGCAT","Bs_TRUEID", "Bs_MC_MOTHER_ID", "Bs_MC_MOTHER_KEY", "Bs_MC_GD_MOTHER_ID", "Bs_MC_GD_MOTHER_KEY",
     "muon_p_ID", "muon_p_TRUEID", "muon_p_MC_MOTHER_ID", "muon_p_MC_GD_MOTHER_ID", "muon_p_MC_GD_GD_MOTHER_ID",
     "kaon_m_ID", "kaon_m_TRUEID", "kaon_m_MC_MOTHER_ID", "kaon_m_MC_GD_MOTHER_ID", "kaon_m_MC_GD_GD_MOTHER_ID",
     "muon_p_MC_MOTHER_KEY", "muon_p_MC_GD_MOTHER_KEY", "muon_p_MC_GD_GD_MOTHER_KEY",
     "kaon_m_MC_MOTHER_KEY", "kaon_m_MC_GD_MOTHER_KEY", "kaon_m_MC_GD_GD_MOTHER_KEY"},
    {"Bs_BKGCAT", "Bs_TRUEID", "Bs_MC_MOTHER_ID", "Bs_MC_MOTHER_KEY", "Bs_MC_GD_MOTHER_ID", "Bs_MC_GD_MOTHER_KEY",
     "muon_p_ID", "muon_p_TRUEID", "muon_p_MC_MOTHER_ID", "muon_p_MC_GD_MOTHER_ID", "muon_p_MC_GD_GD_MOTHER_ID",
     "kaon_m_ID", "kaon_m_TRUEID", "kaon_m_MC_MOTHER_ID", "kaon_m_MC_GD_MOTHER_ID", "kaon_m_MC_GD_GD_MOTHER_ID",
     "muon_p_MC_MOTHER_KEY", "muon_p_MC_GD_MOTHER_KEY", "muon_p_MC_GD_GD_MOTHER_KEY",
     "kaon_m_MC_MOTHER_KEY", "kaon_m_MC_GD_MOTHER_KEY", "kaon_m_MC_GD_GD_MOTHER_KEY"}};

  std::vector<std::string> FN[2] = {
    {"Bs_ENDVERTEX_CHI2", "Bs_FD_S", "kaon_m_PAIR_M", "muon_p_PAIR_M", "TMVA_Charge_BDT_New", "TMVA_SS_Afc_BDT_New"},
    {"Bs_ENDVERTEX_CHI2", "Bs_FD_S", "kaon_m_PAIR_M", "muon_p_PAIR_M", "TMVA_Charge_BDT_New", "TMVA_SS_Afc_BDT_New"}};

  std::vector<std::string> DN[2] = {
    {"Bs_MCORR", "Bs_MCORRERR", "Bs_MM", "Bs_M", "Bs_Regression_Q2_BEST", "Mass_Kmu_asJpsi",
     "kaon_m_IsoMinBDT", "kaon_m_PX", "kaon_m_PY", "kaon_m_PZ",
     "muon_p_IsoMinBDT", "muon_p_PX", "muon_p_PY", "muon_p_PZ" },
    {"Bs_MCORR", "Bs_MCORRERR", "Bs_MM", "Bs_M", "Bs_Regression_Q2_BEST", "Mass_Kmu_asJpsi", 
     "kaon_m_IsoMinBDT", "kaon_m_PX", "kaon_m_PY", "kaon_m_PZ", 
     "muon_p_PX", "muon_p_PY", "muon_p_PZ", "muon_p_IsoMinBDT"}};
  
  for (vector<string>::iterator i = BN[isTrimmer].begin(); i != BN[isTrimmer].end(); ++i) BM[i->c_str()] = 0;
  for (vector<string>::iterator i = IN[isTrimmer].begin(); i != IN[isTrimmer].end(); ++i) IM[i->c_str()] = 0;
  for (vector<string>::iterator i = FN[isTrimmer].begin(); i != FN[isTrimmer].end(); ++i) FM[i->c_str()] = 0;
  for (vector<string>::iterator i = DN[isTrimmer].begin(); i != DN[isTrimmer].end(); ++i) DM[i->c_str()] = 0;

  tree->SetBranchStatus ( "*" , 0 );

  attachBranch( BM );
  attachBranch( IM );
  attachBranch( FM );
  attachBranch( DM );
};

template<typename T>
void superFake::attachBranch( T &b ) 
{
  for (const auto& [key, value] : b)
  {
    tree->SetBranchStatus (key.c_str() , 1 );
    tree->SetBranchAddress(key.c_str(), &b[key.c_str()]);
    if (!(&b[key.c_str()])) std::cout<<"\n\nW A R N : N I L L   P O I N T E R   4  - "<<key.c_str()<<"\n\n";
  };
};


void superFake::SetBM( std::string s)
{
  BM[s.c_str()] = 0;
  tree->SetBranchStatus (s.c_str(), 1 );
  tree->SetBranchAddress(s.c_str(), &BM[s.c_str()]);
};

void superFake::SetIM( std::string s)
{
  IM[s.c_str()] = 0;
  tree->SetBranchStatus (s.c_str(), 1 );
  tree->SetBranchAddress(s.c_str(), &IM[s.c_str()]);
};

void superFake::SetFM( std::string s)
{
  FM[s.c_str()] = 0;
  tree->SetBranchStatus (s.c_str(), 1 );
  tree->SetBranchAddress(s.c_str(), &FM[s.c_str()]);
};

void superFake::SetF3M( std::string s)
{
  tree->SetBranchStatus (s.c_str(), 1 );
  tree->SetBranchAddress(s.c_str(), &F3M[s.c_str()][3]);
};

void superFake::SetDM( std::string s)
{
  DM[s.c_str()] = 0;
  tree->SetBranchStatus (s.c_str(), 1 );
  tree->SetBranchAddress(s.c_str(), &DM[s.c_str()]);
};

template<typename T>
void superFake::dumpMap(T Map)
{
  cout<<" Map size  : "<<Map.size()<<endl;
  for (const auto& [key, value] : Map) print_key_value(key, value);
};

void superFake::dumpMap(TH1Map Map)
{
  cout<<" Map size  : "<<Map.size()<<endl;

  for (const auto& n : Map)
        print_key_value(n.first, n.second);
};

void superFake::dumpMap(F3Map Map)
{
  cout<<" Map size  : "<<Map.size()<<endl;

  // for (const auto& n : Map)
  for (auto i = Map.begin(); i != Map.end(); ++i)
  {
    Float_t *f = i->second;
    
    cout<<" First : ["<<i->first<<"] ; second : ["<<f<<"]"<<endl;
  };
  
};

void superFake::dumpMaps( )
{
  dumpMap( BM );
  dumpMap( IM );
  dumpMap( FM );
  dumpMap( DM );
};

Bool_t superFake::isMeson( Int_t id, Int_t q )
{
  return ((( TMath::Abs(id) / 100)  % 10 == q) && (( TMath::Abs(id) / 1000) % 10 == 0));
};

Bool_t superFake::isBaryon( Int_t id, Int_t q )
{
  return ((( TMath::Abs(id) / 1000) % 10 == q));
};

Bool_t superFake::isHadron( Int_t id, Int_t q )
{
  if ( isMeson (TMath::Abs(id),q) ) return(kTRUE); // Mesons
  if ( isBaryon(TMath::Abs(id),q) ) return(kTRUE); // Baryons

  return(kFALSE);
};

Bool_t superFake::eventSelection( )
{
  if (!isTrimmer) return(eventSelectionNotTrim( ));
  //  else return(eventSelectionTrimmer( ));
  else return(eventSelectionNotTrim( ));
};

Bool_t superFake::eventSelectionNotTrim( )
{
  if (cutTrimmer)
  {
    if ( DM["Bs_MCORR"] < massRange[0] ) return(kFALSE);
    if ( DM["Bs_MCORR"] > massRange[1] ) return(kFALSE);
    if (!BM["Bs_L0MuonDecision_TOS"] )   return(kFALSE);
    if (!BM["Bs_Hlt2TopoMu2BodyDecision_TOS"] && !BM["Bs_Hlt2SingleMuonDecision_TOS"]) return(kFALSE);
    if ( FM["Bs_ENDVERTEX_CHI2"] >= 4 )  return(kFALSE);
    if (TMath::Min(DM["kaon_m_IsoMinBDT"],DM["muon_p_IsoMinBDT"]) <= -0.9) return(kFALSE);
  };

  if (cutOffline)
  {
    if (BM["kaon_m_isMuon"] && ((DM["Mass_Kmu_asJpsi"] > 3072) && (DM["Mass_Kmu_asJpsi"] < 3130))) return(kFALSE);
    //    if ( BM["kaon_m_isMuon"] ) return(kFALSE);
    if ((FM["kaon_m_PAIR_M"]-892.)  <= 60. ) return(kFALSE);
    if ((FM["kaon_m_PAIR_M"]-1430.) <= 90. ) return(kFALSE);
    if ((3072. < FM["muon_p_PAIR_M"] ) && (FM["muon_p_PAIR_M"] < 3130.)) return(kFALSE);
  };

  if (cutFit)
  {
    if (((DM["kaon_m_PX"] * DM["muon_p_PX"]) < 0.) &&
        ((DM["kaon_m_PY"] * DM["muon_p_PY"]) < 0.)) return(kFALSE);
    if ( FM["Bs_FD_S"]  <= 40 ) return(kFALSE);
    // if ( DM["Bs_M"] >= 5175. ) return(kFALSE);
    // if ( DM["Bs_MCORRERR"] >= 100. ) return(kFALSE);
    if ( DM["Bs_Regression_Q2_BEST"] <= 0    ) return(kFALSE);
    // if ( FM["TMVA_Charge_BDT_New"]   <= 0.05 ) return(kFALSE);
    // if ( FM["TMVA_SS_Afc_BDT_New"]   <= 0 ) return(kFALSE);
  };

  return(kTRUE);
};
      
Bool_t superFake::eventSelectionTrimmer( )
{
  if (cutTrimmer)
  {
    if ( DM["B_MCORR"] < massRange[0] ) return(kFALSE);
    if ( DM["B_MCORR"] > massRange[1])  return(kFALSE);
    if (!BM["B_L0MuonDecision_TOS"])    return(kFALSE);
    if (!BM["B_Hlt2TopoMu2BodyDecision_TOS"] &&
        !BM["B_Hlt2SingleMuonDecision_TOS"]) return(kFALSE);
    if ( FM["B_ENDVERTEX_CHI2"] >= 4)   return(kFALSE);
    if (TMath::Min(DM["K_IsoMinBDT"],DM["mu_IsoMinBDT"]) <= -90) return(kFALSE);
  };

  if (cutOffline)
  {
    if ( BM["K_isMuon"] ) return(kFALSE);
    if ((DM["K_PAIR_M"]-892)  <= 60 ) return(kFALSE);
    if ((DM["K_PAIR_M"]-1430) <= 90 ) return(kFALSE);
    if ((3072 < DM["mu_PAIR_M"] ) && (DM["mu_PAIR_M"] < 3130)) return(kFALSE);
  };

  if (cutFit)
  {
    if ((DM["K_PX"] * DM["mu_PX"]) < 0. ) return(kFALSE);
    if ((DM["K_PY"] * DM["mu_PY"]) < 0. ) return(kFALSE);
    if ( DM["B_FD_S"]  <= 40 ) return(kFALSE);
    if ( DM["B_Regression_Q2_BEST"] <= 0 ) return(kFALSE);
    if ( DM["TMVA_Charge_BDT_New"] <= 0.05 ) return(kFALSE);
    if ( DM["TMVA_SS_Afc_BDT_New"] <= 0 ) return(kFALSE);
  };

  return(kTRUE);
};
      
#include <ranges>

void superFake::eventLoop( ) 
{
  TThread *t1 = NULL;
  TThread *t2 = NULL;
  
  if (!nTuplEntries) nTuplEntries = tree -> GetEntries();

  if(!DUMP_INFO && banner) {t1 = new TThread("barAndBanner", barAndBanner, this); t1->Run();};
  t2 = new TThread("threadTree", threadTree, this); t2->Run();

  t2 ->Join();
  
  if(!DUMP_INFO && banner) endBar ( );
  
  return;
};

/*
void superFake::endJob( ) 
{
  t2 ->Join();
  };
*/

void superFake::readTree( ) 
{
  /*********************************************************************************************
   *
   * L O O P   O N   E V E N T S
   *
   *****/
  
  Int_t whichKstr = 323;
  if      ( sampleID == BS2K1430MUNU__ ) whichKstr = 10321;
  else if ( sampleID == BS2K2MUNU_____ ) whichKstr = 325;      

  BKGCAT category = BKGUNKNOWN;

  for(iTuplEntry = 0; iTuplEntry < nTuplEntries; ++iTuplEntry)
  {
    tree->GetEntry(iTuplEntry);

    if (!eventSelection( )) continue;

    if ( q2Flag == 1 && DM["Bs_Regression_Q2_BEST"] >  __Q2_CUT_VALUE__ ) continue;

    if ( q2Flag == 2 && DM["Bs_Regression_Q2_BEST"] <= __Q2_CUT_VALUE__ ) continue;

    /*********************************************************************************************
     *
     * S T U D Y   S I M U L A T I O N   T Y P E
     *
     *****/ 

    switch (sampleID)
    {
    case BS2KMUNU______: 
    case BS2KMUNU_____T: category = selectBs2Kmunu();   break;
    case BS2DSMUNUX____: break; //Bs -> Ds K mu nu TODO
    case B2CCBARKX_____: category = selectB2ccbarKX();  break;
    case B2JPSIK_______: category = selectB2JpsiK();    break;
    case BS2JPSIPHI____: category = selectBs2Jpsiphi(); break;
    case BS2KSTRMUNU___: 
    case BS2K1430MUNU__: 
    case BS2K2MUNU_____: category = selectBs2Kstrmunu(whichKstr); break;
    case B2CKMUNUX_____: category = selectb2cKmunuX(); break;
    default:
      break;
    };

    /*********************************************************************************************
     *
     * F I L L   H I S T O G R A M S   A N D   S T A T I S T I C S
     *
     *****/
    
    nSelEntries++;
  
    fillHistograms( category );
  
  };

  if (timer)
  {
    timer -> TurnOff();
  } else if (banner) cout<<"WARNING : NILL TTimer\n";

  return;
};

void superFake::end( )
{
  if(!DUMP_INFO && banner && fireworks) {thread = new TThread("fireThread", fireThread, this); thread->Run();};
  
  eventSummary( );

  drawHistograms( 0x7 );

  if ( !DUMP_INFO && banner && fireworks && thread ) thread ->Join();

  if ( saveWork ) save( );
};

void superFake::eventDump( TString cat )
{
  std::vector<std::string> IN =
    {
      "muon_p_ID", "muon_p_TRUEID",
      "muon_p_MC_MOTHER_ID", "muon_p_MC_GD_MOTHER_ID", "muon_p_MC_GD_GD_MOTHER_ID",
      
      "kaon_m_ID", "kaon_m_TRUEID",
      "kaon_m_MC_MOTHER_ID", "kaon_m_MC_GD_MOTHER_ID", "kaon_m_MC_GD_GD_MOTHER_ID",

      "Bs_TRUEID", "Bs_MC_MOTHER_ID", "Bs_MC_GD_MOTHER_ID", 
    };
    
  TDatabasePDG *databasePDG = TDatabasePDG::Instance();

  TParticlePDG *mparticlePDG;

  Int_t expectedSize[13] = {
    4, 4, 9, 9, 9,
    4, 4, 9, 9, 9,
    9, 9, 9 };
      
  cout <<cat.Data() << setw(4) << left << IM["Bs_BKGCAT"];

  auto count = 0;
  for (vector<string>::iterator i = IN.begin(); i != IN.end(); ++i)
  {
    count++;
    if ((mparticlePDG = databasePDG->GetParticle(IM[i->c_str()])))
      std::cout<< setw(expectedSize[count-1]) << left <<mparticlePDG->GetName();
    else std::cout<< setw(8) << left <<IM[i->c_str()];
    if (!(count%5)) std::cout<<" || ";
  };
  std::cout<<std::endl;

  return;
}

