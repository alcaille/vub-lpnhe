#ifndef SUPERFAKES_SUPERBANNER_H 
#define SUPERFAKES_SUPERBANNER_H 1
/** @class superBanner superBanner.h superFakes/superBanner.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-04-22
 */

// Include files
#include <stdlib.h>
#include <cstdlib>
#include <string>
#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>

#include <TROOT.h>
#include <TRint.h>
#include <TObject.h>
#include <TTimer.h>

enum BANNER { __BANNER_DATA__ = 0, __BANNER_SIMU__ };

#include "fireWorks.h"

using namespace std;

class superBanner : public TTimer
{
public: 
  /// Standard constructor
  superBanner( ); 
  superBanner( Int_t ); 

  virtual ~superBanner( ); ///< Destructor

  void  drawBar   (   );
  void  endBar    (   );
  Int_t drawBanner( Int_t );
  void  selectBanner( Int_t * );
  TTimer *GetTimer(   ){return timer;};

  static void *barAndBanner( void * ptr ){
    superBanner *p = (superBanner *) ptr;
    p->drawBar(  ); return NULL;
  };
  static void *endBar( void * ptr ){
    superBanner *p = (superBanner *) ptr;
    p->endBar(  ); return NULL;
  };
  virtual Bool_t HandleTimer(TTimer *t) override
  {
    drawBar(  ); return kTRUE;
  };
  void SetFireWorks(Bool_t f) {fireworks = f;};
  void SetNumberEvt(Int_t n) {nTuplEntries = n;};
  

protected:
  Int_t iColor;
  Int_t firstBar = 0;
  Int_t firstBan = 0;

  TTimer *timer;
  Long64_t iTuplEntry   = 0;
  Long64_t nTuplEntries = 0;
  Long64_t nSelEntries  = 0;

  Bool_t fireworks = kFALSE;
  Int_t bannerType;
  
private:
  int wxy[2] = { 0, 0 };
};
#endif // SUPERFAKES_SUPERBANNER_H
