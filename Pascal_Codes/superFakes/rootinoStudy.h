#ifndef SUPERFAKES_ROOTINOSTUDY_H 
#define SUPERFAKES_ROOTINOSTUDY_H 1
/** @class rootinoStudy rootinoStudy.h superFakes/rootinoStudy.h
 *  
 *
 *  @author Pascal Vincent
 *  @date   2024-03-22
 */
// Include files
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <deque>
#include <unordered_map>
#include <vector>


// local
#include "superFake.h"
#include "superStudy.h"
#include "mixedEvent.h"

#define __NBINS_ROO__ 30

using namespace std;

class rootinoStudy : public superFake
{
public: 
  /// Standard constructor
  rootinoStudy( TChain * ); 
  
  virtual ~rootinoStudy( ); ///< Destructor

  void createHistograms( ) override;
  void fillHistograms( BKGCAT ) override;
  void drawHistograms( Int_t ) override;
  void save( ) override;

  void setupHistogram( string, paintHistogram );
  void fillBackgroundCat( BKGCAT );
  void drawLegend( Int_t, TH1F * ); 
  void appendPaveText( TPaveText * );
  
  void SetVerbose( Int_t v){verbose = v;};

protected:

private:
  TCanvas *canvas[4];

  TH1Map BF;
  TH1Map BC;

  TH1Map HM;
  PAINT HP;

  mixedEvent ME;

  StringMap CDESC;
  StringMap CCOND;

  Double_t cutQ2Val = __Q2_CUT_VALUE__;
};

#endif // SUPERFAKES_ROOTINOSTUDY_H
