#include <TString.h>

using namespace std;

TString path2Samples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/Initial_Samples/";
TString path2AllCutsSamples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/AllCuts/";
TString path2AllCutsMlpSamples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/AllCutsMlp/";
TString path2AllMagAllCutsSamples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/AllMagsCuts/";
TString path2AllMagAllCutsAllYearsSamples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/AllMagsCutsYears/";
TString path2DataSamples = "/eos/lhcb/user/a/alcaille/Vub/Tuples/Data/";

TString path2AllCutsSamplesRun1 = "/eos/lhcb/user/a/alcaille/Vub/Tuples/Run1/AllMags/";
TString run1TreeName = "Bs2KmuNuTuple";

TString sigTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> sigTRIMMERPathVec =
{
    {path2Samples + "Bs2KMuNu/DTT_13512010_2016_Down_TRIMMER.root",
     path2Samples + "Bs2KMuNu/DTT_13512010_2017_Down_TRIMMER.root",
     path2Samples + "Bs2KMuNu/DTT_13512010_2018_Down_TRIMMER.root"},

    {path2Samples + "Bs2KMuNu/DTT_13512010_2016_Up_TRIMMER.root",
     path2Samples + "Bs2KMuNu/DTT_13512010_2017_Up_TRIMMER.root",
     path2Samples + "Bs2KMuNu/DTT_13512010_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> sigTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KMuNu/DTT_13512010_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> sigTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KMuNu/DTT_13512010_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> sigTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KMuNu/DTT_13512010_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> sigTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KMuNu/DTT_13512010_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> sigTRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bs2KMuNu/DTT_13512010_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bs2KMuNu/DTT_13512010_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString ccbarTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> ccbarTRIMMERPathVec =
{
    {path2Samples + "B2ccbarKX/DTT_12445022_2016_Down_TRIMMER.root",
     path2Samples + "B2ccbarKX/DTT_12445022_2017_Down_TRIMMER.root",
     path2Samples + "B2ccbarKX/DTT_12445022_2018_Down_TRIMMER.root"},

    {path2Samples + "B2ccbarKX/DTT_12445022_2016_Up_TRIMMER.root",
     path2Samples + "B2ccbarKX/DTT_12445022_2017_Up_TRIMMER.root",
     path2Samples + "B2ccbarKX/DTT_12445022_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> ccbarTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "B2ccbarKX/DTT_12445022_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> ccbarTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "B2ccbarKX/DTT_12445022_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> ccbarTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "B2ccbarKX/DTT_12445022_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> ccbarTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "B2ccbarKX/DTT_12445022_Run2_AllCutsTRIMMER.root"}
};

/******************************************************************************************************************************************/

TString hbTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> hbTRIMMERPathVec =
{
    {path2Samples + "b2cMuX/DTT_10010037_2016_Down_TRIMMER.root",
     path2Samples + "b2cMuX/DTT_10010037_2017_Down_TRIMMER.root",
     path2Samples + "b2cMuX/DTT_10010037_2018_Down_TRIMMER.root"},

    {path2Samples + "b2cMuX/DTT_10010037_2016_Up_TRIMMER.root",
     path2Samples + "b2cMuX/DTT_10010037_2017_Up_TRIMMER.root",
     path2Samples + "b2cMuX/DTT_10010037_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> hbTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "b2cMuX/DTT_10010037_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "b2cMuX/DTT_10010037_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "b2cMuX/DTT_10010037_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "b2cMuX/DTT_10010037_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "b2cMuX/DTT_10010037_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "b2cMuX/DTT_10010037_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> hbTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "b2cMuX/DTT_10010037_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> hbTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "b2cMuX/DTT_10010037_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> hbTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "b2cMuX/DTT_10010037_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> hbTRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "b2cMuX/DTT_10010037_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "b2cMuX/DTT_10010037_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString kstTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> kstTRIMMERPathVec =
{
    {path2Samples + "Bs2KstMuNu/DTT_13512400_2016_Down_TRIMMER.root",
     path2Samples + "Bs2KstMuNu/DTT_13512400_2017_Down_TRIMMER.root",
     path2Samples + "Bs2KstMuNu/DTT_13512400_2018_Down_TRIMMER.root"},

    {path2Samples + "Bs2KstMuNu/DTT_13512400_2016_Up_TRIMMER.root",
     path2Samples + "Bs2KstMuNu/DTT_13512400_2017_Up_TRIMMER.root",
     path2Samples + "Bs2KstMuNu/DTT_13512400_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> kstTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2KstMuNu/DTT_13512400_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kstTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2KstMuNu/DTT_13512400_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> kstTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2KstMuNu/DTT_13512400_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kstTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2KstMuNu/DTT_13512400_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> kstTRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bs2KstMuNu/DTT_13512400_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bs2KstMuNu/DTT_13512400_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString kst2TRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> kst2TRIMMERPathVec =
{
    {path2Samples + "Bs2Kst2MuNu/DTT_13512410_2016_Down_TRIMMER.root",
     path2Samples + "Bs2Kst2MuNu/DTT_13512410_2017_Down_TRIMMER.root",
     path2Samples + "Bs2Kst2MuNu/DTT_13512410_2018_Down_TRIMMER.root"},

    {path2Samples + "Bs2Kst2MuNu/DTT_13512410_2016_Up_TRIMMER.root",
     path2Samples + "Bs2Kst2MuNu/DTT_13512410_2017_Up_TRIMMER.root",
     path2Samples + "Bs2Kst2MuNu/DTT_13512410_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> kst2TRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kst2TRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst2MuNu/DTT_13512410_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> kst2TRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst2MuNu/DTT_13512410_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kst2TRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst2MuNu/DTT_13512410_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> kst2TRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bs2Kst2MuNu/DTT_13512410_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bs2Kst2MuNu/DTT_13512410_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString kst1430TRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> kst1430TRIMMERPathVec =
{
    {path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2016_Down_TRIMMER.root",
     path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2017_Down_TRIMMER.root",
     path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2018_Down_TRIMMER.root"},

    {path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2016_Up_TRIMMER.root",
     path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2017_Up_TRIMMER.root",
     path2Samples + "Bs2Kst1430MuNu/DTT_13512420_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> kst1430TRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kst1430TRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> kst1430TRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bs2Kst1430MuNu/DTT_13512420_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kst1430TRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bs2Kst1430MuNu/DTT_13512420_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> kst1430TRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bs2Kst1430MuNu/DTT_13512420_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bs2Kst1430MuNu/DTT_13512420_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString jpsiKTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> jpsiKTRIMMERPathVec =
{
    {path2Samples + "Bu2JpsiK/DTT_12143001_2016_Down_TRIMMER.root",
     path2Samples + "Bu2JpsiK/DTT_12143001_2017_Down_TRIMMER.root",
     path2Samples + "Bu2JpsiK/DTT_12143001_2018_Down_TRIMMER.root"},

    {path2Samples + "Bu2JpsiK/DTT_12143001_2016_Up_TRIMMER.root",
     path2Samples + "Bu2JpsiK/DTT_12143001_2017_Up_TRIMMER.root",
     path2Samples + "Bu2JpsiK/DTT_12143001_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> jpsiKTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiK/DTT_12143001_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiK/DTT_12143001_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> jpsiKTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiK/DTT_12143001_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiK/DTT_12143001_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> jpsiKTRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bu2JpsiK/DTT_12142001_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bu2JpsiK/DTT_12142001_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString jpsiKstTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> jpsiKstTRIMMERPathVec =
{
    {path2Samples + "Bu2JpsiKst/DTT_12143401_2016_Down_TRIMMER.root",
     path2Samples + "Bu2JpsiKst/DTT_12143401_2017_Down_TRIMMER.root",
     path2Samples + "Bu2JpsiKst/DTT_12143401_2018_Down_TRIMMER.root"},

    {path2Samples + "Bu2JpsiKst/DTT_12143401_2016_Up_TRIMMER.root",
     path2Samples + "Bu2JpsiKst/DTT_12143401_2017_Up_TRIMMER.root",
     path2Samples + "Bu2JpsiKst/DTT_12143401_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> jpsiKstTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bu2JpsiKst/DTT_12143401_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKstTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bu2JpsiKst/DTT_12143401_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> jpsiKstTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bu2JpsiKst/DTT_12143401_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKstTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bu2JpsiKst/DTT_12143401_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> jpsiKstTRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bu2JpsiKst/DTT_12142401_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bu2JpsiKst/DTT_12142401_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString jpsiKst0TRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> jpsiKst0TRIMMERPathVec =
{
    {path2Samples + "Bd2JpsiKst/DTT_11144001_2016_Down_TRIMMER.root",
     path2Samples + "Bd2JpsiKst/DTT_11144001_2017_Down_TRIMMER.root",
     path2Samples + "Bd2JpsiKst/DTT_11144001_2018_Down_TRIMMER.root"},

    {path2Samples + "Bd2JpsiKst/DTT_11144001_2016_Up_TRIMMER.root",
     path2Samples + "Bd2JpsiKst/DTT_11144001_2017_Up_TRIMMER.root",
     path2Samples + "Bd2JpsiKst/DTT_11144001_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> jpsiKst0TRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2JpsiKst/DTT_11144001_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKst0TRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2JpsiKst/DTT_11144001_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> jpsiKst0TRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2JpsiKst/DTT_11144001_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> jpsiKst0TRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2JpsiKst/DTT_11144001_Run2_AllCutsTRIMMER.root"}
};

vector<vector<TString>> jpsiKst0TRIMMERAllMagPathVecRun1 =
{
    {path2AllCutsSamplesRun1 + "Bd2JpsiKst/DTT_11142001_2012_TRIMMER.root"},

    {path2AllCutsSamplesRun1 + "Bd2JpsiKst/DTT_11142001_2012_TRIMMER.root"},
};

/******************************************************************************************************************************************/

TString kpiTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> kpiTRIMMERPathVec =
{
    {path2Samples + "Bd2Kpi/DTT_11102003_2016_Down_TRIMMER.root",
     path2Samples + "Bd2Kpi/DTT_11102003_2017_Down_TRIMMER.root",
     path2Samples + "Bd2Kpi/DTT_11102003_2018_Down_TRIMMER.root"},

    {path2Samples + "Bd2Kpi/DTT_11102003_2016_Up_TRIMMER.root",
     path2Samples + "Bd2Kpi/DTT_11102003_2017_Up_TRIMMER.root",
     path2Samples + "Bd2Kpi/DTT_11102003_2018_Up_TRIMMER.root"}
};
vector<vector<TString>> kpiTRIMMERAllCutsPathVec =
{
    {path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2016_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2017_Down_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2018_Down_AllCutsTRIMMER.root"},

    {path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2016_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2017_Up_AllCutsTRIMMER.root",
     path2AllCutsSamples + "Bd2Kpi/DTT_11102003_2018_Up_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kpiTRIMMERAllCutsMlpPathVec =
{
    {path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2016_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2017_Down_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2018_Down_AllCutsMlpTRIMMER.root"},

    {path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2016_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2017_Up_AllCutsMlpTRIMMER.root",
     path2AllCutsMlpSamples + "Bd2Kpi/DTT_11102003_2018_Up_AllCutsMlpTRIMMER.root"}
};
vector<vector<TString>> kpiTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2018_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2016_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2017_AllCutsTRIMMER.root",
     path2AllMagAllCutsSamples + "Bd2Kpi/DTT_11102003_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> kpiTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root"},

    {path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root",
     path2AllMagAllCutsAllYearsSamples + "Bd2Kpi/DTT_11102003_Run2_AllCutsTRIMMER.root"}
};

/******************************************************************************************************************************************/

TString allBkgsTRIMMERTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> allBkgsTRIMMERAllCutsPathVec =
{
    {path2Samples + "All_Bkgs/All_B_DownkAllCutsgs_2016.root",
     path2Samples + "All_Bkgs/All_BkDown_AllCutsgs_2017.root",
     path2Samples + "All_Bkgs/All_BkDown_AllCutsgs_2018.root"},

    {path2Samples + "All_Bkgs/All_BkUp_AllCutsgs_2016.root",
     path2Samples + "All_Bkgs/All_BkUp_AllCutsgs_2017.root",
     path2Samples + "All_Bkgs/All_BkUp_AllCutsgs_2018.root"}
};
vector<vector<TString>> allBkgsTRIMMERAllMagAllCutsPathVec =
{
    {path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2016.root",
     path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2017.root",
     path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2018.root"},

    {path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2016.root",
     path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2017.root",
     path2AllMagAllCutsSamples + "All_Bkgs/All_Bkgs_2018.root"}
};
vector<vector<TString>> allBkgsTRIMMERAllMagAllCutsAllYearsPathVec =
{
    {path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root",
     path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root",
     path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root"},

    {path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root",
     path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root",
     path2AllMagAllCutsAllYearsSamples + "All_Bkgs/All_Bkgs_AllYears.root"}
};

/******************************************************************************************************************************************/

TString dataTreeName = "B2XuMuNuBs2K_Tuple";
vector<vector<TString>> dataAllMagAllCutsPathVec =
{
    {path2DataSamples + "AllMagsCuts/DTT_90000000_2016_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCuts/DTT_90000000_2017_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCuts/DTT_90000000_2018_AllCutsTRIMMER.root"},

    {path2DataSamples + "AllMagsCuts/DTT_90000000_2016_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCuts/DTT_90000000_2017_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCuts/DTT_90000000_2018_AllCutsTRIMMER.root"}
};
vector<vector<TString>> dataAllMagAllCutsAllYearsPathVec =
{
    {path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root"},

    {path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root",
     path2DataSamples + "AllMagsCutsYears/DTT_90000000_Run2_AllCutsTRIMMER.root"}
};