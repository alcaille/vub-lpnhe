#include <TString.h>

#include "samplePaths.h"
#include "commonTools.h"

using namespace std;

Float_t fu = .362;
Float_t fd = fu;
Float_t fs = .088;

/*****************************************************************************************************************/
/************************************ [B+ -> (Charmonium -> mu+ mu- X) K+ X]cc ***********************************/
/*****************************************************************************************************************/

// K1 = K1(1270)
// K0st0 = K*0(1430)0
// K0st = K*0(1430)

// Decay B
Float_t brB_JpsiK       = 1.02e-3;
Float_t brB_JpsiKpipi   = 8.1e-4;
Float_t brB_JpsiKst     = 1.43e-3;
Float_t brB0_JpsiKst0   = 1.27e-3;
Float_t brB_JpsiK1      = 1.8e-3;
Float_t brB_JpsiKeta    = 1.24e-4;
Float_t brB_JpsiKomega  = 3.20e-4;
Float_t brB_psiK        = 6.24e-4;
Float_t brB_psiKst      = 6.7e-4;
Float_t brB_psiKpipi    = 4.3e-4;
Float_t brB_chic0K      = 1.51e-4;
Float_t brB_chic1K      = 4.74e-4;
Float_t brB_chic1Kst    = 3.0e-4;
Float_t brB_chic1Kpi0   = 3.29e-4;
Float_t brB_chic1Kpipi  = 3.74e-4;
Float_t brB_chic2Kpipi  = 1.34e-4;

// Decay Kst
Float_t brKst_Kpi       = 1.;

// Decay K*0(1430)
Float_t brK0st_Kpi      = 93e-2;

// Decay K1(1270)+
Float_t brK1_Krho       = 38e-2;
Float_t brK1_K0stpi     = 28e-2;
Float_t brK1_Kstpi      = 21e-2;

Float_t brK1_X_K        = brK1_Krho + brK1_Kstpi*brKst_Kpi + brK1_K0stpi*brK0st_Kpi;

// Decay J/psi
Float_t brJpsi_mumu     = 5.961e-2;

// Decay chi_c0
Float_t brchic0_Jpsigamma = 1.4e-2;

// Decay chi_c1
Float_t brchic1_Jpsigamma = 34.3e-2;

// Decay chi_c1
Float_t brchic2_Jpsigamma = 19.e-2;

// Decay psi(2S)
Float_t brpsi_mumu       = 8.e-3;
Float_t brpsi_Jpsipipi   = 34.68e-2;
Float_t brpsi_Jpsipi0pi0 = 18.24e-2;
Float_t brpsi_Jpsieta    = 3.37e-2;
Float_t brpsi_chic0gamma = 9.79e-2;
Float_t brpsi_chic1gamma = 9.75e-2;
Float_t brpsi_chic2gamma = 9.52e-2;

Float_t brpsi_X_mumu = brpsi_mumu + (brpsi_Jpsieta+brpsi_Jpsipi0pi0+brpsi_Jpsipipi)*brJpsi_mumu +
                       (brpsi_chic0gamma*brchic0_Jpsigamma + brpsi_chic1gamma*brchic1_Jpsigamma +
                        brpsi_chic2gamma*brchic2_Jpsigamma)*brJpsi_mumu;

// Detectable Branching Ratios
Float_t brB_JpsiK_DEC       = brB_JpsiK*brJpsi_mumu;
Float_t brB_JpsiKpipi_DEC   = brB_JpsiKpipi*brJpsi_mumu;
Float_t brB_JpsiKst_DEC     = brB_JpsiKst*brJpsi_mumu*brKst_Kpi;
Float_t brB_JpsiK1_DEC      = brB_JpsiK1*brJpsi_mumu*brK1_X_K;
Float_t brB_JpsiKeta_DEC    = brB_JpsiKeta*brJpsi_mumu;
Float_t brB_JpsiKomega_DEC  = brB_JpsiKomega*brJpsi_mumu;
Float_t brB_psiK_DEC        = brB_psiK*brpsi_X_mumu;
Float_t brB_psiKst_DEC      = brB_psiKst*brpsi_X_mumu*brKst_Kpi;
Float_t brB_psiKpipi_DEC    = brB_psiKpipi*brpsi_X_mumu;
Float_t brB_chic0K_DEC      = brB_chic0K*(brchic0_Jpsigamma*brJpsi_mumu);
Float_t brB_chic1K_DEC      = brB_chic1K*(brchic1_Jpsigamma*brJpsi_mumu);
Float_t brB_chic1Kst_DEC    = brB_chic1Kst*brKst_Kpi*(brchic1_Jpsigamma*brJpsi_mumu);
Float_t brB_chic1Kpi0_DEC   = brB_chic1Kpi0*(brchic1_Jpsigamma*brJpsi_mumu);
Float_t brB_chic1Kpipi_DEC  = brB_chic1Kpipi*(brchic1_Jpsigamma*brJpsi_mumu);
Float_t brB_chic2Kpipi_DEC  = brB_chic2Kpipi*(brchic2_Jpsigamma*brJpsi_mumu);

// Inclusive Detectable Branching Ratio With Respect To The DECFILE
Float_t brB_ccbarKX_DEC =   brB_JpsiK_DEC      + brB_JpsiKpipi_DEC  + brB_JpsiKst_DEC  + brB_JpsiK1_DEC     +
                            brB_JpsiKeta_DEC   + brB_JpsiKomega_DEC + brB_psiK_DEC     + brB_psiKst_DEC     +
                            brB_psiKpipi_DEC   + brB_chic0K_DEC     + brB_chic1K_DEC   + brB_chic1Kst_DEC   +
                            brB_chic1Kpi0_DEC  + brB_chic1Kpipi_DEC + brB_chic2Kpipi_DEC;

/*****************************************************************************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

//                  {
// 0 -> MagDown ->    {0 -> 2016, 1 -> 2017, 2 -> 2018}
// 1 -> MagUp   ->    {0 -> 2016, 1 -> 2017, 2 -> 2018}
//                  }

/*****************************************************************************************************************/

TString sigLegend = "B^{0}_{s} #rightarrow K^{#pm} #mu^{#mp} #nu_{#mu}";
Float_t sigBr = 1.06e-4;
vector<vector<Float_t>> sigGenEff = {   
                                        { .20883, .20901, .20881},
                                        { .20866, .20833, .20810}
                                    };
vector<vector<Float_t>> sigRentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> sigFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> sigGenEventNum =    {
                                                { 7510909., 8096269., 10739736.},
                                                { 7512682., 8098492., 10744550.}
                                            };
// vector<vector<Float_t>> sigWeightNormed =
// {
    // {sampleWeightNormed(sigBr, sigGenEff[0][0], sigRentRate[0][0], sigFullEvCutEff[0][0], sigGenEventNum[0][0], sigTRIMMERPathVec[0][0], sigTRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(sigBr, sigGenEff[0][1], sigRentRate[0][1], sigFullEvCutEff[0][1], sigGenEventNum[0][1], sigTRIMMERPathVec[0][1], sigTRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(sigBr, sigGenEff[0][2], sigRentRate[0][2], sigFullEvCutEff[0][2], sigGenEventNum[0][2], sigTRIMMERPathVec[0][2], sigTRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(sigBr, sigGenEff[1][0], sigRentRate[1][0], sigFullEvCutEff[1][0], sigGenEventNum[1][0], sigTRIMMERPathVec[1][0], sigTRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(sigBr, sigGenEff[1][1], sigRentRate[1][1], sigFullEvCutEff[1][1], sigGenEventNum[1][1], sigTRIMMERPathVec[1][1], sigTRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(sigBr, sigGenEff[1][2], sigRentRate[1][2], sigFullEvCutEff[1][2], sigGenEventNum[1][2], sigTRIMMERPathVec[1][2], sigTRIMMERTreeName, fs, lumi[2])}
// };
// vector<vector<Float_t>> sigWeightNormedAllMag =
// {
    // {sampleWeightNormed(sigBr, sigGenEff[0][0], sigRentRate[0][0], sigFullEvCutEff[0][0], sigGenEventNum[0][0] + sigGenEventNum[1][0], sigTRIMMERAllMagPathVec[0][0], sigTRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(sigBr, sigGenEff[0][1], sigRentRate[0][1], sigFullEvCutEff[0][1], sigGenEventNum[0][1] + sigGenEventNum[1][1], sigTRIMMERAllMagPathVec[0][1], sigTRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(sigBr, sigGenEff[0][2], sigRentRate[0][2], sigFullEvCutEff[0][2], sigGenEventNum[0][2] + sigGenEventNum[1][2], sigTRIMMERAllMagPathVec[0][2], sigTRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(sigBr, sigGenEff[0][0], sigRentRate[0][0], sigFullEvCutEff[0][0], sigGenEventNum[0][0] + sigGenEventNum[1][0], sigTRIMMERAllMagPathVec[0][0], sigTRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(sigBr, sigGenEff[0][1], sigRentRate[0][1], sigFullEvCutEff[0][1], sigGenEventNum[0][1] + sigGenEventNum[1][1], sigTRIMMERAllMagPathVec[0][1], sigTRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(sigBr, sigGenEff[0][2], sigRentRate[0][2], sigFullEvCutEff[0][2], sigGenEventNum[0][2] + sigGenEventNum[1][2], sigTRIMMERAllMagPathVec[0][2], sigTRIMMERTreeName, fs, lumi[2])}
// };

/*****************************************************************************************************************/

TString hbLegend = "H_{b} #rightarrow H_{c}(#rightarrow K^{#pm} X) #mu^{#mp} X'";
Float_t hbBr = .1078*.452;
vector<vector<Float_t>> hbGenEff = {   
                                        { .42003, .41986, .42060},
                                        { .42016, .42002, .42006}
                                    };
vector<vector<Float_t>> hbRentRate =       {
                                                { 7.40e-2, 8.24e-2, 7.44e-2},
                                                { 7.40e-2, 8.23e-2, 7.43e-2}
                                            };
vector<vector<Float_t>> hbFullEvCutEff =   {
                                                { .003421, .003447, .003452},
                                                { .003440, .003442, .003442}
                                            };
vector<vector<Float_t>> hbGenEventNum =    {
                                                { 12984331., 7931920., 13005349.},
                                                { 12976284., 7916254., 13010117.}
                                            };
// vector<vector<Float_t>> hbWeightNormed =
// {
    // {sampleWeightNormed(hbBr, hbGenEff[0][0], hbRentRate[0][0], hbFullEvCutEff[0][0], hbGenEventNum[0][0], hbTRIMMERPathVec[0][0], hbTRIMMERTreeName, 1., lumi[0]),
    //  sampleWeightNormed(hbBr, hbGenEff[0][1], hbRentRate[0][1], hbFullEvCutEff[0][1], hbGenEventNum[0][1], hbTRIMMERPathVec[0][1], hbTRIMMERTreeName, 1., lumi[1]),
    //  sampleWeightNormed(hbBr, hbGenEff[0][2], hbRentRate[0][2], hbFullEvCutEff[0][2], hbGenEventNum[0][2], hbTRIMMERPathVec[0][2], hbTRIMMERTreeName, 1., lumi[2])},

//     {sampleWeightNormed(hbBr, hbGenEff[1][0], hbRentRate[1][0], hbFullEvCutEff[1][0], hbGenEventNum[1][0], hbTRIMMERPathVec[1][0], hbTRIMMERTreeName, 1., lumi[0]),
//      sampleWeightNormed(hbBr, hbGenEff[1][1], hbRentRate[1][1], hbFullEvCutEff[1][1], hbGenEventNum[1][1], hbTRIMMERPathVec[1][1], hbTRIMMERTreeName, 1., lumi[1]),
//      sampleWeightNormed(hbBr, hbGenEff[1][2], hbRentRate[1][2], hbFullEvCutEff[1][2], hbGenEventNum[1][2], hbTRIMMERPathVec[1][2], hbTRIMMERTreeName, 1., lumi[2])}
// };
// vector<vector<Float_t>> hbWeightNormedAllMag =
// {
    // {sampleWeightNormed(hbBr, hbGenEff[0][0], hbRentRate[0][0], hbFullEvCutEff[0][0], hbGenEventNum[0][0] + hbGenEventNum[1][0], hbTRIMMERAllMagPathVec[0][0], hbTRIMMERTreeName, 1., lumi[0]),
    //  sampleWeightNormed(hbBr, hbGenEff[0][1], hbRentRate[0][1], hbFullEvCutEff[0][1], hbGenEventNum[0][1] + hbGenEventNum[1][1], hbTRIMMERAllMagPathVec[0][1], hbTRIMMERTreeName, 1., lumi[1]),
    //  sampleWeightNormed(hbBr, hbGenEff[0][2], hbRentRate[0][2], hbFullEvCutEff[0][2], hbGenEventNum[0][2] + hbGenEventNum[1][2], hbTRIMMERAllMagPathVec[0][2], hbTRIMMERTreeName, 1., lumi[2])},

//     {sampleWeightNormed(hbBr, hbGenEff[0][0], hbRentRate[0][0], hbFullEvCutEff[0][0], hbGenEventNum[0][0] + hbGenEventNum[1][0], hbTRIMMERAllMagPathVec[0][0], hbTRIMMERTreeName, 1., lumi[0]),
//      sampleWeightNormed(hbBr, hbGenEff[0][1], hbRentRate[0][1], hbFullEvCutEff[0][1], hbGenEventNum[0][1] + hbGenEventNum[1][1], hbTRIMMERAllMagPathVec[0][1], hbTRIMMERTreeName, 1., lumi[1]),
//      sampleWeightNormed(hbBr, hbGenEff[0][2], hbRentRate[0][2], hbFullEvCutEff[0][2], hbGenEventNum[0][2] + hbGenEventNum[1][2], hbTRIMMERAllMagPathVec[0][2], hbTRIMMERTreeName, 1., lumi[2])}
// };

/*****************************************************************************************************************/

TString ccbarLegend = "B^{#pm} #rightarrow c#bar{c} K^{#pm} X";
Float_t ccbarBr = brB_ccbarKX_DEC;
vector<vector<Float_t>> ccbarGenEff = {   
                                        { .1384, .1380, .1411},
                                        { .1399, .1420, .1438}
                                    };
vector<vector<Float_t>> ccbarRentRate =       {
                                                { 21.05e-2, 22.23e-2, 21.00e-2},
                                                { 21.04e-2, 22.28e-2, 20.93e-2}
                                            };
vector<vector<Float_t>> ccbarFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> ccbarGenEventNum =    {
                                                { 8305526., 3002994., 8280912.},
                                                { 8281380., 3009829., 8251324.}
                                            };
// vector<vector<Float_t>> ccbarWeightNormed =
// {
    // {sampleWeightNormed(ccbarBr, ccbarGenEff[0][0], ccbarRentRate[0][0], ccbarFullEvCutEff[0][0], ccbarGenEventNum[0][0], ccbarTRIMMERPathVec[0][0], ccbarTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(ccbarBr, ccbarGenEff[0][1], ccbarRentRate[0][1], ccbarFullEvCutEff[0][1], ccbarGenEventNum[0][1], ccbarTRIMMERPathVec[0][1], ccbarTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(ccbarBr, ccbarGenEff[0][2], ccbarRentRate[0][2], ccbarFullEvCutEff[0][2], ccbarGenEventNum[0][2], ccbarTRIMMERPathVec[0][2], ccbarTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(ccbarBr, ccbarGenEff[1][0], ccbarRentRate[1][0], ccbarFullEvCutEff[1][0], ccbarGenEventNum[1][0], ccbarTRIMMERPathVec[1][0], ccbarTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(ccbarBr, ccbarGenEff[1][1], ccbarRentRate[1][1], ccbarFullEvCutEff[1][1], ccbarGenEventNum[1][1], ccbarTRIMMERPathVec[1][1], ccbarTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(ccbarBr, ccbarGenEff[1][2], ccbarRentRate[1][2], ccbarFullEvCutEff[1][2], ccbarGenEventNum[1][2], ccbarTRIMMERPathVec[1][2], ccbarTRIMMERTreeName, fu, lumi[2])}
// };
// vector<vector<Float_t>> ccbarWeightNormedAllMag =
// {
    // {sampleWeightNormed(ccbarBr, ccbarGenEff[0][0], ccbarRentRate[0][0], ccbarFullEvCutEff[0][0], ccbarGenEventNum[0][0] + ccbarGenEventNum[1][0], ccbarTRIMMERAllMagPathVec[0][0], ccbarTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(ccbarBr, ccbarGenEff[0][1], ccbarRentRate[0][1], ccbarFullEvCutEff[0][1], ccbarGenEventNum[0][1] + ccbarGenEventNum[1][1], ccbarTRIMMERAllMagPathVec[0][1], ccbarTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(ccbarBr, ccbarGenEff[0][2], ccbarRentRate[0][2], ccbarFullEvCutEff[0][2], ccbarGenEventNum[0][2] + ccbarGenEventNum[1][2], ccbarTRIMMERAllMagPathVec[0][2], ccbarTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(ccbarBr, ccbarGenEff[0][0], ccbarRentRate[0][0], ccbarFullEvCutEff[0][0], ccbarGenEventNum[0][0] + ccbarGenEventNum[1][0], ccbarTRIMMERAllMagPathVec[0][0], ccbarTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(ccbarBr, ccbarGenEff[0][1], ccbarRentRate[0][1], ccbarFullEvCutEff[0][1], ccbarGenEventNum[0][1] + ccbarGenEventNum[1][1], ccbarTRIMMERAllMagPathVec[0][1], ccbarTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(ccbarBr, ccbarGenEff[0][2], ccbarRentRate[0][2], ccbarFullEvCutEff[0][2], ccbarGenEventNum[0][2] + ccbarGenEventNum[1][2], ccbarTRIMMERAllMagPathVec[0][2], ccbarTRIMMERTreeName, fu, lumi[2])}
// };

/*****************************************************************************************************************/

TString kstLegend = "B^{0}_{s} #rightarrow K^{*#pm} #mu^{#mp} #nu_{#mu}";
Float_t kstBr = 2.5 * .33 * sigBr;
vector<vector<Float_t>> kstGenEff = {   
                                        { .18151, .18068, .18148},
                                        { .18116, .18058, .18118}
                                    };
vector<vector<Float_t>> kstRentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kstFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kstGenEventNum =    {
                                                { 4512366., 4781033., 6372027.},
                                                { 4506015., 4795732., 6417076.}
                                            };
// vector<vector<Float_t>> kstWeightNormed =
// {
    // {sampleWeightNormed(kstBr, kstGenEff[0][0], kstRentRate[0][0], kstFullEvCutEff[0][0], kstGenEventNum[0][0], kstTRIMMERPathVec[0][0], kstTRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kstBr, kstGenEff[0][1], kstRentRate[0][1], kstFullEvCutEff[0][1], kstGenEventNum[0][1], kstTRIMMERPathVec[0][1], kstTRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kstBr, kstGenEff[0][2], kstRentRate[0][2], kstFullEvCutEff[0][2], kstGenEventNum[0][2], kstTRIMMERPathVec[0][2], kstTRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kstBr, kstGenEff[1][0], kstRentRate[1][0], kstFullEvCutEff[1][0], kstGenEventNum[1][0], kstTRIMMERPathVec[1][0], kstTRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kstBr, kstGenEff[1][1], kstRentRate[1][1], kstFullEvCutEff[1][1], kstGenEventNum[1][1], kstTRIMMERPathVec[1][1], kstTRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kstBr, kstGenEff[1][2], kstRentRate[1][2], kstFullEvCutEff[1][2], kstGenEventNum[1][2], kstTRIMMERPathVec[1][2], kstTRIMMERTreeName, fs, lumi[2])}
// };
// vector<vector<Float_t>> kstWeightNormedAllMag =
// {
    // {sampleWeightNormed(kstBr, kstGenEff[0][0], kstRentRate[0][0], kstFullEvCutEff[0][0], kstGenEventNum[0][0] + kstGenEventNum[1][0], kstTRIMMERAllMagPathVec[0][0], kstTRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kstBr, kstGenEff[0][1], kstRentRate[0][1], kstFullEvCutEff[0][1], kstGenEventNum[0][1] + kstGenEventNum[1][1], kstTRIMMERAllMagPathVec[0][1], kstTRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kstBr, kstGenEff[0][2], kstRentRate[0][2], kstFullEvCutEff[0][2], kstGenEventNum[0][2] + kstGenEventNum[1][2], kstTRIMMERAllMagPathVec[0][2], kstTRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kstBr, kstGenEff[0][0], kstRentRate[0][0], kstFullEvCutEff[0][0], kstGenEventNum[0][0] + kstGenEventNum[1][0], kstTRIMMERAllMagPathVec[0][0], kstTRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kstBr, kstGenEff[0][1], kstRentRate[0][1], kstFullEvCutEff[0][1], kstGenEventNum[0][1] + kstGenEventNum[1][1], kstTRIMMERAllMagPathVec[0][1], kstTRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kstBr, kstGenEff[0][2], kstRentRate[0][2], kstFullEvCutEff[0][2], kstGenEventNum[0][2] + kstGenEventNum[1][2], kstTRIMMERAllMagPathVec[0][2], kstTRIMMERTreeName, fs, lumi[2])}
// };

/*****************************************************************************************************************/

TString kst1430Legend = "B^{0}_{s} #rightarrow K^{*#pm}(1430) #mu^{#mp} #nu_{#mu}";
Float_t kst1430Br = .93 * .33 * sigBr;
vector<vector<Float_t>> kst1430GenEff = {   
                                        { .18290, .18335, .18364},
                                        { .18279, .18322, .18342}
                                    };
vector<vector<Float_t>> kst1430RentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kst1430FullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kst1430GenEventNum =    {
                                                { 4511491., 4744676., 6324845.},
                                                { 4507605., 4745935., 6335890.}
                                            };
// vector<vector<Float_t>> kst1430WeightNormed =
// {
    // {sampleWeightNormed(kst1430Br, kst1430GenEff[0][0], kst1430RentRate[0][0], kst1430FullEvCutEff[0][0], kst1430GenEventNum[0][0], kst1430TRIMMERPathVec[0][0], kst1430TRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kst1430Br, kst1430GenEff[0][1], kst1430RentRate[0][1], kst1430FullEvCutEff[0][1], kst1430GenEventNum[0][1], kst1430TRIMMERPathVec[0][1], kst1430TRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kst1430Br, kst1430GenEff[0][2], kst1430RentRate[0][2], kst1430FullEvCutEff[0][2], kst1430GenEventNum[0][2], kst1430TRIMMERPathVec[0][2], kst1430TRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kst1430Br, kst1430GenEff[1][0], kst1430RentRate[1][0], kst1430FullEvCutEff[1][0], kst1430GenEventNum[1][0], kst1430TRIMMERPathVec[1][0], kst1430TRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kst1430Br, kst1430GenEff[1][1], kst1430RentRate[1][1], kst1430FullEvCutEff[1][1], kst1430GenEventNum[1][1], kst1430TRIMMERPathVec[1][1], kst1430TRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kst1430Br, kst1430GenEff[1][2], kst1430RentRate[1][2], kst1430FullEvCutEff[1][2], kst1430GenEventNum[1][2], kst1430TRIMMERPathVec[1][2], kst1430TRIMMERTreeName, fs, lumi[2])}
// };
// vector<vector<Float_t>> kst1430WeightNormedAllMag =
// {
    // {sampleWeightNormed(kst1430Br, kst1430GenEff[0][0], kst1430RentRate[0][0], kst1430FullEvCutEff[0][0], kst1430GenEventNum[0][0] + kst1430GenEventNum[1][0], kst1430TRIMMERAllMagPathVec[0][0], kst1430TRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kst1430Br, kst1430GenEff[0][1], kst1430RentRate[0][1], kst1430FullEvCutEff[0][1], kst1430GenEventNum[0][1] + kst1430GenEventNum[1][1], kst1430TRIMMERAllMagPathVec[0][1], kst1430TRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kst1430Br, kst1430GenEff[0][2], kst1430RentRate[0][2], kst1430FullEvCutEff[0][2], kst1430GenEventNum[0][2] + kst1430GenEventNum[1][2], kst1430TRIMMERAllMagPathVec[0][2], kst1430TRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kst1430Br, kst1430GenEff[0][0], kst1430RentRate[0][0], kst1430FullEvCutEff[0][0], kst1430GenEventNum[0][0] + kst1430GenEventNum[1][0], kst1430TRIMMERAllMagPathVec[0][0], kst1430TRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kst1430Br, kst1430GenEff[0][1], kst1430RentRate[0][1], kst1430FullEvCutEff[0][1], kst1430GenEventNum[0][1] + kst1430GenEventNum[1][1], kst1430TRIMMERAllMagPathVec[0][1], kst1430TRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kst1430Br, kst1430GenEff[0][2], kst1430RentRate[0][2], kst1430FullEvCutEff[0][2], kst1430GenEventNum[0][2] + kst1430GenEventNum[1][2], kst1430TRIMMERAllMagPathVec[0][2], kst1430TRIMMERTreeName, fs, lumi[2])}
// };

/*****************************************************************************************************************/

TString kst2Legend = "B^{0}_{s} #rightarrow K^{*#pm}_{2} #mu^{#mp} #nu";
Float_t kst2Br = .499 * .33 * sigBr;
vector<vector<Float_t>> kst2GenEff = {   
                                        { .18222, .18182, .18155},
                                        { .18139, .18235, .18267}
                                    };
vector<vector<Float_t>> kst2RentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kst2FullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kst2GenEventNum =    {
                                                { 4510710., 4732674., 6287016.},
                                                { 4507210., 4734813., 6296201.}
                                            };
// vector<vector<Float_t>> kst2WeightNormed =
// {
    // {sampleWeightNormed(kst2Br, kst2GenEff[0][0], kst2RentRate[0][0], kst2FullEvCutEff[0][0], kst2GenEventNum[0][0], kst2TRIMMERPathVec[0][0], kst2TRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kst2Br, kst2GenEff[0][1], kst2RentRate[0][1], kst2FullEvCutEff[0][1], kst2GenEventNum[0][1], kst2TRIMMERPathVec[0][1], kst2TRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kst2Br, kst2GenEff[0][2], kst2RentRate[0][2], kst2FullEvCutEff[0][2], kst2GenEventNum[0][2], kst2TRIMMERPathVec[0][2], kst2TRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kst2Br, kst2GenEff[1][0], kst2RentRate[1][0], kst2FullEvCutEff[1][0], kst2GenEventNum[1][0], kst2TRIMMERPathVec[1][0], kst2TRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kst2Br, kst2GenEff[1][1], kst2RentRate[1][1], kst2FullEvCutEff[1][1], kst2GenEventNum[1][1], kst2TRIMMERPathVec[1][1], kst2TRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kst2Br, kst2GenEff[1][2], kst2RentRate[1][2], kst2FullEvCutEff[1][2], kst2GenEventNum[1][2], kst2TRIMMERPathVec[1][2], kst2TRIMMERTreeName, fs, lumi[2])}
// };
// vector<vector<Float_t>> kst2WeightNormedAllMag =
// {
    // {sampleWeightNormed(kst2Br, kst2GenEff[0][0], kst2RentRate[0][0], kst2FullEvCutEff[0][0], kst2GenEventNum[0][0] + kst2GenEventNum[1][0], kst2TRIMMERAllMagPathVec[0][0], kst2TRIMMERTreeName, fs, lumi[0]),
    //  sampleWeightNormed(kst2Br, kst2GenEff[0][1], kst2RentRate[0][1], kst2FullEvCutEff[0][1], kst2GenEventNum[0][1] + kst2GenEventNum[1][1], kst2TRIMMERAllMagPathVec[0][1], kst2TRIMMERTreeName, fs, lumi[1]),
    //  sampleWeightNormed(kst2Br, kst2GenEff[0][2], kst2RentRate[0][2], kst2FullEvCutEff[0][2], kst2GenEventNum[0][2] + kst2GenEventNum[1][2], kst2TRIMMERAllMagPathVec[0][2], kst2TRIMMERTreeName, fs, lumi[2])},

//     {sampleWeightNormed(kst2Br, kst2GenEff[0][0], kst2RentRate[0][0], kst2FullEvCutEff[0][0], kst2GenEventNum[0][0] + kst2GenEventNum[1][0], kst2TRIMMERAllMagPathVec[0][0], kst2TRIMMERTreeName, fs, lumi[0]),
//      sampleWeightNormed(kst2Br, kst2GenEff[0][1], kst2RentRate[0][1], kst2FullEvCutEff[0][1], kst2GenEventNum[0][1] + kst2GenEventNum[1][1], kst2TRIMMERAllMagPathVec[0][1], kst2TRIMMERTreeName, fs, lumi[1]),
//      sampleWeightNormed(kst2Br, kst2GenEff[0][2], kst2RentRate[0][2], kst2FullEvCutEff[0][2], kst2GenEventNum[0][2] + kst2GenEventNum[1][2], kst2TRIMMERAllMagPathVec[0][2], kst2TRIMMERTreeName, fs, lumi[2])}
// };

/*****************************************************************************************************************/

TString jpsiKLegend = "B^{#pm} #rightarrow J/#psi K^{#pm}";
Float_t jpsiKBr = brB_JpsiK_DEC;
vector<vector<Float_t>> jpsiKGenEff = {   
                                        { .17328, .17387, .17354},
                                        { .17302, .17312, .17363}
                                    };
vector<vector<Float_t>> jpsiKRentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKGenEventNum =    {
                                                { 2008295., 2226222., 2241077.},
                                                { 2018958., 2219179., 2208325.}
                                            };
// vector<vector<Float_t>> jpsiKWeightNormed =
// {
    // {sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][0], jpsiKRentRate[0][0], jpsiKFullEvCutEff[0][0], jpsiKGenEventNum[0][0], jpsiKTRIMMERPathVec[0][0], jpsiKTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][1], jpsiKRentRate[0][1], jpsiKFullEvCutEff[0][1], jpsiKGenEventNum[0][1], jpsiKTRIMMERPathVec[0][1], jpsiKTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][2], jpsiKRentRate[0][2], jpsiKFullEvCutEff[0][2], jpsiKGenEventNum[0][2], jpsiKTRIMMERPathVec[0][2], jpsiKTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(jpsiKBr, jpsiKGenEff[1][0], jpsiKRentRate[1][0], jpsiKFullEvCutEff[1][0], jpsiKGenEventNum[1][0], jpsiKTRIMMERPathVec[1][0], jpsiKTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(jpsiKBr, jpsiKGenEff[1][1], jpsiKRentRate[1][1], jpsiKFullEvCutEff[1][1], jpsiKGenEventNum[1][1], jpsiKTRIMMERPathVec[1][1], jpsiKTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(jpsiKBr, jpsiKGenEff[1][2], jpsiKRentRate[1][2], jpsiKFullEvCutEff[1][2], jpsiKGenEventNum[1][2], jpsiKTRIMMERPathVec[1][2], jpsiKTRIMMERTreeName, fu, lumi[2])}
// };
// vector<vector<Float_t>> jpsiKWeightNormedAllMag =
// {
    // {sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][0], jpsiKRentRate[0][0], jpsiKFullEvCutEff[0][0], jpsiKGenEventNum[0][0] + jpsiKGenEventNum[1][0], jpsiKTRIMMERAllMagPathVec[0][0], jpsiKTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][1], jpsiKRentRate[0][1], jpsiKFullEvCutEff[0][1], jpsiKGenEventNum[0][1] + jpsiKGenEventNum[1][1], jpsiKTRIMMERAllMagPathVec[0][1], jpsiKTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][2], jpsiKRentRate[0][2], jpsiKFullEvCutEff[0][2], jpsiKGenEventNum[0][2] + jpsiKGenEventNum[1][2], jpsiKTRIMMERAllMagPathVec[0][2], jpsiKTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][0], jpsiKRentRate[0][0], jpsiKFullEvCutEff[0][0], jpsiKGenEventNum[0][0] + jpsiKGenEventNum[1][0], jpsiKTRIMMERAllMagPathVec[0][0], jpsiKTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][1], jpsiKRentRate[0][1], jpsiKFullEvCutEff[0][1], jpsiKGenEventNum[0][1] + jpsiKGenEventNum[1][1], jpsiKTRIMMERAllMagPathVec[0][1], jpsiKTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(jpsiKBr, jpsiKGenEff[0][2], jpsiKRentRate[0][2], jpsiKFullEvCutEff[0][2], jpsiKGenEventNum[0][2] + jpsiKGenEventNum[1][2], jpsiKTRIMMERAllMagPathVec[0][2], jpsiKTRIMMERTreeName, fu, lumi[2])}
// };

/*****************************************************************************************************************/

TString jpsiKstLegend = "B^{#pm} #rightarrow J/#psi K^{*#pm}";
Float_t jpsiKstBr = brB_JpsiKst_DEC;
vector<vector<Float_t>> jpsiKstGenEff = {   
                                        { .15854, .15854, .15854},
                                        { .15895, .15895, .15895}
                                    };
vector<vector<Float_t>> jpsiKstRentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKstFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKstGenEventNum =    {
                                                { 1504619., 1636271., 1629677.},
                                                { 1503362., 1618832., 1629677.}
                                            };
// vector<vector<Float_t>> jpsiKstWeightNormed =
// {
    // {sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][0], jpsiKstRentRate[0][0], jpsiKstFullEvCutEff[0][0], jpsiKstGenEventNum[0][0], jpsiKstTRIMMERPathVec[0][0], jpsiKstTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][1], jpsiKstRentRate[0][1], jpsiKstFullEvCutEff[0][1], jpsiKstGenEventNum[0][1], jpsiKstTRIMMERPathVec[0][1], jpsiKstTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][2], jpsiKstRentRate[0][2], jpsiKstFullEvCutEff[0][2], jpsiKstGenEventNum[0][2], jpsiKstTRIMMERPathVec[0][2], jpsiKstTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[1][0], jpsiKstRentRate[1][0], jpsiKstFullEvCutEff[1][0], jpsiKstGenEventNum[1][0], jpsiKstTRIMMERPathVec[1][0], jpsiKstTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[1][1], jpsiKstRentRate[1][1], jpsiKstFullEvCutEff[1][1], jpsiKstGenEventNum[1][1], jpsiKstTRIMMERPathVec[1][1], jpsiKstTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[1][2], jpsiKstRentRate[1][2], jpsiKstFullEvCutEff[1][2], jpsiKstGenEventNum[1][2], jpsiKstTRIMMERPathVec[1][2], jpsiKstTRIMMERTreeName, fu, lumi[2])}
// };
// vector<vector<Float_t>> jpsiKstWeightNormedAllMag =
// {
    // {sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][0], jpsiKstRentRate[0][0], jpsiKstFullEvCutEff[0][0], jpsiKstGenEventNum[0][0] + jpsiKstGenEventNum[1][0], jpsiKstTRIMMERAllMagPathVec[0][0], jpsiKstTRIMMERTreeName, fu, lumi[0]),
    //  sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][1], jpsiKstRentRate[0][1], jpsiKstFullEvCutEff[0][1], jpsiKstGenEventNum[0][1] + jpsiKstGenEventNum[1][1], jpsiKstTRIMMERAllMagPathVec[0][1], jpsiKstTRIMMERTreeName, fu, lumi[1]),
    //  sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][2], jpsiKstRentRate[0][2], jpsiKstFullEvCutEff[0][2], jpsiKstGenEventNum[0][2] + jpsiKstGenEventNum[1][2], jpsiKstTRIMMERAllMagPathVec[0][2], jpsiKstTRIMMERTreeName, fu, lumi[2])},

//     {sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][0], jpsiKstRentRate[0][0], jpsiKstFullEvCutEff[0][0], jpsiKstGenEventNum[0][0] + jpsiKstGenEventNum[1][0], jpsiKstTRIMMERAllMagPathVec[0][0], jpsiKstTRIMMERTreeName, fu, lumi[0]),
//      sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][1], jpsiKstRentRate[0][1], jpsiKstFullEvCutEff[0][1], jpsiKstGenEventNum[0][1] + jpsiKstGenEventNum[1][1], jpsiKstTRIMMERAllMagPathVec[0][1], jpsiKstTRIMMERTreeName, fu, lumi[1]),
//      sampleWeightNormed(jpsiKstBr, jpsiKstGenEff[0][2], jpsiKstRentRate[0][2], jpsiKstFullEvCutEff[0][2], jpsiKstGenEventNum[0][2] + jpsiKstGenEventNum[1][2], jpsiKstTRIMMERAllMagPathVec[0][2], jpsiKstTRIMMERTreeName, fu, lumi[2])}
// };

/*****************************************************************************************************************/

TString jpsiKst0Legend = "B^{0} #rightarrow J/#psi K^{*0}";
Float_t jpsiKst0Br = brB0_JpsiKst0*brJpsi_mumu*brKst_Kpi;
vector<vector<Float_t>> jpsiKst0GenEff = {   
                                        { .16729, .16820, .16763},
                                        { .16715, .16775, .16744}
                                    };
vector<vector<Float_t>> jpsiKst0RentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKst0FullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> jpsiKst0GenEventNum =    {
                                                { 5006384., 5400775., 5358307.},
                                                { 5020065., 5353631., 5358307.}
                                            };
// vector<vector<Float_t>> jpsiKst0WeightNormed =
// {
    // {sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][0], jpsiKst0RentRate[0][0], jpsiKst0FullEvCutEff[0][0], jpsiKst0GenEventNum[0][0], jpsiKst0TRIMMERPathVec[0][0], jpsiKst0TRIMMERTreeName, fd, lumi[0]),
    //  sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][1], jpsiKst0RentRate[0][1], jpsiKst0FullEvCutEff[0][1], jpsiKst0GenEventNum[0][1], jpsiKst0TRIMMERPathVec[0][1], jpsiKst0TRIMMERTreeName, fd, lumi[1]),
    //  sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][2], jpsiKst0RentRate[0][2], jpsiKst0FullEvCutEff[0][2], jpsiKst0GenEventNum[0][2], jpsiKst0TRIMMERPathVec[0][2], jpsiKst0TRIMMERTreeName, fd, lumi[2])},

//     {sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[1][0], jpsiKst0RentRate[1][0], jpsiKst0FullEvCutEff[1][0], jpsiKst0GenEventNum[1][0], jpsiKst0TRIMMERPathVec[1][0], jpsiKst0TRIMMERTreeName, fd, lumi[0]),
//      sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[1][1], jpsiKst0RentRate[1][1], jpsiKst0FullEvCutEff[1][1], jpsiKst0GenEventNum[1][1], jpsiKst0TRIMMERPathVec[1][1], jpsiKst0TRIMMERTreeName, fd, lumi[1]),
//      sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[1][2], jpsiKst0RentRate[1][2], jpsiKst0FullEvCutEff[1][2], jpsiKst0GenEventNum[1][2], jpsiKst0TRIMMERPathVec[1][2], jpsiKst0TRIMMERTreeName, fd, lumi[2])}
// };
// vector<vector<Float_t>> jpsiKst0WeightNormedAllMag =
// {
    // {sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][0], jpsiKst0RentRate[0][0], jpsiKst0FullEvCutEff[0][0], jpsiKst0GenEventNum[0][0] + jpsiKst0GenEventNum[1][0], jpsiKst0TRIMMERAllMagPathVec[0][0], jpsiKst0TRIMMERTreeName, fd, lumi[0]),
    //  sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][1], jpsiKst0RentRate[0][1], jpsiKst0FullEvCutEff[0][1], jpsiKst0GenEventNum[0][1] + jpsiKst0GenEventNum[1][1], jpsiKst0TRIMMERAllMagPathVec[0][1], jpsiKst0TRIMMERTreeName, fd, lumi[1]),
    //  sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][2], jpsiKst0RentRate[0][2], jpsiKst0FullEvCutEff[0][2], jpsiKst0GenEventNum[0][2] + jpsiKst0GenEventNum[1][2], jpsiKst0TRIMMERAllMagPathVec[0][2], jpsiKst0TRIMMERTreeName, fd, lumi[2])},

//     {sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][0], jpsiKst0RentRate[0][0], jpsiKst0FullEvCutEff[0][0], jpsiKst0GenEventNum[0][0] + jpsiKst0GenEventNum[1][0], jpsiKst0TRIMMERAllMagPathVec[0][0], jpsiKst0TRIMMERTreeName, fd, lumi[0]),
//      sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][1], jpsiKst0RentRate[0][1], jpsiKst0FullEvCutEff[0][1], jpsiKst0GenEventNum[0][1] + jpsiKst0GenEventNum[1][1], jpsiKst0TRIMMERAllMagPathVec[0][1], jpsiKst0TRIMMERTreeName, fd, lumi[1]),
//      sampleWeightNormed(jpsiKst0Br, jpsiKst0GenEff[0][2], jpsiKst0RentRate[0][2], jpsiKst0FullEvCutEff[0][2], jpsiKst0GenEventNum[0][2] + jpsiKst0GenEventNum[1][2], jpsiKst0TRIMMERAllMagPathVec[0][2], jpsiKst0TRIMMERTreeName, fd, lumi[2])}
// };

/*****************************************************************************************************************/

TString kpiLegend = "B^{0} #rightarrow K^{#pm} #pi^{#mp}";
Float_t kpiBr = 1.96e-5;
vector<vector<Float_t>> kpiGenEff = {   
                                        { .19570, .19655, .19575},
                                        { .19700, .19659, .19734}
                                    };
vector<vector<Float_t>> kpiRentRate =       {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kpiFullEvCutEff =   {
                                                { 1., 1., 1.},
                                                { 1., 1., 1.}
                                            };
vector<vector<Float_t>> kpiGenEventNum =    {
                                                { 2018459., 2018767., 2042238.},
                                                { 2017582., 2018039., 2093962.}
                                            };
// vector<vector<Float_t>> kpiWeightNormed =
// {
    // {sampleWeightNormed(kpiBr, kpiGenEff[0][0], kpiRentRate[0][0], kpiFullEvCutEff[0][0], kpiGenEventNum[0][0], kpiTRIMMERPathVec[0][0], kpiTRIMMERTreeName, fd, lumi[0]),
    //  sampleWeightNormed(kpiBr, kpiGenEff[0][1], kpiRentRate[0][1], kpiFullEvCutEff[0][1], kpiGenEventNum[0][1], kpiTRIMMERPathVec[0][1], kpiTRIMMERTreeName, fd, lumi[1]),
    //  sampleWeightNormed(kpiBr, kpiGenEff[0][2], kpiRentRate[0][2], kpiFullEvCutEff[0][2], kpiGenEventNum[0][2], kpiTRIMMERPathVec[0][2], kpiTRIMMERTreeName, fd, lumi[2])},

//     {sampleWeightNormed(kpiBr, kpiGenEff[1][0], kpiRentRate[1][0], kpiFullEvCutEff[1][0], kpiGenEventNum[1][0], kpiTRIMMERPathVec[1][0], kpiTRIMMERTreeName, fd, lumi[0]),
//      sampleWeightNormed(kpiBr, kpiGenEff[1][1], kpiRentRate[1][1], kpiFullEvCutEff[1][1], kpiGenEventNum[1][1], kpiTRIMMERPathVec[1][1], kpiTRIMMERTreeName, fd, lumi[1]),
//      sampleWeightNormed(kpiBr, kpiGenEff[1][2], kpiRentRate[1][2], kpiFullEvCutEff[1][2], kpiGenEventNum[1][2], kpiTRIMMERPathVec[1][2], kpiTRIMMERTreeName, fd, lumi[2])}
// };
// vector<vector<Float_t>> kpiWeightNormedAllMag =
// {
    // {sampleWeightNormed(kpiBr, kpiGenEff[0][0], kpiRentRate[0][0], kpiFullEvCutEff[0][0], kpiGenEventNum[0][0] + kpiGenEventNum[1][0], kpiTRIMMERAllMagPathVec[0][0], kpiTRIMMERTreeName, fd, lumi[0]),
    //  sampleWeightNormed(kpiBr, kpiGenEff[0][1], kpiRentRate[0][1], kpiFullEvCutEff[0][1], kpiGenEventNum[0][1] + kpiGenEventNum[1][1], kpiTRIMMERAllMagPathVec[0][1], kpiTRIMMERTreeName, fd, lumi[1]),
    //  sampleWeightNormed(kpiBr, kpiGenEff[0][2], kpiRentRate[0][2], kpiFullEvCutEff[0][2], kpiGenEventNum[0][2] + kpiGenEventNum[1][2], kpiTRIMMERAllMagPathVec[0][2], kpiTRIMMERTreeName, fd, lumi[2])},

//     {sampleWeightNormed(kpiBr, kpiGenEff[0][0], kpiRentRate[0][0], kpiFullEvCutEff[0][0], kpiGenEventNum[0][0] + kpiGenEventNum[1][0], kpiTRIMMERAllMagPathVec[0][0], kpiTRIMMERTreeName, fd, lumi[0]),
//      sampleWeightNormed(kpiBr, kpiGenEff[0][1], kpiRentRate[0][1], kpiFullEvCutEff[0][1], kpiGenEventNum[0][1] + kpiGenEventNum[1][1], kpiTRIMMERAllMagPathVec[0][1], kpiTRIMMERTreeName, fd, lumi[1]),
//      sampleWeightNormed(kpiBr, kpiGenEff[0][2], kpiRentRate[0][2], kpiFullEvCutEff[0][2], kpiGenEventNum[0][2] + kpiGenEventNum[1][2], kpiTRIMMERAllMagPathVec[0][2], kpiTRIMMERTreeName, fd, lumi[2])}
// };

/*****************************************************************************************************************/