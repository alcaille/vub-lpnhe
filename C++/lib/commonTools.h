#include <iostream>
#include <filesystem>
#include <fstream>

#include <TFile.h>
#include <TTree.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TGraphErrors.h>

#include <TMVA/Factory.h>
#include <TMVA/DataLoader.h>
#include <TMVA/Reader.h>

#include "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/C++/lib/lhcbStyle.C"

using namespace std;
using namespace ROOT;

Float_t lumi[3] = {1.67, 1.71, 2.19};
Float_t sigmabb = 144.e9;

/**
TString offlineCuts =  " !( K_isMuon & (abs(Mass_Kmu_as_mumu-3100)<30) ) & (abs(Mass_MuMuisotr_as_mumu-3096)>30) & \\
                            (abs(K_PAIR_M-892)>60 & abs(K_PAIR_M-1430)>90) & \\
                            (K_PX*mu_PX>0. || K_PY*mu_PY>0.) & B_FD_S>40 & \\
                            B_MCORR<5750 & B_M<5175 & B_Regression_Q2_BEST_LINEAR_Run2>0.";
**/

void moveFile(string from, string to)
{
    filesystem::rename(from, to);
}

void print_progress_bar(double progress)
{
    const int bar_width = 70;
    cout << "[";
    int pos = bar_width * progress;
    for (int i = 0; i < bar_width; ++i)
    {
        if (i < pos) cout << "=";
        else if (i == pos) cout << ">";
        else cout << " ";
    }
    cout << "] " << int(progress * 100.0) << " %\r";
    cout.flush();
}

void copyTreeConds(vector<TString> originFilePath, vector<TString> filtredFilePath,
                          TString conds, TString treeName) {

    for(Int_t i=0; i<originFilePath.size(); i++)
    {
        TFile *originFile = TFile::Open(originFilePath[i], "READ");
        TTree *originTree = (TTree*)originFile->Get(treeName);

        originTree->SetAutoSave(0); // No backup cycle in the root files

        TFile *filtredFile = new TFile(filtredFilePath[i], "RECREATE");
        TTree *filtredTree = originTree->CopyTree(conds);

        filtredTree->Write();

        filtredFile->Close();

        cout << filtredFilePath[i] << " Done !" << endl;
    }

}

void removeBranch(TString magPol, TString year, vector<vector<vector<TString>>> sampPaths, TString treeName, TString branchName)
{
    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    for(int i=0; i<sampPaths.size(); i++)
    {
        TFile *input = TFile::Open(sampPaths[i][magPolInt][yearInt]);
        TTree *inputtree; input->GetObject(treeName,inputtree);

        TFile *output = TFile::Open(sampPaths[i][magPolInt][yearInt],"RECREATE");
        inputtree->SetBranchStatus(branchName,0);
        TTree *outputtree = inputtree->CloneTree(-1,"fast");

        input->Close();
        delete input;

        output->Write();

        output->Close();

        cout<<sampPaths[i][magPolInt][yearInt]<<"  Branch : " + branchName + "  Removed" << "\n"<<endl;
    }
}

int nEventTuple(TString rootPath, TString treeName)
{
    TFile *myFile = TFile::Open(rootPath);
    TTree *myTree = (TTree*)myFile->Get(treeName);
    Int_t Nevtuple = myTree->GetEntries();
    myFile->Close();
    return Nevtuple;
}

int nEventTupleCond(TString rootPath, TString treeName, TString conds)
{
    TFile *myFile = TFile::Open(rootPath);
    TTree *myTree = (TTree*)myFile->Get(treeName);
    Int_t NevtupleConds = myTree->GetEntries(conds);
    myFile->Close();
    return NevtupleConds;
}

Float_t sampleWeight(   Float_t decayBr, Float_t decayGenEff, Float_t decayRentRate,
                        Float_t decayFullEvCutEff, Float_t decayGenEventNum, TString samplePath,
                        TString treeName, Float_t f, Float_t L)
{
    Float_t decayTupleEventNum = nEventTuple(samplePath, treeName);
    return  L * sigmabb * f * decayBr * decayGenEff * decayRentRate * decayFullEvCutEff *
            (decayTupleEventNum / decayGenEventNum);
}

Float_t sampleWeightNormed( Float_t decayBr, Float_t decayGenEff, Float_t decayRentRate,
                            Float_t decayFullEvCutEff, Float_t decayGenEventNum, TString samplePath,
                            TString treeName, Float_t f, Float_t L)
{
    Float_t decayTupleEventNum = nEventTuple(samplePath, treeName);
    return sampleWeight( decayBr, decayGenEff, decayRentRate, decayFullEvCutEff, decayGenEventNum, samplePath, treeName, f, L) / decayTupleEventNum;
}

Float_t err_SSB(Float_t S, Float_t B)
{
    return abs( 1/sqrt(S+B) - S / (2 * pow(sqrt(S+B), 3)) ) * sqrt(S) + ( S / (2 * pow(sqrt(S+B), 3)) ) * sqrt(B);
}

Float_t err_SB(Float_t S, Float_t B)
{
    return sqrt(S) / sqrt(B) + S / (2 * B);
}

Float_t* linspace(Float_t start, Float_t end, Int_t numPoints)
{
    Float_t* result = new Float_t[numPoints];
    Float_t step = (end - start) / (numPoints - 1);

    for (Int_t i = 0; i <= numPoints - 1; ++i)
    {
        result[i] = start + i * step;
    }

    return result;
}

Float_t Normed_S_or_B_With_Response_Cut(TString rootFileName, Float_t Norm_W, Float_t response_Cut, TString weightFile, TString treeName,
                                        vector<TString> condsVar, TString conds, vector<TString> readVarsFloat, vector<TString> readVarsDouble)
{  
    if(conds!=""){conds = "&&(" + conds + ")";}

    TMVA::Reader *reader = new TMVA::Reader( "!V:!Color" );

    Float_t  floatVars[readVarsFloat.size()];

    Float_t  floatedDoubleVars[readVarsDouble.size()];
    Double_t doubleVars[readVarsDouble.size()];

    for(Int_t p=0 ; p<readVarsFloat.size() ; p++)
    {
        reader->AddVariable(readVarsFloat[p], &floatVars[p]);
    }
    for(Int_t l=0 ; l<readVarsDouble.size() ; l++)
    {
        reader->AddVariable(readVarsDouble[l], &floatedDoubleVars[l]);
    }

    reader->BookMVA( "MVA", weightFile );

    TFile* rootFile = TFile::Open(rootFileName, "READ");
    TTree* myTree   = (TTree*)rootFile->Get(treeName);

    myTree->SetBranchStatus("*", 0);

    for(Int_t i=0; i<condsVar.size(); i++)
    {
        myTree->SetBranchStatus(condsVar[i], 1);
    }
    for(Int_t j=0 ; j<readVarsFloat.size() ; j++)
    {
        myTree->SetBranchStatus(readVarsFloat[j], 1);
        myTree->SetBranchAddress(readVarsFloat[j], &floatVars[j]);
    }
    for(Int_t m=0 ; m<readVarsDouble.size() ; m++)
    {
        myTree->SetBranchStatus(readVarsDouble[m], 1);
        myTree->SetBranchAddress(readVarsDouble[m], &doubleVars[m]);
    }

    Int_t condsEntries = 0;

    for(Int_t n=0 ; n<myTree->GetEntries() ; n++)
    {
        myTree->GetEntry(n);

        for(Int_t n=0 ; n<readVarsDouble.size() ; n++)
        {
            floatedDoubleVars[n] = doubleVars[n];
        }

        if(reader->EvaluateMVA("MVA")>response_Cut)
        {
            condsEntries++;
            cout << condsEntries << endl;
        }

    }

    Float_t SB = condsEntries * Norm_W ;

    delete myTree;
    delete rootFile;

    return SB;
}

void Bs_MCORR_Plot( TString magPol, TString year, vector<vector<vector<TString>>> sampleVec, vector<vector<TString>> dataSample, vector<TString> legList,
                    vector<Int_t> colorList, Int_t binNum, TString conds, Float_t xRange[2], Float_t timesMax, TString savePath)
    {

    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    lhcbStyle();

    TLegend* legend = new TLegend(0.63, 0.685, 0.89, 0.915);
    legend->SetLineWidth(0);
    legend->SetFillStyle(0);
    legend->SetTextSize(.02);

    Int_t sampNumb = sampleVec.size();

    THStack* histStack = new THStack("histStack", "Bs_MCORR");

    TH1F* histVec[sampNumb];
    TH1F* dataHist;

    if(dataSample[magPolInt][yearInt]!="")
    {
        TFile* dataFile = new TFile(dataSample[magPolInt][yearInt]);
        TTree* dataTree = (TTree*)dataFile->Get("B2XuMuNuBs2K_Tuple");

        dataHist = new TH1F("dataHist", "", binNum, xRange[0], xRange[1]);
        dataHist->SetAxisRange(xRange[0], xRange[1]);

        if(conds!="")
        {
            dataTree->Project("dataHist", "B_MCORR", "Weight_Normed*(" + conds + ")");
        }
        else
        {
            dataTree->Project("dataHist", "B_MCORR", "Weight_Normed");
        }

        legend->AddEntry(dataHist, "Data", "lep");
    }

    for(Int_t i=0; i<sampNumb; i++){

        TFile* file = new TFile(sampleVec[i][magPolInt][yearInt]);
        TTree* tree = (TTree*)file->Get("B2XuMuNuBs2K_Tuple");

        histVec[i] = new TH1F(Form("Hist %i", i), "", binNum, xRange[0], xRange[1]);
        histVec[i]->SetAxisRange(xRange[0], xRange[1]);

        if(conds!="")
        {
            tree->Project(Form("Hist %i", i), "B_MCORR", "Weight_Normed*(" + conds + ")");
        }
        else
        {
            tree->Project(Form("Hist %i", i), "B_MCORR", "Weight_Normed");
        }

        histVec[i]->SetFillColor(colorList[i]);
        histVec[i]->SetLineWidth(0);

        histStack->Add(histVec[i], "HIST");
        legend->AddEntry(histVec[i], legList[i], "F");

    }

    Int_t maxBin = round(histStack->GetMaximum());

    histStack->SetMaximum(maxBin * timesMax);

    histStack->SetTitle(";M_{corr} [MeV/c^{2}]");

    TCanvas *canvas = new TCanvas("canvas", "Canvas", 1400, 1080);

    histStack->Draw();

    if(dataSample[magPolInt][yearInt]!="")
    {
        dataHist->Draw("E1 && SAME");
    }

    TPaveText* lhcbText = new TPaveText(.17, .855, .32, .925, "BRNDC");
    lhcbText->SetFillColor(0);
    lhcbText->SetTextAlign(12);
    lhcbText->SetBorderSize(0);
    lhcbText->SetLineWidth(0);
    lhcbText->AddText("LHCb MC");
    lhcbText->Draw();

    legend->Draw();

    canvas->SaveAs( savePath );

    canvas->Close();

}