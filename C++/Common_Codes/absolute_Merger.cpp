#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TList.h>

#include "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/lib/decaysEffInfo.h"

using namespace std;

void absolute_Merger_Func(vector<TString> inputFiles, TString outputFileName, TString treeName)
{
    TList* list = new TList;
    TFile* fileList[inputFiles.size()];
    TTree* treeList[inputFiles.size()];

    for(Int_t i=0 ; i<inputFiles.size() ; i++)
    {
        fileList[i] = new TFile(inputFiles[i], "READ");
        treeList[i] = (TTree*)fileList[i]->Get(treeName);
        list->Add(treeList[i]);
    }

    // Open the output file in write mode
    TFile* outputFile = new TFile(outputFileName, "RECREATE");

    TTree* outputTree = TTree::MergeTrees(list);

    outputTree->SetName(treeName);
    outputFile->Write();
    outputFile->Close();

    cout << "Merging complete. Merged TTree saved in " << outputFile->GetName() << endl;
}

Int_t absolute_Merger() {

    TString treeName = sigTRIMMERTreeName;

    vector<TString> inputFiles =
    {
        sigTRIMMERAllMagAllCutsPathVec[0][0],
        sigTRIMMERAllMagAllCutsPathVec[0][1],
        sigTRIMMERAllMagAllCutsPathVec[0][2]
    };

    TString outputFile = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MC/test.root";

    absolute_Merger_Func(inputFiles, outputFile, treeName);

    // vector<TString> inputFiles2 =
    // {
    //     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_11144001_Bd_JpsiKst_mm_DecProdCut_Dn_Py8_MC12.root",
    //     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_11144001_Bd_JpsiKst_mm_DecProdCut_Up_Py8_MC12.root",
    // };

    // TString outputFile2 = "/eos/lhcb/user/a/alcaille/Vub/Tuples/Run1/jpsiKst0.root";

    // absolute_Merger_Func(inputFiles2, outputFile2, treeName);

    // vector<TString> inputFiles3 =
    // {
    //     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/new/DTT_90000000_2018_Down_TRIMMER.root",
    //     "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/Data/new/DTT_90000000_2018_Up_TRIMMER.root",
    // };

    // TString outputFile3 = dataAllMagPathVec[0][2];

    // absolute_Merger_Func(inputFiles3, outputFile3, treeName);

    // vector<TString> inputFiles1 =
    // {
    //     ccbarTRIMMERAllMagPathVec[0][1],
    //     hbTRIMMERAllMagPathVec[0][1],
    //     kpiTRIMMERAllMagPathVec[0][1],
    //     jpsiKst0TRIMMERAllMagPathVec[0][1],
    //     kstTRIMMERAllMagPathVec[0][1],
    //     kst2TRIMMERAllMagPathVec[0][1],
    //     kst1430TRIMMERAllMagPathVec[0][1]
    // };

    // vector<Float_t> weights1 =
    // {
    //     ccbarWeightNormedAllMag[0][1],
    //     hbWeightNormedAllMag[0][1],
    //     kpiWeightNormedAllMag[0][1],
    //     jpsiKst0WeightNormedAllMag[0][1],
    //     kstWeightNormedAllMag[0][1],
    //     kst2WeightNormedAllMag[0][1],
    //     kst1430WeightNormedAllMag[0][1]
    // };

    // TString outputFile1 = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MergedMag/All_Bkgs/All_Bkgs_2017.root";

    // absolute_Merger_Func(inputFiles1, outputFile1, treeName);

    // vector<TString> inputFiles2 =
    // {
    //     ccbarTRIMMERAllMagPathVec[0][2],
    //     hbTRIMMERAllMagPathVec[0][2],
    //     kpiTRIMMERAllMagPathVec[0][2],
    //     jpsiKst0TRIMMERAllMagPathVec[0][2],
    //     kstTRIMMERAllMagPathVec[0][2],
    //     kst2TRIMMERAllMagPathVec[0][2],
    //     kst1430TRIMMERAllMagPathVec[0][2]
    // };

    // vector<Float_t> weights2 =
    // {
    //     ccbarWeightNormedAllMag[0][2],
    //     hbWeightNormedAllMag[0][2],
    //     kpiWeightNormedAllMag[0][2],
    //     jpsiKst0WeightNormedAllMag[0][2],
    //     kstWeightNormedAllMag[0][2],
    //     kst2WeightNormedAllMag[0][2],
    //     kst1430WeightNormedAllMag[0][2]
    // };

    // TString outputFile2 = "/eos/lhcb/user/a/alcaille/Vub/Tuples/MergedMag/All_Bkgs/All_Bkgs_2018.root";

    // absolute_Merger_Func(inputFiles2, outputFile2, treeName);

    return 0;
}
