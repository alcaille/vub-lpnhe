#include <iostream>

#include <TFile.h>
#include <TTree.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TH1.h>
#include <THStack.h>
#include <TROOT.h>
#include "/opt/homebrew/Cellar/root/6.28.06/share/root/macros/lhcbStyle.C"

void Plot_weighted_Response_Func( std::vector<const char*> rootList, std::vector<const char*> legList, std::vector<int> colorList, bool weightorNo, int binNum, const char* Resp_Name, const char* treeName, const char* conds, float xRange[2], const char* savePath) {

    float lumi = 5.57;
    float sigmabb = 144.e9;

    if(weightorNo){
        binNum = binNum * 4;
    }

    gROOT->ProcessLine(".L lhcbStyle.C");
    lhcbStyle();

    int sampNumb = rootList.size();

    THStack* histStack = new THStack("histStack", Resp_Name);

    TH1F* histVec[sampNumb];

    TCanvas *canvas = new TCanvas("canvas", "Canvas", 1300, 1080);

    TLegend* legend = new TLegend(0.66, 0.695, 0.92, 0.915);
    legend->SetLineWidth(0);

    for(int i=0; i<sampNumb; i++) {

        TFile* file = new TFile(rootList[i]);
        TTree* tree = (TTree*)file->Get(treeName);

        histVec[i] = new TH1F(Form("Hist %i", i), "", binNum, xRange[0], xRange[1]);
        histVec[i]->SetAxisRange(xRange[0], xRange[1]);

        if(weightorNo) {

           tree->Project(Form("Hist %i", i), Resp_Name, Form("%f*%f*Weight_Norm%s", lumi, sigmabb, conds));
           canvas->SetLogy();

        } else {

            tree->Project(Form("Hist %i", i), Resp_Name, conds);
            histVec[i]->Scale(1./histVec[i]->Integral());

        }

        histVec[i]->Sumw2(kFALSE);

        histVec[i]->SetFillStyle(0);

    }

    for(int j=0; j<sampNumb ; j++){
        int k = sampNumb-1-j;

        if(!weightorNo){
            histVec[k]->SetLineStyle(1);
            histVec[k]->SetLineWidth(1);
            histVec[k]->SetLineColor(colorList[k]);
            histStack->Add(histVec[k], "HIST");
            legend->AddEntry(histVec[j], legList[j], "L");
        }else{
            histVec[k]->SetMarkerStyle(20);
            histVec[k]->SetMarkerSize(.6);
            histVec[k]->SetMarkerColor(colorList[k]);
            histStack->Add(histVec[k], "P");
            histStack->SetMinimum(1);
            legend->AddEntry(histVec[j], legList[j], "P");
        }
    }

    float maxYTable[sampNumb];

    for(int l=0 ; l<sampNumb ; l++){
        maxYTable[l] = histVec[l]->GetMaximum();
    }

    float maxY = *std::max_element(maxYTable, maxYTable + sampNumb);

    if(weightorNo){
        maxY = 100 * maxY;
    } else {
        maxY = 1.5 * maxY;
    }

    histStack->SetMaximum(maxY);

    histStack->Draw("nostack");

    histStack->SetTitle(Resp_Name);
    histStack->GetXaxis()->SetTitle("MVA Response");

    TPaveText* lhcbText = new TPaveText(.17, .855, .32, .925, "BRNDC");
    lhcbText->SetFillColor(0);
    lhcbText->SetTextAlign(12);
    lhcbText->SetBorderSize(0);
    lhcbText->SetLineWidth(0);
    lhcbText->AddText("LHCb MC");

    lhcbText->Draw();

    //canvas->SetGrid(1,0);

    legend->Draw();

    if(weightorNo){
        canvas->SaveAs(Form("%s_Log.pdf",savePath));
    } else {
        canvas->SaveAs(Form("%s.pdf",savePath));
    }


    canvas->Close();

}

void Plot_weighted_Response() {

    const char* treeName = "DecayTree";

    float xRange[2] = {0, 1};

    int binNum = 50;

    const char* conds = "";
    const char* condsWeighted;

    if(std::strcmp(conds, "") == 0){
        condsWeighted = conds;
    } else {
        condsWeighted = Form("*%s", conds);
    }

    const char* Resp_Name = "MLP_Resp_MinIso_SvsJpsiK_ccbar";
    
    const char* rootFile1 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Signal/Bs2KMuNu.root";

    const char* rootFile2 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_10010037_2016_MagUp_TRIMMER.root";
    const char* rootFile3 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_12445022_2016_MagUp_TRIMMER.root";
    const char* rootFile4 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_12143001_2016_MagUp_TRIMMER.root";
    const char* rootFile5 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_13144001_2016_MagUp_TRIMMER.root";
    

    const char* rootFile6 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kstmunu_MagUp_2016_TRIMMER.root";
    const char* rootFile7 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kst1430munu_MagUp_2016_TRIMMER.root";
    const char* rootFile8 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kst2munu_MagUp_2016_TRIMMER.root";

    const char* l1 = "B^{0}_{s} #rightarrow K^{-} #mu^{+} #nu";
    const char* l2 = "H_{b} #rightarrow H_{c}(#rightarrow K^{+}X)#mu^{+}X'";
    const char* l3 = "B^{+} #rightarrow c #bar{c} K X";
    const char* l4 = "B^{+} #rightarrow J/#psi K^{+}";
    const char* l5 = "B^{0}_{s} #rightarrow J/#psi #phi";

    const char* l6 = "B^{0}_{s} #rightarrow K^{*-} #mu^{+} #nu";
    const char* l7 = "B^{0}_{s} #rightarrow K^{*-}(1430) #mu^{+} #nu";
    const char* l8 = "B^{0}_{s} #rightarrow K^{*-}_{2} #mu^{+} #nu";

    int c1 = 1;
    int c2 = 2;
    int c3 = 51;
    int c4 = 4;
    int c5 = 3;

    int c6 = 28;
    int c7 = 7;
    int c8 = 92;

    std::vector<const char*> rootList;
    rootList.push_back(rootFile1);
    rootList.push_back(rootFile2);
    rootList.push_back(rootFile3);
    rootList.push_back(rootFile4);
    //rootList.push_back(rootFile5);
    rootList.push_back(rootFile6);
    rootList.push_back(rootFile7);
    rootList.push_back(rootFile8);

    std::vector<const char*> legList;
    legList.push_back(l1);
    legList.push_back(l2);
    legList.push_back(l3);
    legList.push_back(l4);
    //legList.push_back(l5);
    legList.push_back(l6);
    legList.push_back(l7);
    legList.push_back(l8);
    

    std::vector<int> colorList;
    colorList.push_back(c1);
    colorList.push_back(c2);
    colorList.push_back(c3);
    colorList.push_back(c4);
    //colorList.push_back(c5);
    colorList.push_back(c6);
    colorList.push_back(c7);
    colorList.push_back(c8);

    const char* savePath = "/Users/acaillet/Documents/PhD/Root_stage_M2/TMVA/Run2/BDT_TMVA_Run2/MLP_MinIso_SvsJpsiK_ccbar/PDF/Resp_MLP_MinIso_SigvsJpsiK_ccbar";

    Plot_weighted_Response_Func(rootList, legList, colorList, false, binNum, Resp_Name, treeName, conds, xRange, savePath);
    Plot_weighted_Response_Func(rootList, legList, colorList, true, binNum, Resp_Name, treeName, condsWeighted, xRange, savePath);
    
}