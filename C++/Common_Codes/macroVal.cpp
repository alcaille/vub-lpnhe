#include <TString.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <THStack.h>
#include <TLegend.h>
#include <TAttFill.h>
#include <TDirectoryFile.h>
#include <TStyle.h>
#include <string>
#include <iostream>

int binNumber = 200;

void macroVal_Func( std::vector<const char*> rootList, std::vector<const char*> legList, std::vector<int> colorList, const char* treeName,
                     const char* variable,float xyRange[2], int binNum, const char* conds, float xPad, const char* savePath) {

    TCanvas *canvas = new TCanvas("canvas", "Canvas", 1920, 1080);

    gStyle->SetOptStat(0);
    gStyle->SetLineScalePS(1.5);

    //TLegend* legend = new TLegend(0.7, 0.6, 0.9, 0.9);
    TLegend* legend = new TLegend(xPad, .6, xPad + .2, .9);
    legend->SetTextSize(0.03);

    float maxBinValue = 0;

    TH1F* histList[rootList.size()];

    for(int i=0; i<rootList.size(); i++) {

        TFile* file = new TFile(rootList[i]);

        TTree* tree = (TTree*)file->Get(treeName);

        histList[i] = new TH1F(Form("histList[i]%i", i), "", binNum, xyRange[0], xyRange[1]);

        tree->Project(histList[i]->GetName(), variable, conds);

        histList[i]->GetXaxis()->SetTitle(variable);

        histList[i]->SetTitle(conds);

        histList[i]->SetLineColor(colorList[i]);

        histList[i]->Scale(1./histList[i]->Integral());

        histList[i]->Sumw2(kFALSE);

        histList[i]->SetFillStyle(0);

        if(colorList[i]==0) {

            histList[i]->SetLineWidth(3);
            histList[i]->SetMarkerColor(colorList[i]);
            histList[i]->SetMarkerStyle(20);
            histList[i]->SetMarkerSize(1);
            histList[i]->Draw("EP");

        }

        else {

            histList[i]->SetLineWidth(2);
            histList[i]->Draw("same");

        }

        legend->AddEntry(histList[i]->GetName(), legList[i], "l");

        if(maxBinValue<histList[i]->GetMaximum()){

            maxBinValue = histList[i]->GetMaximum();

        }

    }

    for(int j=0; j<rootList.size(); j++){

        histList[j]->GetYaxis()->SetRangeUser(0, maxBinValue*1.1);

    }

    canvas->SetGrid();

    legend->Draw();

    canvas->SaveAs(savePath);

    delete canvas;

}

void macroVal() {

    const char* folderPath = "/Users/acaillet/Documents/PhD/Root_stage_M2/Tools_PDF/Var_Compar/MinIso/MLP/All_q2/S_vs_Bu2JpsiK/S_vs_Hb2Hc/S_vs_Bu2ccbar/S_vs_Bs2Ksts/No_More_Cut";

    std::vector<std::tuple<const char*, const char*, float, float, float>> varTab;

    varTab.push_back(std::make_tuple("Bs_MCORR"                              , "Bs_MCORR"                         , .1, 2500 , 6000 ));
    varTab.push_back(std::make_tuple("Bs_cosTheta1_star"                     , "Bs_cosTheta1_star"                , .7, -1   , 1    ));
    varTab.push_back(std::make_tuple("Bs_ENDVERTEX_CHI2"                     , "Bs_ENDVERTEX_CHI2"                , .7, 0.   , 4    ));
    varTab.push_back(std::make_tuple("Bs_PT"                                 , "Bs_PT"                            , .7, 0.   , 20000));
    varTab.push_back(std::make_tuple("Bs_PT-1.5*muon_p_PT"                   , "Bs_PT-1.5*muon_p_PT"              , .7, -8000, 8000 ));
    varTab.push_back(std::make_tuple("kaon_m_0_50_cc_IT"                     , "kaon_m_0_50_cc_IT"                , .1, 0    , 1    ));
    varTab.push_back(std::make_tuple("kaon_m_0_50_IT"                        , "kaon_m_0_50_IT"                   , .1, 0    , 1    ));
    varTab.push_back(std::make_tuple("kaon_m_IsoMinBDT-muon_p_IsoMinBDT"     , "kaon_m_IsoMinBDT-muon_p_IsoMinBDT", .7, -1.  , 1    ));
    varTab.push_back(std::make_tuple("kaon_m_PAIR_M"                         , "kaon_m_PAIR_M"                    , .7, 500  , 4000 ));
    varTab.push_back(std::make_tuple("kaon_m_PT"                             , "kaon_m_PT"                        , .7, 1000 , 6000 ));
    varTab.push_back(std::make_tuple("max(kaon_m_IsoSumBDT,muon_p_IsoSumBDT)", "maxIsoSum"                        , .7, -.4  , 0    ));
    varTab.push_back(std::make_tuple("min(kaon_m_ConeIso,muon_p_ConeIso)"    , "minConeIso"                       , .1, 0    , 1    ));
    varTab.push_back(std::make_tuple("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)", "minIsoMin"                        , .7, -1.  , 1    ));
    varTab.push_back(std::make_tuple("muon_p_0_50_cc_asy_P"                  , "muon_p_0_50_cc_asy_P"             , .1, -.6  , 1    ));
    varTab.push_back(std::make_tuple("muon_p_PAIR_M"                         , "muon_p_PAIR_M"                    , .4, 0    , 4000 ));
    varTab.push_back(std::make_tuple("kaon_m_0_50_cc_deltaEta"               , "kaon_m_0_50_cc_deltaEta"          , .7, -.5  , 5    ));



    const char* conds = "MLP_Resp_MinIso_SvsJpsiK>0.0816327 && MLP_Resp_MinIso_SvsHb_aft_JpsiK>0.612245 && MLP_Resp_MinIso_Svsccbar_aft_JpsiK_Hb>0.204082 && MLP_Resp_MinIso_SvsKsts_aft_JpsiK_Hb_ccbar>0.239";

    const char* rootFile1 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Signal/Bs2KMuNu.root";
    const char* rootFile2 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_12143001_2016_MagUp_TRIMMER.root";
    const char* rootFile3 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_10010037_2016_MagUp_TRIMMER.root";
    const char* rootFile4 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_13144001_2016_MagUp_TRIMMER.root";
    const char* rootFile5 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/DTT_12445022_2016_MagUp_TRIMMER.root";
    const char* rootFile6 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bu2dzmunux_MagUp_2016_TRIMMER.root";
    const char* rootFile7 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bsdsmunux_MagUp_2016_TRIMMER.root";

    const char* rootFile8 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kstmunu_MagUp_2016_TRIMMER.root";
    const char* rootFile9 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kst1430munu_MagUp_2016_TRIMMER.root";
    const char* rootFile10 = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/bs2kst2munu_MagUp_2016_TRIMMER.root";

    const char* legend1 = "B^{0}_{s} #rightarrow K^{-} #mu^{+} #nu";
    const char* legend2 = "B^{+} #rightarrow J/#psi K^{+}";
    const char* legend3 = "H_{b} #rightarrow H_{c}(#rightarrow K^{+}X)#mu^{+}X'";
    const char* legend4 = "B^{0}_{s} #rightarrow J/#psi #phi";
    const char* legend5 = "B^{+} #rightarrow c #bar{c} K X";
    const char* legend6 = "B^{+} #rightarrow D^{0} #mu^{+} #nu_{#mu}";
    const char* legend7 = "B^{0}_{s} #rightarrow D^{+}_{s} #mu #nu_{#mu} X";

    const char* legend8 = "B^{0}_{s} #rightarrow K^{*-} #mu^{+} #nu";
    const char* legend9 = "B^{0}_{s} #rightarrow K^{*-}(1430) #mu^{+} #nu";
    const char* legend10 = "B^{0}_{s} #rightarrow K^{*-}_{2} #mu^{+} #nu";

    int color1 = 1;
    int color2 = 4;
    int color3 = 2;
    int color4 = 3;
    int color5 = 6;
    
    int color8 = 28;
    int color9 = 7;
    int color10 = 92;

    std::vector<const char*> rootList;
    rootList.push_back(rootFile1);
    rootList.push_back(rootFile2);
    //
    rootList.push_back(rootFile3);
    //rootList.push_back(rootFile4);
    rootList.push_back(rootFile5);
    //rootList.push_back(rootFile6);
    //rootList.push_back(rootFile7);
    //
    rootList.push_back(rootFile8);
    rootList.push_back(rootFile9);
    rootList.push_back(rootFile10);
    //

    std::vector<const char*> legList;
    legList.push_back(legend1);
    legList.push_back(legend2);
    //
    legList.push_back(legend3);
    //legList.push_back(legend4);
    legList.push_back(legend5);
    //legList.push_back(legend6);
    //legList.push_back(legend7);
    //
    legList.push_back(legend8);
    legList.push_back(legend9);
    legList.push_back(legend10);
    //
    

    std::vector<int> colorList;
    colorList.push_back(color1);
    colorList.push_back(color2);
    //
    colorList.push_back(color3);
    //colorList.push_back(color4);
    colorList.push_back(color5);
    //colorList.push_back(color6);
    //colorList.push_back(color7);
    //
    colorList.push_back(color8);
    colorList.push_back(color9);
    colorList.push_back(color10);
    //

    const char* tree = "DecayTree";

    int binNum = 100;

    for(int k=0; k<varTab.size(); k++){

        const char* path = Form("%s/%s.pdf", folderPath, std::get<1>(varTab[k]));
        float xRange[2] = {std::get<3>(varTab[k]), std::get<4>(varTab[k])};

        macroVal_Func(rootList, legList, colorList, tree, std::get<0>(varTab[k]), xRange, binNum, conds, std::get<2>(varTab[k]), path);

    }

}