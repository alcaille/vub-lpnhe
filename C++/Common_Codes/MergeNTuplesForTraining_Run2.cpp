#include <iostream>
#include <deque>
#include <vector>
#include <map>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TEnv.h"
#include "iomanip"

using std::size_t;

void MergeNTuplesForTraining_Run2_Func(std::string OutputFileName, std::vector<std::pair<std::string, std::string>> InputFilenames, uint MaxEv = -1) {

  std::sort(InputFilenames.begin(), InputFilenames.end(), [](auto lhs, auto rhs){return lhs.first < rhs.first;});

  std::vector<TFile*> InputFiles;
  std::vector<TTree*> InputTrees;

  std::cout << std::endl;
  for ( auto& F: InputFilenames) {

    TFile* File = TFile::Open(F.first.c_str(), "READ");

    if ( not File ) {

      std::cout << "Unable to Open: " << F.first << std::endl;
      continue;

    }

    InputFiles.push_back(File);

    TTree* T = nullptr;

    T = (TTree*)File->Get(F.second.c_str());
    
    if ( T ) {

      InputTrees.push_back(T);
      std::cout << F.first << "  " << T->GetEntries() << std::endl;
    
    }

    else
    
      std::cout << "Unable to Open Tree: " << F.first << ": " << F.second << std::endl;

  }

  TFile* f_Out = TFile::Open(OutputFileName.c_str(), "RECREATE");
  TTree* T_Out = new TTree("DecayTree", "DecayTree");

  std::vector<std::string> Branches;

  std::vector<std::string> Branches_F;
  std::vector<std::string> Branches_D;
  std::vector<std::string> Branches_O;
  std::vector<std::string> Branches_l;

  // Branches for training
  Branches.push_back("Bs_cosTheta1_star"       );
  Branches.push_back("Bs_ENDVERTEX_CHI2"       );
  Branches.push_back("muon_p_PT"               );
  Branches.push_back("Bs_PT"                   );
  Branches.push_back("kaon_m_PAIR_M"           );
  Branches.push_back("kaon_m_PT"               );
  Branches.push_back("kaon_m_IsoMinBDT"        );
  Branches.push_back("muon_p_IsoMinBDT"        );
  Branches.push_back("kaon_m_IsoSumBDT"        );
  Branches.push_back("muon_p_IsoSumBDT"        );
  Branches.push_back("kaon_m_ConeIso"          );
  Branches.push_back("muon_p_ConeIso"          );
  Branches.push_back("kaon_m_0_50_cc_deltaEta" );
  Branches.push_back("kaon_m_0_50_cc_IT"       );
  Branches.push_back("kaon_m_0_50_IT"          );
  Branches.push_back("muon_p_0_50_cc_asy_P"    );
  Branches.push_back("muon_p_PAIR_M"           );
  Branches.push_back("Bs_Q2SOL1"               );
  Branches.push_back("Bs_Q2SOL2"               );
  Branches.push_back("Bs_M"                    );
  Branches.push_back("Bs_MCORR"                );
  Branches.push_back("Weight_Norm"             );
  Branches.push_back("Bs_Regression_Q2_BEST"   );
  Branches.push_back("Bs_PT_m_1_5xmuon_p_PT"   );
  Branches.push_back("kaon_m_IsoMinBDT_m_muon_p_IsoMinBDT" );
  Branches.push_back("max_IsoSum"              );
  Branches.push_back("min_ConeIso"             );
  Branches.push_back("min_IsoMin"              );
  /*Branches.push_back("BDTG_Resp_MinIso_SvsHb_aft_JpsiK");
  Branches.push_back("BDTG_Resp_MinIso_Svsccbar_aft_JpsiK_Hb");*/
  Branches.push_back("MLP_Resp_MinIso_SvsJpsiK");
  Branches.push_back("MLP_Resp_MinIso_SvsHb_aft_JpsiK");
  Branches.push_back("MLP_Resp_MinIso_Svsccbar_aft_JpsiK_Hb");
  Branches.push_back("MLP_Resp_MinIso_SvsHb_aft_SELF_JpsiK");
  Branches.push_back("MLP_Resp_MinIso_Svsccbar_aft_SELF_JpsiK_Hb");

  // Offline cut additionnal branches
  Branches.push_back("kaon_m_Pi0_M"                       );
  Branches.push_back("kaon_m_MasshPi0"                    );
  Branches.push_back("kaon_m_isMuon"                      );
  Branches.push_back("invMass_muMass"                     );
  Branches.push_back("kaon_m_PX"                          );
  Branches.push_back("kaon_m_PY"                          );
  Branches.push_back("muon_p_PX"                          );
  Branches.push_back("muon_p_PY"                          );
  Branches.push_back("totCandidates"                      );
  Branches.push_back("Bs_MCORRERR"                        );
  Branches.push_back("Bs_L0MuonDecision_TOS"              );
  Branches.push_back("Bs_Hlt2SingleMuonDecision_TOS"      );
  Branches.push_back("Bs_Hlt2TopoMu2BodyDecision_TOS"     );
  Branches.push_back("Bs_Hlt2TopoMu2BodyBBDTDecision_TOS" );

  // First check if all branches exist
  bool abort = false;
  for( int i = 0; i < Branches.size(); i++)
  {
      std::string BName = Branches[i];

      if ( not InputTrees[0]->GetBranch(BName.c_str()) )
      {
          std::cerr << "Requested Branch Not Found: " << BName << std::endl;
          std::cerr << "Removing from lit of branches" << std::endl;

          Branches.erase(Branches.begin()+i);
          i--;
      }

  }
  if( abort ) std::exit(1);

  for( auto& BName: Branches) {
    
    char Type = ((std::string)InputTrees[0]->GetBranch(BName.c_str())->GetTitle()).back();

    switch(Type){
      case 'F': Branches_F.push_back(BName); break;
      case 'D': Branches_D.push_back(BName); break;
      case 'O': Branches_O.push_back(BName); break;
      case 'l': Branches_l.push_back(BName); break;
      default : std::cout << "FUCK Recode this to include Type: " << Type << std::endl;
    }
  }


  std::map<TTree*, std::vector<float>> Values_Formula;

  std::vector<float>     Values_F      (Branches_F.size(), 0.0);
  std::vector<double>    Values_D      (Branches_D.size(), 0.0);
  std::deque<bool>       Values_O      (Branches_O.size(), 0  );
  std::vector<ULong64_t> Values_l      (Branches_l.size(), 0.0);

  for (auto& T: InputTrees)
  {
    T->SetCacheSize(100000000);
    for ( int b = 0; b < Branches_F.size(); b++)
    {
      T->SetBranchAddress(Branches_F[b].c_str(), &Values_F[b]);
    }
    for ( int b = 0; b < Branches_D.size(); b++)
    {
      T->SetBranchAddress(Branches_D[b].c_str(), &Values_D[b]);
    }
    for ( int b = 0; b < Branches_O.size(); b++)
    {
      T->SetBranchAddress(Branches_O[b].c_str(), &Values_O[b]);
    }
    for ( int b = 0; b < Branches_l.size(); b++)
    {
      T->SetBranchAddress(Branches_l[b].c_str(), &Values_l[b]);
    }

  }

  for ( int b = 0; b < Branches_F.size(); b++)
  {
    T_Out->Branch(Branches_F[b].c_str(), &Values_F[b], (Branches_F[b] + "/F").c_str());
  }

  for ( int b = 0; b < Branches_D.size(); b++)
  {
    T_Out->Branch(Branches_D[b].c_str(), &Values_D[b], (Branches_D[b] + "/D").c_str());
  }

  for ( int b = 0; b < Branches_O.size(); b++)
  {
      T_Out->Branch(Branches_O[b].c_str(), &Values_O[b], (Branches_O[b] + "/O").c_str());
  }

  for ( int b = 0; b < Branches_l.size(); b++)
  {
      T_Out->Branch(Branches_l[b].c_str(), &Values_l[b], (Branches_l[b] + "/O").c_str());
  }


  auto ContainsNAN = [](const auto& Container)->bool
    {
      auto NanLoc = find_if( Container.begin(), Container.end(), [](auto v){ return not std::isfinite(v);});
      return NanLoc < Container.end();
    };

  auto PrintStatus = [&OutputFileName](size_t ev, size_t Tot)
    {

      const int PercentComplete = (double)50*ev/Tot + 0.5;

      std::cout << std::setw(30) << std::left << OutputFileName << "  [";
      std::cout << std::string(   PercentComplete, '=');
      std::cout << std::string(50-PercentComplete, ' ');
      std::cout << "]  " << ev << "/" << Tot;
      std::cout << std::string( 20, ' ') << "\r" << std::flush;

    };


  size_t TotEntries = 0;
  for( TTree* T: InputTrees)
    TotEntries = std::max( TotEntries, (size_t)T->GetEntries());

  TotEntries =std::min(TotEntries, (size_t)MaxEv);

  bool Flag = true;
  for ( size_t ev = 0; Flag; ev++)
  {

    if ( ev %1000 == 0)
      PrintStatus(ev, TotEntries);


    Flag = false;
    for (TTree* T: InputTrees)
    {

      if (ev >= T->GetEntries() )
        continue;

      Flag = true;
      T->GetEntry(ev);



      if (ContainsNAN(Values_F))
        continue;
      if (ContainsNAN(Values_D))
        continue;
      if (ContainsNAN(Values_O))
        continue;
      if (ContainsNAN(Values_l))
        continue;

      T_Out->Fill();

    }

    if (ev > MaxEv)
      Flag = false;

  }


  f_Out->cd();
  T_Out->Write();
  f_Out->Close();


  std::cout << std::endl;

}

void MergeNTuplesForTraining_Run2() {

  std::string path_To_Tuples = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples";
  std::string path_To_Tuples_After_Cuts = "/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut";

  std::vector<std::pair<std::string, std::string>> SignalForTraining_Up_Vec;
  SignalForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Signal/Bs2KMuNu_MagUp_2016_TRIMMER.root","DecayTree"));
  //SignalForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Signal/bs2kmunu_Re_MagUp_2016_TRIMMER.root","DecayTree"));

  //MergeNTuplesForTraining_Run2_Func("/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Signal/Bs2KMuNu.root", {SignalForTraining_Up_Vec, });

  std::vector<std::pair<std::string, std::string>> ChargedCocktailBGForTraining_Up_Vec;

  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/bsdsmunux_MagUp_2016_TRIMMER.root"   ,"DecayTree"));
  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/bu2dzmunux_MagUp_2016_TRIMMER.root"   ,"DecayTree"));
  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Hb2Hc_2016_MagUp_TRIMMER_Re.root"   ,"DecayTree"));
  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/DTT_12143001_2016_MagUp_TRIMMER.root"   ,"DecayTree"));
  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Bu2ccbarKX_2016_MagUp_TRIMMER_Re.root"   ,"DecayTree"));
  //ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Bs2JpsiPhi_2016_MagUp_TRIMMER.root"   ,"DecayTree"));

  ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Bs2KstMuNu_2016_MagUp_TRIMMER.root"   ,"DecayTree"));
  ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Bs2Kst2MuNu_2016_MagUp_TRIMMER.root"   ,"DecayTree"));
  ChargedCocktailBGForTraining_Up_Vec.emplace_back(std::make_pair(path_To_Tuples_After_Cuts + "/Bkg/Bs2Kst1430MuNu_2016_MagUp_TRIMMER.root"   ,"DecayTree"));

  MergeNTuplesForTraining_Run2_Func("/Users/acaillet/Documents/PhD/Tuples/Run2_Tuples_MinIso_Cut/Bkg/All_Ksts.root", {ChargedCocktailBGForTraining_Up_Vec, });


}