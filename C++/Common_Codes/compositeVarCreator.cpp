#include <iostream>

#include <TFile.h>
#include "TH1.h"
#include "TTree.h"
#include </afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/lib/decaysEffInfo.h>

void compositeVarCreator_Func(std::vector<TString> rootFilePath, TString treeName, std::vector<Float_t> weigths){

    for(int i=0 ; i<rootFilePath.size() ; i++){

        TFile *myFile = TFile::Open(rootFilePath[i],"UPDATE");
        TTree *myTree = (TTree*)myFile->Get(treeName);

        // float  B_PT;
        // double mu_PT;

        double K_IsoMinBDT;
        double mu_IsoMinBDT;

        // double K_ConeIso;
        // double mu_ConeIso;

        myTree->SetBranchStatus("*",0);

        // myTree->SetBranchStatus("B_PT", 1);
        // myTree->SetBranchStatus("mu_PT", 1);
        // myTree->SetBranchAddress("B_PT", &B_PT);
        // myTree->SetBranchAddress("mu_PT", &mu_PT);

        // myTree->SetBranchStatus("K_IsoMinBDT", 1);
        // myTree->SetBranchStatus("mu_IsoMinBDT", 1);
        // myTree->SetBranchAddress("K_IsoMinBDT", &K_IsoMinBDT);
        // myTree->SetBranchAddress("mu_IsoMinBDT", &mu_IsoMinBDT);

        // myTree->SetBranchStatus("K_ConeIso", 1);
        // myTree->SetBranchStatus("mu_ConeIso", 1);
        // myTree->SetBranchAddress("K_ConeIso", &K_ConeIso);
        // myTree->SetBranchAddress("mu_ConeIso", &mu_ConeIso);

        // float B_PT_m_1_5xmu_PT;
        // TBranch *compVarBranch = myTree->Branch("B_PT_m_1_5xmu_PT", &B_PT_m_1_5xmu_PT, "B_PT_m_1_5xmu_PT/F");

        // float K_IsoMinBDT_m_mu_IsoMinBDT;
        // TBranch *compVarBranch2 = myTree->Branch("K_IsoMinBDT_m_mu_IsoMinBDT", &K_IsoMinBDT_m_mu_IsoMinBDT, "K_IsoMinBDT_m_mu_IsoMinBDT/F");

        float min_IsoMin;
        TBranch *compVarBranch3 = myTree->Branch("min_IsoMin", &min_IsoMin, "min_IsoMin/F");

        // float min_ConeIso;
        // TBranch *compVarBranch4 = myTree->Branch("min_ConeIso", &min_ConeIso, "min_ConeIso/F");

        // Float_t w;
        // TBranch *weight = myTree->Branch("Weight_Normed", &w, "Weight_Normed/F");

        // Int_t runYear;
        // TBranch *runYearBranch = myTree->Branch("runYear", &runYear, "runYear/I");

        Int_t eventNumb = myTree->GetEntries();

        for(int j=0; j<eventNumb; j++) {

            myTree->GetEntry(j);
            // B_PT_m_1_5xmu_PT = B_PT-1.5*mu_PT;
            // K_IsoMinBDT_m_mu_IsoMinBDT = K_IsoMinBDT-mu_IsoMinBDT;
            min_IsoMin = std::min(K_IsoMinBDT,mu_IsoMinBDT);
            // min_ConeIso = std::min(K_ConeIso,mu_ConeIso);

            // compVarBranch->Fill();
            // compVarBranch2->Fill();
            compVarBranch3->Fill();
            // compVarBranch4->Fill();
            // w = weigths[i];
            // weight->Fill();

            // runYear = 2018;
            // runYearBranch->Fill();
        }

        myTree->SetBranchStatus("*",1);
        myTree->AutoSave();
        delete myTree;
        delete myFile;

        std::cout << rootFilePath[i] << " : Done !" << "\n" << std::endl;

    }

}

void compositeVarCreator(){

    vector<TString> endpathVec =
    {
        sigTRIMMERAllMagPathVec[0][0],

        hbTRIMMERAllMagPathVec[0][0],

        ccbarTRIMMERAllMagPathVec[0][0],

        kstTRIMMERAllMagPathVec[0][0],

        kst2TRIMMERAllMagPathVec[0][0],

        kst1430TRIMMERAllMagPathVec[0][0],

        kpiTRIMMERAllMagPathVec[0][0],

        jpsiKst0TRIMMERAllMagPathVec[0][0],

        jpsiKstTRIMMERAllMagPathVec[0][0],

        jpsiKTRIMMERAllMagPathVec[0][0],
    };

    vector<Float_t> weightVec =
    {
        
        // sigWeightNormedAllMag[0][0],

        // hbWeightNormedAllMag[0][0],

        // // ccbarWeightNormedAllMag[0][0],

        // kstWeightNormedAllMag[0][0],

        // kst2WeightNormedAllMag[0][0],

        // kst1430WeightNormedAllMag[0][0],

        // // kpiWeightNormedAllMag[0][0],

        // jpsiKst0WeightNormedAllMag[0][0],

        // jpsiKstWeightNormedAllMag[0][0],

        // jpsiKWeightNormedAllMag[0][0],
    };

    compositeVarCreator_Func(endpathVec, run1TreeName, weightVec);

}