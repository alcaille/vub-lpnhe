#include "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/C++/lib/decaysEffInfo.h"

using namespace std;

void TrainMVA(   const char* trainAlgo, TString magPol, TString year, TString specs, vector<TString> cutVars,
                        TCut trainCut, vector<vector<vector<TString>>> sigPaths, vector<vector<vector<TString>>> bkgPaths, vector<TString> trainVars,
                        vector<TString> sigTreeNamesVec, vector<TString> bkgTreeNamesVec, Float_t sigTrainFrac, Float_t bkgTrainFrac,
                        TString TMVADir, vector<vector<vector<Float_t>>> sigsWeightNormedVec, vector<vector<vector<Float_t>>> bkgsWeightNormedVec)
{
    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    // Create a ROOT output file where TMVA will store ntuples, histograms, etc.
    TString outfileName( year+"_"+specs+"_Charge_Training_Mag"+magPol+".root" );

    TFile* outputFile = TFile::Open( outfileName, "RECREATE" );
    
    TString outfolderName( "Files_"+year+"_"+specs+"_Charge_Training_Mag"+magPol );

    TString terminalOutput = "Terminal_"+year+"_"+specs+"_Charge_Training_Mag"+magPol+".txt";

    freopen(terminalOutput.Data(), "w", stdout);

    TMVA::Factory *factory = new TMVA::Factory( year+"_"+ specs + "_Charge_Training_Mag" + magPol, outputFile,
                                                "!V:Color:DrawProgressBar:AnalysisType=Classification" );

    TMVA::DataLoader *dataloader = new TMVA::DataLoader( outfolderName );

    Float_t sigEntries = 0;
    Float_t bkgEntries = 0;

    //Cuts
    TCut SigCut = trainCut;
    TCut BkgCut = trainCut;
    //

    TTree* sigTrees[sigPaths.size()];
    TTree* bkgTrees[bkgPaths.size()];

    // Training Samples
    for(Int_t sigFilesIndex = 0 ; sigFilesIndex < sigPaths.size() ; sigFilesIndex++)
    {
        TFile* sigFile = TFile::Open(sigPaths[sigFilesIndex][magPolInt][yearInt], "READ");
        sigTrees[sigFilesIndex] = (TTree*)sigFile->Get(sigTreeNamesVec[sigFilesIndex]);
        sigTrees[sigFilesIndex]->SetBranchStatus("*", 0);
        for(Int_t i=0 ; i<cutVars.size() ; i++)
        {
            sigTrees[sigFilesIndex]->SetBranchStatus(cutVars[i]);
        }
        for(Int_t j=0 ; j<trainVars.size() ; j++)
        {
            sigTrees[sigFilesIndex]->SetBranchStatus(trainVars[j], 1);
        }
        dataloader->AddSignalTree(sigTrees[sigFilesIndex], sigsWeightNormedVec[sigFilesIndex][magPolInt][yearInt]);
        sigEntries += sigTrees[sigFilesIndex]->GetEntries(SigCut);
    }

    for(Int_t bkgFilesIndex = 0 ; bkgFilesIndex < bkgPaths.size() ; bkgFilesIndex++)
    {
        TFile* bkgFile = TFile::Open(bkgPaths[bkgFilesIndex][magPolInt][yearInt], "READ");
        bkgTrees[bkgFilesIndex] = (TTree*)bkgFile->Get(bkgTreeNamesVec[bkgFilesIndex]);
        bkgTrees[bkgFilesIndex]->SetBranchStatus("*", 0);
        for(Int_t i=0 ; i<cutVars.size() ; i++)
        {
            bkgTrees[bkgFilesIndex]->SetBranchStatus(cutVars[i]);
        }
        for(Int_t j=0 ; j<trainVars.size() ; j++)
        {
            bkgTrees[bkgFilesIndex]->SetBranchStatus(trainVars[j], 1);
        }
        dataloader->AddBackgroundTree(bkgTrees[bkgFilesIndex], bkgsWeightNormedVec[bkgFilesIndex][magPolInt][yearInt]);
        bkgEntries += bkgTrees[bkgFilesIndex]->GetEntries(BkgCut);
    }

    for(Int_t k=0; k<trainVars.size(); k++)
    {
        dataloader->AddVariable(trainVars[k], 'F');
    }

    Int_t numTrain_Sig = round(sigEntries*sigTrainFrac);
    Int_t numTrain_Bkg = round(bkgEntries*bkgTrainFrac);

    TString mvaOpt = TString::Format("SplitMode=Random:NormMode=EqualNumEvents:!V:nTrain_Signal=%i:nTrain_Background=%i",
                           numTrain_Sig, numTrain_Bkg);

    dataloader->PrepareTrainingAndTestTree( SigCut, BkgCut, mvaOpt );
    
    // Algorithm choice
    if (strcmp(trainAlgo, "BDTG") == 0)
    { // Gradient Boost
        factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTG",
        "!H:!V:NTrees=500:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=20:MaxDepth=2" );

    } else if (strcmp(trainAlgo, "BDT") == 0)
    {  // Adaptive Boost
        factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT",
        "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=SDivSqrtSPlusB:nCuts=20" );

    } else if (strcmp(trainAlgo, "BDTB") == 0)
    { // Bagging
        factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTB",
        "!H:!V:NTrees=400:BoostType=Bagging:SeparationType=SDivSqrtSPlusB:nCuts=20" );

    } else if (strcmp(trainAlgo, "BDTD") == 0)
    { // Decorrelation + Adaptive Boost
        factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTD",
        "!H:!V:NTrees=400:MinNodeSize=2.5%:MaxDepth=5:BoostType=AdaBoost:SeparationType=SDivSqrtSPlusB:nCuts=20:VarTransform=Decorrelate" );

    } else if (strcmp(trainAlgo, "BDTF") == 0)
    {  // Allow Using Fisher discriminant in node splitting for (strong) linearly correlated variables
        factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTMitFisher",
        "!H:!V:NTrees=50:MinNodeSize=2.5%:UseFisherCuts:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=SDivSqrtSPlusB:nCuts=20" );

    } else if (strcmp(trainAlgo, "MLP") == 0)
    {  // Allow Using MLP neural network
        factory->BookMethod( dataloader, TMVA::Types::kMLP, "MLP",
        "!H:!V:NeuronType=sigmoid:VarTransform=N:NCycles=600:HiddenLayers=N,N-1:TestRate=5:UseRegulator=True" );

    } else if (strcmp(trainAlgo, "Likelihood") == 0)
    {
        factory->BookMethod( dataloader, TMVA::Types::kLikelihood, "Likelihood",
        "H:!V:TransformOutput:PDFInterpol=Spline2:NSmoothSig[0]=20:NSmoothBkg[0]=20:NSmoothBkg[1]=10:NSmooth=1:NAvEvtPerBin=50" );
    } else 
    {
        throw runtime_error(Form("THE -> %s <- ALGORITHM IS NOT INCLUDED IN THE CODE", trainAlgo));
    }

    // Train MVAs using the set of training events
    factory->TrainAllMethods();

    // ---- Evaluate all MVAs using the set of test events
    factory->TestAllMethods();

    // ----- Evaluate and compare performance of all configured MVAs
    factory->EvaluateAllMethods();

    // --------------------------------------------------------------

    // Save the output
    outputFile->Close();

    fclose(stdout);

    delete factory;
    delete dataloader;

    moveFile(outfolderName.Data(), (TMVADir + "/" + outfolderName).Data());
    moveFile(outfileName.Data(), (TMVADir + "/" + outfolderName + "/" + outfileName).Data());
    moveFile(terminalOutput.Data(), (TMVADir + "/" + outfolderName + "/" + terminalOutput).Data());

}

void FillingHistoWithMVAResp(TTree* inputTree, TString weightFile, vector<TString> readVarsFloat,
                             vector<TString> readVarsDouble, TH1F* histo)
{
    TMVA::Reader *reader = new TMVA::Reader( "!Color" );

    Float_t  floatVars[readVarsFloat.size()];

    Float_t  floatedDoubleVars[readVarsDouble.size()];
    Double_t doubleVars[readVarsDouble.size()];

    for(Int_t p=0 ; p<readVarsFloat.size() ; p++)
    {
        reader->AddVariable(readVarsFloat[p], &floatVars[p]);
    }

    for(Int_t l=0 ; l<readVarsDouble.size() ; l++)
    {
        reader->AddVariable(readVarsDouble[l], &floatedDoubleVars[l]);
    }

    reader->BookMVA( "MVA", weightFile );

    inputTree->SetBranchStatus("*", 0);

    for(Int_t j=0 ; j<readVarsFloat.size() ; j++)
    {
        inputTree->SetBranchStatus(readVarsFloat[j], 1);
        inputTree->SetBranchAddress(readVarsFloat[j], &floatVars[j]);
    }

    for(Int_t m=0 ; m<readVarsDouble.size() ; m++)
    {
        inputTree->SetBranchStatus(readVarsDouble[m], 1);
        inputTree->SetBranchAddress(readVarsDouble[m], &doubleVars[m]);
    }

    for (Int_t k=0; k<inputTree->GetEntries(); k++)
    {
        inputTree->GetEntry(k);

        for(Int_t n=0 ; n<readVarsDouble.size() ; n++)
        {
            floatedDoubleVars[n] = doubleVars[n];
        }

        histo->Fill(reader->EvaluateMVA("MVA"));

        double progress = double(k+1) / double(inputTree->GetEntries());
        print_progress_bar(progress);
    }

    inputTree->SetBranchStatus("*",1);
    delete inputTree;

}

void Plot_weighted_Response(TString magPol, TString year, vector<vector<vector<TString>>> sampleVec, vector<TString> legList,
                                 vector<Int_t> colorList, Bool_t weightorNo, Int_t binNum, vector<vector<vector<Float_t>>> sampleWeightNormedVec,
                                 TString treeName, Float_t xRange[2], TString savePath, TString title, TString weightFile,
                                 vector<TString> readVarsFloat, vector<TString> readVarsDouble)
{
    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    if(weightorNo){
        binNum = binNum * 2;
    }

    lhcbStyle();

    Int_t sampNumb = sampleVec.size();

    THStack* histStack = new THStack("histStack", title);

    TH1F* histVec[sampNumb];

    TCanvas *canvas = new TCanvas("canvas", "Canvas", 1300, 1080);

    TLegend* legend = new TLegend(0.42, 0.62, 0.66, 0.915);
    legend->SetLineWidth(0);

    for(Int_t i=0; i<sampNumb; i++)
    {
        TFile* file = new TFile(sampleVec[i][magPolInt][yearInt], "READ");
        TTree* mytree = (TTree*)file->Get(treeName);

        histVec[i] = new TH1F(TString::Format("Hist %i", i), title, binNum, xRange[0], xRange[1]);
        
        cout << "\n\n" << sampleVec[i][magPolInt][yearInt] << "\n" << endl;

        FillingHistoWithMVAResp(mytree, weightFile, readVarsFloat, readVarsDouble, histVec[i]);

        if(weightorNo)
        {
            histVec[i]->Scale(histVec[i]->Integral() * sampleWeightNormedVec[i][magPolInt][yearInt]);
        }
        else
        {
            histVec[i]->Scale(1./histVec[i]->Integral());
        }

        histVec[i]->SetFillStyle(0);

    }

    for(Int_t j=0; j<sampNumb ; j++)
    {
        Int_t k = sampNumb-1-j;

        if(!weightorNo)
        {
            histVec[k]->SetLineStyle(1);
            histVec[k]->SetLineWidth(1);
            histVec[k]->SetLineColor(colorList[k]);
            histVec[k]->SetMarkerSize(.1);
            histStack->Add(histVec[k], "H && E1 && EX0");
            legend->AddEntry(histVec[j], legList[j], "L");
        }else
        {
            histVec[k]->SetMarkerSize(.6);
            histVec[k]->SetMarkerColor(colorList[k]);
            histStack->Add(histVec[k], "E1 && EX0");
            histStack->SetMinimum(1);
            legend->AddEntry(histVec[j], legList[j], "P");
        }
    }

    Float_t maxYTable[sampNumb];

    for(Int_t l=0 ; l<sampNumb ; l++)
    {
        maxYTable[l] = histVec[l]->GetMaximum();
    }

    Float_t maxY = *max_element(maxYTable, maxYTable + sampNumb);

    if(weightorNo)
    {
        maxY = 10e5 * maxY;
    }else
    {
        maxY = 1.1 * maxY;
    }

    histStack->SetMaximum(maxY);

    histStack->Draw("nostack");

    histStack->SetTitle(title);
    histStack->GetYaxis()->SetTitle("A.U.");
    histStack->GetXaxis()->SetTitle("MVA Response");

    TPaveText* lhcbText = new TPaveText(.17, .855, .32, .925, "BRNDC");
    lhcbText->SetFillColor(0);
    lhcbText->SetTextAlign(12);
    lhcbText->SetBorderSize(0);
    lhcbText->SetLineWidth(0);
    lhcbText->AddText("LHCb MC");

    lhcbText->Draw();

    //canvas->SetGrid(1,0);

    legend->Draw();

    if(weightorNo)
    {
        canvas->SetLogy();
        canvas->SaveAs(savePath + "_Log.pdf");
    }else
    {
        canvas->SaveAs(savePath + ".pdf");
    }


    canvas->Close();

}

void Add_response_In_Tree(  TString magPol, TString year, vector<vector<vector<TString>>> sampPaths, TString weightFile, TString responseName,
                            std::vector<TString> readVarsFloat, std::vector<TString> readVarsDouble, TString treeName)
{
    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    TMVA::Reader *reader = new TMVA::Reader( "!Color" );

    Float_t  floatVars[readVarsFloat.size()];

    Float_t  floatedDoubleVars[readVarsDouble.size()];
    Double_t doubleVars[readVarsDouble.size()];

    for(Int_t p=0 ; p<readVarsFloat.size() ; p++){
        reader->AddVariable(readVarsFloat[p], &floatVars[p]);
    }
    for(Int_t l=0 ; l<readVarsDouble.size() ; l++){
        reader->AddVariable(readVarsDouble[l], &floatedDoubleVars[l]);
    }

    reader->BookMVA( "MVA", weightFile );

    for(Int_t i=0 ; i<sampPaths.size() ; i++){

        TFile* inputFile = TFile::Open(sampPaths[i][magPolInt][yearInt], "UPDATE");
        TTree* myTree = (TTree*)inputFile->Get(treeName);
        std::cout << sampPaths[i][magPolInt][yearInt] << "\n" << std::endl;

        myTree->SetBranchStatus("*", 0);

        for(Int_t j=0 ; j<readVarsFloat.size() ; j++){
            myTree->SetBranchStatus(readVarsFloat[j], 1);
            myTree->SetBranchAddress(readVarsFloat[j], &floatVars[j]);
        }
        for(Int_t m=0 ; m<readVarsDouble.size() ; m++){
            myTree->SetBranchStatus(readVarsDouble[m], 1);
            myTree->SetBranchAddress(readVarsDouble[m], &doubleVars[m]);
        }


        Float_t response;
        TBranch* response_Branch = myTree->Branch(responseName, &response, responseName + "/F");

        for (Int_t k=0; k<myTree->GetEntries(); k++) {

            myTree->GetEntry(k);

            for(Int_t n=0 ; n<readVarsDouble.size() ; n++){
                floatedDoubleVars[n] = doubleVars[n];
            }

            response = reader->EvaluateMVA("MVA");
            response_Branch->Fill();

            double progress = double(k+1) / double(myTree->GetEntries());
            print_progress_bar(progress);

        }

        myTree->SetBranchStatus("*",1);
        myTree->AutoSave();
        delete myTree;
        delete inputFile;

        std::cout << "\n" << std::endl;

    }

}

Float_t separationPowerFormula(Float_t ys, Float_t yb)
{
    return pow(ys-yb, 2) / (2 * (ys + yb));
}

Float_t separationPower(TH1F* sigHist, TH1F* bkgHist)
{
    int sigBinNum = sigHist->GetNbinsX();
    int bkgBinNum = bkgHist->GetNbinsX();

    if(sigBinNum == bkgBinNum)
    {
        int binNum = sigBinNum;
        Float_t sepPow = 0.;

        for(int i=0 ; i<binNum ; i++)
        {
            if(sigHist->GetBinContent(i) == 0 && bkgHist->GetBinContent(i) == 0)
            {
                sepPow += 0.;
            }else
            {
                sepPow += separationPowerFormula(sigHist->GetBinContent(i), bkgHist->GetBinContent(i));
            }

        }

        return sepPow;

    }else
    {
        return 0.;
    }

}

Float_t Normed_S_or_B_With_Response_Cut(TString rootFileName, Float_t response_Cut, TString varName, TString weightVar, TString treeName) {

    TFile* rootFile = TFile::Open(rootFileName, "READ");
    TTree* myTree   = (TTree*)rootFile->Get(treeName);

    Float_t weight, response;

    myTree->SetBranchStatus("*", 0);
    myTree->SetBranchStatus(weightVar, 1);
    myTree->SetBranchStatus(varName, 1);

    myTree->SetBranchAddress(weightVar, &weight);
    myTree->SetBranchAddress(varName, &response);

    Int_t nEntries = myTree->GetEntries();
    Float_t SB = 0 ;

    for(Int_t i=0; i<nEntries; i++)
    {
        myTree->GetEntry(i);

        if(response>response_Cut)
        {
            SB += weight;
        }
    }

    rootFile->Close();

    return SB;

}

void optiMVACut(  TString magPol, TString year, vector<vector<vector<TString>>> sigPaths, vector<vector<vector<TString>>> bkgPaths,
                vector<vector<vector<TString>>> ssBkgPaths, vector<vector<vector<TString>>> allStatPaths, TString weightVar,
                Int_t pointNum, Float_t start, Float_t end, TString SBorSSB, TString savePath, TString varName, TString treeName)
{
    lhcbStyle();

    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016" || year=="All"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : All / 2016 / 2017 / 2018");}

    Float_t* response_Vec = linspace(start, end, pointNum);

    Float_t Eff_Tab[pointNum];
    Float_t Eff_Err_Tab[pointNum];
    Float_t reponse_Err[pointNum];

    fill_n(reponse_Err, pointNum, (end - start) / (2*pointNum));

    Float_t sTab[pointNum];
    Float_t bTab[pointNum];
    Float_t allbTab[pointNum];

    for(int i=0; i<pointNum; i++) {

        Float_t S = 0.0;
        Float_t B = 0.0;
        Float_t allB = 0.0;

        for(Int_t p=0; p<allStatPaths.size(); p++) {
            
            Float_t temp = Normed_S_or_B_With_Response_Cut(allStatPaths[p][magPolInt][yearInt], response_Vec[i], varName, weightVar, treeName);
           
            if(count(sigPaths.begin(), sigPaths.end(), allStatPaths[p])>0)
            {
                S += temp;
            }

            else if(count(bkgPaths.begin(), bkgPaths.end(), allStatPaths[p])>0)
            {
                allB += temp;
            
                if(count(ssBkgPaths.begin(), ssBkgPaths.end(), allStatPaths[p])>0)
                {
                    B += temp;
                }
            }
        }

        sTab[i] = S;
        bTab[i] = B;
        allbTab [i] = allB;

        if(SBorSSB == "S") {

            Eff_Tab[i] = S;
            Eff_Err_Tab[i] = sqrt(S);

        } else if(SBorSSB == "B") {

            Eff_Tab[i] = B;
            Eff_Err_Tab[i] = sqrt(B);

        } else if(SBorSSB == "S/√B") {

            Eff_Tab[i] = S / sqrt(B);
            Eff_Err_Tab[i] = err_SB( S, B );

        } else if(SBorSSB == "S/B") {

            Eff_Tab[i] = S / B;
            Eff_Err_Tab[i] = (sqrt(S)/B) + (S/pow(B, 1.5));

        } else {

            Eff_Tab[i] = S / sqrt(S+B);
            Eff_Err_Tab[i] = err_SSB( S, B );

        }

        Float_t progress = Float_t(i+1) / Float_t(pointNum);
        print_progress_bar(progress);
    }

    Float_t maxElement = *max_element(Eff_Tab, Eff_Tab + pointNum);

    int maxIndex = 0;

    for (int n = 0; n < pointNum; n++) {
        if (Eff_Tab[n] == maxElement) {
            maxIndex = n;
        }
    }

    Float_t maxElementErr = round(Eff_Err_Tab[maxIndex]*1000.)/1000.;

    TCanvas* canvas = new TCanvas("canvas", "Graph Canvas", 1300, 1080);
    gStyle->SetLineScalePS(1);

    Float_t Eff_Tab_Norm[pointNum];

    TGraphErrors* graphErr = new TGraphErrors(pointNum, response_Vec, Eff_Tab, reponse_Err, Eff_Err_Tab);

    if(SBorSSB == "S" || SBorSSB == "B") {

        graphErr->SetTitle((";Response Cut;" + SBorSSB).Data());

    } else if (SBorSSB == "S/√B"){

        graphErr->SetTitle(" ;Response Cut; S / #sqrt{B}");

    } else if (SBorSSB == "S/B"){

        graphErr->SetTitle(" ;Response Cut; S / B");

    } else {

        graphErr->SetTitle(" ;Response Cut; S / #sqrt{S + B}");

    }

    graphErr->SetLineColor(1);
    graphErr->SetMarkerStyle(20);
    graphErr->SetMarkerColor(2);
    graphErr->SetMarkerSize(1);
    graphErr->GetXaxis()->SetRangeUser(start, end);
    graphErr->Draw("APL");
    graphErr->SetMaximum(maxElement * 1.1);

    freopen((savePath + "/Terminal_OptiMVACut_NewWeighting.txt").Data(), "w", stdout);

    if(SBorSSB == "")
    {
        cout << "\n" << "The optimal response cut is : " << response_Vec[maxIndex]  << endl;

        if(bkgPaths.size() != ssBkgPaths.size())
        {
            for(int n ; n<bkgPaths.size() ; n++)
            {
                if(n==0)
                {
                    cout << "\n" << "With B =  " << bkgPaths[n][magPolInt][yearInt] << endl;
                } else {
                    cout << "        + " << bkgPaths[n][magPolInt][yearInt] << endl;
                }
            }
            cout << "\n" << "S/B = " << sTab[maxIndex]/bTab[maxIndex] << endl;
            cout << "S/√B = " << sTab[maxIndex]/sqrt(bTab[maxIndex]) << endl;
            cout << "S/√S+B = " << sTab[maxIndex]/sqrt(bTab[maxIndex] + sTab[maxIndex]) << endl;
        }

        cout << "\n" << "With B = All" << endl;
        cout << "\n" << "S/B = " << sTab[maxIndex]/allbTab[maxIndex] << endl;
        cout << "S/√B = " << sTab[maxIndex]/sqrt(allbTab[maxIndex]) << endl;
        cout << "S/√S+B = " << sTab[maxIndex]/sqrt(allbTab[maxIndex] + sTab[maxIndex]) << "\n" << endl;

        for(int j=0; j<allStatPaths.size(); j++) {

            int evtNumBef = round(Normed_S_or_B_With_Response_Cut(allStatPaths[j][magPolInt][yearInt], start, varName, weightVar, treeName));
            int evtNumAft = round(Normed_S_or_B_With_Response_Cut(allStatPaths[j][magPolInt][yearInt], response_Vec[maxIndex], varName, weightVar, treeName));
        
            Float_t effMVACut = round(1000.*evtNumAft / evtNumBef) / 10.;

            cout << allStatPaths[j][magPolInt][yearInt] << " " << evtNumBef << " & " << evtNumAft << " & " << effMVACut << " %" << endl;

        }
    }

    fclose(stdout);

    ostringstream oss;

    oss << "The maximum S/#sqrt{S+B} is " << round(maxElement*1000.)/1000. << " #pm " << maxElementErr << " when cutting at " << round(response_Vec[maxIndex]*1000.) / 1000.;

    TLegend* legend = new TLegend(0.38, 0.12, NULL,"brNDC");

    if(SBorSSB == "") {

        legend->AddEntry(graphErr, oss.str().c_str(), "");

    }

    TPaveText* lhcbText = new TPaveText(.17, .855, .32, .925, "BRNDC");
    lhcbText->SetFillColor(0);
    lhcbText->SetTextAlign(12);
    lhcbText->SetBorderSize(0);
    lhcbText->SetLineWidth(0);
    lhcbText->AddText("LHCb MC");

    lhcbText->Draw();

    legend->SetBorderSize(0);
    legend->SetFillColor(0);
    legend->SetFillStyle(0);
    legend->SetTextSize(0.03);

    //canvas->SetGrid();

    legend->Draw();

    canvas->SaveAs(savePath + "/PDF/OptiMVACut_NewWeighting.pdf");
    //canvas->Close();

}