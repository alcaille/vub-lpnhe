#include <fstream>

#include "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/MVA_Discrimination/lib/MVAToolBox.h"
#include <TLeaf.h>
#include <TList.h>

using namespace std;

void sepPowerRank_Func( TString magPol, TString year, TString bkgSamps, vector<vector<vector<TString>>> sigPaths,
                        vector<vector<vector<TString>>> bkgPaths, vector<TString> sigTreeNamesVec,
                        vector<TString> bkgTreeNamesVec, vector<vector<vector<Float_t>>> sigsWeightNormedVec,
                        vector<vector<vector<Float_t>>> bkgsWeightNormedVec, Int_t binNum, TString filters)
{
    Int_t magPolInt;
    if     (magPol=="Down" || magPol=="All"){magPolInt = 0;}
    else if(magPol=="Up"){magPolInt = 1;}
    else{throw runtime_error("THERE IS NO MAGNETIC " + magPol + " POLARITY | PICK ONE OF THIS : Up / Down / All");}

    Int_t yearInt;
    if     (year=="2016"){yearInt = 0;}
    else if(year=="2017"){yearInt = 1;}
    else if(year=="2018"){yearInt = 2;}
    else{throw runtime_error(year + " DOES NOT HAVE DATA | PICK ONE OF THIS YEARS : 2016 / 2017 / 2018");}

    Int_t sigSampNum = sigPaths.size();
    Int_t bkgSampNum = bkgPaths.size();

    TList* sigTreesList = new TList;
    TList* bkgTreesList = new TList;

    for(Int_t sigIndex = 0 ; sigIndex < sigSampNum ; sigIndex++)
    {
        TFile* sigFile = TFile::Open(sigPaths[sigIndex][magPolInt][yearInt], "READ");
        TTree* sigTree = (TTree*)sigFile->Get(sigTreeNamesVec[sigIndex]);
        sigTree->SetWeight(sigsWeightNormedVec[sigIndex][magPolInt][yearInt]);
        sigTreesList->Add(sigTree);
    }

    for(Int_t bkgIndex = 0 ; bkgIndex < bkgSampNum ; bkgIndex++)
    {
        TFile* bkgFile = TFile::Open(bkgPaths[bkgIndex][magPolInt][yearInt], "READ");
        TTree* bkgTree = (TTree*)bkgFile->Get(bkgTreeNamesVec[bkgIndex]);
        bkgTree->SetWeight(bkgsWeightNormedVec[bkgIndex][magPolInt][yearInt]);
        bkgTreesList->Add(bkgTree);
    }

    TFile* sepPowerROOT = new TFile("/eos/lhcb/user/a/alcaille/Vub/SepPowerROOT/" + year + "/sepPowerRank_Sig_vs_" + bkgSamps + "_Mag"
                                    + magPol + "_" + year + filters + ".root","UPDATE");
    TTree* sigMergedTree = TTree::MergeTrees(sigTreesList);
    sigMergedTree->SetName("sigTREE");
    sigMergedTree->Write();

    TTree* bkgMergedTree = TTree::MergeTrees(bkgTreesList);
    bkgMergedTree->SetName("bkgTREE");
    bkgMergedTree->Write();

    delete sigTreesList;
    delete bkgTreesList;

    TObjArray* sigBranchArray = sigMergedTree->GetListOfBranches();
    TObjArray* bkgBranchArray = bkgMergedTree->GetListOfBranches();

    vector<tuple<Float_t, string, string>> rankingVec;

    Int_t numOfBranches = min(sigBranchArray->GetEntries(), bkgBranchArray->GetEntries());

    for(Int_t i=0; i<numOfBranches; i++)
    {
        TBranch* sigBranch = (TBranch*)sigBranchArray->At(i);
        TBranch* bkgBranch = (TBranch*)bkgBranchArray->At(i);

        string branchName = sigBranch->GetName();

        TLeaf* leaf = sigBranch->GetLeaf(sigBranch->GetName());

        if (sigBranch && bkgBranch && (leaf->GetLenType() == 4 || leaf->GetLenType() == 8) && branchName.find("ID") == string::npos &&
            branchName.find("MC") == string::npos && branchName.find("Q2") == string::npos && branchName.find("Reg") == string::npos &&
            branchName.find("Resp") == string::npos && branchName.find("eventNumber") == string::npos && branchName.find("TMVA") == string::npos &&
            branchName.find("TRUE") == string::npos)
        {
            string branchType;

            if(leaf->GetLenType() == 4)
            {
                branchType = "Float";
            }
            else
            {
                branchType = "Double";
            }

            Float_t sigBMinBranch = sigMergedTree->GetMinimum(sigBranch->GetName());
            Float_t xMinbkgBranch = bkgMergedTree->GetMinimum(sigBranch->GetName());

            Float_t sigBMaxBranch = sigMergedTree->GetMaximum(sigBranch->GetName());
            Float_t xMaxbkgBranch = bkgMergedTree->GetMaximum(sigBranch->GetName());

            TH1F* hist1 = new TH1F("Hist 1", "", binNum, max(sigBMinBranch,xMinbkgBranch), min(sigBMaxBranch, xMaxbkgBranch));
            TH1F* hist2 = new TH1F("Hist 2", "", binNum, max(sigBMinBranch,xMinbkgBranch), min(sigBMaxBranch, xMaxbkgBranch));

            sigMergedTree->Project(hist1->GetName(), sigBranch->GetName());
            bkgMergedTree->Project(hist2->GetName(), sigBranch->GetName());

            hist1->Scale(1./hist1->Integral());
            hist2->Scale(1./hist2->Integral());

            Float_t sepPower = separationPower(hist1, hist2);

            if(sepPower != 1 && sepPower != 0){
                rankingVec.push_back(make_tuple(sepPower, sigBranch->GetName(), branchType));
            }

            delete hist1;
            delete hist2;

            double progress = double(i+1) / double(numOfBranches);
            print_progress_bar(progress);

        }

    }

    sort(rankingVec.rbegin(), rankingVec.rend());

    TString sepPowerTxtsFolder = "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/MVA_Discrimination/SepPowerRankTxts/" + year;

    ofstream sepPowerRankingFile(sepPowerTxtsFolder + "/sepPowerRank_Sig_vs_" + bkgSamps + "_Mag" + magPol + "_" + year + filters + ".txt");

    for(Int_t j=0 ; j<numOfBranches ; j++){

        sepPowerRankingFile << Form("%s | Separation Power = %f | Variable Type : %s\n",
                                    get<1>(rankingVec[j]).c_str(), get<0>(rankingVec[j]), get<2>(rankingVec[j]).c_str());

    }

    sepPowerRankingFile.close();

    delete sigMergedTree;
    delete bkgMergedTree;
    delete sepPowerROOT;

}

void sepPowerRanking(){
    TString magPol = "All";
    TString year   = "2016";
    TString bkgSamps   = "Kpi";
    TString filters   = "_TRIMMER_AllCuts";
    vector<vector<vector<TString>>> sigPaths = {sigTRIMMERAllMagAllCutsPathVec};
    vector<vector<vector<TString>>> bkgPaths = {kpiTRIMMERAllMagAllCutsPathVec};
    vector<TString> sigTreeNamesVec = {sigTRIMMERTreeName};
    vector<TString> bkgTreeNamesVec = {kpiTRIMMERTreeName};
    vector<vector<vector<Float_t>>> sigsWeightNormedVec = {sigWeightNormedAllMag};
    vector<vector<vector<Float_t>>> bkgsWeightNormedVec = {kpiWeightNormedAllMag};
    Int_t binNum = 300;

    sepPowerRank_Func( magPol, year, bkgSamps, sigPaths, bkgPaths, sigTreeNamesVec, bkgTreeNamesVec,
                        sigsWeightNormedVec, bkgsWeightNormedVec, binNum, filters);

}