path2Samples = "/eos/user/a/alcaille/Vub/Tuples/"
path2OriginSamples = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna_Run2/Tuples/"
run2TreeName = "B2XuMuNuBs2K_Tuple"

################################################################################################################################################

sigTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/DTT_13512010_2018_Up_TRIMMER.root",
    ],
]

sigNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/new/DTT_13512010_2018_Up_TRIMMER.root",
    ],
]

sigNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KMuNu/mva/DTT_13512010_2018_Up_TRIMMER.root",
    ],
]

################################################################################################################################################

hbTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/DTT_10010037_2018_Up_TRIMMER.root",
    ],
]

hbNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/new/DTT_10010037_2018_Up_TRIMMER.root",
    ],
]

hbNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/b2cMuX/mva/DTT_10010037_2018_Up_TRIMMER.root",
    ],
]

################################################################################################################################################

ccbarTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/DTT_12445022_2018_Up_TRIMMER.root",
    ],
]

ccbarNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/new/DTT_12445022_2018_Up_TRIMMER.root",
    ],
]

ccbarNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/B2ccbarKX/mva/DTT_12445022_2018_Up_TRIMMER.root",
    ],
]

################################################################################################################################################

kstTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/DTT_13512400_2018_Up_TRIMMER.root"
    ]
]

kstNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/new/DTT_13512400_2018_Up_TRIMMER.root"
    ]
]

kstNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2KstMuNu/mva/DTT_13512400_2018_Up_TRIMMER.root"
    ]
]

################################################################################################################################################

kst2TRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/DTT_13512410_2018_Up_TRIMMER.root"
    ]
]

kst2NEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/new/DTT_13512410_2018_Up_TRIMMER.root"
    ]
]

kst2NEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst2MuNu/mva/DTT_13512410_2018_Up_TRIMMER.root"
    ]
]

################################################################################################################################################

kst1430TRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/DTT_13512420_2018_Up_TRIMMER.root"
    ]
]

kst1430NEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/new/DTT_13512420_2018_Up_TRIMMER.root"
    ]
]

kst1430NEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bs2Kst1430MuNu/mva/DTT_13512420_2018_Up_TRIMMER.root"
    ]
]

################################################################################################################################################

jpsiKTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/DTT_12143001_2018_Up_TRIMMER.root"
    ]
]

jpsiKNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bu2JpsiK/new/DTT_12143001_2018_Up_TRIMMER.root"
    ]
]

################################################################################################################################################

jpsiKst0TRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/DTT_11144001_2018_Up_TRIMMER.root"
    ]
]

jpsiKst0NEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer/Bd2JpsiKst/new/DTT_11144001_2018_Up_TRIMMER.root"
    ]
]

################################################################################################################################################
################################################################################################################################################

dataTRIMMERPathVec = [
    [
        f"{path2Samples}/Data/trimmer/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}/Data/trimmer/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]

dataNEWTRIMMERPathVec = [
    [
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/new/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]

dataNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer/mva/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]

dataNEWORIGINTRIMMERPathVec = [
    [
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2OriginSamples}/Data/trimmer/new/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]

################################################################################################################################################
################################################################################################################################################

evtMixCombTRIMMERPathVec = [
    [
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/Comb_90000000_2018_Up_TRIMMER.root",
    ],
]

evtMixCombNEWTRIMMERPathVec = [
    [
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/new/Comb_90000000_2018_Up_TRIMMER.root",
    ],
]

evtMixCombNEW4MVATRIMMERPathVec = [
    [
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}Combinatorial/trimmer/mva/Comb_90000000_2018_Up_TRIMMER.root",
    ],
]

################################################################################################################################################
################################################################################################################################################

norm_mcTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/DTT_12143001_2018_Up_TRIMMER.root"
    ]
]

norm_mcNEWTRIMMERPathVec = [
    [
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2016_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2017_Down_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2018_Down_TRIMMER.root"
    ],
    [
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2016_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2017_Up_TRIMMER.root",
        f"{path2Samples}MC/trimmer_norm/Bu2JpsiK/new/DTT_12143001_2018_Up_TRIMMER.root"
    ]
]

norm_dataTRIMMERPathVec = [
    [
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]

norm_dataNEWTRIMMERPathVec = [
    [
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2016_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2017_Down_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2018_Down_TRIMMER.root",
    ],
    [
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2016_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2017_Up_TRIMMER.root",
        f"{path2Samples}/Data/trimmer_norm/new/DTT_90000000_2018_Up_TRIMMER.root",
    ],
]