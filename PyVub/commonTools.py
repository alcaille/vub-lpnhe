import yaml, re, os
import numpy as np
from array import array
from tqdm import tqdm
from itertools import product
from typing import Union
from ROOT import (  TFile, TMVA, TList, TTree, TChain, gDirectory,
                    TLorentzVector, TVector3, TDatabasePDG, RDataFrame,
                    TDirectory)

LUMI = [1.67, 1.81, 2.19] # Luminosity values in fb-1
SIGMABB = 144.e9 # bb cross section in fb

##########################################################################################
####################################### Some codes #######################################
##########################################################################################


def flatten_list(nested_list: list) -> list:
    """
    Recursively flattens a list of arbitrarily nested lists into a single flat list.
    
    Args:
        nested_list (list): A list containing elements that are either strings or other nested lists.
    
    Returns:
        list: A flat list containing all elements from the nested lists.
    
    Example:
        >>> flatten_list([['apple', 'banana'], ['carrot', ['date', 'egg']], 'fig'])
        ['apple', 'banana', 'carrot', 'date', 'egg', 'fig']
    
    Notes:
        - Handles arbitrarily nested lists.
        - Assumes all elements are strings or lists.
    """
    flat_list = []
    for item in nested_list:
        if isinstance(item, list):
            flat_list.extend(flatten_list(item))  # Recursive call to flatten sublist
        else:
            flat_list.append(item)  # Add non-list items directly to the flat list
    return flat_list


def create_path(path: str) -> None:
    """
    Creates a directory path if it doesn't already exist.
    
    Args:
        path (str): The directory path to create.
    
    Returns:
        None
    """
    if not os.path.exists(path):
        os.system(f"mkdir -p {path}")


##########################################################################################
##################### Creates the selection string from a .yaml file #####################
##########################################################################################


def extract_var_from_sel(input_string: str) -> list[str]:
    """
    Extracts variable-like substrings from the input string, excluding specific keywords.
    
    Args:
        input_string (str): The input string to process.
    
    Returns:
        list[str]: Unique variable names found in the input string, excluding 'fabs', 'min', and 'max'.
    
    Example:
        >>> extract_var_from_sel("fabs(x) + min(y, z) + some_var")
        ['x', 'y', 'z', 'some_var']
    """
    # Regular expression to match variable names (alphanumeric or underscore)
    pattern = r'\b[a-zA-Z_]\w*\b'
    matches = re.findall(pattern, input_string)
    
    # Keywords to exclude
    exclusions = {'fabs', 'min', 'max'}
    
    # Filter matches to exclude keywords and return unique variable names
    unique_variables = {match for match in matches if match not in exclusions}
    return sorted(unique_variables)  # Return as a sorted list for consistent output


def get_vars_config(variables="ALL", config_file=None):
    """
    Retrieve variables and their configurations from a YAML file.
    
    Args:
        variables (str or list): The variable(s) to retrieve. Default is "ALL", which returns the entire configuration.
        config_file (str):  Path to the configuration YAML file. 
                            Defaults to a pre-defined path if not provided.
    
    Returns:
        dict: A dictionary of the requested variables and their configurations.
    
    Example:
        config_vars = get_vars_config(["var1", "var2"], "config.yaml")
    """
    # Use a default path if no config file is specified
    if config_file is None:
        config_file = "/afs/cern.ch/work/a/alcaille/private/Vub/GitLab_Vub_PhD/SelectionList.yaml"
    
    # Load the configuration from the file
    config, _ = get_config(config_file)
    
    # Return all variables if "ALL" is specified
    if variables == "ALL":
        return config
    
    # Convert a single variable to a list if necessary
    if isinstance(variables, str):
        variables = [variables]
    
    # Filter the configuration to return only the requested variables
    config_vars = {k: v for k, v in config.items() if k in variables}
    return config_vars


def get_formula(config, name, particle=""):
    """
    Construct a formula for a given variable from the configuration.
    
    Args:
        config (dict): The configuration dictionary loaded from the YAML file.
        name (str): The name of the variable to retrieve the formula for.
        particle (str): Optional. A placeholder to replace 'PART' in the formula.
    
    Returns:
        str: The expanded formula as a string.
    
    Example:
        formula = get_formula(config, "mass", "muon")
    """
    # Retrieve the configuration for the given variable
    config_vars = get_vars_config(name)
    scale = config_vars[name]['scale']
    formula = f"({config_vars[name]['formula']})/{scale}"
    
    # Replace 'PART' in the formula with the provided particle name
    if particle:
        formula = formula.replace('PART', particle)
    
    # Expand nested formulas in the expression
    formula = expand_formulas(config, formula)
    return formula


def expand_formulas(config, expression):
    """
    Recursively expand nested formulas in an expression.
    
    Args:
        config (dict): The configuration dictionary.
        expression (str): The expression containing formulas to expand.
    
    Returns:
        str: The expression with all nested formulas expanded.
    
    Example:
        expanded_expr = expand_formulas(config, "F(mass) + F(momentum)")
    """
    # Find all occurrences of the pattern "F(variable_name)" in the expression
    matches = re.findall(r"F\([a-z0-9._-]*\)", expression)
    
    # If any matches are found, process them
    if len(matches) > 0:
        for match in matches:
            # Extract the variable name from the match
            name = re.match(r"F\((.*)\)$", match)
            try:
                # Get the expanded formula for the variable
                formula = get_formula(config, name.group(1))
            except:
                pass
            else:
                # Replace the formula placeholder with the actual formula
                expression = expression.replace(match, f"({formula})")
    return expression


def expand_selection(config, selection):
    """
    Recursively expand a selection into its corresponding cuts.
    
    Args:
        config (dict): The configuration dictionary.
        selection (str): The selection name to expand.
    
    Returns:
        str: The expanded selection as a string of logical cuts.
    
    Example:
        expanded_sel = expand_selection(config, "selection1")
    """
    # Check if the selection exists in the configuration
    if selection in config["Selections"].keys():
        list_cuts = config['Selections'][selection].split("+")
    else:
        list_cuts = [selection]
    
    # Initialize the selection cuts
    sel_cuts = "1"
    for cut in list_cuts:
        if cut in config['Selections'].keys():
            sel_cuts += f" && ({expand_selection(config, cut)})"
        elif cut in config['Cuts'].keys():
            sel_cuts += f" && ({config['Cuts'][cut]})"
        else:
            sel_cuts += f" && ({cut})"
    
    # Expand any nested formulas in the selection cuts
    sel_cuts = expand_formulas(config, sel_cuts)
    return sel_cuts


def expand_selections(config, list_selections):
    """
    Expand a list of selections into a single logical selection string.
    
    Args:
        config (dict): The configuration dictionary.
        list_selections (str or list): A single selection or a list of selections to expand.
    
    Returns:
        str: The combined expanded selections as a single string.
    
    Example:
        expanded_selections = expand_selections(config, ["sel1", "sel2"])
    """
    selection = "1"
    
    # Ensure the selections are in list form
    if isinstance(list_selections, str):
        list_selections = [list_selections]
    
    for sel in list_selections:
        selection += f" && ({expand_selection(config, sel)})"
    
    return selection


def make_selection(config, selection="", comp=None):
    """
    Construct the final selection string for a given component.
    
    Args:
        config (dict): The configuration dictionary.
        selection (str or list): The selection(s) to apply.
        comp (str): Optional. The component name to use for weights and cuts.
    
    Returns:
        str: The final selection string.
    
    Example:
        final_selection = make_selection(config, "sel1", "compA")
    """
    # Retrieve the component weight and cuts, or default to "1"
    comp_wgt = config['Components'][comp]['weight'] if comp else "1"
    comp_cuts = config['Components'][comp]['cuts'] if comp else "1"
    
    # Ensure the selection is in list form
    selection = [selection] if not isinstance(selection, list) else selection
    
    # Expand the selection(s) into a combined string
    sel_cuts = expand_selections(config, selection) if selection else "1"
    
    # Construct the final selection string
    sel_str = f"{comp_wgt}*(({sel_cuts}) && ({comp_cuts}))"
    expand_formulas(config, sel_str)
    return sel_str


def get_config(file_config, create_new=False):
    """
    Load a YAML configuration file or create a new one if specified.
    
    Args:
        file_config (str): The path to the configuration file.
        create_new (bool): Whether to create a new file if it doesn't exist.
    
    Returns:
        tuple: A tuple containing the configuration dictionary and the file stream.
    
    Example:
        config, stream = get_config("config.yaml", create_new=True)
    """
    # Check if the file exists
    if os.path.exists(file_config):
        with open(file_config, "r") as stream:
            try:
                # Load the YAML file into a dictionary
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
                exit(0)
            
            # Handle empty files by initializing an empty dictionary
            if not isinstance(config, dict):
                print(f"File {file_config} is empty. Creating a new dict.")
                config = {}
    elif create_new:
        # Create a new file if it doesn't exist
        print(f"Configuration file {file_config} does not exist. Creating a new one.")
        with open(file_config, "w") as stream:
            config = {}
    else:
        print(f"Configuration file {file_config} doesn't exist!")
        exit(0)
    
    return config, stream


##########################################################################################
################################# ROOT tree manipulation #################################
##########################################################################################


def process_directory(
    input_dir: TDirectory, output_dir: TDirectory, selection: str
) -> None:
    """
    Recursively process directories in a ROOT file, applying a selection to all TTrees.
    
    Args:
        input_dir (TDirectory): The input directory to process.
        output_dir (TDirectory): The output directory to write to.
        selection (str): The selection string to apply to TTrees.
    
    Returns:
        None
    """
    # Get list of keys in the input directory
    for key in input_dir.GetListOfKeys():
        obj_name = key.GetName()
        obj = key.ReadObj()
        
        # Check if the object is a directory
        if obj.InheritsFrom("TDirectory"):
            # Create a new directory in the output file
            output_dir.cd()
            new_dir = output_dir.mkdir(obj_name)
            # Recursively process the contents of the new directory
            process_directory(obj, new_dir, selection)
        
        # Check if the object is a TTree
        elif obj.InheritsFrom("TTree"):
            input_dir.cd()
            tree = obj
            # Clone the tree with the selection applied
            output_dir.cd()
            new_tree = tree.CopyTree(selection)
            # Write the new tree to the output directory
            new_tree.Write()
        
        else:
            # For other objects, simply copy them over
            output_dir.cd()
            obj.Write()


def create_root_file_with_selection(
    input_file_name: str, output_file_name: str, selection: str
) -> None:
    """
    Create a new ROOT file with a selection applied to all TTrees in the input file.
    
    Args:
        input_file_name (str): Path to the input ROOT file.
        output_file_name (str): Path to the output ROOT file to be created.
        selection (str): The selection string to apply to TTrees.
    
    Returns:
        None
    """
    # Open the input file
    input_file = TFile.Open(input_file_name, "READ")
    if not input_file or input_file.IsZombie():
        print(f"Error opening input file '{input_file_name}'")
        return
    
    # Create the output file
    output_file = TFile(output_file_name, "RECREATE")
    if not output_file or output_file.IsZombie():
        print(f"Error creating output file '{output_file_name}'")
        input_file.Close()
        return
    
    # Start processing from the root directory
    process_directory(input_file, output_file, selection)
    
    # Close the files
    output_file.Close()
    input_file.Close()


def branch_name_exists(tree: TTree, branch_name: str) -> bool:
    """
    Checks if a branch exists in a TTree.
    
    Args:
        tree (TTree): The tree to check.
        branch_name (str): The name of the branch.
    
    Returns:
        bool: True if the branch exists, False otherwise.
    """
    return bool(tree.GetBranch(branch_name))


def copy_trees_conds(
    origin_path: str,
    filtered_file_path: str,
    tree_names: list[str],
    conds: Union[dict[str, str], str, None] = None
) -> None:
    """
    Copies one or multiple ROOT trees from an original file into a new or existing 
    filtered file based on provided conditions, without removing previously existing 
    trees in the filtered file.
    
    Args:
        origin_path (str): Path to the original ROOT file.
        filtered_file_path (str): Path to the filtered ROOT file (existing or new).
        tree_names (list[str]): A list of tree names to filter and copy.
        conds (dict[str, str] | str | None, optional): A dictionary of selection conditions keyed by tree name, 
                                                       or a single string to apply the same condition to all trees. 
                                                       If None, no filtering is applied.
    
    Returns:
        None
    """
    # Handle conditions
    if conds is None:
        conds = {}
    elif isinstance(conds, str):
        conds = {tree_name: conds for tree_name in tree_names}
    
    # Determine file mode
    mode = "RECREATE" if not os.path.exists(filtered_file_path) else "UPDATE"
    
    # Open the ROOT files
    origin_file = TFile(origin_path, "READ")
    filtered_file = TFile(filtered_file_path, mode)
    
    for tree_name in tree_names:
        origin_tree = origin_file.Get(tree_name)
        if not origin_tree:
            print(f"Tree '{tree_name}' not found in the original file.")
            continue
        
        # Apply the condition for this tree
        condition = conds.get(tree_name, "")
        filtered_tree = origin_tree.CopyTree(condition)
        if filtered_tree:
            filtered_tree.Write()
    
    # Close the files
    filtered_file.Close()
    origin_file.Close()
    
    print(f"Filtering completed. Output saved to: {filtered_file_path}")


def tuple_merger(input_files: list[str], output_file_path: str, tree_name: str) -> None:
    """
    Merges multiple ROOT trees from input files into a single output file.
    
    Args:
        input_files (list[str]): Paths to input ROOT files.
        output_file_path (str): Path to the output ROOT file.
        tree_name (str): The name of the tree to merge.
    
    Returns:
        None
    """
    # Prepare lists to hold files and trees
    list_of_trees = TList()
    file_list = []
    
    for input_file in input_files:
        root_file = TFile(input_file, "READ")
        file_list.append(root_file)
        tree = root_file.Get(tree_name)
        if not tree:
            raise RuntimeError(f"Tree '{tree_name}' not found in file: {input_file}")
        list_of_trees.Add(tree)
    
    # Open the output file and merge the trees
    output_file = TFile(output_file_path, "RECREATE")
    output_tree = TTree.MergeTrees(list_of_trees)
    if output_tree:
        output_tree.SetName(tree_name)
        output_file.Write()
    else:
        raise RuntimeError("Failed to merge trees.")
    
    output_file.Close()
    
    # Close input files
    for root_file in file_list:
        root_file.Close()
    
    print(f"Merging complete. Merged TTree saved in {output_file_path}")


def all_run_chain_tuples(sample_list: list[list[list[str]]], tree_name: str) -> TChain:
    """
    Chains ROOT trees for all samples in the list for all years and 
    magnet configurations.
    
    Args:
        sample_list (list[list[list[str]]]): List of samples. Each sample is a list 
                                             of magnet configurations, where each 
                                             magnet configuration is a list of file paths.
        tree_name (str): The name of the tree to chain.
    
    Returns:
        TChain: The chained ROOT trees.
    """
    chain = TChain(tree_name)
    
    for sample in sample_list:
        for magnet_config in sample:
            for year_file in magnet_config:
                chain.Add(year_file)
    
    return chain


def chain_tuples_one_year(
    sample_list: list[dict], tree_name: str, year: int
) -> TChain:
    """
    Chains ROOT trees for a single year.
    
    Args:
        sample_list (list[dict]): List of sample dictionaries where each entry is a 
                                  dictionary of magnet configurations with years as keys.
        tree_name (str): The name of the tree to chain.
        year (int): The year to chain.
    
    Returns:
        TChain: The chained ROOT trees for the specified year.
    """
    chain = TChain(tree_name)
    
    for sample in sample_list:
        for magnet_config in sample:
            try:
                chain.Add(sample[magnet_config][year])
            except KeyError as e:
                raise KeyError(f"Missing key in sample data: {e}")
    
    return chain


def chain_tuples_modulable(
    sample_list: list[dict], tree_name: str, years: list[int], magnets: list[int]
) -> TChain:
    """
    Chains ROOT trees for multiple years and magnet configurations.
    
    Args:
        sample_list (list[dict]): List of sample dictionaries where keys are magnet 
                                  configurations, and values are dictionaries with 
                                  years as keys and file paths as values.
        tree_name (str): The name of the tree to chain.
        years (list[int]): List of years to chain.
        magnets (list[int]): List of magnet configurations to chain.
    
    Returns:
        TChain: The chained ROOT trees for the specified years and magnets.
    """
    chain = TChain(tree_name)
    
    for sample in sample_list:
        for magnet, year in product(magnets, years):
            try:
                chain.Add(sample[magnet][year])
            except KeyError as e:
                raise KeyError(f"Missing key in sample data: {e}")
    
    return chain


def remove_branch(
    mags: list[int], 
    years: list[int], 
    samps: list[list[str]], 
    tree_name: str, 
    branch_names: list[str]
) -> None:
    """
    Removes specified branches from ROOT trees.
    
    Args:
        mags (list[int]): List of magnet configurations.
        years (list[int]): List of years.
        samps (list[list[str]]): List of sample file paths.
        tree_name (str): Name of the tree to modify.
        branch_names (list[str]): List of branches to remove.
    
    Returns:
        None
    """
    for samp in samps:
        for mag in mags:
            for year in years:
                input_file_path = samp[mag][year]
                input_file = TFile.Open(input_file_path, "READ")
                if not input_file or input_file.IsZombie():
                    print(f"Error opening file: {input_file_path}")
                    continue
                
                input_tree = input_file.Get(tree_name)
                if not input_tree:
                    print(f"Tree '{tree_name}' not found in file: {input_file_path}")
                    input_file.Close()
                    continue
                
                # Track number of branches to be removed
                branches_to_remove = [
                    branch_name for branch_name in branch_names 
                    if branch_name_exists(input_tree, branch_name)
                ]
                
                for branch_name in branches_to_remove:
                    input_tree.SetBranchStatus(branch_name, 0)
                    print(f"Branch '{branch_name}' selected for removal in file: {input_file_path}")
                
                if branches_to_remove:
                    # Create output file and write the modified tree
                    output_file = TFile.Open(input_file_path, "RECREATE")
                    output_tree = input_tree.CloneTree(-1, "fast")
                    output_file.Write()
                    output_file.Close()
                    print(f"File '{input_file_path}': Branches removed successfully.")
                else:
                    print(f"File '{input_file_path}': No valid branches to remove.")
                
                input_file.Close()


def n_event_tuple_cond(
    root_path: str, tree_name: str, conds: str, weight_var: str
) -> int:
    """
    Counts the number of entries in a ROOT tree that satisfy certain conditions.
    
    Args:
        root_path (str): Path to the ROOT file.
        tree_name (str): Name of the tree to evaluate.
        conds (str): The condition string for the selection.
        weight_var (str): Variable to weight the entries.
    
    Returns:
        int: The number of entries satisfying the conditions, weighted if specified.
    """
    # Open the ROOT file
    root_file = TFile.Open(root_path, "READ")
    if not root_file or root_file.IsZombie():
        print(f"Error opening file: {root_path}")
        return 0
    
    # Get the tree from the file
    tree = root_file.Get(tree_name)
    if not tree:
        print(f"Tree '{tree_name}' not found in file: {root_path}")
        root_file.Close()
        return 0
    
    # Create a dataframe from the tree
    df = RDataFrame(tree)
    
    # Apply the filter condition if provided
    if conds:
        df = df.Filter(conds)
    
    # Calculate the weighted or unweighted count
    if weight_var:
        n_entries = df.Sum(weight_var).GetValue()
    else:
        n_entries = df.Count().GetValue()
    
    # Close the file and return the result
    root_file.Close()
    return round(n_entries)


def get_tree_index_list(tree: TTree, selection: str) -> list[int]:
    """
    Gets a list of entry indices from a ROOT tree that satisfy a selection condition.
    
    Args:
        tree (TTree): The ROOT tree to query.
        selection (str): The selection condition to apply.
    
    Returns:
        list[int]: List of entry indices that satisfy the selection.
    """
    # Ensure the tree is valid
    if not tree or not isinstance(tree, TTree):
        print("Invalid TTree object provided.")
        return []
    
    # Get the number of entries in the tree
    n_entries = tree.GetEntries()
    
    # Create a histogram to store the indices
    tree.Draw(f"Entry$>>hist({n_entries},0,{n_entries})", selection, "goff")
    hist = gDirectory.Get("hist")
    if not hist:
        print("Failed to retrieve histogram from TTree.")
        return []
    
    # Extract indices where the selection condition is satisfied
    index_list = [
        int(hist.GetBinLowEdge(bin_index))
        for bin_index in range(1, hist.GetNbinsX() + 1)
        if hist.GetBinContent(bin_index) > 0
    ]
    
    # Clean up the histogram
    gDirectory.Delete("hist")
    return index_list


def add_vars_BFD_frame(fileNames, treeName, particles):
    
    for fileName in fileNames:
        file = TFile.Open(fileName, 'update')
        tree = file.Get(treeName)
        tree.SetBranchStatus("*", 0)
        
        PV_X, PV_Y, PV_Z = array('d', [0]), array('d', [0]), array('d', [0])
        SV_X, SV_Y, SV_Z = array('d', [0]), array('d', [0]), array('d', [0])
        
        particles_PX = [array('d', [0])] * len(particles)
        particles_PY = [array('d', [0])] * len(particles)
        particles_PZ = [array('d', [0])] * len(particles)
        
        particles_Par_PX = [array('f', [0])] * len(particles)
        particles_Par_PY = [array('f', [0])] * len(particles)
        particles_Par_PZ = [array('f', [0])] * len(particles)
        
        particles_Prp_PX = [array('f', [0])] * len(particles)
        particles_Prp_PY = [array('f', [0])] * len(particles)
        particles_Prp_PZ = [array('f', [0])] * len(particles)
        
        particles_Prp_P = [array('f', [0])] * len(particles)
        particles_Par_P = [array('f', [0])] * len(particles)
        
        branch_list = []
        
        for i, particle in enumerate(particles):
            
            tree.SetBranchStatus( f"{particle}_PX", 1)
            tree.SetBranchAddress(f"{particle}_PX", particles_PX[i])
            tree.SetBranchStatus( f"{particle}_PY", 1)
            tree.SetBranchAddress(f"{particle}_PY", particles_PY[i])
            tree.SetBranchStatus( f"{particle}_PZ", 1)
            tree.SetBranchAddress(f"{particle}_PZ", particles_PZ[i])
            
            branch_list.extend(
                (
                    tree.Branch(f"{particle}_Par_PX", particles_Par_PX[i], f"{particle}_Par_PX/F"),
                    tree.Branch(f"{particle}_Par_PY", particles_Par_PY[i], f"{particle}_Par_PY/F"),
                    tree.Branch(f"{particle}_Par_PZ", particles_Par_PZ[i], f"{particle}_Par_PZ/F"),
                    tree.Branch(f"{particle}_Prp_PX", particles_Prp_PX[i], f"{particle}_Prp_PX/F"),
                    tree.Branch(f"{particle}_Prp_PY", particles_Prp_PY[i], f"{particle}_Prp_PY/F"),
                    tree.Branch(f"{particle}_Prp_PZ", particles_Prp_PZ[i], f"{particle}_Prp_PZ/F"),
                    tree.Branch(f"{particle}_Prp_P" , particles_Prp_P[i] , f"{particle}_Prp_P/F"),
                    tree.Branch(f"{particle}_Par_P" , particles_Par_P[i] , f"{particle}_Par_P/F")
                )
            )
        
        tree.SetBranchStatus( "B_OWNPV_X", 1)
        tree.SetBranchAddress("B_OWNPV_X", PV_X)
        tree.SetBranchStatus( "B_OWNPV_Y", 1)
        tree.SetBranchAddress("B_OWNPV_Y", PV_Y)
        tree.SetBranchStatus( "B_OWNPV_Z", 1)
        tree.SetBranchAddress("B_OWNPV_Z", PV_Z)
        
        tree.SetBranchStatus( "B_ENDVERTEX_X", 1)
        tree.SetBranchAddress("B_ENDVERTEX_X", SV_X)
        tree.SetBranchStatus( "B_ENDVERTEX_Y", 1)
        tree.SetBranchAddress("B_ENDVERTEX_Y", SV_Y)
        tree.SetBranchStatus( "B_ENDVERTEX_Z", 1)
        tree.SetBranchAddress("B_ENDVERTEX_Z", SV_Z)
        
        evtNum = tree.GetEntries()
        
        for i in tqdm(range(evtNum)):
            
            tree.GetEntry(i)
            
            PV_Vec = [PV_X[0], PV_Y[0], PV_Z[0]]
            SV_Vec = [SV_X[0], SV_Y[0], SV_Z[0]]
            
            for j, particle in enumerate(particles):
                
                particle_VecCoords = [particles_PX[j][0], particles_PY[j][0], particles_PZ[j][0]]
                
                particle_ParVecCoords , particle_PrpVecCoords = kin_BFD_frame(PV_Vec, SV_Vec, particle_VecCoords)
                
                particles_Par_PX[j][0] = particle_ParVecCoords[0]
                particles_Par_PY[j][0] = particle_ParVecCoords[1]
                particles_Par_PZ[j][0] = particle_ParVecCoords[2]
                particles_Prp_PX[j][0] = particle_PrpVecCoords[0]
                particles_Prp_PY[j][0] = particle_PrpVecCoords[1]
                particles_Prp_PZ[j][0] = particle_PrpVecCoords[2]
                
                particles_Par_P[j][0] = np.sqrt(particle_ParVecCoords[0]**2 +
                                                particle_ParVecCoords[1]**2 +
                                                particle_ParVecCoords[2]**2)
                
                particles_Prp_P[j][0] = np.sqrt(particle_PrpVecCoords[0]**2 +
                                                particle_PrpVecCoords[1]**2 +
                                                particle_PrpVecCoords[2]**2)
                
            for branch in branch_list:
                branch.Fill()
        
        tree.SetBranchStatus("*", 1)
        file.Write("",TFile.kOverwrite)
        file.Close()
        print(fileName, ": Done !\n")

##########################################################################################
########################## Computes the weight per Event/Sample ##########################
##########################################################################################

def sample_weight_per_event(
    branching_ratio, generation_efficiency, rent_rate,
    full_event_cut_efficiency, generated_event_number,
    f
):
    """
    Calculate the weight per event for a given sample based on multiple parameters.
    
    Args:
        branching_ratio (float): The branching ratio for the specific decay channel.
        generation_efficiency (float): The efficiency of generating the event.
        rent_rate (float): The rentention rate
        full_event_cut_efficiency (float):
        generated_event_number (int): The total number of generated events.
        f (float): The fragmentation factor.
        luminosity (float): The luminosity of the data sample.
    
    Returns:
        float: The event weight.
    """
    return (
        f * branching_ratio *
        generation_efficiency * rent_rate *
        full_event_cut_efficiency / generated_event_number
    )

##########################################################################################
################################# Uncertainties formulas #################################
##########################################################################################

def err_SSB(S, B):
    """
    Calculate the uncertainty on the signal significance (S/sqrt(S+B)).
    
    Args:
        S (float): The number of signal events.
        B (float): The number of background events.
    
    Returns:
        float: The uncertainty on the signal significance.
    """
    sqrt_S_plus_B = np.sqrt(S + B)
    first_term = abs(1 / sqrt_S_plus_B - S / (2 * sqrt_S_plus_B**3)) * np.sqrt(S)
    second_term = (S / (2 * sqrt_S_plus_B**3)) * np.sqrt(B)
    return first_term + second_term


def err_S_over_sqrtB(S, B):
    """
    Calculate the uncertainty on the ratio S/sqrt(B).
    
    Args:
        S (float): The number of signal events.
        B (float): The number of background events.
    
    Returns:
        float: The uncertainty on the ratio S/sqrt(B).
    """
    sqrt_B = np.sqrt(B)
    term1 = np.sqrt(S) / sqrt_B
    term2 = S / (2 * B)
    return term1 + term2


def ratio_Err(Na, Nb):
    """
    Calculate the propagated uncertainty of the ratio Na/Nb using error propagation.
    
    Args:
        Na (float): Numerator value.
        Nb (float): Denominator value.
    
    Returns:
        float: Uncertainty on the ratio Na/Nb.
    """
    ratio = Na / Nb
    err_Na = np.sqrt(Na) / Na
    err_Nb = np.sqrt(Nb) / Nb
    return ratio * (err_Na + err_Nb)

##########################################################################################
################################## New variables study ###################################
##########################################################################################

def mu_Bsframe(PV_coords, SV_coords, mu_MomCoords, B_M, B_PZ):
    
    db = TDatabasePDG.Instance()
    Bs = db.GetParticle(531)
    mu = db.GetParticle(13)
    
    PV_Vec = TVector3(PV_coords[0] , PV_coords[1] , PV_coords[2])
    SV_Vec = TVector3(SV_coords[0] , SV_coords[1] , SV_coords[2])
    
    Bs_Vec = SV_Vec - PV_Vec
    Bs_flight_dir = Bs_Vec.Unit()
    Bs_polar_angle = Bs_flight_dir.Theta()
    Bs_PZ = (Bs.Mass() / B_M) * B_PZ
    Bs_P_mag = Bs_PZ * np.sqrt(1 + np.tan(Bs_polar_angle)**2)
    
    Bs_Mom = float(Bs_P_mag) * Bs_flight_dir
    
    Bs_p = TLorentzVector()
    mu_p = TLorentzVector()
    Bs_p.SetXYZM(Bs_Mom.X()     , Bs_Mom.Y()     , Bs_Mom.Z()     , Bs.Mass())
    mu_p.SetXYZM(mu_MomCoords[0], mu_MomCoords[1], mu_MomCoords[2], mu.Mass())
    
    Bs_boost = Bs_p.BoostVector()
    mu_p.Boost(-Bs_boost.X(), -Bs_boost.Y(), -Bs_boost.Z())
    
    return mu_p.P()

def kin_BFD_frame(PV_coords, SV_coords, part_VecCoords):
    
    part_Vec = TVector3(part_VecCoords[0]   , part_VecCoords[1]  , part_VecCoords[2])
    PV_Vec   = TVector3(PV_coords[0]        , PV_coords[1]       , PV_coords[2])
    SV_Vec   = TVector3(SV_coords[0]        , SV_coords[1]       , SV_coords[2])
    
    Bs_Vec   = SV_Vec - PV_Vec
    n_Par    = Bs_Vec.Unit()
    
    part_ParVec  = part_Vec.Dot(n_Par) * n_Par
    part_PerpVec = part_Vec - part_ParVec
    
    part_ParCoords    = [part_ParVec.X()  , part_ParVec.Y()  , part_ParVec.Z()]
    part_PrpVecCoords = [part_PerpVec.X() , part_PerpVec.Y() , part_PerpVec.Z()]
    
    return part_ParCoords, part_PrpVecCoords

def Nu_p_perpar(PV_coords, SV_coords, K_quadrimom_Coords, mu_quadrimom_Coords):
    
    db = TDatabasePDG.Instance()
    Bs = db.GetParticle(531)
    
    K_p4  = TLorentzVector(K_quadrimom_Coords[0] , K_quadrimom_Coords[1],
                           K_quadrimom_Coords[2] , K_quadrimom_Coords[3])
    mu_p4 = TLorentzVector(mu_quadrimom_Coords[0], mu_quadrimom_Coords[1],
                           mu_quadrimom_Coords[2], mu_quadrimom_Coords[3])
    
    X_p4 = K_p4 + mu_p4
    
    PV_Vec = TVector3(PV_coords[0], PV_coords[1], PV_coords[2])
    SV_Vec = TVector3(SV_coords[0], SV_coords[1], SV_coords[2])
    Bs_Vec = SV_Vec - PV_Vec
    n_Par  = Bs_Vec.Unit()
    
    K_pVec  = K_p4.Vect()
    mu_pVec = mu_p4.Vect()
    
    K_pParVec  = K_pVec.Dot(n_Par)  * n_Par
    mu_pParVec = mu_pVec.Dot(n_Par) * n_Par
    
    K_pPerpVec  = K_pVec  - K_pParVec
    mu_pPerpVec = mu_pVec - mu_pParVec
    
    X_pParVec  = K_pParVec  + mu_pParVec
    X_pPerpVec = K_pPerpVec + mu_pPerpVec
    
    X_pParVecNorm  = X_pParVec.Mag()
    X_pPerpVecNorm = X_pPerpVec.Mag()
    
    miss_Inv_Mass2 = (Bs.Mass() * 1e3)**2 - X_p4.M2()
    
    a = 4 *(X_pPerpVecNorm**2 + X_p4.M2())
    b = 4 * X_pParVecNorm * (2 * X_pParVecNorm**2 - miss_Inv_Mass2)
    c = 4 * X_pPerpVecNorm**2 * (X_pParVecNorm**2 + Bs.Mass()**2) - miss_Inv_Mass2**2
    
    delta = b**2 - 4*a*c
    
    x = []
    test = 1
    
    if delta == 0:
        x.extend((-b / a, -b / a))
    elif delta > 0:
        x.extend(((-b - np.sqrt(delta)) / (2*a), (-b + np.sqrt(delta)) / (2*a)))
    elif delta < 0:
        test = -1
        x.extend((0, 0))
        print(test)
    
    nu_pPerpVec = -X_pPerpVec
    nu_pParLowVec  = float(x[0]) * n_Par
    nu_pParHighVec = float(x[1]) * n_Par
    
    nu_pPerpCoords    = [nu_pPerpVec.X()   , nu_pPerpVec.Y()   , nu_pPerpVec.Z()]
    nu_pParLowCoords  = [nu_pParLowVec.X() , nu_pParLowVec.Y() , nu_pParLowVec.Z()]
    nu_pParHighCoords = [nu_pParHighVec.X(), nu_pParHighVec.Y(), nu_pParHighVec.Z()]
    
    nu_Low_pVec  = nu_pPerpVec + nu_pParLowVec
    nu_High_pVec = nu_pPerpVec + nu_pParHighVec
    
    nu_Low_pE  = nu_Low_pVec.Mag()
    nu_High_pE = nu_High_pVec.Mag()
    
    nu_LH_pE = [nu_Low_pE, nu_High_pE]
    
    return test, nu_LH_pE, nu_pParLowCoords, nu_pParHighCoords, nu_pPerpCoords

##########################################################################################
################## Computes the S/√S+B value after a given response cut ##################
##########################################################################################

def Normed_S_or_B_With_Response_Cut(root_file_name, norm_W, response_cut, weight_file,
                                    tree_name, conds_var, conds, read_vars_float,
                                    read_vars_double):
    """
    Computes the normalized signal or background yield from a ROOT file after applying
    a machine learning response cut.
    
    Parameters:
    -----------
    root_file_name : str
        The path to the ROOT file containing the data.
    norm_W : float
        The normalization weight applied to the signal or background count.
    response_cut : float
        The threshold value for the machine learning response to include an entry.
    weight_file : str
        Path to the TMVA weight file for the machine learning model.
    tree_name : str
        The name of the tree inside the ROOT file.
    conds_var : list
        List of variable names to set the branch status (used for conditions).
    conds : str
        Additional conditions for selecting tree entries.
    read_vars_float : list
        List of variable names for float-type variables to read from the tree.
    read_vars_double : list
        List of variable names for double-type variables to read from the tree.
    
    Returns:
    --------
    SB : float
        The scaled signal or background yield after applying the response cut.
    """
    
    # Append condition string if provided
    if conds:
        conds = f"&&({conds})"
    
    # Initialize TMVA reader
    reader = TMVA.Reader("!V")
    
    # Allocate memory for the variables
    float_vars = np.zeros(len(read_vars_float), dtype=np.float32)
    double_vars = np.zeros(len(read_vars_double), dtype=np.float64)
    
    # Add float variables to the reader
    for i, var_name in enumerate(read_vars_float):
        reader.AddVariable(var_name, float_vars[i])
    
    # Add double variables to the reader (casted to float32 for the reader)
    for i, var_name in enumerate(read_vars_double):
        reader.AddVariable(var_name, float_vars[i + len(read_vars_float)])  # Append to float_vars
    
    # Book the machine learning model from the weight file
    reader.BookMVA("MVA", weight_file)
    
    # Open ROOT file and read tree
    with TFile.Open(root_file_name, "READ") as root_file:
        my_tree = root_file.Get(tree_name)
        
        # Disable all branches initially
        my_tree.SetBranchStatus("*", 0)
        
        # Enable only the necessary branches
        for var_name in conds_var:
            my_tree.SetBranchStatus(var_name, 1)
        for i, var_name in enumerate(read_vars_float):
            my_tree.SetBranchStatus(var_name, 1)
            my_tree.SetBranchAddress(var_name, float_vars[i])
        for i, var_name in enumerate(read_vars_double):
            my_tree.SetBranchStatus(var_name, 1)
            my_tree.SetBranchAddress(var_name, double_vars[i])
        
        # Process entries with progress tracking
        conds_entries = 0
        total_entries = my_tree.GetEntries()
        
        for entry_idx in tqdm(range(total_entries), desc="Processing entries"):
            # Get the entry from the tree
            my_tree.GetEntry(entry_idx)
            
            # Copy double vars into float_vars (for TMVA reader evaluation)
            float_vars[len(read_vars_float):] = double_vars.astype(np.float32)
            
            # Evaluate the MVA response and apply the response cut
            if reader.EvaluateMVA("MVA") > response_cut:
                conds_entries += 1
    
    # Return the normalized signal/background value
    SB = conds_entries * norm_W
    return SB

def clean_leg_string(leg_string):
    """
    Cleans and formats a leg string by replacing specific substrings with more readable symbols.
    
    This function is designed to remove or replace certain substrings that are often used
    in symbolic representations. For example, 'rightarrow' is replaced with '-->', 'pm' with '±', 
    and 'mp' with '∓'. It also removes extra braces like '{' and '}'.
    
    Args:
        leg_string (str): The input string representing the leg, which may contain special symbols
                          or substrings that need to be replaced or removed.

    Returns:
        str: A cleaned and formatted version of the input string, with all specified replacements made.
    
    Example:
        Input: "some_text#rightarrowpm{{mp}}"
        Output: "some_text-->±∓"
    
    Replacements:
        - '#'          -> ''  (removed)
        - 'rightarrow' -> '-->'
        - 'pm'         -> '±'
        - 'mp'         -> '∓'
        - '{'         -> ''  (removed)
        - '}'         -> ''  (removed)
    """
    # Replace '#' with an empty string (remove it)
    leg_string = leg_string.replace('#', '')
    
    # Replace 'rightarrow' with '-->'
    leg_string = leg_string.replace('rightarrow', '-->')
    
    # Replace 'pm' with '±' (plus-minus symbol)
    leg_string = leg_string.replace('pm', '±')
    
    # Replace 'mp' with '∓' (minus-plus symbol)
    leg_string = leg_string.replace('mp', '∓')
    
    # Remove '{{' and '}}'
    leg_string = leg_string.replace('{', '').replace('}', '')
    
    return leg_string