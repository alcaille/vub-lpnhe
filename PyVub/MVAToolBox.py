import os
import shutil
import ROOT
import numpy as np
from math import sqrt
from array import array
from tqdm import tqdm
from ROOT import (TFile, TMVA, THStack, TCanvas, TLegend, TH1F, TPaveText, 
                  TGraphErrors, gStyle, RDataFrame)
from .commonTools import (chain_tuples_modulable, 
                          err_S_over_sqrtB, err_SSB, 
                          ratio_Err, clean_leg_string)

# Get the current directory of the script
script_dir = os.path.dirname(__file__)

# Construct the relative path to the lhcbStyle file
lhcb_style = os.path.join(script_dir, '..', 'lhcbStyle.C')

def train_MVA(train_algo, mags, years, specs, cut_vars, train_cut, sig_paths, 
             bkg_paths, train_vars, tree_name, sig_train_frac, bkg_train_frac, 
             tmva_dir, weight_exp, k_folds):
    """
    Train a TMVA model with specified parameters.
    
    Parameters:
        train_algo (str): TMVA algorithm to use for training.
        mags (list): List of magnet settings.
        years (list): List of years for data.
        specs (str): Model specifications.
        cut_vars (list): Variables used for cuts.
        train_cut (ROOT.TCut): Training cut conditions.
        sig_paths (list): Paths to signal files.
        bkg_paths (list): Paths to background files.
        train_vars (list): Variables for training.
        tree_name (str): Name of the tree in the ROOT files.
        sig_train_frac (float): Fraction of signal events for training.
        bkg_train_frac (float): Fraction of background events for training.
        tmva_dir (str): Directory to store TMVA outputs.
        weight_exp (str): Weight expression to use.
        k_folds (int): Number of k-folds for cross-validation.
    
    Returns:
        None
    """
    outfile_name = f"Run2_{specs}.root"
    output_file = TFile(outfile_name, "RECREATE")
    outfolder_name = f"Files_Run2_{specs}"
    
    TMVA.Tools.Instance()
    dataloader = TMVA.DataLoader(outfolder_name)
    
    # Signal and background cuts
    sig_chain = chain_tuples_modulable(sig_paths, tree_name, years, mags)
    bkg_chain = chain_tuples_modulable(bkg_paths, tree_name, years, mags)
    
    all_vars = cut_vars + train_vars
    
    # Setup branches for variables
    for chain in [sig_chain, bkg_chain]:
        chain.SetBranchStatus("*", 0)
        for var in all_vars:
            chain.SetBranchStatus(var, 1)
    
    for train_var in train_vars:
        dataloader.AddVariable(train_var, 'F')
    
    if k_folds > 1:
        sig_chain.SetBranchStatus('eventNumber', 1)
        bkg_chain.SetBranchStatus('eventNumber', 1)
        dataloader.AddSpectator('eventNumber', 'l')
    
    dataloader.AddSignalTree(sig_chain)
    dataloader.AddBackgroundTree(bkg_chain)
    
    # Set weight expressions
    dataloader.SetSignalWeightExpression(weight_exp)
    dataloader.SetBackgroundWeightExpression(weight_exp)
    
    # Entries and split for training
    sig_entries = sig_chain.GetEntries(train_cut.GetTitle())
    bkg_entries = bkg_chain.GetEntries(train_cut.GetTitle())
    num_train_sig = round(sig_entries * sig_train_frac)
    num_train_bkg = round(bkg_entries * bkg_train_frac)
    
    mva_opt = (f"!V:SplitMode=Random:NormMode=NumEvents:nTrain_Signal="
               f"{num_train_sig}:nTrain_Background={num_train_bkg}")
    dataloader.PrepareTrainingAndTestTree(train_cut, train_cut, mva_opt)
    
    cv_options = (f"!V:!Silent:ModelPersistence:AnalysisType=Classification:"
                  f"SplitType=Deterministic:NumFolds={k_folds}:"
                  f"SplitExpr=int(fabs([eventNumber]))%int([NumFolds])")
    
    if k_folds > 1:
        factory = TMVA.CrossValidation(f"Run2_{specs}_{k_folds}kFold", dataloader, 
                                       output_file, cv_options)
    else:
        factory = TMVA.Factory(f"Run2_{specs}", output_file, 
                               "!V:!Silent:Color:ROC:DrawProgressBar:"
                               "AnalysisType=Classification")
    
    # Book the MVA method based on algorithm type
    tmva_specs = {
        "TestCorr": [TMVA.Types.kBDT, 
                     "!H:!V:NTrees=1"],
        "BDTG": [TMVA.Types.kBDT, 
                 "!H:!V:NTrees=500:MinNodeSize=2.5%:BoostType=Grad:"
                 "Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:"
                 "nCuts=20:MaxDepth=2"],
        "BDT": [TMVA.Types.kBDT, 
                "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:"
                "BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
                "BaggedSampleFraction=0.5:SeparationType=SDivSqrtSPlusB:"
                "nCuts=20"],
        "BDTB": [TMVA.Types.kBDT, 
                 "!H:!V:NTrees=400:BoostType=Bagging:SeparationType="
                 "SDivSqrtSPlusB:nCuts=20"],
        "BDTD": [TMVA.Types.kBDT, 
                 "!H:!V:NTrees=400:MinNodeSize=2.5%:MaxDepth=5:BoostType="
                 "AdaBoost:SeparationType=SDivSqrtSPlusB:nCuts=20:"
                 "VarTransform=Decorrelate"],
        "BDTF": [TMVA.Types.kBDT, 
                 "!H:!V:NTrees=50:MinNodeSize=2.5%:UseFisherCuts:MaxDepth=3:"
                 "BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType="
                 "SDivSqrtSPlusB:nCuts=20"],
        "MLP": [TMVA.Types.kMLP, 
                "!H:!V:NeuronType=sigmoid:VarTransform=N:NCycles=600:"
                "HiddenLayers=N,N,N,N,N,N:TestRate=5:UseRegulator=True"],
        "MLPv1": [TMVA.Types.kMLP, 
                  "!H:!V:NeuronType=sigmoid:VarTransform=N:NCycles=600:"
                  "HiddenLayers=N,N-1:TestRate=5:UseRegulator=True"],
        "DNN": [TMVA.Types.kDL, 
                "H:!V:Layout=LINEAR|13|RELU,LINEAR|13|RELU,LINEAR|13|RELU,"
                "LINEAR|13|RELU,LINEAR|13|RELU,LINEAR|1|SIGMOID:"
                "ErrorStrategy=CROSSENTROPY:Architecture=GPU:"
                "ValidationSize=0.2:RandomSeed=12:VarTransform=None:"
                "TrainingStrategy=Optimizer=ADAM"]
    }.get(train_algo, None)
    
    if tmva_specs is None:
        raise ValueError(f"Unsupported algorithm: {train_algo}")
    
    # Book and run the training
    if k_folds > 1:
        factory.BookMethod(tmva_specs[0], train_algo, tmva_specs[1])
        factory.Evaluate()
    else:
        factory.BookMethod(dataloader, tmva_specs[0], train_algo, tmva_specs[1])
        factory.TrainAllMethods()
        factory.TestAllMethods()
        factory.EvaluateAllMethods()
    
    output_file.Close()
    shutil.move(outfolder_name, os.path.join(tmva_dir, outfolder_name))
    shutil.move(outfile_name, os.path.join(tmva_dir, outfolder_name, outfile_name))
    os.mkdir(os.path.join(tmva_dir, outfolder_name, 'PDF'))

def FillingHistoWithMVAResp(input_tree, weight_file, read_vars_float, 
                            read_vars_double, histo):
    """
    Fill histograms with MVA response using a TMVA Reader.
    
    Parameters:
        input_tree (ROOT.TTree): Input tree to process.
        weight_file (str): Path to the trained TMVA weight file.
        read_vars_float (list): List of variables of type float.
        read_vars_double (list): List of variables of type double.
        histo (ROOT.TH1F): Histogram to fill with MVA response.
    
    Returns:
        None
    """
    reader = TMVA.Reader("!Color")
    
    float_vars = [array('f', [0])] * len(read_vars_float)
    floated_double_vars = [array('f', [0])] * len(read_vars_double)
    double_vars = [array('d', [0])] * len(read_vars_double)
    
    for p, var_read_f in enumerate(read_vars_float):
        reader.AddVariable(var_read_f, float_vars[p])
    for l, var_read_d in enumerate(read_vars_double):
        reader.AddVariable(var_read_d, floated_double_vars[l])
    
    reader.BookMVA("MVA", weight_file)
    
    input_tree.SetBranchStatus("*", 0)
    
    for j, var_samp_f in enumerate(read_vars_float):
        input_tree.SetBranchStatus(var_samp_f, 1)
        input_tree.SetBranchAddress(var_samp_f, float_vars[j])
    for m, var_samp_d in enumerate(read_vars_double):
        input_tree.SetBranchStatus(var_samp_d, 1)
        input_tree.SetBranchAddress(var_samp_d, double_vars[m])
    
    for k in tqdm(range(input_tree.GetEntries())):
        input_tree.GetEntry(k)
        
        for n in range(len(read_vars_double)):
            floated_double_vars[n][0] = float(double_vars[n][0])
        
        mva_response = reader.EvaluateMVA("MVA")
        histo.Fill(mva_response)
    
    input_tree.SetBranchStatus("*", 1)

def response_plot(mags, years, sample_vec, leg_list, color_list, bin_num, 
                 tree_name, x_range, save_path, title, resp_name, selection):
    """
    Plot MVA response for multiple samples and save the plot.
    
    Parameters:
        mags (list): Magnet settings.
        years (list): Years of data.
        sample_vec (list): List of sample file paths.
        leg_list (list): Legends for the plot.
        color_list (list): Colors for the plot lines.
        bin_num (int): Number of bins in the histogram.
        tree_name (str): Name of the tree in the ROOT files.
        x_range (tuple): Range for the x-axis.
        save_path (str): Path to save the plot.
        title (str): Plot title.
        resp_name (str): Response variable name.
        selection (str): Selection string for ROOT.
    
    Returns:
        None
    """
    ROOT.gROOT.ProcessLine(f".L {lhcb_style}")
    ROOT.lhcbStyle()
    
    samp_num = len(sample_vec)
    hist_stack = THStack("histStack", title)
    hist_vec = []
    
    legend = TLegend(0.42, 0.62, 0.66, 0.915)
    legend.SetLineWidth(0)
    
    for i, samp in enumerate(sample_vec):
        mychain = chain_tuples_modulable([samp], tree_name, years, mags)
        hist_temp = TH1F(f"Hist {i}", title, bin_num, x_range[0], x_range[1])
        
        mychain.Project(f"Hist {i}", resp_name, selection)
        hist_temp.SetDirectory(0)
        hist_temp.Scale(1. / hist_temp.Integral())
        hist_temp.SetFillStyle(0)
        hist_temp.SetLineStyle(1)
        hist_temp.SetLineWidth(1)
        hist_temp.SetMarkerSize(.1)
        hist_vec.append(hist_temp)
    
    for j in range(samp_num):
        k = samp_num - 1 - j
        hist_vec[k].SetLineColor(color_list[k])
        hist_stack.Add(hist_vec[k], "H && E1 && EX0")
        legend.AddEntry(hist_vec[j], leg_list[j], "L")
    
    max_y = max([hist.GetMaximum() for hist in hist_vec]) * 1.1
    
    canvas = TCanvas("canvas", "Canvas", 1300, 1080)
    hist_stack.SetMaximum(max_y)
    hist_stack.Draw("nostack")
    hist_stack.SetTitle(title)
    hist_stack.GetYaxis().SetTitle("A.U.")
    hist_stack.GetXaxis().SetTitle("MVA Response")
    
    lhcb_text = TPaveText(.17, .855, .32, .925, "BRNDC")
    lhcb_text.SetFillColor(0)
    lhcb_text.SetTextAlign(12)
    lhcb_text.SetBorderSize(0)
    lhcb_text.SetLineWidth(0)
    lhcb_text.AddText("LHCb MC")
    lhcb_text.Draw()
    
    legend.Draw()
    canvas.SaveAs(save_path)
    canvas.Close()

def add_response_in_tree(mags, years, samp_paths, weight_file, response_name, 
                      read_vars_float, read_vars_double, tree_name, train_algo):
    """
    Add the MVA response as a new branch in the ROOT tree.
    
    Parameters:
        mags (list): Magnet settings.
        years (list): Years of data.
        samp_paths (list): Paths to the sample files.
        weight_file (str): Path to the trained TMVA weight file.
        response_name (str): Name of the new branch.
        read_vars_float (list): List of float variables to read.
        read_vars_double (list): List of double variables to read.
        tree_name (str): Name of the tree in the ROOT files.
        train_algo (str): Training algorithm used.
    
    Returns:
        None
    """
    TMVA.Tools.Instance()
    reader = TMVA.Reader("Color:!Silent")
    
    float_vars = []
    floated_double_vars = []
    double_vars = []
    
    for i, var in enumerate(read_vars_float):
        float_vars.append(array('f', [0.]))
        reader.AddVariable(var, float_vars[i])
    for p, var in enumerate(read_vars_double):
        floated_double_vars.append(array('f', [0.]))
        reader.AddVariable(var, floated_double_vars[p])
    
    reader.BookMVA(train_algo, weight_file)
    
    for samp_path in samp_paths:
        for mag in mags:
            for year in years:
                input_file = TFile.Open(samp_path[mag][year], "update")
                my_tree = input_file.Get(tree_name)
                
                my_tree.SetBranchStatus("*", 0)
                for j, var in enumerate(read_vars_float):
                    my_tree.SetBranchStatus(var, 1)
                    my_tree.SetBranchAddress(var, float_vars[j])
                for m, var in enumerate(read_vars_double):
                    double_vars.append(array('d', [0.]))
                    my_tree.SetBranchStatus(var, 1)
                    my_tree.SetBranchAddress(var, double_vars[m])
                
                print(samp_path[mag][year], "\n")
                
                response = array('f', [0.])
                response_branch = my_tree.Branch(response_name, response, 
                                                 f"{response_name}/F")
                
                for k in tqdm(range(my_tree.GetEntries())):
                    my_tree.GetEntry(k)
                    
                    for r in range(len(read_vars_double)):
                        floated_double_vars[r][0] = float(double_vars[r][0])
                    
                    response[0] = reader.EvaluateMVA(train_algo)
                    response_branch.Fill()
                
                my_tree.SetBranchStatus("*", 1)
                my_tree.AutoSave()
                input_file.Close()
                print("\n")

def add_two_fold_response(mags, years, samp_paths, response_name, tree_name):
    """
    For each provided sample path, magnetic polarity, and year, this function adds a new 
    "two-fold response" branch to a ROOT TTree. The "two-fold response" is derived 
    from two existing branches corresponding to even and odd folds of the response.
    
    Specifically:
    - It opens a ROOT file for each combination of magnetic polarity and year for each sample path.
    - It retrieves the specified TTree.
    - It reads two existing response branches (assumed to be named 
      "<response_name>_Even_Fold" and "<response_name>_Odd_Fold").
    - It creates a new branch named <response_name> that is filled based on the event number:
      - For events with an even event number, the new response is taken from the "<response_name>_Odd_Fold" branch.
      - For events with an odd event number, the new response is taken from the "<response_name>_Even_Fold" branch.
    - After filling all entries, the TTree is saved and the file is closed.
    
    Parameters
    ----------
    mags : list
        A list of magnetic polarity strings or identifiers.
    years : list
        A list of year strings or identifiers.
    samp_paths : list of dict
        A list of dictionaries where each dictionary maps from mag -> year -> file path string.
        For example: samp_path[mag][year] should return a string with the ROOT file path.
    response_name : str
        The base name of the response variable.
        The code expects that branches "<response_name>_Even_Fold" and "<response_name>_Odd_Fold" exist.
    tree_name : str
        The name of the TTree from which to read and to which the new branch will be added.
    
    Returns
    -------
    None
        The function directly modifies and updates the ROOT files.
    """
    
    # We loop over each sample path dictionary, magnitude, and year.
    for samp_path in samp_paths:
        for mag in mags:
            for year in years:
                
                # Retrieve the file path for the given mag and year from the current sample path dictionary.
                print(samp_path[mag][year], "\n")
                
                # Open the ROOT file in update mode to modify it.
                file = TFile.Open(samp_path[mag][year], "update")
                
                if not file or file.IsZombie():
                    print(f"Could not open file: {file}")
                    continue
                
                # Retrieve the TTree
                tree = file.Get(tree_name)
                if not tree:
                    print(f"Could not find tree '{tree_name}' in file: {file}")
                    file.Close()
                    continue
                
                # Prepare arrays for reading even and odd fold responses.
                # According to ROOT's Python bindings, we define them as arrays of float ('f').
                even_fold_response = array('f', [0.])
                odd_fold_response  = array('f', [0.])
                
                # Connect the arrays to the respective branches.
                # The existing branches are named based on the given response_name.
                tree.SetBranchAddress(f'{response_name}_Even_Fold', even_fold_response)
                tree.SetBranchAddress(f'{response_name}_Odd_Fold', odd_fold_response)
                
                # Prepare the array for the new two-fold response.
                two_fold_response = array('f', [0.])
                # Create a new branch in the tree. This branch will hold our computed two-fold response.
                two_fold_response_branch = tree.Branch(response_name,
                                                       two_fold_response,
                                                       f"{response_name}/F")
                
                # Loop over all entries in the tree to fill the new branch.
                for k in tqdm(range(tree.GetEntries())):
                    # Read the k-th entry from the tree to get the even/odd response values.
                    tree.GetEntry(k)
                    
                    # Determine which response to use based on the event number:
                    # If eventNumber is even, use the odd_fold_response.
                    # If eventNumber is odd, use the even_fold_response.
                    # (This logic swaps the fold responses based on event parity.)
                    if tree.eventNumber % 2 == 0:
                        two_fold_response[0] = odd_fold_response[0]
                    else:
                        two_fold_response[0] = even_fold_response[0]
                    
                    # Fill the newly created branch with the computed value.
                    two_fold_response_branch.Fill()
                
                # Write changes to the tree and close the file.
                tree.AutoSave()
                file.Close()
                
                print("\n")

def separationPowerFormula(ys, yb):
    """
    Calculate the separation power between signal and background.
    
    Parameters:
        ys (float): Signal yield.
        yb (float): Background yield.
    
    Returns:
        float: Separation power.
    """
    return (ys - yb) ** 2 / (2 * (ys + yb))

def separationPower(sig_hist, bkg_hist):
    """
    Calculate the separation power from histograms.
    
    Parameters:
        sig_hist (ROOT.TH1F): Signal histogram.
        bkg_hist (ROOT.TH1F): Background histogram.
    
    Returns:
        float: Separation power.
    """
    if sig_hist.GetNbinsX() != bkg_hist.GetNbinsX():
        return 0.0
    
    sep_pow = sum(
        separationPowerFormula(sig_hist.GetBinContent(i), 
                               bkg_hist.GetBinContent(i))
        for i in range(1, sig_hist.GetNbinsX() + 1)
    )
    
    return sep_pow

def weightedNumEventUnderMVACut(mags, years, decay_paths, tree_name, resp_name, 
                                cut_value, selection, weight_name):
    """
    Calculate weighted number of events under MVA cut.
    
    Parameters:
        mags (list): Magnet settings.
        years (list): Years of data.
        decay_paths (list): Paths to decay files.
        tree_name (str): Name of the tree in ROOT files.
        resp_name (str): Name of the response variable.
        cut_value (float): MVA cut value.
        selection (str): Selection string.
        weight_name (str): Name of the weight variable.
    
    Returns:
        float: Weighted number of events.
    """
    decay_chain = chain_tuples_modulable(decay_paths, tree_name, years, mags)
    
    decay_df = RDataFrame(decay_chain).Filter(
        f'{selection} && {resp_name}>{cut_value}'
    )
    
    decay_df = decay_df.Define('weight',f'{weight_name}*total_weight*Lumi*Sigmabb*2')
    
    SB = decay_df.Sum('weight').GetValue()
    
    del decay_df
    del decay_chain
    return SB

def opti_mva_cut(mags, years, sig_paths, sig_legs, bkg_paths, all_bkg_paths, 
                 all_bkg_legs, tree_name, weight_var_name, point_num, resp_range, 
                 resp_name, selection, save_path, spef):
    """
    Optimize the MVA cut based on the signal-to-background ratio and save 
    the results.
    
    Parameters:
        mags (list): List of magnet settings.
        years (list): List of data years.
        sig_paths (list): List of paths to signal data.
        sig_legs (list): List of signal legends.
        bkg_paths (list): List of paths to background data.
        all_bkg_paths (list): List of all background paths.
        all_bkg_legs (list): List of all background legends.
        tree_name (str): Name of the tree.
        weight_var_name (str): Variable name for weighting events.
        point_num (int): Number of points to calculate.
        resp_range (tuple): Range for response values.
        resp_name (str): Response variable name.
        selection (str): Selection criteria.
        save_path (str): Path to save the output.
        spef (str): Specification for file saving.
    
    Returns:
        None
    """
    ROOT.gROOT.ProcessLine(f".L {lhcb_style}")
    ROOT.lhcbStyle()
    
    response_vec = np.linspace(resp_range[0], resp_range[1], point_num)
    response_err_vec = [(resp_range[1] - resp_range[0]) / (2 * point_num)] * point_num
    
    s_tab = []
    b_tab = []
    ssb_tab = []
    ssb_err_tab = []
    
    for i, resp in enumerate(tqdm(response_vec)):
        S = weightedNumEventUnderMVACut(mags, years, sig_paths, tree_name, 
                                        resp_name, resp, selection, 
                                        weight_var_name)
        B = weightedNumEventUnderMVACut(mags, years, bkg_paths, tree_name, 
                                        resp_name, resp, selection, 
                                        weight_var_name)
        
        if S + B == 0:
            ssb = 0
            ssb_err = 0
        else:
            ssb = S / sqrt(S + B)
            ssb_err = err_SSB(S, B)
        
        s_tab.append(S)
        b_tab.append(B)
        ssb_tab.append(ssb)
        ssb_err_tab.append(ssb_err)
    
    ssb_max = max(ssb_tab)
    ssb_max_index = ssb_tab.index(ssb_max)
    
    canvas = TCanvas("canvas", "Graph Canvas", 1400, 1080)
    gStyle.SetLineScalePS(1)
    
    graph_err = TGraphErrors(int(point_num), array('f', response_vec), 
                             array('f', ssb_tab), array('f', response_err_vec), 
                             array('f', ssb_err_tab))
    
    graph_err.SetTitle(" ;Response Cut; S / #sqrt{S + B}")
    graph_err.SetLineColor(1)
    graph_err.SetMarkerStyle(20)
    graph_err.SetMarkerColor(2)
    graph_err.SetMarkerSize(1)
    graph_err.GetXaxis().SetRangeUser(resp_range[0], resp_range[1])
    graph_err.Draw("APL")
    graph_err.SetMaximum(ssb_max * 1.1)
    
    S_bef, B_bef = s_tab[0], b_tab[0]
    S_aft, B_aft = s_tab[ssb_max_index], b_tab[ssb_max_index]
    
    with open(f"{save_path}/Terminal_OptiMVACut_{spef}.txt", "w") as f:
        f.write(f"\nThe optimal response cut is : {response_vec[ssb_max_index]}\n")
        f.write(f"\nS : {round(S_bef)} ---> {round(S_aft)} | ε = "
                f"{round(100 * S_aft / S_bef, 2)} ± "
                f"{round(100 * ratio_Err(S_aft, S_bef), 2)}")
        f.write(f"\nB : {round(B_bef)} ---> {round(B_aft)} | ε = "
                f"{round(100 * B_aft / B_bef, 2)} ± "
                f"{round(100 * ratio_Err(B_aft, B_bef), 2)}\n")
        f.write(f"\nS/B    = {round(S_aft / B_aft, 2)} ± "
                f"{round(ratio_Err(S_aft, S_bef), 2)}")
        f.write(f"\nS/√B   = {round(S_aft / sqrt(B_aft), 2)} ± "
                f"{round(err_S_over_sqrtB(S_aft, S_bef), 2)}")
        f.write(f"\nS/√S+B = {round(S_aft / sqrt(S_aft + B_aft), 2)} ± "
                f"{round(err_SSB(S_aft, S_bef), 2)}\n")
        
        all_decay_tab = sig_paths + all_bkg_paths
        all_legs_tab = sig_legs + all_bkg_legs
        
        for i, decay in enumerate(all_decay_tab):
            evt_num_bef = weightedNumEventUnderMVACut(mags, years, [decay], 
                                                      tree_name, resp_name, 
                                                      response_vec[0], 
                                                      selection, weight_var_name)
            evt_num_aft = weightedNumEventUnderMVACut(mags, years, [decay], 
                                                      tree_name, resp_name, 
                                                      response_vec[ssb_max_index], 
                                                      selection, weight_var_name)
            
            eff_mva_cut = evt_num_aft / evt_num_bef
            eff_mva_cut_err = ratio_Err(evt_num_aft, evt_num_bef)
            
            # Clean the leg string
            cleaned_leg = clean_leg_string(all_legs_tab[i])
            
            # Prepare the numerical values
            evt_num_bef_rounded = round(evt_num_bef)
            evt_num_aft_rounded = round(evt_num_aft)
            eff_mva_cut_percent = round(100 * eff_mva_cut, 2)
            eff_mva_cut_err_percent = round(100 * eff_mva_cut_err, 2)
            
            # Write the formatted string to file
            f.write(f"\n{cleaned_leg} : {evt_num_bef_rounded} & {evt_num_aft_rounded} & "
                    f"{eff_mva_cut_percent} ± {eff_mva_cut_err_percent}")
    
    lhcb_text = TPaveText(.7, .855, .85, .925, "BRNDC")
    lhcb_text.SetFillColor(0)
    lhcb_text.SetTextAlign(12)
    lhcb_text.SetBorderSize(0)
    lhcb_text.SetLineWidth(0)
    lhcb_text.AddText("LHCb MC")
    lhcb_text.Draw()
    
    mva_cut_quote = TPaveText(.23, .125, .79, .25, "BRNDC")
    mva_cut_quote.SetFillColor(0)
    mva_cut_quote.SetBorderSize(0)
    mva_cut_quote.SetLineWidth(0)
    mva_cut_quote.AddText(f"The maximum S/#sqrt{{S+B}} is {round(ssb_max, 3)} #pm "
                          f"{round(ssb_err_tab[ssb_max_index], 3)} when cutting at "
                          f"{round(response_vec[ssb_max_index], 3)} #pm "
                          f"{round(response_err_vec[ssb_max_index], 3)}")
    mva_cut_quote.Draw()
    
    canvas.SaveAs(f"{save_path}/PDF/OptiMVACut_{spef}.pdf")