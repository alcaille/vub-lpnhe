import os
import yaml
from .commonTools import sample_weight_per_event

# Load YAML data
def load_data(yaml_file):
    """Load data from a YAML file."""
    with open(yaml_file, 'r') as file:
        return yaml.safe_load(file)

# Get the current directory of the script
script_dir = os.path.dirname(__file__)

# Construct the relative path to the YAML file
yaml_file = os.path.join(script_dir, 'decays_eff_data.yaml')

# Load data from the YAML file
data = load_data(yaml_file)

# Constants
FU = data['constants']['fu']
FD = data['constants']['fd']
FS = data['constants']['fs']

# Branching ratios from YAML
br = data['branching_ratios']

##############################################################################
################## [B+ -> (Charmonium -> mu+ mu- X) K+ X]cc ##################
##############################################################################

# Calculate K1_X_K for K1(1270)+ decay
def calculate_br_k1_x_k():
    """
    Calculate the branching ratio for K1 -> X K where X includes 
    Krho, Kstpi (multiplied by Kst->Kpi branching ratio), and K0stpi 
    (multiplied by K0st->Kpi branching ratio).
    """
    return (br['br_k1_krho'] +
            br['br_k1_kstpi0'] * br['br_kst_kpi0'] +
            br['br_k1_kst0pi'] * br['br_kst0_kpi'] +
            br['br_k1_k0st0pi'] * br['br_k0st0_kpi'] +
            br['br_k1_k0stpi0'] * br['br_k0st_kpi0'])

br_k1_x_k = calculate_br_k1_x_k()

# Decay B branching ratios with mu+ mu- final state
br_b_jpsi_k_dec = br['br_b_jpsi_k'] * br['br_jpsi_mumu']
br_b_jpsi_kpipi_dec = br['br_b_jpsi_kpipi'] * br['br_jpsi_mumu']
br_b_jpsi_kst_dec = (br['br_b_jpsi_kst'] * br['br_jpsi_mumu'] *
                     br['br_kst_kpi0'])
br_b_jpsi_k1_dec = (br['br_b_jpsi_k1'] * br['br_jpsi_mumu'] *
                    br_k1_x_k)
br_b_jpsi_keta_dec = br['br_b_jpsi_keta'] * br['br_jpsi_mumu']
br_b_jpsi_komega_dec = br['br_b_jpsi_komega'] * br['br_jpsi_mumu']

# Calculate Psi(2S) -> X mu+ mu- branching ratio
def calculate_br_psi_x_mumu():
    """
    Calculate the branching ratio for psi(2S) -> X mu+ mu-, where X can 
    be J/psi + eta, pi0pi0, or pipi, and various chi_c states.
    """
    return (br['br_psi_mumu'] +
            (br['br_psi_jpsi_eta'] +
             br['br_psi_jpsi_pi0pi0'] +
             br['br_psi_jpsi_pipi']) * br['br_jpsi_mumu'] +
            (br['br_psi_chic0_gamma'] * br['br_chic0_jpsi_gamma'] +
             br['br_psi_chic1_gamma'] * br['br_chic1_jpsi_gamma'] +
             br['br_psi_chic2_gamma'] * br['br_chic2_jpsi_gamma']) *
            br['br_jpsi_mumu'])

br_psi_x_mumu = calculate_br_psi_x_mumu()

# Psi(2S) decay channels
br_b_psi_k_dec = br['br_b_psi_k'] * br_psi_x_mumu
br_b_psi_kst_dec = (br['br_b_psi_kst'] * br_psi_x_mumu *
                    br['br_kst_kpi0'])
br_b_psi_kpipi_dec = br['br_b_psi_kpipi'] * br_psi_x_mumu

# Chi_c decay channels
br_b_chic0_k_dec = (br['br_b_chic0_k'] *
                    br['br_chic0_jpsi_gamma'] * br['br_jpsi_mumu'])
br_b_chic1_k_dec = (br['br_b_chic1_k'] *
                    br['br_chic1_jpsi_gamma'] * br['br_jpsi_mumu'])
br_b_chic1_kst_dec = (br['br_b_chic1_kst'] * br['br_kst_kpi0'] *
                      br['br_chic1_jpsi_gamma'] * br['br_jpsi_mumu'])
br_b_chic1_kpi0_dec = (br['br_b_chic1_kpi0'] *
                       br['br_chic1_jpsi_gamma'] * br['br_jpsi_mumu'])
br_b_chic1_kpipi_dec = (br['br_b_chic1_kpipi'] *
                        br['br_chic1_jpsi_gamma'] * br['br_jpsi_mumu'])
br_b_chic2_kpipi_dec = (br['br_b_chic2_kpipi'] *
                        br['br_chic2_jpsi_gamma'] * br['br_jpsi_mumu'])

# Total inclusive detectable branching ratio
def calculate_total_br_ccbar():
    """
    Calculate the inclusive detectable branching ratio for B -> ccbar K X 
    decays based on various branching ratios defined above.
    """
    return (br_b_jpsi_k_dec +
            br_b_jpsi_kpipi_dec +
            br_b_jpsi_kst_dec +
            br_b_jpsi_k1_dec +
            br_b_jpsi_keta_dec +
            br_b_jpsi_komega_dec +
            br_b_psi_k_dec +
            br_b_psi_kst_dec +
            br_b_psi_kpipi_dec +
            br_b_chic0_k_dec +
            br_b_chic1_k_dec +
            br_b_chic1_kst_dec +
            br_b_chic1_kpi0_dec +
            br_b_chic1_kpipi_dec +
            br_b_chic2_kpipi_dec)

data['decay_processes']['ccbar']['br'] = calculate_total_br_ccbar()

data['decay_processes']['kst']['br'] = 2.4 * 0.33 * data['decay_processes']['sig']['br']
data['decay_processes']['kst2']['br'] = 0.499 * 0.33 * data['decay_processes']['sig']['br']
data['decay_processes']['kst1430']['br'] = 0.93 * 0.33 * data['decay_processes']['sig']['br']

##############################################################################
##############################################################################
##############################################################################

# Function to calculate event weights for each process
def calculate_event_weights(br, gen_eff, rent_rate, full_ev_cut_eff, 
                            gen_event_num, factor):
    """
    Calculate the event weights based on branching ratio and efficiencies.
    """
    weights = [
        sample_weight_per_event(
            br, gen_eff[i], rent_rate[i], full_ev_cut_eff[i], 
            gen_event_num[i], factor
        ) for i in range(3)
    ]
    return weights

# Function to calculate event weights for a year for each process
def calculate_event_weights_year(br, gen_eff, rent_rate, full_ev_cut_eff, 
                            gen_event_num, factor):
    """
    Calculate the event weights for a year based on branching ratio and efficiencies.
    """
    weights = [
        sample_weight_per_event(
            br, gen_eff[i], rent_rate[i], full_ev_cut_eff[i], 
            gen_event_num[0][i]+gen_event_num[1][i],
            factor
        ) for i in range(3)
    ]
    return weights

# Function to calculate event weights for Run2 for each process
def calculate_event_weights_Run2(br, gen_eff, rent_rate, full_ev_cut_eff, 
                            gen_event_num, factor):
    """
    Calculate the event weights for a year based on branching ratio and efficiencies.
    """
    weights = [
        sample_weight_per_event(
            br, gen_eff[i], rent_rate[i], full_ev_cut_eff[i], 
            gen_event_num[0][0]+gen_event_num[1][0] +
            gen_event_num[0][1]+gen_event_num[1][1] +
            gen_event_num[0][2]+gen_event_num[1][2],
            factor
        ) for i in range(3)
    ]
    return weights

# Sig process
sig_data = data['decay_processes']['sig']
sig_legend = sig_data['latex_legend']
sig_weight_per_evt = [
    calculate_event_weights(
        sig_data['br'],
        sig_data['gen_eff'][i],
        sig_data['rent_rate'][i],
        sig_data['full_ev_cut_eff'][i],
        sig_data['gen_event_num'][i],
        FS
    ) for i in range(2)
]

sig_weight_per_evt_year = [
    calculate_event_weights_year(
        sig_data['br'],
        sig_data['gen_eff'][i],
        sig_data['rent_rate'][i],
        sig_data['full_ev_cut_eff'][i],
        sig_data['gen_event_num'],
        FS
    ) for i in range(2)
]

sig_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        sig_data['br'],
        sig_data['gen_eff'][i],
        sig_data['rent_rate'][i],
        sig_data['full_ev_cut_eff'][i],
        sig_data['gen_event_num'],
        FS
    ) for i in range(2)
]

# H_b process
hb_data = data['decay_processes']['hb']
hb_legend = hb_data['latex_legend']
hb_weight_per_evt = [
    calculate_event_weights(
        hb_data['br'],
        hb_data['gen_eff'][i],
        hb_data['rent_rate'][i],
        hb_data['full_ev_cut_eff'][i],
        hb_data['gen_event_num'][i],
        1
    ) for i in range(2)
]

hb_weight_per_evt_year = [
    calculate_event_weights_year(
        hb_data['br'],
        hb_data['gen_eff'][i],
        hb_data['rent_rate'][i],
        hb_data['full_ev_cut_eff'][i],
        hb_data['gen_event_num'],
        1
    ) for i in range(2)
]

hb_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        hb_data['br'],
        hb_data['gen_eff'][i],
        hb_data['rent_rate'][i],
        hb_data['full_ev_cut_eff'][i],
        hb_data['gen_event_num'],
        1
    ) for i in range(2)
]

# c\bar{c} process
ccbar_data = data['decay_processes']['ccbar']
ccbar_legend = ccbar_data['latex_legend']
ccbar_weight_per_evt = [
    calculate_event_weights(
        ccbar_data['br'],
        ccbar_data['gen_eff'][i],
        ccbar_data['rent_rate'][i],
        ccbar_data['full_ev_cut_eff'][i],
        ccbar_data['gen_event_num'][i],
        FU
    ) for i in range(2)
]

ccbar_weight_per_evt_year = [
    calculate_event_weights_year(
        ccbar_data['br'],
        ccbar_data['gen_eff'][i],
        ccbar_data['rent_rate'][i],
        ccbar_data['full_ev_cut_eff'][i],
        ccbar_data['gen_event_num'],
        FU
    ) for i in range(2)
]

ccbar_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        ccbar_data['br'],
        ccbar_data['gen_eff'][i],
        ccbar_data['rent_rate'][i],
        ccbar_data['full_ev_cut_eff'][i],
        ccbar_data['gen_event_num'],
        FU
    ) for i in range(2)
]

# Kst process
kst_data = data['decay_processes']['kst']
kst_legend = kst_data['latex_legend']
kst_weight_per_evt = [
    calculate_event_weights(
        kst_data['br'],
        kst_data['gen_eff'][i],
        kst_data['rent_rate'][i],
        kst_data['full_ev_cut_eff'][i],
        kst_data['gen_event_num'][i],
        FS
    ) for i in range(2)
]

kst_weight_per_evt_year = [
    calculate_event_weights_year(
        kst_data['br'],
        kst_data['gen_eff'][i],
        kst_data['rent_rate'][i],
        kst_data['full_ev_cut_eff'][i],
        kst_data['gen_event_num'],
        FS
    ) for i in range(2)
]

kst_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        kst_data['br'],
        kst_data['gen_eff'][i],
        kst_data['rent_rate'][i],
        kst_data['full_ev_cut_eff'][i],
        kst_data['gen_event_num'],
        FS
    ) for i in range(2)
]

# Kst2 process
kst2_data = data['decay_processes']['kst2']
kst2_legend = kst2_data['latex_legend']
kst2_weight_per_evt = [
    calculate_event_weights(
        kst2_data['br'],
        kst2_data['gen_eff'][i],
        kst2_data['rent_rate'][i],
        kst2_data['full_ev_cut_eff'][i],
        kst2_data['gen_event_num'][i],
        FS
    ) for i in range(2)
]

kst2_weight_per_evt_year = [
    calculate_event_weights_year(
        kst2_data['br'],
        kst2_data['gen_eff'][i],
        kst2_data['rent_rate'][i],
        kst2_data['full_ev_cut_eff'][i],
        kst2_data['gen_event_num'],
        FS
    ) for i in range(2)
]

kst2_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        kst2_data['br'],
        kst2_data['gen_eff'][i],
        kst2_data['rent_rate'][i],
        kst2_data['full_ev_cut_eff'][i],
        kst2_data['gen_event_num'],
        FS
    ) for i in range(2)
]

# Kst1430 process
kst1430_data = data['decay_processes']['kst1430']
kst1430_legend = kst1430_data['latex_legend']
kst1430_weight_per_evt = [
    calculate_event_weights(
        kst1430_data['br'],
        kst1430_data['gen_eff'][i],
        kst1430_data['rent_rate'][i],
        kst1430_data['full_ev_cut_eff'][i],
        kst1430_data['gen_event_num'][i],
        FS
    ) for i in range(2)
]

kst1430_weight_per_evt_year = [
    calculate_event_weights_year(
        kst1430_data['br'],
        kst1430_data['gen_eff'][i],
        kst1430_data['rent_rate'][i],
        kst1430_data['full_ev_cut_eff'][i],
        kst1430_data['gen_event_num'],
        FS
    ) for i in range(2)
]

kst1430_weight_per_evt_Run2 = [
    calculate_event_weights_Run2(
        kst1430_data['br'],
        kst1430_data['gen_eff'][i],
        kst1430_data['rent_rate'][i],
        kst1430_data['full_ev_cut_eff'][i],
        kst1430_data['gen_event_num'],
        FS
    ) for i in range(2)
]